from picostatus import pico_error_string
class Oscill_base(object):
    def setSampleRate(self,sample_rate):
        self.setTimeResolution(1/sample_rate)
    def getSampleRate(self):
        return 1/self.dx
    def getMaxSamples(self):
        return self.maxSamples
    def setBufferLength(self,buff_len):
        self.bufflen = int(buff_len)
        self.sampCountWanted = self.bufflen
        self.setDataBuffers()
    def call(self,func_name,*args):
        """
        universal method for calling functions from dll by name
        :param func_name:
        :param args:
        :return:
        """
        func = self.__getattr__(func_name)
        # print(args,func,args[0])
        # self.test(*args)
        err = func(*args)
        if err:
            err_str = pico_error_string(err)
            if args:
                print("{}{} returned error {:X}".format(func,args,err))
            else:
                print("{}() returned error {:X}".format(func,err))
            print(err_str)
        return err
