__author__ = 'SFB'

import sys,os,time,traceback
sys.path.append( '../modules')
#from PyQt5 import  QtCore,QtWidgets
from qtpy import QtCore,QtWidgets
import quickQtApp,QMenuFromDict
import picoscope_2000_class
import fft
import numpy as np
import si_units
import zmq
from zmq_publisher import Zmq_Publisher


PUBLISH_ADDRESS_FREQ = 'tcp://*:5012'
PUBLISH_ADDRESS_PWR = 'tcp://*:5013'
PUBLISH_ADDRESS_REFL = 'tcp://*:5018'

app,opt,win,cons = quickQtApp.mkAppWin("RF stats")

gl = QtWidgets.QGridLayout()
cw = QtWidgets.QWidget()
cw.setLayout(gl)
win.setCentralWidget(cw)

context = zmq.Context()
pub_freq = Zmq_Publisher(context,PUBLISH_ADDRESS_FREQ)
pub_pwr = Zmq_Publisher(context,PUBLISH_ADDRESS_PWR)
pub_reflect = Zmq_Publisher(context, PUBLISH_ADDRESS_REFL)



lfreq = QtWidgets.QLabel()
ldbm1 = QtWidgets.QLabel()
ldbm2 = QtWidgets.QLabel()

f = lfreq.font()
#f = QtGui.QFont()
fs = f.pointSizeF()*opt.get("gui.MainLabelsFontScale",2)
f.setPointSizeF(round(fs))
lfreq.setFont(f)
ldbm1.setFont(f)
f.setPointSizeF(round(fs*.75))
ldbm2.setFont(f)
gl.addWidget(lfreq)
gl.addWidget(ldbm1)
gl.addWidget(ldbm2)
gl2 = QtWidgets.QGridLayout()
gl.addLayout(gl2,0,1,3,1)
lstat = QtWidgets.QLabel()
gl2.addWidget(lstat)
#win.statusBar().addWidget(lstat)

startpath = os.getcwd()
dllpath = r"C:\Program Files (x86)\Pico Technology\PicoScope6\\"
os.chdir(dllpath)




class PicoScopQt(QtCore.QObject):
    blockReady = QtCore.Signal()
    dataReady = QtCore.Signal()
    processingFinished = QtCore.Signal()
    processingFinishedD = QtCore.Signal(object)
    statsReady = QtCore.Signal(object)
    def __init__(self,picoscope):
        super(PicoScopQt,self).__init__()
        self.rxBytes  = 0
        self.rxSpeed = 0
        self.timeout = 0
        #self.thrd = QtCore.QThread()
        #self.thrd.start()
        self.picoscope = picoscope
        self.picoscope.onBlockReady = self.blockReady.emit
        self.picoscope.onDataReady = self.dataReady.emit
        #self.blockReady.connect(self.onBlockReady)
        #self.blockReady.connect(self.repeat)
        self.blockReady.connect(self.picoscope.getValuesA)
        self.dataReady.connect(self.repeat)
        self.process = None
        self.t0 = time.time()
    def close(self):
        self.blockSignals(True)
        self.picoscope.close()
        #self.thrd.stop()
    def repeat(self):
        t1 = time.time()
        dt = t1-self.t0
        self.rxBytes+=self.picoscope.rxBytes
        speed = self.picoscope.rxBytes/dt
        # print(si_units.si_str(dt,"s"))
        # print(si_units.si_str(self.picoscope.rxBytes,"B")+" "+\
        #       si_units.si_str(speed,"Bps"))
        self.statsReady.emit({"rxTime":dt,"rxSpeed":speed,"rxBytes":self.picoscope.rxBytes,"samplingRate":1/self.picoscope.dx})
        self.t0 = t1
        #self.picoscope.getValuesS()
        if self.process and self.picoscope.rxBytes:
            try:
                self.processingFinishedD.emit(self.process())
                self.processingFinished.emit()
            except:
                traceback.print_exc()         
        self.picoscope.runBlockA()
    # def onBlockReady(self):
    #     if self.process:
    #         freq,dbm1,dbm2 = self.process()
    #     self.processingFinished.emit()
    #     self.processingFinishedD.emit(freq,dbm1,dbm2)
    def createSensMenu(self,window):
        m = window.menuBar().addMenu("Sensitivity")
        self.sens_actions = []
        for c in range(0, self.picoscope.numChannels):
            chan_menu = m.addMenu("Channel {}".format(c + 1))
            self.sens_actions.append([])
            for i, r in enumerate(picoscope_2000_class.RANGES):
                ar = QtWidgets.QAction(si_units.si_str(r, "V"))
                ar.setCheckable(True)
                if self.picoscope.yrange[c]==i:
                    ar.setChecked(True)
                self.sens_actions[c].append(ar)
                chan_menu.addAction(ar)
                ar.triggered.connect(self.onActionSensitivity)
            m.addMenu(chan_menu)

    def onActionSensitivity(self):
        sndr = self.sender()
        for c in range(0,self.picoscope.numChannels):
            if sndr in self.sens_actions[c]:
                for i,a in enumerate(self.sens_actions[c]):
                    if sndr == a:
                        a.setChecked(True)
                        print("ch {} sensitivity is {}".format(c,picoscope_2000_class.RANGES[i]))
                        self.picoscope.setChannel(c,i)
                    else:
                        a.setChecked(False)



class GUI(QtCore.QObject):
    def __init__(self):
        super(GUI,self).__init__()
    def report(self,args):
        t = time.time()
        lfreq.setText(si_units.si_str(args[0],"Hz"))
        pub_freq.send_pyobj((t,args[0]))
        ldbm1.setText("{:.1f} dBm".format(args[2]))
        pub_pwr.send_pyobj((t,args[2],args[3]))
        ldbm2.setText("""return losses {:.1f} dB
Pulse length {}\nCh2 Vpp = {}\nCh3 Vpp = {}""".format(args[4],
                                                        si_units.si_str(args[5],"s"),
                                                        si_units.si_str(args[7],"V"),
                                                        si_units.si_str(args[8], "V")))
        pub_reflect.send_pyobj((t, args[4]))
        #print(args[4:])
    def updatestats(self,obj):
        if isinstance(obj,dict):
            rep = ""
            for key,val in list(obj.items()):
                if key=="samplingRate":
                    msg = si_units.si_str(val,"S/s")
                elif key=="rxBytes":
                    msg = "received "+ si_units.si_str(val,"B")
                elif key=="rxSpeed":
                    msg = si_units.si_str(val,"Bps")
                elif key=="rxTime":
                    msg = "readout time " + si_units.si_str(val,"s")
                else:
                    msg = "{}:{}\t".format(key,val)
                rep += msg+ "\n"
            lstat.setText(rep)
gui = GUI()

def ptp2dbm(ptp):
    return np.log10(ptp*ptp*2.5)*10

def rms2dbm(Vrms):
    return np.log10(Vrms**2/50)*10+30

def analyse(y,y3):
    #global a,d,peri
    ys = y**2
    trs = ys.ptp()/10
    a = np.where(ys>trs)[0]
    d = np.diff(a)
    # ai = a[np.argmax(d)]
    # if ai<2:
    ai = ys.size
    peri = ai-a[0]
    rms = np.sqrt(ys[a[0]:ai].sum()/peri)
    per = peri*ps.dx

    ys = y3[a[0]:ai+peri]**2
    if ys.any():
        trs = ys.ptp()/10
        a = np.where(ys>trs)[0]
        #d = np.diff(a)
        #ai = a[np.argmax(d)]
        #peri = ai-a[0]
        rms3 = np.sqrt(ys[a[0]:a[-1]].sum()/peri)
    else:
        rms3 = 0
    return per, d.max()*ps.dx+per,rms,rms3

class Attenuator(object):
    def __init__(self,attenuation = 0):
        try:
            self.attenuation = abs(float(attenuation))
            self.scalar = True
        except:
            self.attenuation = 0.
            self.scalar = True
    def attenuate(self,frequency):
        if self.scalar:
            return self.attenuation

coupler = Attenuator(opt.get("RF.coupler",0))

def attenuate():
    return

def calculate():
    # sz = ps.buff[0].size
    # if sz%2:
    #     lim = sz-1
    # else:
    #     lim = sz
    freq,freqerr = fft.freq_from_fft(ps.buff[0][:1000],ps.dx)
    c1 = ps.getChanValuesF(1)
    #print c1
    c2 = ps.getChanValuesF(2)
    c2pp = c1.ptp()
    c3pp = c2.ptp()
    pul_len,puls_rate,dbm1,dbm2 = analyse(c1,c2)
    att = coupler.attenuate(freq)
    dbm1 = rms2dbm(dbm1)-att
    dbm2 = rms2dbm(dbm2)-att
    if dbm1<dbm2:
        dbm1,dbm2 = dbm2,dbm1
    #print dbm1
    #dbm2 = ptp2dbm(c2)

    return freq,freqerr,dbm1,dbm2,dbm2-dbm1,pul_len,puls_rate,c2pp,c3pp

# def process():
#     ps.getValuesS()
#     #t.append(time.time())
#     done = True
#     freq,freqerr = fft.freq_from_fft(ps.buff[0],ps.dx)
#     #t.append(time.time())
#     c1 = ps.getChanValuesF(1).ptp()
#     c2 = ps.getChanValuesF(2).ptp()
#     dbm1 = ptp2dbm(c1)
#     dbm2 = dbm1-ptp2dbm(c2)
#     #t.append(time.time())
#     #print(c1,dbm1,c2,dbm2)
#     return freq,dbm1,dbm2
#     #print("f = ",freq,"(",freqerr,")\nreflection losses:", dbm1-dbm2,"dB,\nelapsed time:",[tt-t[0] for tt in t])


ps = picoscope_2000_class.Pico2000(dx = 4e-9, bufszPreferred=1000000)
ps.open()
ps.enableChannel(0,yrange  = 6)
ps.enableChannel(1,yrange = 3)
ps.enableChannel(2,yrange = 3)


pq = PicoScopQt(ps)
pq.createSensMenu(win)
pq.process = calculate
pq.processingFinished.connect(ps.runBlockA)
pq.processingFinishedD.connect(gui.report)
pq.statsReady.connect(gui.updatestats)
#pq.moveToThread(pq.thrd)
#pq.blockReady.connect(process)

win.show()



ps.runBlockA()
app.exec_()
pq.close()
opt.savetodisk()