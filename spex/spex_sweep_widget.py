__author__ = 'Tolmachev Daniel'

import sys,time
sys.path.append( '../modules')
import sweep_widget
import spex_class
from detect_significant_order import detect_significant_order_from_string

class Spex_Sweep_Widget(sweep_widget.SweepWidget):
    def __init__(self,*args,**kwargs):
        super(Spex_Sweep_Widget,self).__init__(*args,**kwargs)
        self.label_step.setText('nm/s')

        # self.closeEvent = self.onClose
    def connect_options_dictionary(self,opt_dict):
        super(Spex_Sweep_Widget,self).connect_options_dictionary(opt_dict)
        if self.options is not None:
            self.sig_params_changed.connect(self.options.savetodisk)
    def detectSpeedMode(self):
        #check if we need to switch from continuous or stepping mode
        print("speed = {} nm/s ( -> step = {} nm".format(self.speed/1e-9,self.speed*self.dt*1e9))
        if self.speed < 0.25e-9 and self.step_as_speed: # speed to slow for continuous mode
            self.step = self.speed*self.dt
            self.step_as_speed = False
            self.label_step.setText("nm")
            self.le_step.setText(self.units2disp(self.step))
        elif self.speed >=0.25e-9 and not self.step_as_speed: #speed to fast for stepping mode
            self.step = self.speed
            self.step_as_speed = True
            self.label_step.setText("nm/s")
            self.le_step.setText(self.units2disp_speed(self.step))
        print("Spex_Sweep_Widget, step =", self.step)
    def _loadfromdict(self):
        super(Spex_Sweep_Widget,self)._loadfromdict()
        self.speed = self.step/self.dt
        self.detectSpeedMode()
        # self._step2speed()
    def _step2speed(self): #update either speed or step from gui
        if self.step_as_speed:
            self.speed = self.step
            #self.step = self.speed/self.dt
        else:
            self.speed = self.step/self.dt
        self.detectSpeedMode()
    def value_changed(self,val):
        self.left = self.units2std(self.le_left.text())
        self.right = self.units2std(self.le_right.text())
        self.step = self.units2std_speed(self.le_step.text())
        self.dt = self.str2f(self.le_dt.text())
        self._step2speed()
        self.time = self.calc_time()
        # self.le_time.setText(self.f2str(self.time))
        self.le_time.setText(self.units2disp_time(self.time))
        if self._checkValues():
            self.update_gui_values()
        self._value_changed() #self.value_changed_Timer.start(sweep_widget.DEFAULT_AUTOSAVE_TIME) #self._value_changed()
        for le in self.list_le:
            if le!=self.sender():
                le.setCursorPosition(0)
        self._announce_range()
    # def _value_changed(self):
    #     # print("Spex.step:",self.options.get("Spex.step",None),self.step)
    #     super(Spex_Sweep_Widget,self)._value_changed()
    #     # print("Spex.step:", self.options.get("Spex.step", None))
    #     self.options.savetodisk()
    def setWorkingRange(self,left,right):
        if left<spex_class.Spex.MIN or right>spex_class.Spex.MAX: #TODO find who sends wrong range
            return
        min_delta = self.findDelta()
        delta = left - self.left
        changed = False
        if abs(delta)>min_delta:
            if delta>0:
                delta = min_delta*(int(delta/min_delta)+1)
            else:
                delta = min_delta*(int(delta/min_delta)-1)
            self.left += delta
            changed = True
        delta = right-self.right
        if abs(delta)>min_delta:
            if delta>0:
                delta = min_delta*(int(delta/min_delta)+1)
            else:
                delta = min_delta*(int(delta/min_delta)-1)
            self.right += delta
            changed = True
        if changed:
            print(delta,min_delta)
            self._checkValues()
            self.update_gui_values()
            self._set_cursors()
            self._announce_range()
    def findDelta(self):
        s = str(self.le_left.text())
        stats = detect_significant_order_from_string(s)
        prec = stats['precision']
        zeros = stats['zeros']
        delta = 10**(-prec)
        #print delta
        if type(self.units_multiplier)== float:
            ran = (self.right-self.left)/self.units_multiplier
        else:
            ran = (self.right-self.left)
        if zeros>0:
            if delta<ran/100:
                delta = delta*10
            elif delta>ran/10:
                delta = delta/10
        return self.units2std(delta)
    # def __del__(self):
    #     print("closing SPEX window")
    #     if self.value_changed_Timer.isActive():
    #         self.value_changed_Timer.stop()
    #     self._value_changed() #self._value_changed() to save parameters
