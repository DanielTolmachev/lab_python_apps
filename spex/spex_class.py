import sys, time, traceback, re, socket

sys.path.append('../modules')
# from spex_socket import Instr_Socket
from instrument_base import InstrumentBase

TIME_TO_WAIT_FOR_ANSWER = 0
MAX_WAVELENGTH = 2e-6
SPEXSTARTPOSITION = 152340
OUTRANGE = 9999999
STEP = 2e-12
HIGHSPEED = 1000
LOWSPEED = 8000
socket_timeout = 0.01


class Spex(InstrumentBase):
    position = None
    lambda0 = None
    wavelength = None
    speed = None
    scan_speed = None
    HIGHSPEED = 1000
    SPEXSTEP = 2e-12
    MIN = 200e-9
    MAX = 3000e-9
    stats_query_response_time = 0
    stats_query_count = 0
    stats_average_query_response_time = 0
    backlash = 0
    backlashEnab = False
    backlash_steps = 0
    dead_move = 0
    dead_move_start_pos = 0
    direction_change_wl = 0
    # connected = False
    def __init__(self, address):
        super(Spex, self).__init__(address,timeout = socket_timeout)
        self.time_to_wait = TIME_TO_WAIT_FOR_ANSWER
        # self.interface.sock.settimeout(socket_timeout)
        self.open()
    def ask(self, query):
        self.write('{:s}:'.format(query))
        return self.readall()
    def readall(self):
        rep = b''
        try:
            if self.interface.connected:
                while True:
                    rep += self.interface.recv(64)
            else:
                return self.read()
        except socket.timeout:
            return rep
        except:
            traceback.print_exc()
        return rep
    def readline(self):
        rep = b''
        run = True
        try:
            while run:
                c = self.interface.recv(12)
                rep +=c
                if c=="\r\n":
                    run = False
        except socket.timeout:
            return rep
        except:
            traceback.print_exc()
        return rep
    def parseStateStr(self,res):
        try:
            #     res += self.recv(100)
            # res = self.recv(100)
            # while not res.endswith('ok\r\n') or 'P#' not in res:# wait till end of line

            #s = res.split()
            #if s[0]=='ok':
            r = re.findall('\w#([\d\w]+)', str(res))
            if len(r) == 5:
                self.position = int(r[0])
                self.step_adj = r[1]
                self.direction = r[2]
                self.state = r[3]
                self.speed = int(r[4])

                # if self.backlashEnab:
                #     wl = self.div2wl(self.position)
                #     if wl:
                #         if self.direction == "R":
                #             wl += self.backlash
                #             if wl<self.wavelength:
                #                 self.wavelength = wl
                #             elif not self.wavelength: #if current wavelengh was not yet set
                #                 self.wavelength = wl
                #         else:
                #             if wl>self.wavelength:
                #                 self.wavelength = wl
                #             elif not self.wavelength: #if current wavelengh was not yet set
                #                 self.wavelength = wl
                #
                #     # if self.dead_move!=0:
                #     #     steps_made = self.position-self.dead_move_start_pos
                #     #     if abs(steps_made)>abs(self.dead_move):
                #     #         self.dead_move = 0
                #     #     else:
                #     #         self.dead_move -= steps_made
                #     #         print "dead steps ", self.dead_move
                #     #         self.wavelength = self.div2wl(self.dead_move_start_pos)
                #     # if self.dead_move==0:
                #     #     self.wavelength = self.div2wl(self.position)
                #     #     if self.direction == "R":
                #     #             self.wavelength += self.backlash
                #
                #     # if abs(self.position-self.dead_move_start_pos)<self.backlash:
                #     #     print abs(self.position-self.dead_move_start_pos)-self.backlash
                #     # else:
                #     #     self.wavelength = self.div2wl(self.position)
                #     #     if self.direction == "R":
                #     #             self.wavelength += self.backlash
                # else:
                wl = self.div2wl(self.position)
                self.wavelength = self.compensate_backlash(wl)
                return True
            else:
                print('I:>> ', repr(res))
        except ValueError:
            traceback.print_exc()
            print('I:>> ', repr(res))
        except:
            traceback.print_exc()
    def querystate(self):
        self.write('I:')
        t = time.time()
        # time.sleep(self.time_to_wait)
        res = self.readall()
        t2 = time.time()
        state = self.parseStateStr(res)
        if state:
            self.stats_query_response_time = t2 - t
            self.stats_query_count += 1
            self.stats_average_query_response_time = \
                     (self.stats_average_query_response_time * (
                     self.stats_query_count - 1) + self.stats_query_response_time) / self.stats_query_count
        else:
            print("Spex.querystate error",repr(res))
        # return (t, res)
        return state
    def correct_wl(self, wl):
        while wl > MAX_WAVELENGTH:
            wl = wl / 10.
        return wl
    def compensate_backlash(self,wl):
        if self.backlashEnab:
            if wl:
                if self.direction == "R":
                    wl += self.backlash
                    if self.wavelength:
                        if wl>self.wavelength:
                            wl = self.wavelength
                else:
                    if self.wavelength:
                        if wl<self.wavelength:
                            wl = self.wavelength
        return wl

    def div2wl(self, div):
        if self.lambda0:
            # try:
            #     a = self.lambda0 + STEP*div
            # except:
            #     traceback.print_exc()
            #     print 'dbg:',self.lambda0,STEP,div
            wl = self.lambda0 + STEP * div
            return wl

    def wl2div(self, wl):
        wl = self.correct_wl(wl)
        return int(round((wl - self.lambda0) / STEP))

    def goto(self, div):
        if type(div) != int:
            div = int(div)
        self.target = div
        cnt = div - self.position
        if cnt > 0:
            self.write('F{}:'.format(cnt))
        elif cnt < 0:
            self.write('R{}:'.format(-cnt))

    def setWaveLength(self, wl):
        div = self.wl2div(wl)
        forward = div>self.position

        # print "setWavelength:",self.wavelength," ->",wl
        if self.backlashEnab:
            if self.wavelength>wl: #we want to go back
                if self.direction =="F":
                    print("Spex has changed direction")
                    self.dead_move -=self.backlash
                    self.dead_move_start_pos = self.position
                    self.direction_change_wl = self.wavelength
                div -= self.backlash_steps
            elif self.wavelength<wl: #we are going forward
                if self.direction =="R":
                    print("Spex has changed direction")
                    self.dead_move +=self.backlash
                    self.dead_move_start_pos = self.position
                    self.direction_change_wl = self.wavelength
        self.goto(div)
        return div,forward

    def getWaveLength(self):
        self.querystate()
        return self.wavelength

    def getPosition(self):
        _, res = self.querystate()
        # while not self.position or res =='':
        #     self.querystate()
        #     print res
        return self.position

    def stop(self):
        self.write('S:')

    def getSavedPosition(self):
        return self.ask('X:')

    def setCalibr(self, wl):
        if self.position == None:
            self.querystate()
        if type(wl) != float:
            wl = float(wl)
        wl = self.correct_wl(wl)
        self.lambda0 = wl - self.position * STEP
        if self.backlashEnab:
            if self.direction == "R":
                wl += self.backlash
        self.wavelength = wl
        print("calibration set to", self.wavelength)
        return wl

    def setCalibrFromPos(self, pos, wl):
        wl = self.correct_wl(wl)
        self.lambda0 = wl - pos * STEP  #provided position is used
        self.wavelength = self.div2wl(self.position)
        return wl

    def _isMoving(self, res):
        if 'RunS' in res:
            return True
        elif 'StoP' in res:
            return False

    def isMoving(self):
        try:
            self.interface.send('D:')
            rep = self.readall()
            #ans = res.split('\r\n')
            cnt = 0
            res = self._isMoving(rep)
            # while not rep.endswith('ok\r\n') or res == None:
            #     rep += self.recv(100)
            #     res = self._isMoving(rep)
            return res
        except:
            traceback.print_exc()

    def setSpeed(self, delay):
        if type(delay) != int:
            delay = int(delay)
        self.ask('V{}:'.format(delay))
    def resetPos(self):
        self.ask('C{}:'.format(SPEXSTARTPOSITION))
    def speed_si2int(self,mps):
        return int(round(1e6*STEP/mps))
    def speed_int2si(self,speed):
        return 1e6/speed*STEP
    def setBacklash(self,f):
        self.backlash = f
        self.backlash_steps = f/STEP



tabl = []


def measure_time(steps, dt=0):
    pos = spx.getPosition()
    t = time.time()
    spx.goto(pos + steps)
    while spx.isMoving() != False:
        pass
        time.sleep(dt)
        #print spx.getPosition()
    dt = time.time() - t
    print('{} steps, run in {} speed is {:.0f} steps/sec'.format(steps, dt, steps / dt))
    print('last position is {}'.format(spx.getPosition()))
    global tabl
    tabl.append((steps, dt))
    return steps, dt


def measure_speed(steps, dt=0):
    pos = spx.getPosition()
    t = time.time()
    spx.goto(pos + steps)
    t1 = t
    while spx.isMoving() != False:
        time.sleep(dt)
        t2 = time.time()
        pos2 = spx.getPosition()
        print((pos2 - pos) / (t2 - t1))
        t1 = t2
        pos = pos2
    dt = time.time() - t
    print('{} steps, run in {} speed is {:.0f} steps/sec'.format(steps, dt, steps / dt))
    print('last position is {}'.format(spx.getPosition()))
    global tabl
    tabl.append((steps, dt))
    return steps, dt


def benchmark():
    pos = spx.getPosition()
    print('Start position is {}'.format(pos))

    # measure_time(1e3)
    # measure_time(2e3)
    # measure_time(-3e3)
    # measure_time(+4e3)
    # measure_time(-5e3)
    #print tabl
    measure_speed(5e3, dt=1)


if __name__ == '__main__':
    address = ('129.217.155.157', 51234)
    spx = Spex(('129.217.155.157', 51234))
    print(spx.querystate())
    #benchmark()
