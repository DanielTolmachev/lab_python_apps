from qtpy import QtCore
import time,traceback
from atof_comma_safe import atof
import spex_class

class SpexQtSignalProxy(QtCore.QObject):
    calibration_set = QtCore.Signal(float)
    wavelength_set = QtCore.Signal(object)
    position_changed = QtCore.Signal(int)
    started = QtCore.Signal()
    stopped = QtCore.Signal()
    stopped_intermediate = QtCore.Signal()
    speed_adjusted = QtCore.Signal(float)
    dt_too_small = QtCore.Signal(float)
    connected = QtCore.Signal(bool)
    def __int__(self):
        super(SpexQtSignalProxy,self).__init__()

class SpexQtBackgroundWorker(QtCore.QThread):
    def __init__(self,parent_spex):
        self.started = False
        self.dt = 0.01
        self.spex = parent_spex
        self._dest_div = None
        self._forward = None
        super(SpexQtBackgroundWorker,self).__init__()
    def run(self):
        self.started = True
        self.spex.sig.started.emit()
        pos = self.spex.position
        ts = 0
        while self.started:
            t = time.time()
            #self.spex.isMoving()
            self.spex.querystate()
            #rep = self.spex.readall()
            #if rep:
            #    print rep

            self.spex.sig.calibration_set.emit(self.spex.wavelength)
            self.spex.sig.position_changed.emit(self.spex.position)
            self.spex.sig.wavelength_set.emit((t,self.spex.wavelength))
            if self.spex.messager_func:
                self.spex.messager_func((t,self.spex.wavelength))
            #print self.spex.wavelength
            if self.spex.target == self.spex.position:
                self.started = False
            # if (self._forward and self.spex.position>=self._dest_div) or\
            #    (self._forward==False and self.spex.position<=self._dest_div):
            #         self.started = False
            #         print("SPEX reached it's destination")
            if pos == self.spex.position:  #spex is not moving
                print("spex stuck at",pos)
                if not ts:
                    ts = t
                if t-ts>1:
                    self.started = False
            time.sleep(self.dt)
            pos = self.spex.position
            t1 = t
        print("Spex finished movement to {} at {}".format(self._dest_div,pos))
        self.spex.sig.stopped_intermediate.emit() #emit only intermediate
        self.spex.skipoff()
        self.spex.setSpeed(self.spex.scan_speed)
        self.spex.save(self.spex.position,self.spex.wavelength)
    def stop(self):
        self.started = False
    def setDestinationDiv(self,div,forward):
        self._dest_div = div
        self._forward = forward
class SpexQtBackgroundStepper(QtCore.QThread):
    started = False
    dt = 0.01
    def __init__(self,parent_spex):
        self.spex = parent_spex
        super(SpexQtBackgroundStepper,self).__init__()
        self.auto_rewind = False
    def set(self,left, right,step, count, two_way = False,resume = False,dt = 0):
        self.left = left
        self.right = right
        self.step = step
        self.count = count
        self.two_way = two_way
        self.resume = resume
        self.dt = dt
    def run(self):
        self.started = True
        self.spex.sig.started.emit()
        self.spex.setSpeed(self.spex.HIGHSPEED)
        #while self.started:
        t = time.time()
        self.spex.querystate()
        print("stepper: {}".format(self.spex.wavelength))
        self.spex.sig.calibration_set.emit(self.spex.wavelength)
        # cnt = 0
        while self.count!=0 and self.started:
            # print cnt
            # cnt+=1
            while (self.spex.wavelength> self.left and self.step<0) or \
                    (self.step>0 and self.spex.wavelength<self.right) and self.started:
                #print self.spex.wavelength, self.left,self.step,self.right
                wl = self.spex.wavelength+self.step
                #self.spex.querystate()
                #print self.spex.wavelength,wl
                t = time.time()
                self.spex.setWaveLengthSilent(wl)
                #self.spex.querystate()
                #rep = self.spex.readline()
                #if rep and rep!="ok\r\n":
                #    print "1:",repr(rep)
                #rep = self.spex.readline()
                # print "2:",repr(rep)

                # self.spex.sig.calibration_set.emit(self.spex.wavelength)

                # while not rep:
                #     rep = self.spex.readline()
                # while self.spex.isMoving():
                #     self.spex.querystate()
                #     self.spex.sig.calibration_set.emit(self.spex.wavelength)
                t2 = time.time()
                #print t2-t,self.dt
                dt1 = t-t2+self.dt
                if dt1 > 0:
                    time.sleep(dt1)
                    t2 = time.time()
                else:
                    self.spex.sig.dt_too_small.emit(dt1)
                    print("dt is too small:",dt1)
                self.spex.sig.calibration_set.emit(self.spex.wavelength)
                self.spex.sig.position_changed.emit(self.spex.position)
                self.spex.sig.wavelength_set.emit((t2,wl))
                if self.spex.messager_func:
                    self.spex.messager_func((t2,wl))
                t = t2
            if self.two_way:
                self.step= -self.step
            elif self.auto_rewind:
                if self.step>0:
                    self.rewind(self.left)
                elif self.step<0:
                    self.rewind(self.right)
            self.count = self.count-1
        self.spex.sig.stopped.emit() #emit only intermediate
        self.spex.save(self.spex.position,self.spex.wavelength)
        print("Background stepper stopped")
    def rewind(self,wl):
        print("rewinding to {}".format(wl));
        self.spex.skipon();
        self.spex.setWaveLengthSilent(wl)
        while self.spex.wavelength-wl>self.spex.SPEXSTEP:
            self.spex.querystate()
            self.spex.sig.calibration_set.emit(self.spex.wavelength)
        if self.spex.messager_func:
            self.spex.messager_func((time.time(),"skip off" ))
            print("rewind: skip off")
    def stop(self):
        self.started = False

class SpexQT(spex_class.Spex):
    f = None
    sig = SpexQtSignalProxy()
    nextMove = None
    messager_func = None
    skipping = False
    calibration = None
    calibrated = False
    def __init__(self,*args):
        # spex_class.Spex.__init__(self)
        # QtCore.QObject.__init__(self)
        super(SpexQT,self).__init__(*args) #this automatically openes connection
        #self.spex = spex_class.Spex(*args)
        self.worker_thrd = SpexQtBackgroundWorker(self)
        self.sig.stopped_intermediate.connect(self.continueWork)
        self.stepper = SpexQtBackgroundStepper(self)
        self.open_interface = self.open
        self.open = self._open
        self.sig.connected.emit(self.isOpen())
        self.checkCalibr()
    # def connect(self,*args):
    #     super(SpexQT,self).open()
    def _open(self,*args):
        print("SpexQT.open")
        #super(SpexQT,self).open()
        self.open_interface()
        self.sig.connected.emit(self.isOpen())
        self.checkCalibr()
    def close(self):
        super(SpexQT,self).close()
        self.sig.connected.emit(self.isOpen())
    def save(self,pos,wl):
        if not self.f:
            self.f = open('position.txt','w+')
        elif self.f.closed:
            self.f = open('position.txt','w+')
        try:
            self.f.write('{} {}'.format(pos,wl))
            self.f.truncate()
            self.f.flush()
            self.f.seek(0)
        except:
            traceback.print_exc()
    def load(self): #load file with calibration
        try:
            with open('position.txt') as f:
                l = f.readline()
            pos,wl = l.split()
            self.calibration = int(pos), float(wl)  # remember calibratiob in case, device is not connected
        except:
            traceback.print_exc()
    def setCalibr(self,wl):
        wl = super(SpexQT,self).setCalibr(wl)
        if wl:
            self.sig.calibration_set.emit(wl)
            self.save(self.position,wl)
    def checkCalibr(self):
        if not self.calibrated:
            if not self.calibration:
                self.load()
            # print("checking calibration: saved:",pos,wl,"device",self.querystate())
            pos,wl = self.calibration
            self.querystate()
            if self.position:
                if self.position == spex_class.OUTRANGE:
                    self.resetPos()
                    self.querystate()
                if self.position == spex_class.SPEXSTARTPOSITION: #spex controlled was just started
                    self.setCalibr(wl) #set to saved wavelength
                    self.save(self.position,wl)
                elif self.position == pos: #saved position == current, good
                    print('Current wavelength is {}'.format(wl))
                    self.setCalibr(wl) #we still need to inform controlling class what current wavelength is
                else: #spex controller remembers some position we will believe it
                    #but it remembers the position not wl,
                    self.setCalibrFromPos(pos,wl) # so we need to teach it to saved calibration
                    self.save(self.position,self.div2wl(self.position))
                    wl = self.div2wl(self.position)  #real wavelength from motor position
                    print('reported position is {}\ncheck the wavelength. is it {}?'.format(self.position,wl))
                self.calibrated = True
                self.sig.calibration_set.emit(wl)
            else:
                print('Error querying state')
    def setWaveLength(self,wl):
        self.setMaxSpeed()
        div,forward = super(SpexQT,self).setWaveLength(wl)
        self.worker_thrd.setDestinationDiv(div,forward)
        self.worker_thrd.start()
    def setWaveLengthSilent(self,wl):
        #print self.getWaveLength()
        # if self.messager_func:
        #     self.messager_func((time.time(),"skip on"))
        #     print "setWaveLengthSilent: skip on"
        super(SpexQT,self).setWaveLength(wl)
        rep = self.readline()
        if rep:
            if not self.parseStateStr(rep):
                print("setWLsilent",repr(rep))
#self.wavelength = wl
    def setMaxSpeed(self):
        self.scan_speed = self.speed
        self.setSpeed(self.HIGHSPEED)
    def checkSpeed(self,speed_SI):
        scan_speed  = self.speed_si2int(speed_SI)
        if scan_speed > spex_class.LOWSPEED:
            scan_speed = spex_class.LOWSPEED
        elif scan_speed < spex_class.HIGHSPEED:
            scan_speed = spex_class.HIGHSPEED
        return self.speed_int2si(scan_speed)
    def setSpeedSI(self,speed_SI):
        spex_speed  = abs(self.speed_si2int(speed_SI))
        new_speed = self.setSpeed(spex_speed)
        if spex_class != new_speed:
            self.sig.speed_adjusted.emit(self.speed_int2si(new_speed))
    # def _sweepWaveLength(self,start,stop,speed,dt):
        # if speed < 0:
        #     reverse = True
        #     speed = -speed
        # else:
        #     reverse = False
        # self.scan_speed  = self.speed_si2int(speed)
        # if self.scan_speed > spex_class.LOWSPEED:
        #     self.scan_speed = spex_class.LOWSPEED
        #     self.sig.speed_adjusted.emit(self.speed_int2si(self.scan_speed))
        # elif self.scan_speed < spex_class.HIGHSPEED:
        #     self.scan_speed = spex_class.HIGHSPEED
        #     self.sig.speed_adjusted.emit(self.speed_int2si(self.scan_speed))
        # self.setSpeed(self.scan_speed)
        # print 'Sweeping from {} to {} with speed {} nm/s {}'.format(start,stop,speed,self.scan_speed)
        # if reverse:
        #     super(SpexQT,self).setWaveLength(start)
        # else:
        #     super(SpexQT,self).setWaveLength(stop)
        # self.thrd.start()
    def setSpeed(self,spex_speed):
        self.scan_speed  = spex_speed
        if self.scan_speed > spex_class.LOWSPEED:
            self.scan_speed = spex_class.LOWSPEED
        elif self.scan_speed < spex_class.HIGHSPEED:
            self.scan_speed = spex_class.HIGHSPEED
        super(SpexQT,self).setSpeed(self.scan_speed)
        return self.scan_speed
    def sweepWaveLength(self,target):
        super(SpexQT,self).setWaveLength(target)
        self.worker_thrd.start()
    def speedSweep(self,left, right,speed, count, two_way = False,resume = False,dt = 0):
        if not resume:
             if abs(self.wavelength-left)> 2e-12 and speed>0:
                print("Need to rewind to {}".format(left))
                self.nextMove = self.speedSweep,left,right,speed,count, \
                                two_way, dt
                self.skipon()
                self.setWaveLength(left) #set wavelength on max speed (rewind)
                return
             elif speed<0 and abs(self.wavelength-right)>2e-12:
                print("Need to fast forward to {}".format(right))
                self.nextMove = self.speedSweep,left,right,speed,count, \
                                two_way, dt
                self.setWaveLength(right) #set wavelength on max speed (rewind)
                return
        if count>0:
            count -= 1 #count -1 means run in a loop
        if count:
            if two_way: #change direction on next move for two-way scan
                self.nextMove = self.speedSweep,left,right,-speed,count, \
                                two_way, dt
            else:
                self.nextMove = self.speedSweep,left,right,speed,count, \
                                two_way, dt
        else: #stop after this move
            self.nextMove = None
        self.setSpeedSI(speed)
        if speed>0:
            print("speedSweep: going from {} to {} at {}".format(self.wavelength,right,speed))
            self.sweepWaveLength(right)
        elif speed<0:
            print("speedSweep: going from {} to {} at {}".format(self.wavelength,left,speed))
            self.sweepWaveLength(left)
    def stepSweep(self,left, right,step, count, two_way = False,resume = False,dt = 0):
        if not resume:
             if abs(self.wavelength-left)> 2e-12 and step>0:
                print("Need to rewind to {}".format(left))
                self.nextMove = self.stepSweep,left,right,step,count, \
                                two_way, dt
                self.skipon()
                self.setWaveLength(left) #set wavelength on max speed (rewind)
                return
             elif step<0 and abs(self.wavelength-right)>2e-12:
                print("Need to fast forward to {}".format(right))
                self.nextMove = self.stepSweep,left,right,step,count, \
                                two_way, dt
                self.skipon()
                self.setWaveLength(right) #set wavelength on max speed (rewind)
                return

        self.nextMove = None
        print("stepSweep: going from {} to {} at {} nm at {} nm/s".\
            format(self.wavelength*1e9,right*1e9,step*1e9,step*1e9*dt))
        self.stepper.set(left,right,step,count, two_way=two_way, dt=dt)
        self.stepper.start()
    def continueWork(self):
        self.querystate()
        if self.nextMove:
            func, left,right,speed,count, two_way, dt = self.nextMove
            if func== self.speedSweep:
                self.speedSweep(left,right,speed,count,two_way=two_way,dt=dt)
            elif func== self.stepSweep:
                self.stepSweep(left,right,speed,count,two_way=two_way,dt=dt)
        else:
            print("Spex is stopped")
            self.sig.stopped.emit()
    def stop(self):
        print("sending stop to SPEX")
        super(SpexQT,self).stop()
        self.stepper.stop()
        self.worker_thrd.stop()
        self.nextMove = None
    def setBacklash(self,arg):
        f = atof(arg)
        if f<2 and f>=0.002: #nanometers
            f = f*1e-9
        if f<2e-9 and f>2e-11:
            print("SPEX backlash set to {}".format(f))
            super(SpexQT,self).setBacklash(f)
        else:
            print("backlash value is too large or too small: {}".format(f))
    def setBacklashEnab(self,state):
        self.backlashEnab = state
    def skipon(self):
        if self.messager_func:
            self.messager_func((time.time(),"skip on"))
            print("skip on")
            self.skipping = True
    def skipoff(self):
        if self.messager_func:
            if self.skipping == True:
                self.messager_func((time.time(),"skip off"))
                print("skip off")
                self.skipping = False
    def refresh(self):
        if not self.isOpen():
            self.open()
        if super(SpexQT,self).querystate()==True:
            if self.wavelength:
                self.sig.calibration_set.emit(self.wavelength)
            if self.position:
                self.sig.position_changed.emit(self.position)
