# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wl_calibration_dlg.ui'
#
# Created: Tue Aug 04 15:20:47 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets

class Wl_Calibration_Dlg(QtWidgets.QWidget):
    hidden = True
    sig_wl_set = QtCore.Signal(float)
    get_init_value_callback = None
    def __init__(self):
        super(Wl_Calibration_Dlg,self).__init__()
        self.setFixedWidth(150)
        self.setFixedHeight(75)
        self.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed))
        self.btn = QtWidgets.QPushButton('Wavelength\ncal.',self)
        self.btn.setGeometry(0,0,75,35)
        self.btn_cancel = QtWidgets.QPushButton('Cancel',self)
        self.btn_cancel.setGeometry(75,0,75,35)
        self.btn_cancel.hide()
        self.lbl_info = QtWidgets.QLabel(self)
        self.lbl_info.setGeometry(0,35,75,35)
        self.ledit = QtWidgets.QLineEdit(self)
        self.ledit.setGeometry(0,37,150,35)
        f = self.ledit.font()
        f.setPointSize = f.pointSize()*1.3
        self.ledit.setFont(f)
        self.ledit.hide()
        # lo = QtGui.QLayout()
        # self.setLayout(lo)
        # lo.addWidget(self.btn)
        self.btn.pressed.connect(self.toggle)
        self.ledit.returnPressed.connect(self.on_accept)
        self.ledit.returnPressed.connect(self.hide_input)
        self.btn_cancel.pressed.connect(self.hide_input)
    def toggle(self):
        if self.hidden:
            self.show_input()
        else:
            self.hide_input()
            self.on_accept()
    def show_input(self):
        self.hidden = False
        self.lbl_info.hide()
        if self.get_init_value_callback:
            val = self.get_init_value_callback()
            self.ledit.setText('{}'.format(val))
        self.ledit.show()
        self.ledit.setFocus()
        self.btn_cancel.show()
        self.btn.setText('Ok')
    def hide_input(self):
        self.hidden = True
        self.lbl_info.show()
        self.btn_cancel.hide()
        self.ledit.hide()
        self.btn.setText('Wavelength\ncal.')
        self.lbl_info.setText('')
    def on_accept(self):
        s = self.ledit.text()
        try:
            wl = float(s)
        except ValueError:
            s = s.replace(",",".")
            wl = float(s)
        print(wl)
        self.sig_wl_set.emit(wl)
    def showMessage(self,txt):
        self.lbl_info.setText(txt)



if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    wlc = Wl_Calibration_Dlg()
    wlc.show()
    wlc.showMessage('Hello')
    app.exec_()

