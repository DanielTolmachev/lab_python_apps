#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     09.04.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import socket,traceback,six
from os import strerror
MAX_REPLY_LENGTH = 100 #bytes
DEFAULT_TIMEOUT = 1 #sec
class Instr_Socket(socket.socket):
    max_reply_len = MAX_REPLY_LENGTH
    connected = False
    def __init__(self,address):
        super(Instr_Socket,self).__init__(socket.AF_INET, socket.SOCK_STREAM)
        self.settimeout(DEFAULT_TIMEOUT)
        if type(address)==tuple:
            if len(address)>1:
                self.address = address[0]
                self.port = address[1]
        elif type(address)==str:
##            port_i = address.rfind(':')
##            add_i = address.rfind('/')
##            if add_i<0:
##                add_i = 0
##            self.port = int(address[a:])
##            self.port
            pass
    def write(self,data):
        try:
            if six.PY2:
                self.send(data)
            else:
                self.send(bytes(data,"utf8"))
        except:
            traceback.print_exc()
    def read(self):
        try:
            return self.recv(self.max_reply_len)
        except:
            traceback.print_exc()
            return ''
    def open(self,*address):
        self.connected = False
        try:
            if len(address)>0:
                self.address = address[0]
            self.connect((self.address,self.port))
            self.connected = True
            print("connected to {}:{}".format(self.address,self.port))
        except Exception as e:
            en = e.errno
            if en== 10056:
                print("already connected")
                self.connected = True
            elif en == 9:
                self = Instr_Socket((self.address,self.port))
            elif en!=None:
                print("unable to connect to {}:{}".format(self.address,self.port))
                print("Errno ", en, strerror(en))
            #print sys.exc_info()
            #traceback.print_exc()
        return self.connected
    def close(self):
        if self.connected:
            print("disconnecting")
            self.connected = False
            self.shutdown(socket.SHUT_RDWR)
            super(Instr_Socket,self).close()
        else:
            print("already disconnected")
    def ask(self,data):
        self.write(data)
        return self.read()





