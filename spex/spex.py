#-------------------------------------------------------------------------------
# Name:      	spex.py
# Purpose:	This app controls SPEX monocromator
#		and allows to set/sweep wavelength
#		and broadcast wavelengths during sweep using 
#		ZMQ messaging library  (PUB pattern)
#		
# Made for:	e3.physik.tu-dortmund.de	
#
# Created:     24.07.2015
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

DEV_ADDRESS = ('129.217.155.157',51234)
PUBLISH_ADDRESS = 'tcp://127.0.0.1:5007'
SUB_PORT = 6007

# START = 0 #mA
# STOP = 300 #mA
# STEP = 0.2 #mA
# DT= 0.3 #s
SPEXSTARTPOSITION = 152340
SPEXMINSPEED = 0.25e-9 #m/s
print("SPEX is starting...")
import zmq
from qtpy import QtWidgets,QtCore,QtGui
import sys,time,traceback
sys.path.append('../modules')
import install_exception_hook
from spex_qt import SpexQT
from wl_calibration_dlg import Wl_Calibration_Dlg
from spex_sweep_widget import Spex_Sweep_Widget
from sweeper_msg import SweeperWMsg
from zmq_publisher import Zmq_Publisher
from zmq_sub2qt import Zmq2Qt
import quickQtApp
from roundto import roundTo

class CmdProxy(object):
    def __init__(self,gui,sweeper,publisher):
        self.gui = gui
        self.sweeper = sweeper
        self.publisher = publisher
    def cmd_parse(self,cmd):
        if 'start' in cmd:
            print(cmd)
            loop = not 'once' in cmd
            resume = 'continue' in cmd
            bidirectional = not 'oneway' in cmd
            #pws.set_remote()
            cmd = CmdProxy(sw,sw_field,pub)
            self.sweeper.sweep(  self.gui.left,
                            self.gui.right,
                            self.gui.step,
                            self.gui.dt,
                            resume = resume,
                            loop = loop,
                            bidirectional = bidirectional)
            #start,stop,step,dt,resume = False,
            #    loop =  True,
            #    bidirectional = False,
            #    maxloops = 0
        elif 'stop' in cmd:
            self.sweeper.stop()
            #pws.setcurrent(0)
            #pws.setvoltage(0)
        #self.publisher.send_pyobj(unicode(cmd))

class SignalProxy(QtCore.QObject):
    sig_float = QtCore.Signal(object)
    sig_tuple = QtCore.Signal(tuple)
    sig_string = QtCore.Signal(str)
    sig = QtCore.Signal(object)
    sig_new_range = QtCore.Signal(float,float)
    def rcv(self,t):
        #print type(t),': ',t
        if type(t)==tuple:
            self.sig.emit(t)
            if len(t)>1:
                if type(t[1])==float:
                    self.sig_float.emit(t[1])
        elif type(t)==str:
            self.sig.emit(t)
        elif type(t)==float:
            self.sig.emit((time.time(),t))
            #print t
    def sendrange(self,begin,end):
        self.sig.emit(('range',(begin,end)))
    def processCommand(self,arg):
        if type(arg)==tuple:
            if len(arg)>1:
                if arg[0]=="range" and type(arg[1])==tuple:
                    self.sig_new_range.emit(arg[1][0],arg[1][1])
                    return
        elif type(arg)==str:
            if "start" in arg:
                sw.on_btn_start_pressed()
                return
            elif "goto" in arg:
                if "begin" in arg:
                    sw.gotoBegin()
        print("received command:", arg)

def send_stop():
    global pub
    pub.send_pyobj('stop')
def sweep_start(start,stop,speed,dt,resume,loop,bidirectional_scan):
    #print "sweep_start: speed is {},".format(speed)
    #if abs(speed)>= SPEXMINSPEED:
    if sw.step_as_speed:
        print(" continouous mode")
        if loop:
            count = -1
        else:
            if bidirectional_scan:
                count = 2
            else:
                count = 1
        spx.speedSweep(start,stop,speed, count, two_way = bidirectional_scan,resume = resume,dt = 0)
    else:
        print(" stepper mode")
        #step = speed*dt
        step = speed
        if loop:
            count = -1
        else:
            if bidirectional_scan:
                count = 2
            else: count = 1
        spx.setMaxSpeed()
        #sw_wl.sweep(start,stop,step,dt,resume,loop,bidirectional_scan)
        spx.stepSweep(start,stop,step,count,two_way = bidirectional_scan,resume = resume,dt = dt)

signalproxy = SignalProxy()
context = zmq.Context()
app,opt,win,cons = quickQtApp.mkAppWin()

pub = Zmq_Publisher(context,PUBLISH_ADDRESS)
sub = Zmq2Qt(context,SUB_PORT,verbose = False)
spx = SpexQT(DEV_ADDRESS)
spx.messager_func = pub.send_pyobj;

# menu = { 'device':{'refresh':spx.querystate}}
action_connect = QtWidgets.QAction('connect',win)
action_refresh = QtWidgets.QAction('refresh',win)
action_stop = QtWidgets.QAction('stop',win)

stb = win.statusBar()#QtGui.QStatusBar(win)

sbl_pos = QtWidgets.QLabel(str(spx.position))
stb.addWidget(sbl_pos)
sbl_info = QtWidgets.QLabel('')
stb.addWidget(sbl_info)
# for m in menu:
#     QtGui.QMenu
#win = QtGui.QMainWindow()
def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk
def save_backlash(v):
    opt['Spex.backlash_compensation'] = spx.backlash
    opt.savetodisk
def save_backlash_state(s):
    opt['Spex.backlash_compensation_enabled'] = bool(s)
    opt.savetodisk
def update_statusBar_position(pos):
    sbl_pos.setText(str(pos))
def printStatusBarInfo(msg):
    sbl_info.setText(msg)
def error_dt_small(dt):
    printStatusBarInfo("time constant is too small {:.3f}".format(dt))

def connect():
    if not spx.isOpen():
        print("connecting")
        spx.open()
    else:
        print("disconnecting")
        spx.close()
    menu_connect_set()

def menu_connect_set():
    if spx.isOpen():
        action_connect.setText("disconnect")
    else:
        action_connect.setText("connect")

menu_connect_set()

action_connect.triggered.connect(connect)
action_refresh.triggered.connect(spx.refresh)

if 'Window.geometry' in opt:
    win.setGeometry(*opt['Window.geometry'])
win.GeometryChangedSig.connect(save_win_position)

win.setWindowTitle('SPEX monocromator')

#set application icon
icon = QtGui.QIcon("spex.png")
win.setWindowIcon(icon)



#sw = sweep_widget.SweepWidget
sw = Spex_Sweep_Widget(units='nm',
                              sweepValueDesc='Spex',
                              units_multiplier=1e-9,
                              step_as_speed = True,
                              units_speed = 'nm/s',
                              units_speed_multiplier = 1e-9,
                              precision=3
                              #auto_switch_direction = True
                              )

wl_cal = Wl_Calibration_Dlg()
wl_cal.setFocusPolicy(QtCore.Qt.ClickFocus)
wdgt = QtWidgets.QWidget()
gl = QtWidgets.QGridLayout()
blcb = QtWidgets.QCheckBox("Backlash compensation")
blle = QtWidgets.QLineEdit()
blcb.setFocusPolicy(QtCore.Qt.ClickFocus)
blle.setFocusPolicy(QtCore.Qt.ClickFocus)
wdgt.setLayout(gl)

# tw = QtGui.QTabWidget()
# win.setCentralWidget(tw)
# tw.addTab(sw,'Spex control')
# tw.addTab(widget_pwr,'Power')

win.setCentralWidget(wdgt)
gl.addWidget(sw,0,0,4,1)
gl.addWidget(wl_cal,0,1)
gl.addWidget(blcb,1,1)
gl.addWidget(blle,2,1)
sw.setFocus()



action_connect.triggered.connect(spx.open)
action_stop.triggered.connect(spx.stop)

#w.menubar.addAction(action_view)
win.menuBar().addAction(action_connect)
win.menuBar().addAction(action_refresh)
win.menuBar().addAction(action_stop)



sw_wl = SweeperWMsg(messager=signalproxy.rcv,
                    setfunc=spx.setWaveLengthSilent,
                    queryfunc=spx.getWaveLength,
                    verbose=False)

# sw_wl.set_units(1e-9,'nm')
#sw.setunits('V',1)
#pws.setvoltage(0)
#pws.setcurrent(.5)
# mc = magnet_control(mg)
spx.setBacklash(roundTo(opt.get("Spex.backlash_compensation",0),1e-12))
spx.backlashEnab = opt.get("Spex.backlash_compensation_enabled",False)
blcb.setChecked(spx.backlashEnab)
blle.setText(str(spx.backlash))
blcb.stateChanged.connect(save_backlash_state)
blcb.stateChanged.connect(blle.setEnabled)
blcb.stateChanged.connect(spx.setBacklashEnab)
blle.textEdited.connect(spx.setBacklash)
blle.textEdited.connect(save_backlash)


sw.connect_options_dictionary(opt)
sw.sig_params_changed.connect(opt.savetodisk) #save options immidiately
#cmd_freq = CmdProxy(sw,sw_field,pub)
#sw.sig_started.connect(cmd_freq.cmd_parse)
sub.sigGotMessage.connect(signalproxy.processCommand)
signalproxy.sig_new_range.connect(sw.setWorkingRange)
sw.sig_range_changed.connect(signalproxy.sendrange)
signalproxy.sig.connect(pub.send_pyobj)

sw.sig_start_sweeper.connect(sweep_start)
sw.sig_stop_sweeper.connect(spx.stop)
sw.sig_stop_sweeper.connect(sw_wl.stop)
#sw.sig_start_sweeper.connect(sw_field.sweep)
#sw.sig_stop_sweeper.connect(sw_field.stop)


spx.sig.started.connect(sw.setGuiStarted)
spx.sig.stopped.connect(sw.setGuiStopped)
spx.sig.stopped.connect(send_stop)
spx.sig.connected.connect(menu_connect_set)

sw.sig_started.connect(pub.send_pyobj)
#sw.sig_started.connect(mg.setlocal) #for convenience
sw.sig_value_set.connect(spx.setWaveLength) #set current from GUI widget
#signalproxy.sig.connect(pub.send_pyobj) #send tuple away using zmq
signalproxy.sig_float.connect(sw.set_central_label) #modify widget central value

wl_cal.sig_wl_set.connect(spx.setCalibr)
spx.sig.calibration_set.connect(sw.set_central_label)
spx.sig.position_changed.connect(update_statusBar_position)
spx.sig.speed_adjusted.connect(sw.setSpeed)
spx.sig.dt_too_small.connect(error_dt_small)
#spx.sig.wavelength_set.connect(pub.send_pyobj)
#sw.setExtValueChecker('step',spx.checkSpeed)
sw.setRange('step',0,2e-9)
#------------------run

sw.set_central_label(spx.getWaveLength())
print("Wavelength =",spx.getWaveLength())
update_statusBar_position(spx.position)
win.show()
app.exec_()
spx.close() #Do not forget to close connection!
opt.savetodisk()
