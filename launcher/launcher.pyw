﻿#coding:utf-8
from __future__ import unicode_literals
import sys,json,os,traceback,collections,codecs
sys.path.append( sys.path[0]+'/../modules')
import enable_exception_logging
from quickQtApp import *
from options import Options
from updater import Updater

confname = "apps.json"
MAX_ICON_SIZE = 64
BOM = b'\xef\xbb\xbf' #byte order marker

_handle = None
if os.name=="nt":
    try:
        import win32gui
        # def _window_enum_callback(self, hwnd, wildcard):
        #     '''Pass to win32gui.EnumWindows() to check all the opened windows'''
        #     if re.match(wildcard, str(win32gui.GetWindowText(hwnd))) != None:
        #         global _handle
        #         _handle = hwnd

        def _window_enum_callback(hwnd, windowname):
            '''Pass to win32gui.EnumWindows() to check all the opened windows'''
            try:
                wnd = win32gui.GetWindowText(hwnd)
                if windowname in wnd:
                    global _handle,wnd_list,wildcard
                    _handle = hwnd
                    print("found window {}: '{}' ({})".format(wildcard,
                          windowname,hwnd))
                    #wnd_list.append((wnd,hwnd))
            except:
                traceback.print_exc()
                print(win32gui.GetWindowText(hwnd))
        def find_window_wildcard(self, wildcard):
            global _handle
            _handle = None
            win32gui.EnumWindows(self._window_enum_callback, wildcard)

        def raiseWindowNT(windowname):
            try:
                if not windowname:
                    return False
                global _handle,wildcard
                _handle = None
                wildcard = windowname
                win32gui.EnumWindows(_window_enum_callback,windowname)
            #     handle = win32gui.FindWindow(None, windowname)
                if _handle:
                    if win32gui.IsIconic(_handle):
                        win32gui.ShowWindow(_handle,9)
                    win32gui.SetForegroundWindow(_handle)                    
                    return True
                else:
                    return False
            except win32gui.error:
                return False

        raiseWindow = raiseWindowNT
    except:
        traceback.print_exc()
        raiseWindow = lambda x:False
else:
    raiseWindow = lambda x:False

if raiseWindow("Lab App Launcher"):
    print("Launcher is already running")
    #sys.exit(0)

app,opt,win,cons = mkAppWin(title = "Lab App Launcher",icon = "green-spin.png")
awd = {} #applications widget dict
awl = [] #applications widget list

upd = Updater("..")


def reload_info():
    global d
    try:
        # os.chdir(startdir)
        with codecs.open(startdir + os.sep+"apps_config",'r', 'utf-8-sig') as f:
            d1 = json.load(f,object_pairs_hook=collections.OrderedDict)
            d = d1
            load_info(d)
            constructGui()
    except:
        traceback.print_exc()

def allowResize():
    win.setMinimumSize(0,0)
    win.setMaximumSize(10000,10000)
    #win.setFixedSize(QtCore.Qt.MaximumSize,QtCore.Qt.MaximumSize)

QMenuFromDict.AddMenu(win.menuBar(),
                      {
                          "service":{
                          "renew info":reload_info,
                          "make resizeable":allowResize},
                          "update":upd.update

                      })

def load_description_from_comments(filename):
    desc = u""
    try:
        f = open(filename,"rb")
        utfsign = False
        
        s = f.readline()
        if s.startswith(BOM):
            s = s[3:]
        while s.strip().startswith(b"#"):
            if b"utf-8" in s and not utfsign:
                utfsign = True
                s = f.readline()
                continue
            if s.startswith(b"#!"):
                 s = f.readline()
                 continue
            if utfsign:
                desc += s.decode("utf-8")
            else:
                desc += s.decode()
            s = f.readline()
        f.close()
    except:
        traceback.print_exc()
    return desc

def load_info_path(d,path):
    if os.path.exists(path):
        desc = load_description_from_comments(path)
        if desc:
            d["descr"] = desc
        mtime_py = os.path.getmtime(path)
        exti = path.rfind(".py")
        pyc = path[:exti]+".pyc"
        #print pyc
        if os.path.exists(pyc):
            mtime_pyc = os.path.getmtime(pyc)
            print(mtime_pyc,mtime_py)
            if mtime_pyc>mtime_py:
                d["pyc"] = pyc
        icon = path[:exti]+".png"
        icon2 = os.path.dirname(path)+os.sep+"icon.png"
        if os.path.exists(icon):
            d["icon"] = icon
        elif os.path.exists(icon2):
            d["icon"] = icon2
        optf = path
        opts = Options(optf)
        opts.autosave = False

        if "dev.address" in opts:
            d["dev.address"] = opts["dev.address"]
        elif "device_address" in opts:
            d["dev.address"] = opts["device_address"]
        if "pub.address" in opts:
            d["pub.address"] = opts["pub.address"]
        elif "publish_port" in opts:
            d["pub.address"] = opts["publish_port"]
    aw = AppWidget(d)
    #gl.addWidget(aw)
    awd[path] = aw
    #print "size,sizeHint",aw.size(),aw.sizeHint()
catlist = []

# class Item(QtGui.QListWidgetItem):
#     def __init__(self,aw):
#         super(Item,self).__init__()
#         self.setToolTip("boo")
#         self.setText("boo")

# class Delegate(QtGui.QItemDelegate):
#     def __init__(self,itemlist):
#         super(Delegate,self).__init__()
#         self.itemlist = itemlist
#     def paint(self, qPainter, QStyleOptionViewItem, QModelIndex):
#         path = self.itemlist[QModelIndex.row()]["path"]
#         aw = awd[path]
#         print qPainter,aw
#         aw = AppWidget()
#         qPainter = QtGui.QPainter()
#         qPainter.dr
#         app = QtGui.QApplication()
#         st = app.style()
#         st = QtGui.QStyle()


# class Model(QtCore.QAbstractListModel):
#     def __init__(self,itemlist):
#         super(Model,self).__init__()
#         self.itemlist = itemlist
#     def rowCount(self, QModelIndex_parent=None, *args, **kwargs):
#         return len(self.itemlist)
#     def data(self, index, int_role=None):
#         if index.isValid() and int_role == QtCore.Qt.DisplayRole:
#             return QtCore.QVariant(self.itemlist[index.row()])
#         else:
#             return QtCore.QVariant()
def listadditem(lst,itemlist):
    #lst = QtGui.QListWidget()
    for aw in itemlist:
        wit = QtWidgets.QListWidgetItem()

        lst.addItem(wit)
        lst.setItemWidget(wit,aw)
        sh = wit.sizeHint()
        sh.setHeight(opt.get("gui.app_widget.height",60))
        wit.setSizeHint(sh)
    #dlgt = QtGui.QItemDelegate()
    #mod = QtGui
    #dlgt.setModelData(aw)
    #lst.setItemDelegate(aw)
    pass

def constructGui():
    lastrows = 0
    ccol = 0
    for lst in catlist:
        gl.removeWidget(lst)
    #make list or table look like widget
    col = win.palette().color(win.backgroundRole())
    pal = QtGui.QPalette()
    pal.setColor(QtGui.QPalette.Base, col)


    tbl.setFrameShape(QtWidgets.QFrame.NoFrame)
    tbl.setPalette(pal)
    tbl.setShowGrid(False)
    tbl.horizontalHeader().hide()
    tbl.verticalHeader().hide()

    for key,val in d.items():
        itemlist = []
        awlist = []
        if isinstance(val,dict):
            itemlist.append(val)
            awlist.append(AppWidget(val))
            # listadditem(lst,awd[val["path"]])
            # print awd[val["path"]]
        elif type(val)==list:
            for el in val:
                itemlist.append(el)
                awlist.append(AppWidget(el))
                # print awd[el["path"]]
                # listadditem(lst,awd[el["path"]])
        #lst = QtGui.QListView()
        lst = QtWidgets.QListWidget()
        lst.setFrameShape(QtWidgets.QFrame.NoFrame)

        # pal.setColor(QtGui.QPalette.Base, QtGui.QColor((,155,155)))
        lst.setPalette(pal)


        #model = Model(itemlist)
        #dlgt = Delegate(itemlist)
        #lst.setModel(model)
        #lst.setItemDelegate(dlgt)
        listadditem(lst,awlist)
        row = 0
        trc = tbl.rowCount()
        if lastrows+len(lst)>trc: #add new column, if list is long enough
            ccol+=1
            row = 0
        else:
            row = lastrows+1
        # if len(catlist)==2:
        #     row = 3
        #     ccol = 3
        #gl.addWidget(lst,row,ccol,len(lst),1)
        # for aw in awlist:
        #     gl.addWidget(aw)
        if trc<len(awlist):
            tbl.setRowCount(len(awlist))
        elif trc<row+1:
            tbl.setRowCount(row+1)
        if tbl.columnCount()<ccol:
            tbl.setColumnCount(ccol)
        for i,aw in enumerate(awlist):
            print(aw.title,aw.size(),aw.sizehintW,aw.sizehintH)
            tbl.setCellWidget(row+i,ccol-1,aw)
            tbl.setRowHeight(row+i,aw.height())
            tbl.setColumnWidth(ccol-1,max(aw.width(),tbl.columnWidth(ccol-1)))

        lastrows = row+len(lst)
        catlist.append(lst)



def load_info(d):
    if not isinstance(d,dict):
        print(d)
        return
    for key,val in list(d.items()):
        print(key,val)
        if key == "path":
            load_info_path(d,val)
        elif isinstance(val,dict):
            load_info(val)
        elif type(val)==list:
            for el in val:
                load_info(el)

def is_process_running(process_id):
    try:
        os.kill(process_id, 0)
        return True
    except OSError:
        return False

class AppWidget(QtWidgets.QToolButton):
    def __init__(self,argdict):
        super(AppWidget,self).__init__()
        #self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,QtGui.QSizePolicy.MinimumExpanding)
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.MinimumExpanding)
        self.setAutoRaise(True)
        self.data = argdict
        self.gl = QtWidgets.QGridLayout()
        self.gl.setSpacing(0)
        self.setLayout(self.gl)
        self.path = os.path.abspath(argdict["path"])
        self.procid = 0
        if not os.path.exists(self.path):
            self.setEnabled(False)
        self.interpreter = None
        if "pythonpath" in argdict:
            pp = argdict["pythonpath"]
            if os.path.exists(pp):
                self.interpreter = pp
        self.dir, self.basename = os.path.split(self.path)
        self.args = argdict.get("args","")
        self.title = argdict.get("title","")
        self.sizehintW = 0; self.sizehintH = 0
        if self.title:
            self.l_appname = QtWidgets.QLabel(self.title)
        else:
            self.l_appname = QtWidgets.QLabel(argdict["path"])
        self.l_appname.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding,QtWidgets.QSizePolicy.Minimum)
        self.l_path = QtWidgets.QLabel(argdict["path"])
        ft = self.l_appname.font(); fts = self.l_appname.font()
        #ft = QtGui.QFont(); fts = QtGui.QFont()
        fsz = ft.pointSizeF()
        
        #print("font metrics", QtGui.QFontMetrics(ft).height(),"font size",fsz)        
        ft.setPointSize(int(fsz*1.4))
        fm = QtGui.QFontMetrics(ft)
        self.sizehintH += fm.height()
        #print("font metrics", QtGui.QFontMetrics(ft).height(),"font size",ft.pointSizeF())
        fts.setPointSize(int(fsz*.8))
        self.l_appname.setFont(ft)
        self.l_icon = QtWidgets.QLabel()
        mis = opt.get("gui.miniconsize",32)
        self.l_icon.setMinimumSize(mis,mis)        
        #io = str(argdict.get("dev.address",""))+"\t>>\t"+str(argdict.get("pub.address",""))
        devaddr = argdict.get("dev.address","")
        if type(devaddr)== tuple or type(devaddr)==list and len(devaddr)>0:
            devaddr = devaddr[0]
        io = str(devaddr)
        io_out = str(argdict.get("pub.address",""))
        if io and io_out:
            io = io+ u"→" + io_out
        if "icon" in argdict:
            #print "loading",argdict["icon"],os.path.exists(argdict["icon"])
            #print "getcwd()",os.getcwdu()
            pm = QtGui.QPixmap(argdict["icon"])
        else:
            pm = QtGui.QPixmap(startdir+os.sep+"python.png")
            # if pm.size().width()>mis or pm.size().height()>mis:
        pm = pm.scaled(mis,mis)
        self.l_icon.setPixmap(pm)
        row = 0
        fm = QtGui.QFontMetrics(ft)
        self.sizehintH += fm.height()*1.2
        self.sizehintW = max(self.getRealWidth(self.l_appname), self.sizehintW)
        #print(self.title, self.sizehintH, self.sizehintW)
        self.gl.addWidget(self.l_appname,row,1)
        #print unicode(self.l_appname.text())
        row +=1
        if opt.get("gui.showpath",False) and self.title:
            self.gl.addWidget(self.l_path,row,1)
            self.sizehintH += self.l_path.fontMetrics().height()
            self.sizehintW = max(self.getRealWidth(self.l_path), self.sizehintW)
            row+=1
        if io!="\t>>\t":
            self.l_io = QtWidgets.QLabel(io)
            self.l_io.setFont(fts)
            self.gl.addWidget(self.l_io,row,1)
            fm = QtGui.QFontMetrics(fts)
            self.sizehintH += fm.height()
            #print("font metrics", QtGui.QFontMetrics(fts).height(),"font size",fts.pointSizeF())
            self.sizehintW = max(self.getRealWidth(self.l_io), self.sizehintW)
            row +=1
        descr = argdict.get("descr",u"")
        if not descr:
            descr = u"author added no specific description"+"\n"+self.title + "\n"
            # if not opt["gui.showpath"]:
            #     descr += self.path
            descr += "\n"+io
        descr = descr.strip()
        if not opt["gui.showpath"]:
            descr += "\n"+self.path
        if self.interpreter:
            descr += "\ninterpreter: "+self.interpreter
        self.setToolTip(descr)
        self.gl.addWidget(self.l_icon, 0, 0, row, 1)
        if self.sizehintH<mis:
            self.sizehintH = mis
        self.sizehintH += self.gl.spacing()
        # self.sizehintW *= 1.2
        self.sizehintW += self.l_icon.sizeHint().width()+self.gl.spacing()+30
        self.setMinimumHeight(self.sizehintH)
        self.setMinimumWidth(self.sizehintW)
        #print(self.sizehintH)
    def getRealWidth(self,obj):
        fm = QtGui.QFontMetrics(obj.font())
        return fm.width(obj.text())
    def mousePressEvent(self,evt):
        if evt.button()==1:
            self.launch()
    def launch(self):
        try:
            os.chdir(self.dir)
            #if not is_process_running(self.procid):
            #if not raiseWindow(self.title) or self.procid==0:
            if True:
                if not self.interpreter:
                    self.interpreter = sys.executable
                #self.proc = subprocess.Popen([sys.executable, '-'] + self.basename , stdin=subprocess.PIPE, cwd=self.dir)
                print("launching",self.interpreter,self.path,os.getcwd())
                if sys.platform.startswith('win'):
                    self.procid = os.spawnl(os.P_NOWAIT, self.interpreter, '"'+self.interpreter+'"', '"' + self.path + '"',self.args)
                else:
                    self.procid = os.spawnl(os.P_NOWAIT, self.interpreter, self.interpreter, self.path,self.args)
                print("procid is",self.procid)
            else:
                print("Process is already running, id",self.procid)
        except:
            traceback.print_exc()





win.setCentralWidget(QtWidgets.QWidget())
gl = QtWidgets.QGridLayout()
tbl = QtWidgets.QTableWidget()
gl.addWidget(tbl)
win.centralWidget().setLayout(gl)
w,h = win.geometry().width(),win.geometry().height()
#win = QtGui.QMainWindow()
win.setFixedSize(w,h)
startdir = os.getcwd()
print("current dir is",startdir)

d = None
try:
    with open(confname) as f:
        d = json.load(f,object_pairs_hook=collections.OrderedDict)
except:
    traceback.print_exc()
if not d:
    try:
#        with open("e3apps_1st.json") as f:
        with codecs.open("apps_config1",'r', 'utf-8-sig') as f:
            d1 = json.load(f,object_pairs_hook=collections.OrderedDict)
    except:
        traceback.print_exc()
        print("Could not load any jsons")
        exit()

os.chdir("..")
if not d:
    d = d1
    load_info(d)
try:
    constructGui()
except:
    traceback.print_exc()
win.show()
# for aw in awl:
#     print aw.size(),aw.sizeHint()

app.exec_()
#print(d)
os.chdir(startdir)
with open(confname,"w+") as f:
    json.dump(d,f,indent=4)
opt.savetodisk()