from __future__ import unicode_literals
from qtpy import QtWidgets
from collections import OrderedDict


def AddMenu(menu,dic):
    submenus = None
    if isinstance(dic,(list,tuple)):
        dic = OrderedDict(dic)
    for key,val in dic.items():
        if callable(val):
            a = QtWidgets.QAction(key,menu)
            a.triggered.connect(val)
            menu.addAction(a)
            continue
        if isinstance(val, (list, tuple)):
            val = OrderedDict(val)
        if isinstance(val,dict):
            if submenus==None:
                menus = menu.findChildren(QtWidgets.QMenu)
                #submenus = {unicode(m.title()):m for m in menus}
                submenus = OrderedDict([(str(m.title()),m) for m in menus])
                # submenus = {str(m.title()):m for m in menus}
#            key = unicode(key)
            if key in submenus:
                m = submenus[key] #use existing submenu
            else:
                m = menu.addMenu(key) #create new submenu
            AddMenu(m,val)
    pass

def QMenuFromDict(menu,dic):
    mb = QtWidgets.QMenuBar()
    return mb
