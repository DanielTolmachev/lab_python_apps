# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'options_editor_ui.ui'
#
# Created: Fri Jul 08 18:41:47 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore,QtWidgets

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_options_editor(object):
    def setupUi(self, options_editor):
        options_editor.setObjectName(_fromUtf8("options_editor"))
        options_editor.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(options_editor)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.table = QtWidgets.QTableWidget(self.centralwidget)
        self.table.setObjectName(_fromUtf8("table"))
        self.table.setColumnCount(3)
        self.table.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.table.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.table.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.table.setHorizontalHeaderItem(2, item)
        self.verticalLayout.addWidget(self.table)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.centralwidget)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)
        options_editor.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(options_editor)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        options_editor.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(options_editor)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        options_editor.setStatusBar(self.statusbar)

        self.retranslateUi(options_editor)
        QtCore.QMetaObject.connectSlotsByName(options_editor)

    def retranslateUi(self, options_editor):
        options_editor.setWindowTitle(_translate("options_editor", "Edit settings", None))
        item = self.table.horizontalHeaderItem(0)
        item.setText(_translate("options_editor", "Parameter", None))
        item = self.table.horizontalHeaderItem(1)
        item.setText(_translate("options_editor", "Value", None))
        item = self.table.horizontalHeaderItem(2)
        item.setText(_translate("options_editor", "Default value", None))

