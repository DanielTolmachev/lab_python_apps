import sys,traceback
sys._excepthook = sys.excepthook
def exception_hook(exctype, value, trace_back):
    traceback.print_exc()
    sys._excepthook(exctype, value, trace_back)
sys.excepthook = exception_hook