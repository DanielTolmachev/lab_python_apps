# -*- coding: utf-8 -*-
# This package was adjusted to fit Python3
import DAQmxConfig

from DAQmxTypes import *
from DAQmxConstants import *
from DAQmxFunctions import *
from Task import Task


__version_info__ = (1, 3)
__version__ = '.'.join(str(num) for num in __version_info__)

__author__ =u'Pierre Cladé'
