#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     13.03.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
from sweeper import Sweeper
import zmq_publisher
import time
import threading

class SweeperWMsg(Sweeper,object):
    setfunc = None
    queryfunc = None
    bound = False
    thread = None
    messager = None
    def __init__(self,messager = None,**kwargs):
        #super(zmq_Sweeper,self).__init__(kwargs)
        Sweeper.__init__(self,**kwargs)
        #threading.Thread.__init__()
        self.messager = messager

    def setfunc(self,value):
        Sweeper.setfunc(value)
        t = time.time()
        if self.messager:
            self.messager((t,value))
    def sweep(self,*args,**kwargs):
        #Sweeper.sweep(self,*args,**kwargs)
        self.thread = threading.Thread(target = Sweeper.sweep,
                                args = (self,)+args,
                                kwargs = kwargs)
        self.thread.start()
        #threading.Thread.start()
##    def run(self):
##        Sweeper.sweep(self,*args,**kwargs)

if __name__ == '__main__':
    print ('test various regimes in a thread')
    ADDRESS = 'tcp://127.0.0.1:5000'
    context = zmq_publisher.zmq.Context()
    pub = zmq_publisher.Zmq_Publisher(context,ADDRESS)
    sw = SweeperWMsg(messager = pub.send_pyobj)
    print( 'up once')
    sw.sweep(0,10,1,.01,loop = False)
    sw.thread.join()
    print( 'down once')
    sw.sweep(3,10,-1,.01,loop = False)
    sw.thread.join()
    print ('resume up and down once')
    sw.sweep(0,10,1,.01,loop = False,bidirectional = True,resume = True)
    sw.thread.join()
    print( 'loop')
    sw.sweep(0,10,1,.01,maxloops = 100)
    time.sleep(1)
    sw.stop()
