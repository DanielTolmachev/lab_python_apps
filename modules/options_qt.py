from __future__ import print_function
__author__ = 'SFB'
from options import Options
from qtpy import QtCore
class QtOptions(Options):
    def __init__(self,*args,**kwargs):
        super(QtOptions,self).__init__(*args,**kwargs)
        #super(QtCore.QObject,self).__init__()
        self.signals = {}
    def connect(self,signal,key):
        signal.connect(self.setKeyFromSignal)
        self.signals[signal] = key
    def setKeyFromSignal(self,arg):
        print(arg)


options = QtOptions()

if __name__ == "__main__":
    opt = QtOptions()
    opt["a"] = "b"
    print( opt.get("a"))
