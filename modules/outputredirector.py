from __future__ import print_function
__author__ = 'user'
import sys,traceback
class StreamMirror(object):
    def __init__(self,stream,callback,stderr):
        self.stream = stream
        if hasattr(stream,"encoding"):
            self.encoding = stream.encoding
        self.stderr = stderr
        self.callback = callback
    def write(self,s):
        try:
            if self.stream:
                self.stream.write(s)
            self.callback(s)
        except:
            traceback.print_exception(sys.exc_type, sys.exc_value, sys.exc_traceback,None,self.stderr)
            self.stderr.write(repr(self.stream))
    def flush(self):
        self.stream.flush()


class OutputRedirector(object):
    callback_out = []
    callback_err = []
    def catch(self):
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        sys.stdout = StreamMirror(sys.stdout,self.write_out,self.stderr)
        sys.stderr = StreamMirror(sys.stderr,self.write_err,self.stderr)
    def release(self):
        sys.stdout = self.stdout
        sys.stderr = self.stderr
    def printout(self,s):
        self.stdout.write("Console got:{}".format(repr(s)))
    def write_out(self,string):
        # func = None
        try:
            for func in self.callback_out:
                func(string)
        except:
            traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2],None,self.stderr)
            # self.stderr.write(repr(func))
    def write_err(self,string):
        try:
            for func in self.callback_out:
                func(string)
        except:
            traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1], sys.exc_info()[2],None,self.stderr)
    def addMirror(self,callback):
        self.addOut(callback)
        self.addErr(callback)
    def addOut(self,write_func):
        self.callback_out.append(write_func)
    def addErr(self,write_func):
        self.callback_err.append(write_func)

output = OutputRedirector()
if __name__ == '__main__':
    output.catch()
    print("hello")
    output.addMirror(output.stdout.write)
    print("hello")
    output.release()
    print("hello")

