#coding:utf-8
from __future__ import unicode_literals

from numpy import isscalar,isnan,log10,floor

#si_prefixes = {0:"",1:"k",2:"M",3:"G",-1:"m",-2:"u",-3:"n"}
si_prefixes = {0:"",1:"k",2:"M",3:"G",-1:"m",-2:"μ",-3:"n"}

PRECISION_SWITCH_HISTERESIS = 0.015 # 10**0.015 = 3.5%
PREFIX_SWITCH_HISTERESIS = PRECISION_SWITCH_HISTERESIS/3

_id_dictionary = {}
_precision_dict = {}
def si_fmt(val,units,hashable_id = None):
    """
    find appropriate SI unit prefix
    examples:
        1.4e5,"Hz"  -> 140 "kHz"
        8.1254e-7,"m"  -> 812.54 "nm"
    in case of failure return same pair

    hashable_id - enter id (maybe any hashable: string, number, or gui widget) to
    remember last modes choise to prevent sudden jumps between modes
    to make readout easily readable
    In this example frequency is actually same:
        999.999 kHz
          1.000 MHz
        999.999 kHz
          1.000 MHz
          1.000 MHz
        999.999 kHz

    """
    if val and isscalar(val) and not isnan(val):
        thous = log10(abs(val))/3
        i = int(floor(thous))
        if hashable_id:
            if hashable_id in _id_dictionary:
                last_un = _id_dictionary[hashable_id]
                if i==last_un-1: #modes are stepping one step down
                    # print(i,floor(thous+0.005))
                    i = int(floor(thous+0.005))
        #print val,thous,i
        if i in si_prefixes:
            val = val/10**(i*3)
            units = si_prefixes[i]+units
            if hashable_id:
                _id_dictionary[hashable_id] = i
    elif val==0:
            if hashable_id:
                if hashable_id in _id_dictionary:
                    units = si_prefixes[_id_dictionary[hashable_id]]+units
                else:
                    _id_dictionary[hashable_id] = 0
                #print _id_dictionary[hashable_id]
    return val,units

def si_str(val,units,hashable_id = None,precision = 4):
    val,units = si_fmt(val,units,hashable_id)
    if isscalar(val) and not isnan(val):
        mag = log10(abs(val))
        if mag>0:
            prec = precision-int(floor(mag))
        else:
            prec = precision
        # print(precision,prec,precision-int(floor(mag-0.015)))
        if hashable_id:
            if hashable_id in _precision_dict:
                last_pr = _precision_dict[hashable_id]
                if prec==last_pr-1: #modes are stepping one step down
                    # print(i,floor(thous+0.005))
                    prec = precision-int(floor(mag-0.015))
        if prec<0:
            prec = 0
        if hashable_id:
            _precision_dict[hashable_id] = prec
        return "{: .0{}f} {}".format(val,prec,units)
    return " - {}".format(units)
