# coding:utf8
#-------------------------------------------------------------------------------
# Name:        options
# Purpose:     easily load/save program options(setings/preferrences) to text file
#              Options is a dictionary with ability to load save parameters to text file and
#              remember default values (by using get(key,default) method)
#              Options uses singleton pattern: only one object exist connected to one file,
#              you can have multiple Option object linked to different filenames
#
#              Default config location is <yourscript>.cfg
#
#              syntax of cfg file uses "key = value" pairs:
#                   ValueName = 'string' //comments
#                   float_Value.name = .3
#
#
# Author:      Daniel.Tolmachev@gmail.com
#
# Created:     12-08-2013
# Copyright:   Copyleft
#       Py2/Py3 compatible
#-------------------------------------------------------------------------------
from __future__ import print_function,unicode_literals
import sys,os,traceback,codecs,six
from ast import literal_eval
_options = None


class OptionsSingletonMetaClass(type):
    _instance = None
    _instances = {}
    def __call__(cls, *filename):
        if filename and filename[0]:
            if filename[0] not in cls._instances:
                cls._instances[filename[0]] = super(OptionsSingletonMetaClass, cls).__call__(*filename)
            # else:
            return cls._instances[filename[0]]
        else:
            if cls._instance is None:
                cls._instance = super(OptionsSingletonMetaClass, cls).__call__()
            return cls._instance


class Options(dict,metaclass=OptionsSingletonMetaClass):
    exists = True
    def __init__(self,*filename):
        global _options
        _options = self
        self.modified = {}
        self.defaults = {}
        if filename!=():
            if filename[0]:
                path = filename[0]
            else:
                path = sys.argv[0]
        else:
            path = sys.argv[0]
        if path.endswith('.py'):
            self.filename = path[0:path.rfind('.py')]+'.cfg'
        elif path.endswith('.pyw'):
            self.filename = path[0:path.rfind('.pyw')]+'.cfg'
        else:
            self.filename = path+'.cfg'
        self.readfile()
        self.filename = os.path.abspath(self.filename)
        self.autosave = True

    # def __del__(self):
    #     if self.autosave:
    #         self.savetodisk()
    def readfile(self):
        try:
            f = codecs.open(self.filename,'r',encoding="utf-8")
            #f = open(self.filename,'r')
        except IOError:
            print('cfg file does not exists yet:\n%s' % self.filename)
            self.exists = False
            return
        except:
            print('Unhandled exception, while opening %s' % self.filename)
            print(sys.exc_info())
            return
        for ln in f.readlines():
            #ln = ln.decode("utf-8",errors = "ignore")
            cmnts = ln.rfind('//') #strip comments but beware, double slash can be in string!
            if cmnts > -1:
                ln2 = ln[0:cmnts]
            else:
                ln2 = ln
            try:
                pos = ln2.find('=')
                if pos > 0 and pos<len(ln)-1:
                    key = ln[0:pos].strip()
                    s = ln[pos+1:len(ln)].strip()
                    try:
                        val = literal_eval(s)
                        if six.PY2 and type(val)==str:
                            val = val.decode("utf-8")
##                        print(type(s),s,type(val),val)
##                        if six-type(val) == str:
##                            try:
##                                val.decode()
##                            except:
##                                val = val.decode('cp1251')
##                        elif type(val) == str:
##                            s = s[1:].strip('\'\"')
##                            val = s.decode('utf8',errors = "ignore")
                    except:
                        val = self.set2str(s)
                        if not val:
                            print('wrong line in cfg file:',s)
                            traceback.print_exc()
##                        val= None
##                    if val.startswith('utf8'):
##                        val = eval(val[4:])
##                        if type(val)==str:
##                            val = val.decode('utf8') #decode str to unicode
##                    else:
##                        val = eval(val)
                    if key != '' and val != None:
                        self[key] = val
            except:
                print("Error reading cfg file:", sys.exc_info()[0],",", sys.exc_info()[1])
        f.close()
        print('config was loaded from {} ({})'.format(self.filename,len(self)))
    def savetodisk(self):
        #if len(self)<=0: return
        if not self.modified:
            print("cfg was not modified, nothing to save")
            return
        try:
            #f = open(self.filename,'w')
            f = codecs.open(self.filename,'w',encoding="utf-8")
        except:
            print("Error opening file:",self.filename)
            traceback.print_exc()
            return
        if not f.closed and not self.exists:
            print('cfg file was created:\n%s' % self.filename)
        else:
            print("saving cfg: %s" % self.filename)
        try:
            if six.PY3:
                keys = list(self.keys())
                keys.sort(key=str.lower)
            elif six.PY2:
                keys = [unicode(ky) for ky in self.keys()]
                keys.sort(key=unicode.lower)
            for key in keys:
                val = self[key]
                if six.PY2 and type(val) is unicode:
                    if val!='':
                        val = val.replace('\n','\\n') #str must be written in one line
                        val = val.replace('\r','\\r')
                        #f.write(('%s = \'%s\'\n' % (key,val)).encode("utf-8",errors = "replace"))
                        f.write('%s = \'%s\'\n' % (key,val))
                elif type(val) is str:
                    if val!='':
                        val = val.replace('\n','\\n') #str must be written in one line
                        val = val.replace('\r','\\r')
                        try:
                            s = str(val) #firstly we try to convert to str
                        except:
                            #unicode is saved in utf8 not in some local codepage
                            #to prevent internatinal paths and filenames
                            s = val.encode('utf8')
                        #file.write('%s = utf8\'%s\'\n' % (key,s))
                        f.write('%s = u\'%s\'\n' % (key,s))
                else:
                    #f.write(('%s = %s\n' % (key,val)).encode("utf-8"))
                    if six.PY3:
                        f.write('%s = %s\n' % (key,val))
                    elif six.PY2 and type(val) == set:
    ##                    outl = "{" + ", ".join(map(unicode,val))+"}"
                        outl = unicode(list(val)).decode('unicode_escape')
    ##                    lst = []
    ##                    for it in val:
    ##                        if type(val)==unicode:
    ##                            outl+=" "
                        f.write('%s = {%s}\n' % (key,outl[1:-1]))
                    else:
                        outl = unicode(val).decode('unicode_escape')
                        f.write('%s = %s\n' % (key,outl))
            self.modified = {}
        except:
            traceback.print_exc()
        f.close()
    def get(self,key, * default):
        if default!=():
            self.defaults[key] = default[0]
        if key in self:
            return self[key]
        elif default!=():
            self[key]=default[0]
            return default[0]
        return None
    def __setitem__(self, key, value):
        super(Options,self).__setitem__(key,value)
        self.modified[key] = value
    def set2str(self,s):
        """
        ast.literal_eval can not parse setin Python2
        returns None if s does not represents string
        """
        if s[0]=="{" and s[-1]=="}":
            try:
                lst = literal_eval("["+s[1:-1]+"]")
                if lst:
                    return set(lst)
            except:
                pass
        return None
    def reset(self,key):
        """
        resets value corresponding to key to its default value, if any
        """
        if key in self.defaults:
            self[key] = self.defaults[key]
    def resetAll(self):
        """
         reset all values, which have default values, to their default values
        """
        for k,v in self.defaults.items():
            self[k] = v

def loadOptions(*filename):
    if _options is not None:
        return _options
    else:
        return Options(*filename)


if __name__ == '__main__':
    options = Options()
    for key,val in options.items():
        print(key,type(val),": ",val)
    if "tuple" in options:
        tup = options["tuple"]
        print("tuple:",type(tup),tup[2])
    #print(options.filename)
    print ("\nNow writing")
    options["int"] = 1
    options["float"] = .5
    options["str3"] = options.get("str",None)
    options["str"] = "Привет!"
    options["str2"] = "Hello"
    options["list"] = list(range(0,10))+["Фишка"]
    options["dict"] = {"key1":1,"key2":23.,"keyU":"Чёрт!"}
    options["set2"] = options.get("set",None)
    options["set"] = set(list(range(0,10))+["Письма в ЖЭТФ"])
    options["tuple"] = (1,2,"Hello. Я здесь")
    for key,val in options.items():
        print(key,type(val),": ",val)
    options.savetodisk()
