#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      SFB
#
# Created:     04.12.2015
# Copyright:   (c) SFB 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import socket,traceback,threading,time,six

class SocketA(object):
    connected = False
    keep_connection = False
    autoconnect = False
    address = None
    sock = None
    timeout = 1
    recon_interval = 1
    recon_active = False
    recon_cnt = 0
    def __init__(self, *address, **kwargs):
        """
        address = ("192.168.0.4",1234)
        kwargs:
            timeout = 1
            autoconnect = 0
        """
        self._parseargs(*address, **kwargs)
        self.recon_timer = threading.Timer(self.recon_interval,self.reconnect)
    def _parseargs(self,*address, **kwargs):
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
        if "autoconnect" in kwargs:
            self.autoconnect = kwargs["autoconnect"]
        if address and type(address[0])==tuple:
            self.address = address[0]
            print("address = ",self.address)
    def open(self, *address, **kwargs):
        """
        address = ("192.168.0.4",1234)
        kwargs:
            timeout = 1
        """
        self._parseargs(*address, **kwargs)

        # print("SocketA.open")
        #print(caller_name())

        try:
            print("connecting to {}".format(self.address))
            if self.autoconnect:
                self.keep_connection = True
            self.sock = socket.create_connection(self.address,timeout = self.timeout)
            self.connected = True
            msg = "connected"
            print(msg)
            return msg
        except socket.timeout:
            msg = "timed out ({})".format(self.timeout)
            self._reconnect_if_need()
            return msg
        except:
            traceback.print_exc()
            self._reconnect_if_need()
    def close(self):
        print("SocketA: closing socket")
        if self.sock:
            self.sock.close()
        self.connected = False
        self.keep_connection = False
        self.recon_timer.cancel()
    def send(self,*args,**kwargs):
        rep = self.sock.send(*args,**kwargs)
        return rep
    def recv(self,*args,**kwargs):
        rep = self.sock.recv(*args,**kwargs)
        return rep
    def read(self):
        try:
            if self.connected:
                return self.recv(1024)
            else:
                if self.keep_connection and not self.recon_timer.is_alive():
                    self.recon_timer.start()
                else:
                    return b""
        except Exception as exc:
            if type(exc) == socket.timeout:
                return b""
            self.onException(exc)
    def write(self,data):
        try:
            if self.connected:
                if not six.PY2 and type(data)==str:
                    data = data.encode()
                return self.send(data)
            else:
                self._reconnect_if_need()
        except Exception as exc:
            return self.onException(exc)
    def onException(self,exc):
        if type(exc) == ConnectionResetError:
            self.connected = False
        elif type(exc) == OSError:
            self.connected = False
        traceback.print_exc()
    def _reconnect_if_need(self):
        if self.keep_connection and not self.recon_timer.is_alive():
            print(self.recon_timer,self.recon_timer.is_alive())
            self.reconnect_start()
            print("thread successfully started")
    def reconnect_start(self):
        self.recon_timer.cancel()
        self.recon_timer = threading.Timer(self.recon_interval, self.reconnect)
    def reconnect(self):
        # while not self.connected and self.keep_connection:
            print("trying to reconnect ({})".format(self.recon_cnt), end=' ')
            #print self.connected,self.keep_connection
            res = self.open()
            self.recon_cnt+=1
            if self.connected and self.recon_timer.is_alive():
                self.recon_timer.cancel()
            # print(res)
            # time.sleep(self.recon_interval)
    # def reconnect(self):
    #     if not self.connected and self.keep_connection:
    #         print "trying to reconnect ",
    #         #print self.connected,self.keep_connection
    #         res = self.open()
    #         print res
    #         if not self.connected and self.keep_connection:
    #             print self.recon_timer.is_alive()
    #             self.recon_timer.start()
    #     else:
    #         self.recon_timer.cancel()


if __name__ == '__main__':
    soc = SocketA(("129.217.155.192",1234),timeout = 1)
    soc.open()
