#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      root
#
# Created:     13-05-2015
# Copyright:   (c) root 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
from qtpy import QtCore
import re
from numpy import nan

class Connectify(QtCore.QObject):
##    def lineedit2spinbox(self,le,up_btn,dnw_btn):
    connections = dict()
    senders = dict()
    callbacks = dict()

##def lineedit2spinbox = Lineedit2SpinBox()
    def lineedit2spinbox(self,wdgt,le,up_btn,dn_btn):
        self.connections[str(le.__hash__())] = le
        self.callbacks[str(le.__hash__())] = wdgt
        if le.objectName() in self.senders:
            self.senders[str(le.__hash__())] += (up_btn,dn_btn)
        else:
            self.senders[str(le.__hash__())] = (up_btn,dn_btn)
        up_btn.pressed.connect(self.btn_increase)
        dn_btn.pressed.connect(self.btn_decrease)
    def find_linked_obj(self):
        sndr = self.sender()
        for key,val in self.senders.items():
            if sndr in val:
                return self.connections[key]
    def btn_increase(self):
        le = self.find_linked_obj()
        objname = str(le.__hash__())
        wdgt = self.callbacks[str(le.__hash__())]
        wdgt.increasedecrease(le.objectName(),'increase')
    def btn_decrease(self):
        le = self.find_linked_obj()
        objname = str(le.__hash__())
        wdgt = self.callbacks[str(le.__hash__())]
        wdgt.increasedecrease(le.objectName(),'decrease')
    def increase(self,le):
        v = le.text()
        v = float(v)
        v+= v*0.01
        le.setText(str(v))
    def decrease(self,le):
        v = le.text()
        v = float(v)
        v -= v*.01
        le.setText(str(v))
    def str2float(self,st): #comma wise
##        st = st.split()
##        s = st[0]
##        dot = s.('.')
##        comma = s.find(',')
##        if comma<0:
##            return float(s)
####        while comma>-1:
####            comma = s[comma+1:].find(',')
##        if comma>0 and dot<0:
##            s[comma] = '.'
##            return float(s)
        m = re.match('[0-9.,]+',st)
        if m:
            units = st[m.end():].split()
            if units:
                units = units[0]
            else:
                units = ''
            s = st[m.start():m.end()]
            res = nan
            try:
                res = float(s)
            except:
                dots = s.count('.')
                if dots:
                    dot = s.find('.')
                    if dots>1: #if there is more then 1 dot
                        sd = s[dot:].find('.')
                        s = s[:dot+sd]
                commas = s.count(',')
                if commas>1:
                    comma = [m.start() for m in re.finditer(',', s)]
                    stop = 0
                    for i,c in enumerate(comma[:-1]):
                        if comma[i+1]!=c+4:
                            stop=comma[i+1]
                            break
                        if dots:
                            if dot< comma[i+1]:
                                stop = comma[i+1]
                                break
                    if stop:
                        s = s[:stop]
                    s= re.sub(',','',s)
                    res = float(s)
                if commas>1:
                    stop = s.find(',')

                res = s
                #re.match('\d')
            return res,units
    def detect_order(self,s):
        dot = s.find('.')
##        if dot<0:
##            zeros = s.rfind('0')
##            while zeros>0
##            zeros = s.rfind('0')
        if dot>-1:
            return -(len(s)-1-s.find('.'))
        else:
            return 0
connectifier = Connectify()

if __name__ == '__main__':
    print('comma wise str2float')
    s = ['1.52,abra-cadabra',
    '1.38eV abra-cadabra',
    '1,524,241.526',
    '5,54',
    '.54',
    '1,524,247.54,457']
    for l in s:
        f,u = connectifier.str2float(l)
        o = connectifier.detect_order(f)
        print(l,' ',' ',f,u,' ',o)
