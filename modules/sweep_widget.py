﻿# coding:utf-8
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     25.02.2014
# Copyright:   (c) Daniel 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function,unicode_literals

import re
import six
import sys
import traceback

from qtpy import QtCore,QtGui,QtWidgets
from numpy import log10,floor,ceil,round,abs

from atof_comma_safe import atof
from detect_significant_order import detect_significant_order_from_string
from qt_connectify import connectifier
from sweep_widget_form import Ui_SweepWidget
import SweeperQT

TIME_ADJUSTS_STEP = True
##UDP_IP = "127.0.0.1"
##UDP_PORT = 6000
##UDP_tr_port = 5000
##print "UDP target IP:", UDP_IP
##print "UDP target port:", UDP_PORT

##sock = socket.socket(socket.AF_INET, # Internet
##                     socket.SOCK_DGRAM) # UDP
##
##import zmq
##
##context = zmq.Context()
##sock = context.socket(zmq.PUB)
###sock.bind("tcp://127.0.0.1:5000")


##bound = 0
##while not bound:
##    try:
##        sock.bind('tcp://127.0.0.1:%d'% UDP_tr_port)
##        bound = UDP_tr_port
##        print 'bound to %d' % UDP_tr_port
##    except:
##        print sys.exc_info()
##        UDP_tr_port+=1
##        if UDP_tr_port>65535: break;
##    except:
##        print sys.exc_info()

##def udp_port():
##    if bound:
##        return UDP_tr_port
##    else: return 0
btn_repeat_str = 'loop'
btn_no_repeat_str ='run once'
btn_one_way_str = u'one way↑'
btn_two_way_str = u'two way↕'
SCAN_UP = u'→' #u'↔'
SCAN_DWN = u'←'
DEFAULT_AUTOSAVE_TIME = 10000 #ms
#['up','down','two-way','resume','loop']
sweep_modes = ['up',
 'down',
 'up from current point',
 'down from current point']

##for d in list(sweep_modes):
##    print d
##    sweep_modes.append(d+' from current point')

class SweepWidget(QtWidgets.QWidget,Ui_SweepWidget):
##    left = opt.get('range.left',0.)
##    right = opt.get('range.right',1.)
##    time = opt.get('time',60.)
##    speed = opt.get('speed',(opt['range.right']-opt['range.left'])/opt['time'])
    extcmd_toggled = QtCore.Signal(bool)
    sig_params_changed = QtCore.Signal()
    sig_speed_set = QtCore.Signal(float)
    sig_value_set = QtCore.Signal(float)
    sig_started = QtCore.Signal(object)
    sig_start_sweeper = QtCore.Signal(float,float,float,float,bool,bool,bool)
    sig_stop_sweeper = QtCore.Signal()
    sig_range_changed = QtCore.Signal(float,float)
    BTN_START_LABELS = {'start': 'Start |>',
                         'stop' :'Stop',
                         'finish':'Finish',
                         'start reverse':'<| Start',
                         'start continue':'Start >',
                         'resume':'Start >',
                         'start reverse continue': '< Start',
                         'resume reverse': '< Start'
                        }
    COMMAND_LIST = ['start',
                     'stop' ,
                     'finish',
                     'start reverse',
                     'resume',
                     'resume reverse']
    TIME_ADJUSTS_STEP = TIME_ADJUSTS_STEP
    def __init__(self,left = 0.,right = 1.,step = 0.01, dt =0.1,
            optionsdict = None,
            sweepValueDesc = 'Sweep',
            units = '',
            units_multiplier = 1,
            units_used_dict = {},
            step_as_speed = False,
            units_speed ='',
            units_speed_multiplier = 1,
            auto_switch_direction = False,
            precision = 5,
            time_resolution = 0.01,
            use_internal_sweeper = False):
        #defaults
        self.command = 'start'
        self.command_start = 'start'
        self.command_stop = 'stop'
        self.units_dict = dict()
        self.precision = precision
        self.time_resolution = time_resolution
        self.time_precision = int(-floor(log10(time_resolution)))
        self.extValueChecker = dict()
        self.acceptableRanges = dict()
        #initialization
        self.step_as_speed = step_as_speed
        self.units_speed = units_speed
        self.units_speed_multiplier = units_speed_multiplier
        self.auto_switch_direction = auto_switch_direction
        super(SweepWidget,self).__init__()
        self.setupUi(self)
        self.current_value = None
        if use_internal_sweeper:
            self.setupInternalSweeper()
        self.value_changed_Timer = QtCore.QTimer()
        self.value_changed_Timer.setSingleShot(True)
        # self.value_changed_Timer.timeout.connect(self._value_changed)
        self.value_changed_Timer.timeout.connect(self.sig_params_changed.emit)
        self.sweepValueDesc = sweepValueDesc
        self.setCurrentUnit(units,units_multiplier)
        self.setUnits(units_used_dict)
        self.left = left
        self.right = right
        self.step = step
        self.dt = dt
        self.speed = step/dt
        self.bidirectional_scan = self.loop = self.resume = self.reverse = False


        self.list_le = [self.le_left,self.le_right,
            self.le_dt,self.le_step,self.le_time]
        self.connect_options_dictionary(optionsdict)
        #self.sb_time.setVisible(False)
        self.le_left.textEdited.connect(self.value_changed)
        if step_as_speed:
            self.label.setText('speed')
        self.le_right.textEdited.connect(self.value_changed)
        self.le_time.textEdited.connect(self.value_changed_time)
        self.le_step.textEdited.connect(self.value_changed)
        self.le_dt.textEdited.connect(self.value_changed)
        #self.pb_extcmd.toggled.connect(self.enab_ext_start)
        self.le_central.textEdited.connect(self.central_Value_Edited)
        self.le_central.returnPressed.connect(self.onValueSet)
        self.tb_gotoLeft.pressed.connect(self.gotoBegin)
        self.tb_gotoRight.pressed.connect(self.gotoEnd)
        self.tb_repeat.toggled.connect(self.repeat_toggled)
        self.tb_scan_direction.toggled.connect(self.setBidirectional)
        self.tb_direction.pressed.connect(self.toggleScanDirection)
        self.menu_start = QtWidgets.QMenu(self.btn_start)
        for s in sweep_modes:
            q = QtWidgets.QAction(s,self.btn_start)
            q.triggered.connect(self.setSweepModeAndRun)
            self.menu_start.addAction(q)
        self.btn_start.setMenu(self.menu_start)
        self.menu_stop = QtWidgets.QMenu(self.btn_start)
        for s in ['Stop','Finish']:
            q = QtWidgets.QAction(s,self.btn_start)
            q.triggered.connect(self.setSweepModeAndRun)
            self.menu_stop.addAction(q)
        connectifier.lineedit2spinbox(self,self.le_left,self.tb_left_up,self.tb_left_dn)
        connectifier.lineedit2spinbox(self,self.le_right,self.tb_right_up,self.tb_right_dn)
        connectifier.lineedit2spinbox(self,self.le_step,self.tb_step_up,self.tb_step_dn)
        connectifier.lineedit2spinbox(self,self.le_dt,self.tb_dt_up,self.tb_dt_dn)
        connectifier.lineedit2spinbox(self,self.le_time,self.tb_time_total_up,self.tb_time_total_dn)

        self.spinboxpairs = { self.le_left: (self.tb_left_up,self.tb_left_dn),
            self.le_right:(self.tb_right_up,self.tb_right_dn),
            self.le_step:(self.tb_step_up,self.tb_step_dn),
            self.le_dt:(self.tb_dt_up,self.tb_dt_dn),
            self.le_time:(self.tb_time_total_up,self.tb_time_total_dn)}

        self.le_left.installEventFilter(self)
        self.le_right.installEventFilter(self)
        self.le_step.installEventFilter(self)
        self.le_dt.installEventFilter(self)
        self.le_time.installEventFilter(self)


        self.tb_units.pressed.connect(self.units_cycle)
        #self.grabKeyboard()
        self.setFocus()
        #self._set_cursors()

        #self.btn_start.pressed(self.)
##        self.btn_start.pressed.connect(self.startstop)
##    def startstop(self):
##        if 'Start' in self.btn_start.text():
##            self.btn_start.setText('Stop')
    def setCurrentUnit(self,units,mult):
        self.units = units
        self.units_multiplier = mult
        self.tb_units.setText(self.units)
        if not self.step_as_speed:
            self.label_step.setText("{}/s".format(self.units))
    def setUnits(self,units):
        self.units_dict = units
        if self.units_dict:
            if self.units not in self.units_dict:
                print('unit {} is not in list of allowed measurment units:\n'.\
                format(self.units,str(self.units_dict)))
    def setUnitsSpeed(self,units):
        self.units_speed_dict = units
        if self.units_speed_dict:
            if self.units_speed not in self.units_speed_dict:
                print('unit {} is not in list of allowed measurment units:\n'.\
                format(self.units_speed, str(self.units_speed_dict)))
    def units_cycle(self):
        if self.units_dict:
            central_val = self.units2std(self.le_central.text()) #central value should also be updated
            lst = list(self.units_dict.keys())
            if self.units in lst:
                i = lst.index(self.units)
                i = i+1
                if i>=len(lst):
                    i=0
            else:
                i = 0
            self.units = lst[i]
            self.units_multiplier = self.units_dict[lst[i]]
            print('Measurment units was changed to {}\
            and multiplier is set to {}'.format(self.units,self.units_multiplier))
            self.tb_units.setText(self.units)
            if not self.step_as_speed:
                self.label_step.setText("{}/s".format(self.units))
            self.update_gui_values()
            self.set_central_label(central_val) #we are setting the same value as before, but using different measurement modes
            self._set_cursors()
            if self.options:
                self.options['current_units'] = self.units
    def _loadValfromdict(self,list_of_params):
        for p in list_of_params:
            key = self.sweepValueDesc+'.'+p
            if key in self.options and hasattr(self,p):
                    self.__dict__[p] = self.options[key]

    def _loadfromdict(self):
        if self.options:
            self._loadValfromdict(['left','right','step','dt',
                                'command_start','loop','bidirectional_scan'])
            if self.step==0:
                self.step = 1
    def connect_options_dictionary(self,opt_dict):
        self.options = opt_dict
        self._loadfromdict()
        self.update_gui_values()
    def units2disp(self,val,precision = None):
        if type(self.units_multiplier)==tuple:
            ret = self.units_multiplier[0](val)
        else:
            ret = val/self.units_multiplier
        if precision!=None:
            return '{:.{precision}f}'.format(ret,precision = precision)
        else:
            return '{:.{precision}f}'.format(ret, precision=self.precision)
        # return str(ret)
    def units2disp_time(self,val,precision = None): #controols that time and dt have same precision
        if precision!=None:
            return '{:.{precision}f}'.format(val,precision = precision)
        else:
            dts = str(self.le_dt.text())
            pos = dts.find(".")
            if pos==-1:
                precision = 0
            elif pos>-1:
                pos2 = len(dts)-1
                while dts[pos2]=="0":
                    pos2 -= 1
                precision = pos2-pos
            return '{:.{precision}f}'.format(val, precision=precision)
    def units2disp_speed(self,val):
        if self.step_as_speed:
            return str(val/self.units_speed_multiplier)
        else:
            return self.units2disp(val)
    def units2std(self,val):
        if hasattr(self.units_multiplier,"__iter__"):
            ret = self.units_multiplier[1](self.str2f(val))
        else:
            ret = atof(val)*self.units_multiplier
        return ret
    def units2std_speed(self,val):
        if self.step_as_speed:
            return atof(val)*self.units_speed_multiplier
        else:
            return self.units2std(val)
    def f2str(self,f):
        return str(f)
    def _round_total_time(self): #deprecated
        lg = log10(self.time/self.dt)
        if self.dt<1 and lg<2:
            ndigits = int(ceil(abs(log10(self.dt))))
            t = round(self.time,ndigits)
        else:
            t = round(self.time)
        return t
    def update_gui_values(self):
        #self.le_right.setDisabled(True)
        for c in self.list_le:
            c.blockSignals(True)
        self.le_left.setText(self.units2disp(self.left))
##        self.le_left.setCursorPosition(0)
        self.le_right.setText(self.units2disp(self.right))
##        self.le_right.setCursorPosition(0)
        self.le_step.setText(self.units2disp_speed(self.step))
##        self.le_step.setCursorPosition(0)
        dt = self.dt
        self.le_dt.setText(self.f2str(self.dt))
##        self.le_dt.setCursorPosition(0)
        self.time = self.calc_time()
        # self.le_time.setText(self.f2str(self._round_total_time()))
        self.le_time.setText(self.units2disp_time(self.time))
##        self.le_time.setCursorPosition(0)
        #self.le_right.setEnabled(True)
        self.tb_repeat.setChecked(self.loop)
        self.tb_scan_direction.setChecked(self.bidirectional_scan)
        for c in self.list_le:
            c.blockSignals(False)
    def central_Value_Edited(self):
        f = self.le_central.font()
        f.setItalic(True)
        self.le_central.setFont(f)
    def onValueSet(self):
        val = self.units2std(self.le_central.text())
        self.sig_value_set.emit(val)
        # Font should be set to italic till receiving of confirmation from device
        # f = self.le_central.font()
        # f.setItalic(False)
        # self.le_central.setFont(f)
        self.central_Value_Edited()
    def gotoBegin(self):
        self.sig_value_set.emit(self.left)
    def gotoEnd(self):
        self.sig_value_set.emit(self.right)
    def str2f(self,txt):
        s = str(txt)
        try:
            f = float(s)
        except ValueError:
            #traceback.print_exc()
            s = re.sub(",",".",s)
            print(s)
            f = float(s)
        return f
    def calc_time(self):
        if self.step==0:
            return 0
        else:
            if self.step_as_speed:
                return round(abs((self.right-self.left)/self.step),1)
            else:
                return abs((self.right-self.left)/self.step*self.dt)
    def _value_changed(self):
        if self.options!=None:
            self.options[self.sweepValueDesc+'.left'] = self.left
            self.options[self.sweepValueDesc+'.right'] = self.right
            self.options[self.sweepValueDesc+'.step'] = self.step
            self.options[self.sweepValueDesc+'.dt'] = self.dt
            self.options[self.sweepValueDesc+'.command_start'] = self.command_start
            self.options[self.sweepValueDesc+'.modes'] = self.units
        self.value_changed_Timer.start(DEFAULT_AUTOSAVE_TIME) #we do not emit signal immideately, but we start timer
        #self.sig_params_changed.emit()
    def value_changed(self,val):
        self.left = self.units2std(self.le_left.text())
        self.right = self.units2std(self.le_right.text())
        self.step = self.units2std_speed(self.le_step.text())
        self.dt = self.str2f(self.le_dt.text())
        self.time = self.calc_time()
        self.le_time.setText(self.f2str(self.time))
        if self._checkValues():
            self.update_gui_values()
        self._value_changed() #update dictionary
        #self.value_changed_Timer.start(DEFAULT_AUTOSAVE_TIME) #
        for le in self.list_le:
            if le!=self.sender():
                le.setCursorPosition(0)
        self._announce_range()
    def _announce_range(self):
        #if self.command!= self.command_stop:
         #   print "sweep_widget range is {}:{}".format(self.left,self.right)
            self.sig_range_changed.emit(self.left,self.right)
    def _set_cursors(self,*sndr):
        if sndr!=():
            sndr = sndr[0]
        for le in self.list_le:
            if le!=sndr:
                le.setCursorPosition(0)
    def _checkValues(self):
        changes = False
        if self.extValueChecker:
            try:

                for key in self.extValueChecker.iterkeys():
                    oldval = self.__getattribute__(key)
                    newval = self.extValueChecker[key]( oldval)
                    if oldval!=newval:
                        self.__setattr__(key, newval)
                        changes = True
            except:
                traceback.print_exc()
        if self.acceptableRanges:
            try:

                for key in self.acceptableRanges:
                    val = self.__getattribute__(key)
                    min,max = self.acceptableRanges[key]
                    if val>max:
                        self.__setattr__(key, max)
                        changes = True
                    elif val<min:
                        self.__setattr__(key, min)
                        changes = True
            except:
                traceback.print_exc()
        return changes
    def setExtValueChecker(self,key,callback):
        if hasattr(self,key):
            self.extValueChecker[key] = callback
    def setRange(self,key,min,max):
        if hasattr(self,key):
            self.acceptableRanges[key] = (min,max)
    def setWorkingRange(self,left,right):
        self.left = left
        self.right = right
        self._checkValues()
        self.update_gui_values()
        self._set_cursors()
    def setWorkingRange_announce(self,left,right):
        self.setWorkingRange(left,right)
        self._announce_range()
    def value_changed_time(self,val):
        self.time = self.str2f(self.le_time.text())
        if self.time:
            if self.step_as_speed:
                self.step = (self.right-self.left)/self.time/60
            else:
                self.step = (self.right-self.left)/self.time*self.dt
            self.le_step.setText(self.units2disp(self.step))
            self.le_step.setCursorPosition(0)
            self.value_changed_Timer.start(DEFAULT_AUTOSAVE_TIME)#self._value_changed()
    def on_btn_start_pressed(self):
        if 'start' in self.command: # or 'resume' in self.command:
            self.setFocus()
            print(self.command)
            self.sig_started.emit(self.command)
            self.sig_start_sweeper.emit(*self.cmd2sweeper())
            self.setGuiStarted()

        elif self.command == 'stop' or self.command=='finish':
            print(self.command)
            self.sig_started.emit(self.command)
            self.sig_stop_sweeper.emit()
            self.setGuiStopped()
    def stop(self):
        self.command = "stop"
        print(self.command)
        self.sig_started.emit(self.command)
        self.sig_stop_sweeper.emit()
        self.setGuiStopped()
    def setGuiStarted(self):
        if not self.loop:
                self.command_stop = 'stop' # 'finish' is only available when looping
                self.btn_start.setMenu(None)
        else:
            self.btn_start.setMenu(self.menu_stop)
        self.command = self.command_stop
        self.btn_start.setText(self.BTN_START_LABELS[self.command_stop])
    def setGuiStopped(self):
        if self.auto_switch_direction:
            self.current_value = self.units2std(self.le_central.text())
            if self.current_value == self.right:
                print('We have reached a right side of range')
                self.reverse = True
                self.prepStartCommand()
            elif self.current_value == self.left:
                print('We have reached a left side of range')
                self.reverse = False
                self.prepStartCommand()
            else:
                print('Stopped at {}'.format(self.current_value))
        self.command = self.command_start
        self.btn_start.setText(self.BTN_START_LABELS[self.command_start])
        self.btn_start.setMenu(self.menu_start)
        self._announce_range() #announce range in case it was changed while sweeping
    def prepStartCommand(self):
        self.command_start = 'start'
        if self.reverse:
            self.command_start+=' reverse'
    def getCurrentValue(self):
        if not self.current_value:
            self.current_value = self.units2std(self.le_central.text())
        return self.current_value
    def set_central_label(self,val):
            self.current_value = val
        #if not self.le_central.isModified():
            f = self.le_central.font()
            f.setItalic(False)
            self.le_central.setFont(f)
            if type(val)!=float:
                try:
                    val = float(val)
                except:
                    self.le_central.setText('')
                    return
            self.le_central.setText(self.units2disp(val,precision=self.precision))
            self.le_central.setCursorPosition(0)

    def keyPressEvent(self, QKeyEvent):
        key = QKeyEvent.key()
        mod = QKeyEvent.modifiers()
        #mod = QtGui.QKeyEvent.modifiers()
        #mod.
        print(mod)
        if key == 16777236:
            print("->", end=' ')
            self._shift_manual(+1)
        elif key==  16777234:
            print("<-", end=' ')
            self._shift_manual(-1)
        # elif key == 16777235: #up
        #     print "->",
        #     self._shift_manual(+1)
        # elif key==  16777237: #down
        #     print "<-",
        #     self._shift_manual(-1)
        elif key == 16777220 and mod==QtCore.Qt.ControlModifier: #Ctrl+Enter key
            if 'start' in self.command: # or 'resume' in self.command:
                self.on_btn_start_pressed()
        elif key == 16777216: #Escape key
            self.setFocus()
            self.stop()
        else:
            global evt
            evt = QKeyEvent
            print(self.sender(),key)
    def eventFilter(self,target,event):
        if type(target)== QtWidgets.QLineEdit:
            if target in self.spinboxpairs:
                if type(event) == QtGui.QKeyEvent:
                    if event.key()== 16777235: #up
                        self.spinboxpairs[target][0].animateClick()
                        return True
                    elif event.key()== 16777237: #down
                        self.spinboxpairs[target][1].animateClick()
                        return True
                    elif event.key()== 16777216: #Escape key
                        self.setFocus()
                        return True
                elif type(event) == QtGui.QWheelEvent:
                    delta = getattr(event,"angleDelta",None)
                    if delta:
                        delta = delta().y()
                    else:
                        delta = event.delta()
                    print(delta)
                    if delta>0:
                        #for i in range(0,event.delta()):
                        self.spinboxpairs[target][0].blockSignals(True)
                        self.spinboxpairs[target][0].animateClick()
                        self.spinboxpairs[target][0].blockSignals(False)
                        self.increasedecrease(target,"increase")
                        # print time.time()-t
                        return True
                    else:
                        # for i in range(0,abs(event.delta())):
                        self.spinboxpairs[target][1].blockSignals(True)
                        self.spinboxpairs[target][1].animateClick()
                        self.spinboxpairs[target][1].blockSignals(False)
                        self.increasedecrease(target, "decrease")
                        return True
        return False
    def _shift_manual(self,direction):
        if self.step_as_speed:
            step = self.step*self.dt
        else:
            step = self.step
        if direction<0:
            step = -step
        oldval = self.units2std(self.le_central.text())
        newval = oldval +step
        print(oldval, step, newval)
        self.sig_value_set.emit(newval)
    def enab_ext_start(self,enable):
        if self.pb_extcmd.isChecked()!=enable:
                self.pb_extcmd.toggle()
        if enable:
            self.pb_extcmd.setText('Wait for \n command')
        else:
            self.pb_extcmd.setText('No external\nlaunch')
##        print enable
        self.extcmd_toggled.emit(enable)
    def increasedecrease(self,objname,direction):
##        print objname,direction
        if type(objname)== QtWidgets.QLineEdit:
            le = objname
        else:
            le = self.findChild(QtWidgets.QLineEdit,objname)
        # if self.TIME_ADJUSTS_STEP and le==self.le_time:
        #     le = self.le_step
        s = six.u(le.text()) #  unicode(le.text())
        stats = detect_significant_order_from_string(s)
        prec = stats['precision']
        zeros = stats['zeros']
        v = self.str2f(s)
        if prec==1:
            exp = floor(log10(v))-self.precision
            if exp>prec:
                order = 10 ** exp
                v = round(v/order)*order
                delta = order
            else:
                delta = 10 ** (-prec)
        else:
            delta = 10 ** (-prec)

        if le in [self.le_left,self.le_right]:  #choose delta depending on range
            if type(self.units_multiplier)== float:
                ran = (self.right-self.left)/self.units_multiplier
            else:
                ran = (self.right-self.left)
            if zeros>0:
                if delta<ran/100:
                    delta = delta*10
                elif delta>ran/10:
                    delta = delta/10


        if direction=='increase':
            v+=delta
        elif direction=='decrease':
            v-=delta
        if v<=0:
            if le in [self.le_step,self.le_dt,self.le_time]:
                v+=delta
                delta = delta*0.1
                prec += 1
                if le == self.le_step and prec>=self.precision:
                    prec = self.precision
                elif le!= self.le_step and prec>=self.time_precision:
                    prec = self.time_precision
                v-=delta
            else:
                v=0
        vr = round(v,prec)
        if vr == 0:
            prec+=1
            vr = round(v,prec)
        print(s, stats,delta,v,"->",vr)
        le.setText(self.f2str(vr))
        le.textEdited.emit(le.text())
    def repeat_toggled(self,state):
        self.loop = state
        if self.loop:
            self.tb_repeat.setText(btn_repeat_str)
        else:
            self.tb_repeat.setText(btn_no_repeat_str)
        if self.options:
            self.options[self.sweepValueDesc+'.loop'] = state
    def setBidirectional(self,state):
        self.bidirectional_scan = state
        if self.bidirectional_scan:
            self.tb_scan_direction.setText(btn_two_way_str)
        else:
            self.tb_scan_direction.setText(btn_one_way_str)
        if self.options:
            self.options[self.sweepValueDesc+'.bidirectional_scan'] = state
    def toggleScanDirection(self):
        self.reverse = not self.reverse
        self.setScanDirection()
    def setScanDirection(self):
        if self.reverse:
            txt = SCAN_DWN
            if self.bidirectional_scan:
                txt += "\n"+SCAN_UP
            self.tb_direction.setText(txt)

        else:
            txt = SCAN_UP
            if self.bidirectional_scan:
                txt += "\n"+SCAN_DWN
            self.tb_direction.setText(txt)
        self.btn_start.setText(self.BTN_START_LABELS[self.prepCommandStart()])
    def prepCommandStart(self):
        command = "start"
        if self.reverse:
            command += ' reverse'
        if self.resume:
            command += ' continue'
        return command
    def setSweepModeAndRun(self):
        #if type(self.sender())==QtWidgets.QAction:
        t = six.u(self.sender().text()) #unicode(self.sender().text())
        print(t,self.command)
        if 'start' in self.command:# or 'resume' in self.command:
            self.command = 'start'
            if 'down' in t:
                self.reverse = True
                #if not 'reverse' in self.command:
                self.command += ' reverse'
            else:
                self.reverse = False
            if 'from current point' in t:
                self.resume = True
                #if not 'continue' in self.command:
                self.command += ' continue'
            else:
                self.resume = False
                #self.command = 'start'
            self.command_start = self.command
        elif 'Finish' in t:
            self.command = self.command_stop = 'finish'
        self.on_btn_start_pressed()
    def cmd2sweeper(self):
        if 'reverse' in self.command:
            step = -self.step
        else:
            step = self.step
        return  self.left, self.right, step, self.dt, \
                self.resume, self.loop,  self.bidirectional_scan
    def setSpeed(self,speed):
        if self.step_as_speed:
            self.step = speed
        else:
            self.step = speed*self.dt
        self.update_gui_values()
    def setupInternalSweeper(self):
        self.internalSweeper = SweeperQT.SweeperQT()
        self.internalSweeper_thread = QtCore.QThread()
        self.internalSweeper.moveToThread(self.internalSweeper_thread)
        self.internalSweeper_thread.start()
        self.internalSweeper.getCurrentValue = self.getCurrentValue #needed if we want to resume from current point
        self.sig_start_sweeper.connect(self.internalSweeper.sweep)
        self.sig_stop_sweeper.connect(self.internalSweeper.stop_sweep)
        self.internalSweeper.sigSweeperStopped.connect(self.setGuiStopped)
        self.internalSweeper.sigSweeperStopped.connect(self.sig_stop_sweeper.emit)
        # self.internalSweeper.sigSweeperStopped.connect(self.onInternalSweeperStop)
        self.internalSweeper.sigNewValue.connect(self.set_central_label)
        self.internalSweeper.sigNewValue.connect(self.sig_value_set)
    def onInternalSweeperStop(self):
        #(re) emit sig_stop_sweeper if it was not emitted first time (sweeper stopped by itself)
        self.sig_stop_sweeper.disconnect(self.internalSweeper.stop_sweep)
        self.sig_stop_sweeper.emit()



class MainWndWSweep(QtWidgets.QMainWindow):
    """
        Simple window with SweepWidget
        all parameters go to SweepWidget
    """
    def __init__(self,*args,**kwargs):
        super(MainWndWSweep,self).__init__()
        self.gridlayout = QtWidgets.QGridLayout()
        self.sweeper = SweepWidget(*args,**kwargs)
        self.gridlayout.addWidget(self.sweeper,0,0)
        cw = QtWidgets.QWidget()
        #self.setGeometry(cw.geometry())
        self.setCentralWidget(cw)
        cw.setLayout(self.gridlayout)


if __name__ == '__main__':

    for c in SweepWidget.COMMAND_LIST:
        if c not in SweepWidget.BTN_START_LABELS:
            print('Warning: There is no label in BTN_START_LABELS \
            corresponding to command {}'.format(c))
    app = QtWidgets.QApplication(sys.argv)
##    w = QtWidgets.QMainWindow()
##    mw = SweepWidget() #create main widget
##    mw.show()
##    w.show()
##    w.setCentralWidget(mw) #set that widget as main
    import sweeper_msg
    sw = sweeper_msg.SweeperWMsg()
    swi = SweepWidget(use_internal_sweeper=True)
    swi.setWindowTitle("sweep widget with internal sweep")
    w2 = MainWndWSweep()
    w2.setWindowTitle("w2")
    w2.sweeper.sig_start_sweeper.connect(sw.sweep)
    w2.sweeper.sig_stop_sweeper.connect(sw.stop)
    w2.show()
    w2.sweeper.setFocus()
    swi.show()
    sys.exit(app.exec_())