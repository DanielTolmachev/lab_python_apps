import sys,logging,os
_excepthook = sys.excepthook
path = sys.argv[0]
if path.endswith('.py') or path.endswith('.pyw'):
    filename = path[0:path.rfind('.py')]
    filename = os.path.split(filename)[1]
    filename = "../log/"+filename+".log"
if filename is None or not os.path.exists("../log"):
    filename = 'exceptions.log'
logging.basicConfig(filename=filename,format='%(asctime)s %(levelname)-8s %(message)s', filemode="w+", level=logging.DEBUG,datefmt='%Y-%m-%d %H:%M:%S')
# logging.basicConfig(filename=filename,filemode="w+", level=logging.DEBUG)

def exception_hook(exctype, value, trace_back):
    logging.exception("Exception",exc_info=(exctype, value, trace_back),stack_info = False)
    _excepthook(exctype, value, trace_back)


sys.excepthook = exception_hook