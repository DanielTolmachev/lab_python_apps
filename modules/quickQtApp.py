from __future__ import print_function
from qtpy import QtGui,QtWidgets
import sys
from options import Options
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
from console_qt import QConsole
from options_editor import Options_Editor
import QMenuFromDict

#app = opt = cons = None
def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk

def mkAppWin(title = "quick PyQt Application",**kwargs):
    """
    app,opt,win,cons = mkAppWin()

        opt = Options()  Dictionary load/saved to cfg file
        app = QtWidgets.QApplication(sys.argv) QtGui application
        cons = QConsole() GUI window that catches console output
        win = QMainWindowAutoSaveGeometry()  window that autosaves position in options dict (see above)

        keyword arguments can be:
            device_command_line = True  create device console win.device_command_line
            QuitOnClose = True          exit application (close all windows) after closing window
            icon = None                 application icon
    """
    global opt
    opt = Options()
    app = QtWidgets.QApplication(sys.argv)
    cons = QConsole()
    win = QMainWindowAutoSaveGeometry(QuitOnClose = kwargs.get("QuitOnClose",True))
    win.setWindowTitle(title)
    win.opted = Options_Editor(opt)
    if 'Window.geometry' in opt:
        win.loadGeometry(opt['Window.geometry'])
    win.GeometryChangedSig.connect(save_win_position)
    if cons:
        QMenuFromDict.AddMenu(win.menuBar(),
                      {
                          "tools":{
                              "console":cons.show,
                              "settings":win.opted.show
                                }
                      })
    if "device_command_line" in kwargs:
        import device_console
        win.device_command_line = device_console.Device_Console()
        QMenuFromDict.AddMenu(win.menuBar(),{"tools":{
                              "device_command_line":win.device_command_line.show
                                }})
    # set application icon
    iconfile = kwargs.get("icon")
    if iconfile:
        icon = QtGui.QIcon(iconfile)
        app.setWindowIcon(icon)
    return app,opt,win,cons
