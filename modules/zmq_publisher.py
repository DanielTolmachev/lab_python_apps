#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     16.03.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
import zmq
import traceback

class Zmq_Publisher(object):
    bound = False
    def __init__(self,context=None,address=None):
        if not context:
            context = zmq.Context()
        # if not context or not address:
        #     return
        self.pub = context.socket(zmq.PUB)
        if address:
            self.address = address
            self.bind()
    def bind(self):
        try:
                self.pub.bind(self.address)
                self.bound = True
                print('bound to {}'.format(self.address))
        except:
                print('Error while binding to {}'.format(self.address))
                traceback.print_exc()
    def send_pyobj(self,obj):
        if self.bound:
            self.pub.send_pyobj(obj)


