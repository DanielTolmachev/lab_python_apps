from numpy import argmax,nan,log,where,diff,sign,mean,linspace
from numpy.fft import rfft
from parabolic import parabolic
import traceback
try:
    from scipy.signal import blackmanharris
except:
    print("cannot import from scipy.signal")
    traceback.print_exc()
    def blackmanharris(*args):
        raise NotImplementedError


def freq_from_fft(sig, dt):
    """Estimate frequency from peak of FFT

    """
    # Compute Fourier transform of windowed signal
    windowed = sig * blackmanharris(len(sig))
    f = rfft(windowed)

    #plot spectrum to global var for later analysis
##    global ff
##    ff = np.array([abs(f) ,
##    [1.*i / len(windowed)/dt for i in xrange(0,len(f))]])

    # Find the peak and interpolate to get a more accurate peak
    i = argmax(abs(f)) # Just use this for less-accurate, naive version
    if i==len(f)-1:
        #print "cannot find freq"
        return (nan,nan)
    true_i = parabolic(log(abs(f)), i)[0]

    # Convert to equivalent frequency
    freq = 1.*true_i / len(windowed)/dt
    freqerr = 1./len(windowed)/dt
    return (freq, freqerr)

def freq_from_crossings(sig, fs):
    """
    Estimate frequency by counting zero crossings
    """
    # Find all indices right before a rising-edge zero crossing
    #indices = np.find((sig[1:] >= 0) & (sig[:-1] < 0))
    indices = where(diff(sign(sig)))[0]

    # Naive (Measures 1000.185 Hz for 1000 Hz, for instance)
    # crossings = indices

    # More accurate, using linear interpolation to find intersample
    # zero-crossings (Measures 1000.000129 Hz for 1000 Hz, for instance)
    crossings = [i - sig[i] / (sig[i+1] - sig[i]) for i in indices]

    # Some other interpolation based on neighboring points might be better.
    # Spline, cubic, whatever

    return 1./ mean(diff(crossings))/fs

def spectrum(x,y):
    m = abs(rfft(y))

    f = linspace(0,.5/(x[1]-x[0]),len(m))
    return f,m
