#encoding:utf-8
from __future__ import print_function
from __future__ import unicode_literals
import six
from qtpy import QtCore,QtWidgets
from options_editor_ui import Ui_options_editor
from ast import literal_eval

class Options_Editor(Ui_options_editor,QtWidgets.QMainWindow):
    valueChanged = QtCore.Signal(str,object)
    def __init__(self,dictionary):
        super(Options_Editor,self).__init__()
        self.setupUi(self)
        if hasattr(dictionary,"filename"):
            self.setWindowTitle("Edit settings: {}".format(dictionary.filename))
        self.table.setRowCount(len(dictionary))
        self.table.setColumnWidth(0,300)
        self.table.setColumnWidth(1,300)
        self.table.setColumnWidth(2,100)
        self.dictionary = dictionary
        if hasattr(dictionary,"defaults"):
            self.defaults = dictionary.defaults
        else:
            self.defaults = {}
        self.load_table()

    def load_table(self):
        self.table.blockSignals(True)
        self.keys = list(self.dictionary.keys())
        if self.keys:
            self.keys.sort()
        for i, key in enumerate(self.keys):
            val = self.dictionary[key]
            # it = QtWidgets.QTableWidgetItem()
            # it.setText(key)
            # self.table.setItem(i,0,it)
            self.table.setCellWidget(i, 0, QtWidgets.QLabel(key))
            itv = QtWidgets.QTableWidgetItem()
            itv.setText(self.tostring(val))
            self.table.setItem(i, 1, itv)
            if key in self.defaults:
                itd = QtWidgets.QTableWidgetItem()
                itd.setText(self.tostring(self.defaults[key]))
                self.table.setItem(i, 2, itd)
        self.table.blockSignals(False)
        # self.table.itemChanged()
        # self.buttonBox.accepted

    def tostring(self,s):
        #if not (six.PY2 and type(s)==unicode):
        if type(s) != six.string_types:
            s = str(s)
        return s
    def on_table_itemChanged(self,it):
        #print it.text()
        key = self.keys[it.row()]
        try:
            val = str(it.text())
            try:
                val = literal_eval(val)
            except:
                pass
        except:
            val = six.u(it.text())
        print ("options parameter changed:",key,val,type(val))
        self.dictionary[key] = val
    def on_buttonBox_accepted(self):
        self.close()
    def show(self):
        super(Options_Editor,self).show()
        self.load_table()



if __name__ == "__main__":
    from options import Options
    opt = Options()
    opt.get("greeting",u"Здравствуй")
    opt.get("value",1.45)
    opt.get("string","hello")
    app = QtWidgets.QApplication([])
    ed = Options_Editor(opt)
    ed.show()
    app.exec_()
