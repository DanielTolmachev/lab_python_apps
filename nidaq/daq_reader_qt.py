from qtpy import QtCore
import numpy as np
import sys,time,traceback
try:
    sys.path.append( '../modules')
    sys.path.append('../modules/PyDAQmx')
    from PyDAQmx import *
    DAQ = True
except ImportError:
    DAQ = False
    print("PyDAQmx import error")
    traceback.print_exc()
except:
    DAQ = False
    print("PyDAQmx import error")
    traceback.print_exc()


class daq_reader_qt(QtCore.QThread):
    data_received = QtCore.Signal(object)
    def __init__(self,chan,time = 0.1):
        super(daq_reader_qt,self).__init__()
        self.chan = chan
        self.time = time
        self.stopped = False
    def stop(self):
        self.stopped = True
    def run(self):
        if DAQ:
            # Declaration of variable passed by reference
            taskHandle = TaskHandle()
            read = int32()
            data = numpy.zeros((1000,), dtype=numpy.float64)
            value = c_double()
            reserved = bool32()
            try:
                # DAQmx Configure Code
                DAQmxCreateTask("",byref(taskHandle))
                DAQmxCreateAIVoltageChan(taskHandle,self.chan,"",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,None)
                DAQmxCfgSampClkTiming(taskHandle,"",1./self.time,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000)

                # DAQmx Start Code
                DAQmxStartTask(taskHandle)

                # DAQmx Read Code
                #DAQmxReadAnalogF64(taskHandle,1000,10.0,DAQmx_Val_GroupByChannel,data,1000,byref(read),None)
                print("reading chan",self.chan)
                while not self.stopped:
                    DAQmxReadAnalogScalarF64(taskHandle,self.time,byref(value),None)
                    t = time.time()
                    self.data_received.emit((t,value.value))
                #print "Acquired %d points"%read.value
            except DAQError as err:
                print("DAQmx Error: %s"%err)
            finally:
                if taskHandle:
                    # DAQmx Stop Code
                    DAQmxStopTask(taskHandle)
                    DAQmxClearTask(taskHandle)
        else: #demo
            while not self.stopped:
                self.data_received.emit((time.time(),np.random.random()))
                time.sleep(self.time)

