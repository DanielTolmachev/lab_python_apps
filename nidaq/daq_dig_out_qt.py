from qtpy import QtCore
import numpy as np
import sys,traceback
try:
    sys.path.append( '../modules')
    sys.path.append('../modules/PyDAQmx')
    from PyDAQmx import *
    DAQ = True
except ImportError:
    DAQ = False
    print("PyDAQmx import error")
    traceback.print_exc()
except:
    DAQ = False
    print("other error during PyDAQmx import")
    traceback.print_exc()


class daq_dig_out_qt(QtCore.QObject):
    data_received = QtCore.Signal(object)
    def __init__(self,chan,time = 0.1):
        super(daq_dig_out_qt,self).__init__()
        self.time = time
        self.data_on = np.array([1],dtype = np.uint8)
        self.data_off = np.array([0],dtype = np.uint8)
        self.data_onoff = np.array([0,1,0],dtype = np.uint8)
        self.setupDigChan(chan)

    def close(self):
        if self.taskHandle:
                    # DAQmx Stop Code
                    DAQmxStopTask(self.taskHandle)
                    DAQmxClearTask(self.taskHandle)
    def setupDigChan(self,chan):
        if DAQ:
            self.taskHandle = TaskHandle()
            self.chan = chan
            try:
                    # DAQmx Configure Code
                    DAQmxCreateTask("",byref(self.taskHandle))
                    DAQmxCreateDOChan(self.taskHandle, self.chan, "", DAQmx_Val_ChanPerLine)
                    DAQmxStartTask(self.taskHandle)
            except DAQError as err:
                print("DAQmx Error: %s"%err)
        else:
            self.taskHandle = None

    def setDO(self,state):
        if self.taskHandle:
            try:
                if state:
                    data = self.data_on
                else:
                    data = self.data_off
                DAQmxWriteDigitalLines(self.taskHandle,1,1,0,DAQmx_Val_GroupByChannel,data,None,None)
            except DAQError as err:
                print("DAQmx Error: %s"%err)
    def mkPulse(self):
        if self.taskHandle:
            try:
                DAQmxWriteDigitalLines(self.taskHandle,3,1,0,DAQmx_Val_GroupByChannel,self.data_onoff,None,None)
            except DAQError as err:
                print("DAQmx Error: %s"%err)