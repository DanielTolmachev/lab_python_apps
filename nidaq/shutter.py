#-------------------------------------------------------------------------------
# Name:		    shutter.py
# Purpose:  Controls (on/off) one digital channel of NI USB DAQ
#           Initially intented to open/close shutter, but can be used elswhere
#	    where TTL is required.
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import shutter_control_gui
import daq_dig_out_qt
import sys,time,datetime
from qtpy import QtWidgets,QtCore,QtGui
sys.path.append( '../modules')
from options import Options
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
from console_qt import QConsole
import QMenuFromDict
#import SI_format


if len(sys.argv)>1:
        conf = sys.argv[1]
        print("conf is",conf)
        opt = Options(conf)
else:
    opt = Options()

nidaq_chan = "Dev1/port1/line0"



class MainWnd(QMainWindowAutoSaveGeometry,shutter_control_gui.Ui_shutter_control_gui):
    sigDIOstatechanged = QtCore.Signal(bool)
    def __init__(self):
        super(MainWnd,self).__init__()
        self.setupUi(self)
        self.state = False
        self.lastt = time.time()
        title = "Shutter control"
        if not daq_dig_out_qt.DAQ:
            title += " DEMO"
        self.setWindowTitle(title)
    def on_toolButton_pressed(self):
        self.state = not self.state
        self.toolButton.setText(str(int(self.state)))
        self.sigDIOstatechanged.emit(self.state)
        if self.state:
            msg = "shutter was opened at "
        else:
            msg = "shutter was closed at "
        #msg += datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
        msg += datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S')
        t = time.time()
        ti = t-self.lastt
        self.lastt = t
        msg += " ({} s)".format(int(ti))
        self.textEdit.append(msg)


def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk

if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    cons = QConsole()
    win = MainWnd()
    #set application icon
    icon = QtGui.QIcon("shutter.png")
    win.setWindowIcon(icon)

    if 'Window.geometry' in opt:
        win.setGeometry(*opt['Window.geometry'])
    win.GeometryChangedSig.connect(save_win_position)


    win.label.setText("{}".format(nidaq_chan))
    win.tb_set.hide()

    daqr = daq_dig_out_qt.daq_dig_out_qt(nidaq_chan)
    daqthrd = QtCore.QThread()
    daqthrd.start()
    daqr.moveToThread(daqthrd)

    win.sigDIOstatechanged.connect(daqr.setDO)

    win.show()
    app.exec_()
    daqthrd.quit()
