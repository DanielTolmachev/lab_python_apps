import os,traceback,re
import numpy as np

CALIBR_FILE_EXT = [".txt",".dat"]

class Calibrator(object):
    def __init__(self, calibrations_directory = "Calibration"):
        self.index = 0
        self.calibr = None
        self.calibrationDir = calibrations_directory
        self.loadCalList()
        self.units = None
    def loadCalList(self):
        if not os.path.exists(self.calibrationDir):
            os.mkdir(self.calibrationDir)
        if os.path.exists(self.calibrationDir) and os.path.isdir(self.calibrationDir):
            l = os.listdir(self.calibrationDir)
            self.calibrations = [f for f in l if os.path.splitext(f)[1] in CALIBR_FILE_EXT]
        else:
            self.calibrations = []
    def selectCal(self,calibration_file_or_filenumber):
        if type(calibration_file_or_filenumber)== int:
            index = calibration_file_or_filenumber-1
            calibration_file = self.calibrations[index]
            fn = self.calibrationDir + os.path.sep + calibration_file
        elif calibration_file_or_filenumber is None:
            self.calibr = None
            return
        else:
            fn = self.calibrationDir+os.path.sep+calibration_file_or_filenumber
            if not calibration_file_or_filenumber in self.calibrations:
                if os.path.exists(fn):
                    self.calibrations.append(calibration_file_or_filenumber)
                    self.cb_calibr.addItems(calibration_file_or_filenumber)
                else:
                    print("file",calibration_file_or_filenumber,"not found")
                    return
            index = self.calibrations.index(calibration_file_or_filenumber)

        try:
            f = open(fn,"br")
            for l in f.readlines():
                m = re.search(r"units = *(\w+)",l.decode())
                if m:
                    self.units = m.groups()[0]
            f.seek(0)
            n = np.genfromtxt(f)
            f.close()
            if n.ndim>3:
                print("file", calibration_file_or_filenumber, "contains wrong shape array:", n.shape)
                return
            self.calibr = n
            self.index = index
            # self.calibr_dac_max = self.calibr[:, 0].max()
            # self.calibr_dac_min = self.calibr[:, 0].min()
            if self.calibr.ndim == 1:
                print("loaded calibration from {} ({}-th order polynom)".format(calibration_file_or_filenumber,n.size))
            else:
                print("loaded calibration from", calibration_file_or_filenumber, n.shape)
            # self._setMult()
            # self.cb_calibr.setCurrentIndex(index+1)
            self.calibrationFile = calibration_file_or_filenumber
            # self.calibrLoaded.emit(calibration_file)
            # if n.shape[0]*2<self.calibr_dac_max-self.calibr_dac_min:
            #     dac = np.arange(self.calibr_dac_min, self.calibr_dac_max + 1)
            #     pwr = np.interp(dac, self.calibr[:, 0], self.calibr[:, 1])
            #     self.calibr = np.vstack((dac,pwr)).transpose()
            #     print("calibration table is too sparse, interpolating to", self.calibr.shape)
        except:
            print("cannot import file", calibration_file_or_filenumber)
            traceback.print_exc()
    def convert(self,value):
        if self.calibr is None:
            return value
        elif self.calibr.ndim == 1:
            x = 0
            for i in range(self.calibr.size):
                x += self.calibr[i]*value**i
            return x
        elif self.calibr.ndim == 2:
            raise(NotImplementedError)
        elif self.calibr.ndim == 0:
            raise(NotImplementedError)
