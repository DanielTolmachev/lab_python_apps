# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'nidaq_adc_display_gui.ui'
#
# Created: Mon Apr 04 16:34:35 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_nidaq_adc_display(object):
    def setupUi(self, nidaq_adc_display):
        nidaq_adc_display.setObjectName(_fromUtf8("nidaq_adc_display"))
        nidaq_adc_display.resize(248, 143)
        self.centralwidget = QtWidgets.QWidget(nidaq_adc_display)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.tb_set = QtWidgets.QToolButton(self.centralwidget)
        self.tb_set.setObjectName(_fromUtf8("tb_set"))
        self.horizontalLayout.addWidget(self.tb_set)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_data = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label_data.setFont(font)
        self.label_data.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_data.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_data.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_data.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_data.setObjectName(_fromUtf8("label_data"))
        self.horizontalLayout_2.addWidget(self.label_data)
        self.toolButton = QtWidgets.QToolButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolButton.sizePolicy().hasHeightForWidth())
        self.toolButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.toolButton.setFont(font)
        self.toolButton.setAutoRaise(True)
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.horizontalLayout_2.addWidget(self.toolButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        nidaq_adc_display.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(nidaq_adc_display)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 248, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        nidaq_adc_display.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(nidaq_adc_display)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        nidaq_adc_display.setStatusBar(self.statusbar)

        self.retranslateUi(nidaq_adc_display)
        QtCore.QMetaObject.connectSlotsByName(nidaq_adc_display)

    def retranslateUi(self, nidaq_adc_display):
        nidaq_adc_display.setWindowTitle(_translate("nidaq_adc_display", "MainWindow", None))
        self.label.setText(_translate("nidaq_adc_display", "dev1/ai0", None))
        self.tb_set.setText(_translate("nidaq_adc_display", "set", None))
        self.label_data.setText(_translate("nidaq_adc_display", "     0.000", None))
        self.toolButton.setText(_translate("nidaq_adc_display", "V", None))

