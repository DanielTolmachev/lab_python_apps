# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'shutter_control_gui.ui'
#
# Created: Tue Apr 05 12:31:05 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_shutter_control_gui(object):
    def setupUi(self, shutter_control_gui):
        shutter_control_gui.setObjectName(_fromUtf8("shutter_control_gui"))
        shutter_control_gui.resize(310, 160)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(shutter_control_gui.sizePolicy().hasHeightForWidth())
        shutter_control_gui.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(shutter_control_gui)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.tb_set = QtWidgets.QToolButton(self.centralwidget)
        self.tb_set.setObjectName(_fromUtf8("tb_set"))
        self.horizontalLayout.addWidget(self.tb_set)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.toolButton = QtWidgets.QToolButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolButton.sizePolicy().hasHeightForWidth())
        self.toolButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.toolButton.setFont(font)
        self.toolButton.setAutoRaise(False)
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.horizontalLayout_2.addWidget(self.toolButton)
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.textEdit.sizePolicy().hasHeightForWidth())
        self.textEdit.setSizePolicy(sizePolicy)
        self.textEdit.setLineWidth(0)
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.horizontalLayout_2.addWidget(self.textEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        shutter_control_gui.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(shutter_control_gui)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 310, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        shutter_control_gui.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(shutter_control_gui)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        shutter_control_gui.setStatusBar(self.statusbar)

        self.retranslateUi(shutter_control_gui)
        QtCore.QMetaObject.connectSlotsByName(shutter_control_gui)

    def retranslateUi(self, shutter_control_gui):
        shutter_control_gui.setWindowTitle(_translate("shutter_control_gui", "MainWindow", None))
        self.label.setText(_translate("shutter_control_gui", "Dev1/port0/line0:1", None))
        self.tb_set.setText(_translate("shutter_control_gui", "set", None))
        self.toolButton.setText(_translate("shutter_control_gui", "1", None))

