#-------------------------------------------------------------------------------
# Name:		    nidaq_display_qt.py
# Purpose:  Reads out one analog channel of NI USB DAQ
#           displays it on gui and
#		    sends it using ZMQ messaging library (PUB pattern)
#           as a python tuple (time,value)
#
# Created:     30.07.2015
#-------------------------------------------------------------------------------
from __future__ import print_function
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import nidaq_adc_display_gui
import daq_reader_qt
import sys,zmq
from qtpy import QtCore,QtWidgets
sys.path.append( '../modules')
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
from console_qt import QConsole
import QMenuFromDict
from zmq_publisher import Zmq_Publisher
from options import Options
import calibration
from collections import OrderedDict

NO_CALIBRATION_DEF_TEXT = "no calibration"

if len(sys.argv)>1:
        conf = sys.argv[1]
        print("conf is",conf)
        opt = Options(conf)
else:
    opt = Options()

nidaq_chan = opt.get("device_address", "Dev1/ai0")

precision = opt.get("daq.precision",5)
zmq_publish_port = opt.get("publish_port",5015)
context = zmq.Context()
pub_address = 'tcp://*:{}'.format(zmq_publish_port)
pub = Zmq_Publisher(context,pub_address)
units =     ["V", "mV"]
units_val = [1,1e3]

class MainWnd(QMainWindowAutoSaveGeometry,nidaq_adc_display_gui.Ui_nidaq_adc_display):
    sigData = QtCore.Signal(object)
    def __init__(self):
        super(MainWnd,self).__init__()
        self.setupUi(self)
        self.units = "V"
        self.mult = 1
        self.calibration = None
        self.calibrator = calibration.Calibrator()
        cal_name = opt.get("Calibration_file",None)
        if self.calibrator.calibrations:
            menu = OrderedDict([(NO_CALIBRATION_DEF_TEXT,self.selectCalibration)])
            menu.update([(k,self.selectCalibration) for k in self.calibrator.calibrations])
            QMenuFromDict.AddMenu(self.menubar,{"Calibrations":menu})
            for a in self.menubar.actions():
                if a.text()=="Calibrations":
                    self.calibration_actions = a.menu().actions()
                    [action.setCheckable(True) for action in self.calibration_actions]
            if cal_name in self.calibrator.calibrations:
                self.selectCalibration(cal_name)
    def selectCalibration(self,*cal_name):
        action = self.sender()
        if cal_name and type(cal_name[0])==str:
            for a in self.calibration_actions:
                if a.text()==cal_name[0]:
                    action = a
        action.setChecked(True)
        for a in self.calibration_actions:
            if a != action:
                a.setChecked(False)
        cal = action.text()
        if cal!= NO_CALIBRATION_DEF_TEXT:
            self.calibration = True
            self.calibrator.selectCal(cal)
            if self.calibrator.units:
                units.append(self.calibrator.units)
            else:
                units.append("V (cal)")
            units_val.append(self.calibrator.convert)
            self.units = units[-2]
            opt["Calibration_file"] = cal
        else:
            self.calibrator.selectCal(None)
            units.remove(self.calibration)
            units_val.remove(self.calibrator.convert)
            self.calibration = None
            self.units = units[-1]
            opt["Calibration_file"] = None
        self.on_toolButton_pressed()
    def on_toolButton_pressed(self):
        idx = units.index(self.units)
        idx +=1
        if idx>=len(units):
            idx = 0
        self.units = units[idx]
        self.toolButton.setText(self.units)
        self.mult = units_val[idx]
        print("switching units to",self.units)
    def setText(self,arg):
        if type(arg)==tuple and len(arg)>1:
            if callable(self.mult):
                val = self.mult(arg[1])
            else:
                val = arg[1]*self.mult

            if self.calibration:
                #arg  = arg[0],val
                arg = arg+(val,)
            self.sigData.emit(arg)

            #val,units = SI_format.si_units(arg[1],"V",min = 1e-3)
            msg = "{:.{}g}".format(val,precision)
            #msg = "{:.}".format(val)
            self.label_data.setText(msg)
            #self.toolButton.setText(units)


def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk

if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    cons = QConsole()
    win = MainWnd()
    if 'Window.geometry' in opt:
        win.setGeometry(*opt['Window.geometry'])
    win.GeometryChangedSig.connect(save_win_position)

    win.setWindowTitle("DAQ reader")
    win.label.setText("{} --> ZMQ_PUB {}".format(nidaq_chan,pub_address))
    win.tb_set.hide()

    daqr = daq_reader_qt.daq_reader_qt(nidaq_chan)
    daqr.data_received.connect(win.setText)
    # daqr.data_received.connect(pub.send_pyobj)
    win.sigData.connect(pub.send_pyobj)
    daqr.start()
    win.show()
    app.exec_()
    daqr.stop()
    daqr.wait()
    opt.savetodisk()
