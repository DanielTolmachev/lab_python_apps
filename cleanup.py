from __future__ import print_function,unicode_literals
import os,re,hashlib,traceback
pyc = dupes = ren = renam = cachedirs = 0
gspat = re.compile(".* \(\d\)\.\w+")

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

for dirpath,dirnames,filenames in os.walk("."):
    #print(dirpath,filenames)
    if os.path.basename(dirpath) == "__pycache__":
        try:
            if os.path.exists(dirpath):
                os.rmdir(dirpath)
                cachedirs +=1
                continue
        except:
            traceback.print_exc()
    for f in filenames:
##        print(f)
        name,ext = os.path.splitext(f)
        fp = dirpath+os.sep+f
        dirname = os.path.basename(dirpath)
##        print(name,ext)
        if ext==".pyc":
            if os.path.exists(dirpath+os.sep+name+".py") or \
            os.path.exists(dirpath+os.sep+name+".pyw"):
                print(f)
                os.remove(dirpath+os.sep+f)
                pyc +=1
                continue
            elif dirname == "__pycache__":
                os.remove(dirpath+os.sep+f)
                pyc +=1
                continue
            else:
                print('orphan .pyc file',dirpath+os.sep+f)
        m = re.match(gspat,f)
        if m:
##            print(f)
            orig = re.sub("(.*) \(\d\)(\.\w+)",r"\1\2",f)
            print(f,orig)
            origp = dirpath+os.sep + orig
            if os.path.exists(origp):
                if os.path.getsize(origp) == os.path.getsize(fp):
                    try:
                        origh = md5(origp)
                        fh = md5(fp)
                        if origh == fh:
                            print(f,orig,"duplicates by hash")
                            os.remove(fp)
                            dupes +=1
                            continue
                    except:
                        traceback.print_exc()
                ft = os.path.getmtime(fp)
                origt = os.path.getmtime(origp)
                if ft<origt:
                    print(fp,"is older than",orig)
                    os.remove(fp)
                    dupes +=1
                    continue
                elif ft>origt:
                    print(fp,"is newer than",orig)
                    os.remove(origp)
                    os.rename(fp,origp)
                    ren +=1
                    continue
                elif ft==origt:
                    print(fp,"and",orig,"have same time!")
                print(fp,orig)
            else:
                print(fp,"has no original",orig)
                os.rename(fp,origp)
                renam +=1
                continue


print("total",pyc,".pyc files were removed")
print("total",cachedirs,"__pycache__ directories were removed")
print("duplicates removed:",dupes)
print("files replaced:",ren)
print("files renamed:",renam)