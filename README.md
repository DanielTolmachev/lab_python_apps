These are applications that I created for data acquisition and conducting scientific experiments in physical lab of Experimental Physics III of TU Dortmund.

*I want to inspire other people to have a look at Python as powerful and convenient tool for scientific work.*

* Application are written in Python, and use PyQt for graphical user interface, and ZMQ for interprocess communication.

* Each instrument or device is controlled by separate independent program.
Device programs broadcast data (acquired or, vice versa, sent to device) using ZMQ messaging library via TCP.

* Data can be broadcasted and received by multiple subscribers simultaneously across different computers.

## Some features: ##

### **Cross-platform** ###
This solution is cross-platform (though some devices require drivers, that may exist only on certain platforms, for ex. NI does not provide drivers for MacOS)

### **Free** ###
Python is completely free, so you can install this wherever you want, without need to register or acquire expensive license for each computer.

### **Portable** ###
If you use portable python distribution, that this software become portable.

### **Easy to develop** ###
Simple program without gui will take no more than 20 lines of code. It will take longer to connect it with labview, if there is no labview driver provided. 

### **Flexible** ###
There is data plotter/recorder application for displaying data on screen or saving it to file.
However broadcasted data can be received by multiple subscribers simultaneously. Multiple publishers and receivers can run across different computers with different OS’es. 

### **Reliable** ###
Because each instrument is controlled by separate independent program, bug in one program do not crash whole system, and you do not have to restart everything and set parameters to other devices. 
 
# **Morale**: #
Python is very convenient tool not only for data visualization but also for conducting experiments and running physical lab.

### ... ###
I made a presentation about this matter at Uni, and in my plan it is to upload some slides and description.

### disclaimer ###
this was used in differnet labs with different equipment, in different times, so there is some legacy code