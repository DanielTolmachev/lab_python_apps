__author__ = 'C7LabPC'
import sys,time
sys.path.append( '../modules')
import sweep_widget
from qtpy import QtCore,QtWidgets
from e3_bruker_class import e3Bruker
class Magnet_Sweep_Widget (sweep_widget.SweepWidget):
    modes = ["field","voltage"]
    modes_speed_labels = ["T","V"]
    moden = 0
    maxvalues = [1.5,3]
    timer = QtCore.QTimer()
    timer_cont = QtCore.QTimer()
    refresh_interval = 40 #ms
    SPEED_LIMIT = 5 #sec to go full scale, just a
    sig_current_value = QtCore.Signal(float)
    sigContinuousModeEnabled = QtCore.Signal(bool)
    def __init__(self,*args,**kwargs):
        if 'sweepers' in kwargs:
            self.sweepers = kwargs.pop('sweepers')
        if 'device' in kwargs:
            self.device = kwargs.pop('device')
            self.units_used = [{"T":1,"V":(self.device.field2voltage,self.device.Voltage2Field)},
                               {"V":1,"T":(self.device.Voltage2Field,self.device.field2voltage)}]
        super(Magnet_Sweep_Widget,self).__init__(*args,**kwargs)
        self.tb_speed_units.setEnabled(True)
        self.tb_speed_units.pressed.connect(self.toggleMode)
        self.label_step.hide()
        self.sig_value_set.connect(self.set_central_label_delayed)
        self.timer.setSingleShot(True)
        self.timer.setInterval(self.refresh_interval)
        self.timer.timeout.connect(self.set_central_label_delayed)
        self.setSweepStepMode("field")
        self.setUnits(self.units_used[self.moden])
        #continuous mode
        self.tb_continuous = QtWidgets.QToolButton(self)
        self.tb_continuous.setText("send field value continuously")
        self.tb_continuous.setCheckable(True)
        self.tb_continuous.pressed.connect(self.switch_continuous_mode)
        self.tb_continuous.setSizePolicy(sweep_widget.QtWidgets.QSizePolicy.Fixed,sweep_widget.QtWidgets.QSizePolicy.Fixed)
        mh = self.minimumHeight()
        self.tb_continuous.setGeometry(5,mh,200,20)
        self.setMinimumHeight(mh+20+5)
        self.timer_cont.timeout.connect(self.cont_mode_send)
        self.timer_cont.setSingleShot(False)
    def toggleMode(self):
            i = self.modes.index(self.mode)
            i = i+1
            if i>=len(self.modes):
                i=0
            self.mode == self.modes[i]
            self.setSweepStepMode(i)
            self.moden = i
    def setSweepStepMode(self,i):
            if type(i)==str:
                if i in self.modes:
                    i = self.modes.index(i)
            self.mode = self.modes[i]
            self.tb_speed_units.setText(self.modes_speed_labels[i])
            self.sig_start_sweeper.connect(self.sweepers[i].sweep)
            self.sig_stop_sweeper.connect(self.sweepers[i].stop)
            self.units_multiplier = self.units_used[i][self.modes_speed_labels[i]]
            # for i,val in enumerate [self.left,self.right]
            #     val =
            #     le.setText = newtext
    def update_central_label(self):
        self.current_value = self.sweepers[self.moden].queryfunc()
        self.set_central_label(self.current_value)
    def units2disp_speed(self,val):
        return str(val)
    def set_central_label_delayed(self,*val):
        if val:
            self.target = val[0]
            self.t0 = time.time()
        txt = self.le_central.text()
        if txt:
            self.current_value = self.units2std(txt)
        else:
            self.current_value = self.target
        print(self.current_value)
        speed = self.maxvalues[self.moden]/self.SPEED_LIMIT
        step = speed*self.refresh_interval*1e-3
        deltat = time.time()-self.t0
        print("setting",self.target,self.current_value,speed,step,deltat)
        if deltat < self.SPEED_LIMIT and (self.target-self.current_value)>step:
            if self.target>self.current_value:
                self.current_value += step
            else:
                self.current_value -= step
            self.timer.start()
        else:
            self.current_value = self.target
            self.timer.stop()
        self.set_central_label(self.current_value)
    def switch_continuous_mode(self):
        if self.tb_continuous.isChecked():
            self.timer_cont.stop()
            self.sigContinuousModeEnabled.emit(False)
        else:
            self.timer_cont.setInterval(self.dt*1000)
            self.sigContinuousModeEnabled.emit(True)
            self.timer_cont.start()
    def cont_mode_send(self):
        # if hasattr(self,"current_value"):
            self.sig_current_value.emit(self.current_value)
        # else:
        #     print("current value not set")
        # self.onValueSet()
    def on_btn_start_pressed(self):
        if 'start' in self.command: # or 'resume' in self.command:
            self.timer_cont.stop()
        elif self.command == 'stop' or self.command=='finish':
            if self.tb_continuous.isChecked():
                self.timer_cont.start()
        super(Magnet_Sweep_Widget,self).on_btn_start_pressed()
    def value_changed(self,val):
        super(Magnet_Sweep_Widget,self).value_changed(val)
        self.timer_cont.setInterval(self.dt*1000)




