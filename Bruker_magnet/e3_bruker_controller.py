#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      E3a
#
# Created:     17/09/2015
# Copyright:   (c) E3a 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
DEFAULT_NET_TIMEOUT = 1
DEFAULT_RECONNECT_TIME = 1
import socket,time,traceback,sys
from qtpy import QtCore
from e3_bruker_class import e3Bruker

class e3BrukerController(QtCore.QObject,e3Bruker):
    reconnect_timer = QtCore.QTimer()
    conn_lost = QtCore.Signal()
    conn_established = QtCore.Signal()
    valueReceived = QtCore.Signal(object)
    # def __init__(self,mag_class):
        # self.mag_class = mag_class
        # self.mag_class.process_exception = self.process_exception
        # super(e3BrukerController,self).__init__()
        # self.address = address
        # #super(e3Bruker,self).__init__(socket.AF_INET, socket.SOCK_STREAM)
        # self.socket.settimeout(DEFAULT_NET_TIMEOUT)
        # self.reconnect_timer.timeout.connect(self.open)
    def __init__(self,address):
        #super(e3Bruker,self).__init__(address)
        super(QtCore.QObject,self).__init__()
        

        self.address = address
        self.socket.settimeout(DEFAULT_NET_TIMEOUT)
        self.reconnect_timer.timeout.connect(self.open)
    # def setVoltage(self,u):
    #     return self.mag_class.setVoltage(u)
    # def getVoltage(self):
    #     return self.mag_class.getVoltage()
    def process_exception(self):
        exc,info,_ = sys.exc_info()
        if not self.connected:
            self.conn_lost.emit()
        if exc==socket.error:
            print("Error ",exc,info)
            if exc.errno == 10065:
                self.connected = False
                self.conn_lost.emit()
        elif exc!=socket.timeout:
            print(exc,info)
        else:
            print(exc,info)
    def open(self):
        if not self.connected:
            e3Bruker.open(self)
        if self.connected:
            self.valueReceived.emit(self.getVoltage())
            self.conn_established.emit()
    def getField(self):
        ret = super(e3BrukerController,self).getField()
        self.valueReceived.emit(ret)
        return ret


