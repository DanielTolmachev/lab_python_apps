#coding:utf-8-------------------------------------------------------------------
# Name:        	Bruker Netzgerät
#		magnet.py
#
# Purpose:	This app controls Bruker magnet power supply
#		and allows to set/sweep magnetic field
#		and broadcast field value using 
#		ZMQ messaging library (PUB pattern)
#		
# Made for:	e3.physik.tu-dortmund.de
#
# Created:  	16.03.2015
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

PUB_ADDRESS = 'tcp://*:5009'
#PUB_ADDRESS = 'tcp://127.0.0.1:50099'
DEV_ADDRESS = ('129.217.155.128',51234)
SUB_PORT = 6009

UNITS_USED = ['V',"T"] #measurment units
UNITS_DEFAULT = 'T'
##START = 0 #mA
##STOP = 300 #mA
##STEP = 0.2 #mA
##DT= 0.3 #s
FIELD_PRECISION = 4

import zmq
from qtpy import QtWidgets,QtCore,QtGui
import sys,traceback,time
from e3_bruker_controller import e3BrukerController
from e3_bruker_class import e3Bruker
from magnet_sweep_widget import Magnet_Sweep_Widget
sys.path.append( '../modules')
#sys.path.append( sys.path[0]+'/../modules')
from zmq_sub2qt import Zmq2Qt
from console_qt import QConsole
import QMenuFromDict
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
from sweeper_msg import SweeperWMsg
from zmq_publisher import Zmq_Publisher
from options import Options
import sweep_widget
import quickQtApp
import install_exception_hook


# app = QtWidgets.QApplication(sys.argv)
# opt = Options()
# cons = QConsole()
app,opt,win,cons = quickQtApp.mkAppWin('Bruker Netzgerät', icon = "magnet.png")


DEV_ADDRESS = opt.get("dev.address",DEV_ADDRESS)
PUB_ADDRRESS = opt.get("pub.address",PUB_ADDRESS)

class CmdProxy(object):
    def __init__(self,gui,sweeper,publisher):
        self.gui = gui
        self.sweeper = sweeper
        self.publisher = publisher
    def cmd_parse(self,cmd):
        if 'start' in cmd:
            loop = not 'once' in cmd
            resume = 'continue' in cmd
            bidirectional = not 'oneway' in cmd
            self.sweeper.sweep(  self.gui.left,
                            self.gui.right,
                            self.gui.step,
                            self.gui.dt,
                            resume = resume,
                            loop = loop,
                            bidirectional = bidirectional)
            #start,stop,step,dt,resume = False,
            #    loop =  True,
            #    bidirectional = False,
            #    maxloops = 0
        if 'stop' in cmd:
            self.sweeper.stop()
            #mag.setVoltage(0)
        #self.publisher.send_pyobj(unicode(cmd))
class SignalProxy(QtCore.QObject):
    sig_float = QtCore.Signal(object)
    sig_tuple = QtCore.Signal(tuple)
    sig_string = QtCore.Signal(str)
    sig = QtCore.Signal(object)
    sig_new_range = QtCore.Signal(float,float)
    send_start_stop = True
    def rcv(self,t):
        #print type(t),': ',t
        if type(t)==tuple:
            self.sig.emit(t)
            if len(t)>1:
                if type(t[1])==float:
                    self.sig_float.emit(t[1])
        elif type(t)==str:
            if self.send_start_stop or ("start" not in t and "stop" not in t):
                self.sig.emit(t)
            #self.processCommand(t)
            if "stop" in t:
                 sw.setGuiStopped()
        elif type(t)==float:
            self.sig_float.emit(t)
            self.sig.emit((time.time(),t))
    def sendrange(self,begin,end):
        self.sig.emit(('range',(begin,end)))
    def processCommand(self,arg): #process external commands
        if type(arg)==tuple:
            if len(arg)>1:
                if arg[0]=="range" and type(arg[1])==tuple:
                    self.sig_new_range.emit(arg[1][0],arg[1][1])
                    return
        elif type(arg)==str:
            if "start" in arg:
                sw.on_btn_start_pressed()
                return
            elif "advance"in arg:
                sw._shift_manual(+1)
                return
            # elif "stop" in arg:
            #     sw.setGuiStopped()
        print("received command:", arg)
    def disableStartStopStrings(self,status):
        self.send_start_stop = not status


signalproxy = SignalProxy()
#sigproxy2 = SignalProxy()

##def modifylabel(t):
##    if type(t)==tuple:
##        if len(t)>2:
##            sw.set_central_label(t[1])
context = zmq.Context()
pub = Zmq_Publisher(context,PUB_ADDRESS)
sub = Zmq2Qt(context,SUB_PORT)
#mag1 = e3Bruker(DEV_ADDRESS)

mag = e3BrukerController(DEV_ADDRESS)
mag_thread = QtCore.QThread()
mag_thread.start()
mag.moveToThread(mag_thread)
try:
    mag.open()
except:
    traceback.print_exc()
#conthrd = QtCore.QThread()
#mc = e3BrukerController(mag)
#mc.moveToThread(conthrd)
#conthrd.start()

units = opt.get('current_units',UNITS_DEFAULT)
UNITS_USED = {"V":1,"T":(mag.Voltage2Field,mag.field2voltage)}
if not units in UNITS_USED.keys():
    units = UNITS_DEFAULT
sw_volt = SweeperWMsg(  messager = signalproxy.rcv,
                        setfunc = mag.setVoltage,
                        queryfunc = mag.getVoltage,
                        verbose = False)
sw_field = SweeperWMsg(  messager = signalproxy.rcv,
                        setfunc = mag.setField,
                        queryfunc = mag.getField,
                        verbose = False)


# win = QMainWindowAutoSaveGeometry()
# if 'Window.geometry' in opt:
#     win.setGeometry(*opt['Window.geometry'])
# def save_win_position(pos):
#     opt['Window.geometry'] = pos
#     opt.savetodisk
# win.GeometryChangedSig.connect(save_win_position)

#set application icon
#icon = QtGui.QIcon(QtGui.QPixmap("magnet.png"))
#win.setWindowIcon(icon)

sw = Magnet_Sweep_Widget( units = units,
                            units_multiplier = UNITS_USED[units],
                            sweepers = [sw_field,sw_volt],
                            device = mag)
sw.precision = FIELD_PRECISION
win.setCentralWidget(sw)
# win.setWindowTitle('Bruker Netzgerät')
recon_timer = QtCore.QTimer()
recon_timer.setInterval(1000)

QMenuFromDict.AddMenu(win.menuBar(),
    { "console":cons.show})

#sig_current_set = QtCore.Signal(tuple)
#sw = win.sweeper
#sw.setUnits(UNITS_USED)
##sw_volt = SweeperWMsg(  messager = signalproxy.rcv,
##                        setfunc = pws.setcurrent,
##                        queryfunc = pws.querycurrent,
##                        verbose = True)
#sw.setunits('V',1)
#pws.setvoltage(0)
#pws.setcurrent(.5)

sub.sigGotMessage.connect(signalproxy.processCommand)
signalproxy.sig_new_range.connect(sw.setWorkingRange)
recon_timer.timeout.connect(mag.open)
mag.conn_lost.connect(recon_timer.start)
mag.conn_established.connect(recon_timer.stop)
mag.conn_established.connect(recon_timer.stop)
mag.valueReceived.connect(signalproxy.rcv)

sw.connect_options_dictionary(opt)
sw.sig_params_changed.connect(opt.savetodisk) #save options immidiately
cmd = CmdProxy(sw,sw_volt,pub)
#sw.sig_started.connect(cmd.cmd_parse)
#sw.sig_start_sweeper.connect(sw_volt.sweep)
#sw.sig_stop_sweeper.connect(sw_volt.stop)
sw.sig_value_set.connect(mag.setgetField) #set current from widget central edit
sw.sig_value_set.connect(signalproxy.rcv)
sw.sig_range_changed.connect(signalproxy.sendrange)
sw.sig_current_value.connect(signalproxy.rcv)
sw.sigContinuousModeEnabled.connect(signalproxy.disableStartStopStrings)
signalproxy.sig.connect(pub.send_pyobj) #send tuple away using zmq
signalproxy.sig_float.connect(sw.set_central_label) #modify widget central value

win.show()
sw.update_central_label()

#First Simple message box variant
##text ='''Sweeping current from {} to {}
##Time per sweep is {}'''.format(START,STOP,(STOP-STEP)/STEP*DT)
##mb = QtWidgets.QMessageBox()
##mb.addButton('Stop',QtWidgets.QMessageBox.YesRole)
if __name__ == '__main__':
    #sw_volt.sweep(START,STOP,STEP,DT,loop = True,bidirectional = True)
    #mb.question(None,'',text)
    win.show()
    app.exec_()
    sw_volt.stop()
    sw_field.stop()
    sw_volt.join()
    sw_field.join()
    #mag.setVoltage(0)
    opt.savetodisk()
    print(opt)



