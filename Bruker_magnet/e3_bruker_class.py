#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      E3a
#
# Created:     17/09/2015
# Copyright:   (c) E3a 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
DEFAULT_NET_TIMEOUT = 0.2
import six
import socket
import traceback
from numpy import interp,linspace,array


class e3Bruker(object):
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connected = False
    con_lost_t = 0
    process_exception = traceback.print_exc
    poly_field2voltage = [-0.00165,
            +1.77268,
            -0.75042,
            1.36396,
            -0.89945,
            -0.08078,
            0.2342]
    poly_voltage2field =  [-0.001385199908162,
                0.596998142965356,
                0.051501341808530,
                -0.067565951914305,
                0.061153889545867,
                0.029148795307394,
                -0.004372381431817]
    tab_voltage = None
    tab_field = None

    #C = -0.008700935057429*V.^6 + 0.011578357928470*V.^5+  0.060888979625536*V.^4 -0.175395396724867*V.^3   +0.165464727748214*V.^2 +  0.560839888220548*V  +  0.001034344720774
    def __init__(self,address = None):
        self.address = address
        #super(e3Bruker,self).__init__(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(DEFAULT_NET_TIMEOUT)
        #self.open()
    def ask(self,req):
        try:
            if six.PY3 and type(req)==str:
                self.socket.send(req.encode())
            else:
                self.socket.send(req)
            return self.socket.recv(1024)
        except:
            self.process_exception()
    def process_exception(self):
        traceback.print_exc()
    def open(self):
        try:
            print("trying ",self.address)
            self.socket = socket.create_connection(self.address,1)
            self.connected = True
            print("connected")
        except:
            self.process_exception()
    def setDAC(self,dac):
        h = hex(dac)[2:].upper()
        rep = self.ask(h+":")
        #print "setDac: ", dac, " " ,h, rep
        return rep
    def setVoltage(self,u):
        dac = self.Voltage2DAC(u)
        return self.setDAC(dac)        
    def getDAC(self):
        rep = self.ask("R:")        
        if rep:
            if type(rep)!=str:
                rep = rep.decode()
            if rep.startswith("DAC = "):
                h = rep.strip()[6:]
                dac = int(h,16)
                print("getDAC: ",rep,h,dac)
                return dac
        print("getDAC: ",rep)
    def getVoltage(self):
        return self.DAC2Voltage(self.getDAC())
    def DAC2Voltage(self,dac):
        if type(dac)== int:
            if dac>524287:
                return round((float(dac)-1048575)/174762.33333333334,7)
            else:
                return round(float(dac)/174762.33333333334,7)
    def Voltage2DAC(self,u):
        if u>0:
            v = round(174762.33333333334*u) #(2**19-1)/3
        elif u<0:
            v = round(174762.33333333334*u+1048575) # u*(2**19-1)/3+(2**20-1)
        else:
            v = 0
        #print u,"V = ",v," dac"
        return int(v)
    def Voltage2Field(self,u):
        ##        b = 0.
        ##        for i,c in enumerate(self.poly_voltage2field):
        ##            b += c*u**i
        ##        return b
        if u is None:
            return
        if self.tab_field is None:
            self.tab_field = linspace(0,1.5,1500)
            self.tab_voltage = array([self.field2voltage(n) for n in self.tab_field])
        try:
            return interp(u,self.tab_voltage,self.tab_field)
        except:
            traceback.print_exc()

    def field2voltage(self,b):
        v = 0.
        for i,c in enumerate(self.poly_field2voltage):
            v+= c*b**i
        return v
    def setField(self,b):
        #print "setField",b
        self.setVoltage(self.field2voltage(b))
    def getField(self):
        res = self.Voltage2Field(self.getVoltage())
        print("Field = ",res)
        return res
    def setgetField(self,b):
        self.setField(b)
        return self.getField()
    def getIP(self):
        self.socket.send("IR:")
        rep = ""
        try:
            r = self.socket.recv(1024)
            while(r):
                r = self.socket.recv(1024)
                rep +=r
        except socket.timeout:
            pass
        return rep



if __name__ == '__main__':
    DEV_ADDRESS = ('129.217.155.128',51234)    
    mag = e3Bruker(DEV_ADDRESS)
#    mag.open()
    
#    import numpy as np
#    import pyqtgraph as pg
#    x = linspace(0,3,1000)
#    b = linspace(0,1.5,1500)
#    u = array([mag.field2voltage(n) for n in b])
#    y = [mag.Voltage2Field(n) for n in x]
#    pg.plot(x,y)
#    pg.plot(b,u)
#    print(mag.Voltage2Field(0))


