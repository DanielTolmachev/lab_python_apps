import instrument_base,re,time
from collections import OrderedDict

class PicoMotor(instrument_base.InstrumentBase):
    def __init__(self,*args,**kwargs):
        super(PicoMotor,self).__init__(*args,**kwargs)
        self.driversEnabled = {}
        self.activeChannels = {}
        self.params = OrderedDict()
    def getID(self):
        return self.ask("ver\r\n")
    def start(self):
        self.ask("go\r\n")
    def stop(self):
        self.ask("hal\r\n")
    def enableDriver(self,driver):
        rep = self.ask("MON A{}\r\n".format(driver))
        if rep == b">":
            self.driversEnabled[driver] = True
        return rep
    def disableDriver(self,driver):
        rep = self.ask("MOF A{}\r\n".format(driver))
        if rep == b">":
            self.driversEnabled[driver] = False
        return rep
    def enableDrivers(self,*drivers):
        for d in self.params:
            if d in drivers:
                self.enableDriver(d)
            else:
                self.disableDriver(d)
    def selectChannel(self,chan,*driver):
        if not driver:
            driver = [d for d in self.driversEnabled if d]
        for d in driver:
            self.ask("chl A{} {}\r\n".format(d,chan))

    def getActiveChannels(self):
        resp = self.ask("chl\r\n")
        if resp==b">":
            self.getActiveChannels()
        elif resp:
            ac = [(int(d), int(m)) for d, m in set(re.findall(br"A(\d)=(\d)",resp))]
            for d,m in ac:
                self.activeChannels[d] = m
    def getVelocities(self):
        resp = self.ask("vel\r\n")
        if resp:
            vel = [ (int(d),int(m),int(v)) for d,m,v in  set(re.findall(br"A(\d) M(\d)=(\d+)", resp))]
            for d,m,v in vel:
                if d not in self.params:
                    self.params[d] = OrderedDict()
                if m not in self.params[d]:
                    self.params[d][m] = {}
                self.params[d][m]["velocity"] = v
    def setVelocity(self,driver,motor,velocity):
        resp = self.ask("vel A{} {} {}\r\n".format(driver,motor,velocity))
        if resp==":>":
            self._checkDriver()
            self.params[driver][motor]["velocity"] = velocity
    def _checkDriver(self,driver):
        if driver not in self.driversEnabled:
            self.enableDriver(driver)
        if driver not in self.activeChannels:
            self.getActiveChannels()
        if driver not in self.params:
            self.getVelocities()
    def mkStep(self,driver,motor,step,speed = 0):
        if not self.isOpen():
            self.open()
        if self.isOpen():
            self._checkDriver(driver)
            if motor!= self.activeChannels[driver]:
                self.selectChannel(motor,driver)
            if speed and speed!= self.params[driver][motor]["velocity"]:
                self.setVelocity(driver,motor,speed)
            return self.ask("rel A{} {} g\r\n".format(driver,step))





if __name__ == "__main__":
    DEVICE_ADDRESS = "129.217.155.13"
    DEVICE_PORT = 23
    pm = PicoMotor((DEVICE_ADDRESS,DEVICE_PORT))
    pm.open()
    pm.mkStep(1, 1, -1000)
