import sys
sys.path.append("../modules")
import install_exception_hook
from qtpy import QtWidgets,QtCore
from quickQtApp import mkAppWin
import pm_device_class

DEVICE_ADDRESS = "129.217.155.13"
DEVICE_PORT = 23

class MirrorControl(QtWidgets.QWidget):
    def __init__(self,PicoMotorClass:pm_device_class.PicoMotor,parent):
        self.pm = PicoMotorClass
        self.parent = parent
        super().__init__()
        gl = QtWidgets.QGridLayout()
        self.setLayout(gl)
        self.sb = QtWidgets.QSpinBox()
        self.sb.setMaximum(100000)
        self.sb.setValue(opt.get("Motor.step",2000))
        gl.addWidget(QtWidgets.QLabel("Step size"))
        gl.addWidget(self.sb, 0, 1)
        self.sb_speed = QtWidgets.QSpinBox()
        self.sb_speed.setMaximum(2000)
        self.sb_speed.setValue(opt.get("Motor.step", 2000))
        gl.addWidget(QtWidgets.QLabel("Velocity"),0,2)
        gl.addWidget(self.sb_speed, 0, 3)
        self.btn_zero_x = QtWidgets.QPushButton("set x=0")
        self.btn_zero_y = QtWidgets.QPushButton("set y=0")
        self.lbl = QtWidgets.QLabel("Ready")
        self.lbl_vert = QtWidgets.QLabel("0")
        self.lbl_vert.setAlignment(QtCore.Qt.AlignCenter)
        gl.addWidget(self.lbl_vert,1,1,1,2)
        gl.addWidget(self.btn_zero_y,1,4)
        gl.addWidget(self.lbl, 2, 1, 1, 3)
        gl.addWidget(self.btn_zero_x, 2, 4)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.vertMove = [0,0]
        self.horMove = [0,0]
        self.btn_zero_x.pressed.connect(self._setZero)
        self.btn_zero_y.pressed.connect(self._setZero)
    def eventFilter(self, QObject, QEvent):
        print(QEvent)
    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key()== QtCore.Qt.Key_Left:
            self.pm.mkStep(1,0,-self.sb.value(),speed=self.sb_speed.value())
            self.horMove[0]+=self.sb.value()
        if QKeyEvent.key()== QtCore.Qt.Key_Right:
            self.pm.mkStep(1,0,self.sb.value(),speed=self.sb_speed.value())
            self.horMove[1] += self.sb.value()
        if QKeyEvent.key()== QtCore.Qt.Key_Up:
            self.pm.mkStep(1,1,-self.sb.value(),speed=self.sb_speed.value())
            self.vertMove[0] += self.sb.value()
        if QKeyEvent.key()== QtCore.Qt.Key_Down:
            self.pm.mkStep(1,1,self.sb.value(),speed=self.sb_speed.value())
            self.vertMove[1] += self.sb.value()
        if QKeyEvent.key()== QtCore.Qt.Key_Escape:
            self.pm.stop()
        self._setLabels()
    def _setLabels(self):
        self.lbl.setText("{} {}".format(self.horMove[0], self.horMove[1]))
        self.lbl_vert.setText("{} {}".format(self.vertMove[0], self.vertMove[1]))
    def _setZero(self):
        if self.sender()==self.btn_zero_x:
            self.horMove = [0,0]
        elif self.sender()==self.btn_zero_y:
            self.vertMove = [0,0]
        self._setLabels()


app,opt,win,cons = mkAppWin("mirror control")

pm = pm_device_class.PicoMotor(opt.get("Device.address",(DEVICE_ADDRESS,DEVICE_PORT)))
mcw = MirrorControl(pm,win)
win.setCentralWidget(mcw)
#win.installEventFilter(mcw)
win.show()
app.exec()