from qtpy import QtCore
import pulse_blaster_class

class PB_Controller(QtCore.QObject):
    sigCallReturned = QtCore.Signal(object)
    sigStopped = QtCore.Signal(object)
    sigValueSet = QtCore.Signal(object)
    sweep_timer = QtCore.QTimer()
    def __init__(self,pulse_blaster_class):
        super(PB_Controller,self).__init__()
        self.pb = pulse_blaster_class
        self.current_duration = 1e-6
        self.sigValueSet.emit(self.current_duration)
    def call(self,func,*args):
        if isinstance(func,str):
            if hasattr(self.pb,func):
                func = getattr(self.pb,func)
            else:
                print("can not find function",func)
        if callable(func):
            ret = func(*args)
            self.sigCallReturned.emit(ret)
            return ret
    def sweep(self,left,right,step,dt, cont_from_current_position, *args):
        self.sweep_left = left
        self.sweep_right = right
        self.sweep_step = step
        if not cont_from_current_position:
            if step>0:
                self.current_duration = left
            elif step<0:
                self.current_duration = right
        self.sendPulse(self.current_duration)
        self.sweep_timer = QtCore.QTimer()
        self.sweep_timer.timeout.connect(self.setNextValue)
        self.sweep_timer.setInterval(dt*1000)
        self.sweep_timer.setSingleShot(False)
        self.sweep_timer.start()
        print(args)
    def setNextValue(self):
        self.current_duration+=self.sweep_step
        print(self.current_duration)
        if self.sweep_left>self.current_duration or self.current_duration>self.sweep_right:
            self.sweep_timer.stop()
            self.sigStopped.emit()
        else:
            self.sendPulse(self.current_duration)
            self.sigValueSet.emit(self.current_duration)
    def stop(self):
        pass
    def sendPulse(self,duration):
        self.pb.writeInstructions([(0xFFFFFF,pulse_blaster_class.STOP,0,int(duration*1e9))])
        self.pb.stop()
        self.pb.pb_start()