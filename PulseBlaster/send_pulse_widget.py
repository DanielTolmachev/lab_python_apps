#-------------------------------------------------------------------------------
# Name: 	send_pulse_widget.py
#
# Purpose:   This app allows to send pulse using Spin-Core PulseBlaster PCI Card
#            tested with PulseBlasterESR 200
#
# Created:     2016
#-------------------------------------------------------------------------------
from __future__ import print_function,unicode_literals
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import send_pulse_widget_form
import pulse_blaster_class
from ctypes import c_double
from qtpy import QtWidgets,QtGui

class SendPulseWidget(send_pulse_widget_form.Ui_SendPulseWidget,QtWidgets.QWidget):
    def __init__(self,pulseblasterobject):
        super(SendPulseWidget,self).__init__()
        self.setupUi(self)
        self.pb = pulseblasterobject
        self.pb.open()
        self.pb.call("pb_core_clock",c_double(200.))
        self.setPulse(self.doubleSpinBox.value())
        self.doubleSpinBox.valueChanged.connect(self.setPulse)
        self.pushButton.pressed.connect(self.sendPulse)
        self.instructions_set = False
    def setPulse(self,val):
        print(val)
        self.pb.writeInstructions([(0xFFFFFF,pulse_blaster_class.STOP,0,val*1e3)])
    def sendPulse(self):
        self.pb.stop()
        self.pb.pb_start()
        self.instructions_set = False

if __name__ == "__main__":
    import sys
    sys.path.append('../modules')
    import quickQtApp
    app,opt,win,cons = quickQtApp.mkAppWin(title = "PulseBlaster")
    # set application icon
    icon = QtGui.QIcon("spbi.png")
    win.setWindowIcon(icon)

    win.show()
    pb = pulse_blaster_class.PulseBlaster()
    pw = SendPulseWidget(pb)
    win.setCentralWidget(pw)
    app.exec_()
