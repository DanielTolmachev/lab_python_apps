#-------------------------------------------------------------------------------
# Name: 	pulse_blaster.py
#
# Purpose:   This app allows to send pulses using Spin-Core PulseBlaster PCI Card
#            tested with PulseBlasterESR 200
#
# Created:     2017
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import sys
from qtpy import QtWidgets,QtCore
sys.path.append("../modules")
import install_exception_hook
import quickQtApp,sweep_widget
import send_pulse_widget,pulse_blaster_class,pb_controller_qt






app,opt,win,cons = quickQtApp.mkAppWin(title = "Pulse Blaster",icon = "spbi.png")
pb = pulse_blaster_class.PulseBlaster()

tw = QtWidgets.QTabWidget()
win.setCentralWidget(tw)
win.show()
pw = send_pulse_widget.SendPulseWidget(pb)
tw.addTab(pw,"Simple Pulse")
sweep_len = sweep_widget.SweepWidget(optionsdict=opt,units="μs",
                                     units_multiplier=1e-6,sweepValueDesc="SweepPulseLength",
                                     precision = 4,
                                     use_internal_sweeper=True)
tw.addTab(sweep_len,"sweep pulse length")
controller = pb_controller_qt.PB_Controller(pb)
controller_t = QtCore.QThread()
controller.moveToThread(controller_t)
controller_t.start()

# sweep_len.sig_start_sweeper.connect(controller.sweep)
# sweep_len.sig_stop_sweeper.connect(controller.stop)
sweep_len.sig_value_set.connect(controller.sendPulse)
controller.sigStopped.connect(sweep_len.setGuiStopped)
controller.sigValueSet.connect(sweep_len.set_central_label)

app.exec_()
controller_t.quit()
opt.savetodisk()