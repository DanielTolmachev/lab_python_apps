# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'send_pulse_widget_form.ui'
#
# Created: Mon Aug 29 13:07:44 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_SendPulseWidget(object):
    def setupUi(self, SendPulseWidget):
        SendPulseWidget.setObjectName(_fromUtf8("SendPulseWidget"))
        SendPulseWidget.resize(201, 41)
        self.horizontalLayout = QtWidgets.QHBoxLayout(SendPulseWidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.doubleSpinBox = QtWidgets.QDoubleSpinBox(SendPulseWidget)
        self.doubleSpinBox.setDecimals(1)
        self.doubleSpinBox.setMinimum(0.1)
        self.doubleSpinBox.setMaximum(999999999.0)
        self.doubleSpinBox.setProperty("value", 10.0)
        self.doubleSpinBox.setObjectName(_fromUtf8("doubleSpinBox"))
        self.horizontalLayout.addWidget(self.doubleSpinBox)
        self.label = QtWidgets.QLabel(SendPulseWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.pushButton = QtWidgets.QPushButton(SendPulseWidget)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.horizontalLayout.addWidget(self.pushButton)

        self.retranslateUi(SendPulseWidget)
        QtCore.QMetaObject.connectSlotsByName(SendPulseWidget)

    def retranslateUi(self, SendPulseWidget):
        SendPulseWidget.setWindowTitle(_translate("SendPulseWidget", "Form", None))
        self.label.setText(_translate("SendPulseWidget", "<html><head/><body><p>μs</p></body></html>", None))
        self.pushButton.setText(_translate("SendPulseWidget", "Send pulse", None))

