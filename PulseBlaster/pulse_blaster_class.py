from __future__ import print_function
import ctypes,os,time
from platform import architecture

path = r"C:\SpinCore\SpinAPI\dll"
libname64 = "spinapi64.dll"
libname32 = r"C:\SpinCore\SpinAPI\lib32\spinapi.dll"

PULSE_PROGRAM = 0

CONTINUE	=	0	 #	Ignored
STOP	=	1	 #	Ignored
LOOP	=	2	 #	Number of desired loops. This value must be greater than or equal to 1.
END_LOOP	=	3	 #	Address of beginning of loop
JSR	=	4	 #	Address of first subroutine instruction
RTS	=	5	 #	Ignored
BRANCH	=	6	 #	Address of next instruction
LONG_DELAY	=	7	 #	Number of desired loops. This value must be greater than or equal to 3.
WAIT	=	8	 #	Ignored



class PulseBlaster(ctypes.CDLL):
    def __init__(self):
        arch,_ = architecture()
        if arch=='64bit':
            libname = libname64
        else:
            libname = libname32
        super(PulseBlaster,self).__init__(libname)
        self.getErr = self.pb_get_error
        self.getErr.restype = ctypes.c_char_p
        self.pbInstPbonly = self.pb_inst_pbonly
        self.pbInstPbonly.argtypes = [ctypes.c_uint,
                                      ctypes.c_int,
                                      ctypes.c_int,
                                      ctypes.c_double]
        self.startProgr = self.pb_start_programming
        self.startProgr.argtypes = [ctypes.c_int]
        self.startProgr.restype = ctypes.c_int
        #self.open()
    # def pb_init(self):
    #      return super(ctypes.WinDLL,self).pb_init()
    # def pb_close(self):
    #     return self.pb_close()
    # def pb_start_programming(self):
    #     return self.pb_start_programming(PULSE_PROGRAM)
    # def pb_inst( self,flags,  inst,  inst_data,  length):
    #     return self.inst(flags, inst, inst_data, length);
    # def pb_stop_programming(self):
    #     return self.pb_stop_programming()
    # def pb_start(self):
    #     return self.pb_start()
    # def pb_stop(self):
    #     return self.pb_stop()
    def open(self):
        return self.pb_init()
    def close(self):
        return self.pb_close()
    def stop(self):
        err = self.pb_stop()
        if err<0:
           err_str = self.getErr()
           print( "error in pb_stop():",err_str)
    def loadppg(self,ppg):
        #print self.pb_start_programming(PULSE_PROGRAM)
        self.startProgr(PULSE_PROGRAM)
        #self.pb_start_programming()
        wcnt = len(ppg)-1
        # if len(ppg)>0:
        #     if
        for i,(word,delay) in enumerate(ppg):
            #res = self.pb_inst_pbonly ()
            if i==wcnt:
                res = self.pbInstPbonly(word,BRANCH,0,delay*1e9)
            else:
                res = self.pbInstPbonly(word,CONTINUE,0,delay*1e9)
            #res = self.pb_inst_pbonly (ctypes.c_uint(word), CONTINUE, ctypes.c_int(0),ctypes.c_double(10000000000))
            if res<0:
                err = self.getErr()
                #err = ctypes.c_char
                print( "error in pb_inst_pbonly:",err)
                print( "instruction: {:06X} {:g}s".format(word,delay))
        if res>0:
            print (res,"instructions was loaded")
        print (self.pb_stop_programming())
    def __del__(self):
        pass
    def test(self,arg1,arg2):
        print (arg1,arg2)
    def call(self,func_name,*args):
        func = self.__getattr__(func_name)
        # print args,func,args[0]
        # self.test(*args)
        err = func(*args)
        if err<0:
            err_str = self.getErr()
            if args:
                print ("{}{} returned error {}".format(func,args,err))
            else:
                print ("{}() returned error {}".format(func,err))
            print (err_str)
        return err
    def allON(self):
        err = self.startProgr(PULSE_PROGRAM)
        if err<0:
           err_str = self.getErr()
           print ("error in startProgr(PULSE_PROGRAM):",err_str)


        err = self.pbInstPbonly(0xFFFFFF,CONTINUE,10,1e10)
        if err<0:
           err_str = self.getErr()
           print ("error in pb_inst_pbonly(...):",err_str)

        err = self.pbInstPbonly(0xFFFFFF,STOP,0,1e10)
        if err<0:
           err_str = self.getErr()
           print( "error in pb_inst_pbonly(...):",err_str)

        err = self.pb_stop_programming()
        if err<0:
           err_str = self.getErr()
           print ("error in pb_stop_programming():",err_str)

        err = self.pb_start()
        if err<0:
           err_str = self.getErr()
           print ("error in pb_start():",err_str)
    def writeInstructions(self,instructions):
        err = self.startProgr(PULSE_PROGRAM)
        if err<0:
           err_str = self.getErr()
           print( "error in startProgr(PULSE_PROGRAM):",err_str)

        for instr in instructions:
            self.writeSingleInstruction(*instr)

        err = self.pb_stop_programming()
        if err<0:
           err_str = self.getErr()
           print ("error in pb_stop_programming():",err_str)
    def writeSingleInstruction(self,*args):
        err = self.pbInstPbonly(*args)
        if err<0:
           err_str = self.getErr()
           print( "error in pb_inst_pbonly(...):",err_str)
           print( "last instruction:",args)
        return err

if __name__ =="__main__":
    pb = PulseBlaster()
    #ctypes.c_int(int(1e9*1.e-5))
    print (pb.open())
    #print pb.pb_sleep_ms(1000)
    #print pb.pb_core_clock(ctypes.c_double(200.))
    # print pb.getErr()
    pb.call("pb_core_clock",ctypes.c_double(200.))
    pb.loadppg([(0,1.e-6),(0x1,1.e-6),(0x2,1.e-6),(0x4,1.e-6),(0x10,1.e-6),(0,1.e-6)])
    pb.pb_start()

    #time.sleep(10)
    #print pb.pb_stop()
    #print pb.close()
    #pb.allON()
    #time.sleep(5)
    #pb.stop()