#-------------------------------------------------------------------------------
# Name:
# Purpose: dds1
#
# Author:      Daniel Tolmachev
#
# Created:     14.12.2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#



from instrument_base import InstrumentBase
import numpy as np
import time

DDS_MIN_STEP = 100. #Hz min  is .25
FSYS = 300.e6
FREQUENCY_RESOLUTION = 48 #bits
GPIB_ADDR_SELECT_STR = "++addr"

def hex2freq(hex_str):
    return float(int(hex_str,16))/2**FREQUENCY_RESOLUTION*FSYS

registers = { "C": ("frequency1",48,hex2freq) }


class e3DDS1_Class(InstrumentBase):
    OK = 'OK \n\r'
    # amplitude = 1
    # amplitudeS = '1000'
    # phase = 0
    # phaseS = '0000'
    # frequency = 0
    # frequencyS = ''
    #fsys=1e9
    fsys = FSYS
    dt_min = 4*1/fsys
    onOpenCallback = None
    # profile = None
    registers_list = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"]
    # register_names = {  "CFR1":0x00,
    #                     "CFR2":0x01,
    #                     "CFR3":0x02,
    #                     "POW":0x08,
    #                     "ASF":0x09,
    #                     "Digital Ramp Limit":0x0B,
    #                     "Digital Ramp Step Size":0x0C,
    #                     "Digital Ramp Rate":0x0D,
    #                     "Profile 0":0x0E,
    #                     "Profile 1":0x0F,
    #                     "Profile 2":0x10,
    #                     "Profile 3":0x11  }
    # cfr = {"SDIO input only":1,
    #         "Internal profile control":(17,21),
    #         "OSK enable":9,
    #         "Select auto OSK":8,
    #         'Clear DRG accumulator':12,
    #         "RAM enable":31}
    # cfr2 = {"Enable amplitude scale from single tone profiles":24,
    #         'Internal I/O update active':23,
    #         "SYNC_CLK enable":22,
    #         "DRG Enable":19,
    #         "DRG no dwell high":18,
    #         "DRG no dwell low":17,
    #         "Sync timing validation disable":5
    #         }
    # registers = [cfr, cfr2, None, None,
    #              None, None, None, None,
    #              None, None, None, None,
    #              None, None, None, None,
    #              None, None, None, None, None, None,None,None]
    # dstatus = { "active profile":(0,3)}
    # laststatus = {}
#     def verbal_output(self,dic,bs):
# ##        print bs
#         sz = len(bs)
#         ret = {}
#         for k,v in dic.iteritems():
#             if type(v)==int:
#                 v2 = bool(int(bs[sz-v-1]))
#                 ret[k] = v2
#             elif len(v)==1:
#                 v2 = bool(int(bs[sz-v[0]-1]))
#                 ret[k] = v2
#             elif len(v)>1:
#                 s = bs[sz-v[1]:sz-v[0]]
#                 if len(v)>2:
#                     ret[k] = v[2](s)
#                 else:
#                     ret[k] = int(s,2)
#         self.laststatus = ret
#         return ret
    def __init__(self,*args,**kwargs):
        super(e3DDS1_Class,self).__init__(*args,**kwargs)
        # self.setFreq = self.setFrequency
        # self.queryFreq = self.queryFrequency
        # self.singleton_info = {"frequency":(0,32,self.freq_from_bin),
        #                 "phase":(32,48),
        #                 "amplitude":(48,62)}

        self.gpib_single_device = kwargs.get("gbib_single_device",True)
        self.gpib_addr = kwargs.get("gpib",27)
        self.autoupdate = kwargs.get("autoupdate",True)

        self.dev_open = self.open
        self.open = self.open1
        self.setClockinternal()
        self.enableIntAmpControl()

    def open1(self,*args,**kwargs):
        #super(e3DDS_Class,self).open(*args,**kwargs)
        self.dev_open(*args,**kwargs)
        if self.gpib_single_device:
            self.write(GPIB_ADDR_SELECT_STR+str(self.gpib_addr)+"\r")
##        self.status()
##        if self.profile!=None:
##            self.amplitude,self.phase,self.frequency = self.getProfile(self.profile)
##            print "frequency = {:g} Hz\namplitude = {:2f}\n".format(self.frequency,self.amplitude)
    # def set(self):
    #     msg = 'N{}W{}{}{}:'.format(self.address,self.amplitudeS,self.phaseS,self.frequencyS)
    #     return self.ask(msg)
    #     #print self.ask('P1:')
    # def setAmplitude(self,amp,profile = -1):
    #     if profile<0:
    #         profile = self.profile
    #     # self.amplitudeS = self.amp2hex(amp)
    #     # regN = self.single_tone_reg(profile)
    #     # msg = 'N{}W0000{}:'.format('09',self.amplitudeS)
    #     #ret = self.writeRegister(regN,self.amplitudeS + self.phaseS + self.frequencyS)
    #     # ret = self.writeRegister(9,self.phaseS + self.amplitudeS)
    #     #rep = self.ask(msg)
    #     #rep += self.ask('P1:')
    #     # ret = self.setProfile(0)
    #     self.setProfile(profile,amp,self.phase,self.frequency)
    #     self.amplitude = amp
    #     # return ret
    # def setAmpCommon(self,amp):
    #     self.amplitudeS = self.amp2hex(amp)
    #     ret = self.writeRegister(9,self.phaseS + self.amplitudeS)
    #     self.amplitude = amp
    def freq2hex(self,freq):
        return "{:012X}".format(int(round((2**FREQUENCY_RESOLUTION)*(freq/self.fsys))))
    def freq_from_hex(self,freqS):
        return int(freqS,16)*self.fsys/2**32
    def freq_from_bin(self,freqS):
        return int(freqS,2)*self.fsys/2**32
##    def single_tone_reg(self,profile):
##        return 0x0E+profile
    def setFrequency(self,freq):
        self.writeRegister("C",self.freq2hex(freq))
        self.frequency = freq
    def setFrequency1(self,freq):
        self.writeRegister("D",self.freq2hex(freq))
        self.frequency1 = freq
        # return ret
##    def setAmplitude(self,amp):
##        self.ask("N00W00080202:")
##        self.ask("N01W80000000:")
##        self.amplitudeS="{:04X}".format(int(amp))
##        self.set()
##        ret = self.ask('P1:')
##        self.amplitude = amp
##        print self
##        return ret
    # def setPhase(self,phas):
    #     self.phaseS="{:04X}".format(int(phas))
    #     self.set()
    #     ret = self.ask('P1:')
    #     self.phase = phas
    #     print self.phaseS
    #     return ret
    def enableIntAmpControl(self):
        self.writeRegister("K","20")
    def setAmplitude(self,amplitude):
        self.setAmpI(amp)
        self.setAmpQ(amp)
    def setAmpI(self,amp):
        self.amplitude = amp
        self.writeRegister("L",self.amp2hex(amp))
    def setAmpQ(self,amp):
        self.writeRegister("M",self.amp2hex(amp))
    def reset(self):
        self.ask("S:")
    def status(self):
        return [self.readRegister(reg) for reg in self.registers_list]
    # def get(self,profile):
    #     regN = self.single_tone_reg(profile)
    #     rep = self.ask("N{}R:".format(self.address))
    #     if rep:
    #         amp = int(rep[0:4],16)
    #         phas = int(rep[4:8],16)
    #         freq = float.fromhex(rep[8:16])
    #         self.frequency = round(freq/2**32*self.fsys)
    #         print amp,phas,freq
    #         print "frequency = {:g} Hz".format(self.frequency)
    #     return rep
    def queryFrequency(self):
        if not self.frequency:
            if not self.profile:
                self.status()
            self.amplitude,self.phase,self.frequency = self.getProfile(self.profile)


##        self.get()
        # if
        #
        # rep = self.readRegister(0x0E)
        # freq = float.fromhex(rep)
        # self.frequency = round(freq/2**32*self.fsys)
        return self.frequency

    # def open(self):
    #     print "aopo"
    #     super(e3DDS_Class,self)._open()
    #     if self.interface.connected:
    #         if self.onOpenCallback:
    #             self.onOpenCallback()
    #         else:
    #             print "connection established but no callback provided"
    # def prepRegisterString(self,register):
    #     if type(register)==str:
    #         if register in self.register_names:
    #             register = self.register_names[register]
    #     if type(register)==int:
    #         register = "{:02X}".format(register)
    #     return register
    def setClockinternal(self):
        self.write("X5:\n")
    def setMultiplierMax(self):
        self.setbit("I",0,"1111")
    def writeRegister(self,register,data,verbose = True):
        msg = "{}{}:".format(register,data)
        if self.autoupdate:
            msg +="U:"
        if not self.gpib_single_device:
            msg = GPIB_ADDR_SELECT_STR+str(self.gpib_addr)+"\r"+msg
        self.write(msg+"\n")
        if verbose:
            print("{}<<{}".format(register,msg))
    def readRegister(self,register,verbose = True):
        rep = self.ask("R{}:\n".format(register))
        if verbose and rep:
            print(register,">>",rep.strip())
            if register in registers:
                if type(registers[register])!=list:
                    regl = [registers[register]]
                else:
                    regl = registers[register]
            cnt = 0
##            for i,it in enumerate(regl):
##                if type(it)==str:
##                    sz = 1
####                    print it,
##                if type(it)==tuple:
##                    name = it[0]
##                    sz = it[1]

            # print self.bin_repr_raw(self.hex2bin(rep.strip()))
            # if self.registers[register]:
            #     print self.verbal_output(self.registers[register],self.hex2bin(rep.strip()))
        return rep
#     def getProfile(self,profile):
#         regN = self.single_tone_reg(profile)
#         ret = self.readRegister(regN)
#         retB = self.hex2bin(ret)
#         self.verbal_output(self.singleton_info,retB)
#         freq = self.laststatus.get("frequency",None)
#         amp = self.laststatus.get("amplitude",None)
#         phas = self.laststatus.get("phase",None)
#         return self.ampfromint(amp),phas,freq
#     def setProfile(self,profile,amp,phas,freq):
#         ampS = self.amp2hex(amp)
#         phaS = "0000" #self.p2hex(phas)
#         freqS = self.freq2hex(freq)
#         regN = self.single_tone_reg(profile)
#         return self.writeRegister(regN,ampS+phaS+freqS)
#     def setRAMprofile(self,profile,startaddress,endaddress,rate,
#                          nodwell = True,
#                          mode = 3,
#                          zero_crossing = False):
#         print "setting up RAM profile", profile
#         if mode==0:
#             print "direct switch"
#         elif mode==1:
#             print "ramp up"
#         elif mode==2:
#             print "bidirectional ramp"
#         elif mode==3:
#             print "continuous bidirectional ramp"
#         elif mode==4:
#             print "continuous recirculate"
#         print "no dwell = {}".format(nodwell)
#         msg = "00000000"+"{:016b}".format(rate) +\
#             "{:010b}".format(endaddress)+\
#             "000000"+"{:010b}".format(startaddress)+\
#             "0"+"{:b}".format(nodwell)+"00000000"+\
#             "{:b}".format(zero_crossing)+ "{:03b}".format(mode)
#         msg2 = int(msg,2)
#         msg3 = "{:016X}".format(msg2)
#         regN = self.single_tone_reg(profile)
#         #startaddress = startaddress byte
#         print msg,">>"
#         print "{:064b}".format(msg2)
#         print msg3
#         return self.writeRegister(regN,msg3)
#     def selectProfile(self,profile_number):
#         ret = self.ask("P{}:".format(profile_number))
#         if ret == self.OK:
#             self.profile = profile_number
#         else:
#             self.status()
#         return ret
#     def binformat(self,hex_str):
#         l = len(hex_str.strip())
#         ret = ""
#         for i in range(0,l,2):
#             ret+= "{:08b}\n".format(int(hex_str[i:i+2],16))
#         return ret
#     def setupSweepRegister(self,freq_max,freq_min,stepup,stepdn,dtup,dtdn):
#         if freq_max < freq_min:
#             freq_max,freq_min = freq_min,freq_max
#         self.writeRegister(0x0b,self.freq2hex(freq_max)+self.freq2hex(freq_min))
#         self.writeRegister(0x0c,self.freq2hex(stepup)+self.freq2hex(stepdn))
#         self.writeRegister(0x0d,"{:04X}{:04X}".format(dtup,dtdn))
#         nstepup = (freq_max-freq_min)/stepup
#         nstepdn = (freq_max-freq_min)/stepdn
#         timeup = nstepup*dtup*self.dt_min
#         timedn = nstepdn*dtdn*self.dt_min
#         print "DDS3 Sweep setup:"
#         print "from {} to {}".format(freq_min,freq_max)
#         print "-> {} points {}s".format(nstepup,timeup)
#         print "<- {} points {}s".format(nstepdn,timedn)
#     def setupSweep(self,freq_max,freq_min,stepup,stepdn,dtup,dtdn,direction = 'up'):
#         self.setupSweepRegister(freq_max,freq_min,stepup,stepdn,dtup,dtdn)
#         print "sweeping",direction
#         self.writeRegister(0x00,"00001002") # reset DRG accum
#         self.writeRegister(0x01,"00480020") #defaults + DRG enable
#                                             #(both no-dwells disabled)
#         if direction == "up":
#             self.ask("K1:")
#     def startSweep(self):
#         self.writeRegister(0,"00000002")
#         return time.time()
#     def calcSweep(self,start,stop,time):
#         rng = abs(float(stop)-start)
#         speed = rng/time
#         step = DDS_MIN_STEP
#         dt = 1
#         if step/dt/self.dt_min< speed:
#             step = speed*dt*self.dt_min
#         else:
#             dt = int(round(step/speed/self.dt_min))
#             if dt>65535:
#                 dt = 65535
#                 step = speed*dt*self.dt_min
#         return step,dt
#     def startSingleTone(self,*profile):
#         if profile == ():
#             profile = 0
#         else:
#             profile = profile[0]
# ##        self.writeRegister(0,'00080202')
# ##        self.writeRegister(01,'80000000')
#         self.writeRegister(0,'00080002')
#         self.writeRegister(01,'01400020')
#         #self.setProfile(profile,1,0,1e6)
    def amp2hex(self,amp):
        if amp>1:
            amp = 1
        elif amp<0:
            amp = 0
        amp = int(16383*amp)
        return "{:04X}".format(amp)
    def ampfromint(self,amp):
        return amp/16383.
    def hex2bin(self,hx):
        sz = len(hx)
        return "{:0{}b}".format(int(hx,16),sz*4)
    def bin2hex(self,bs):
        sz = len(bs)
        return "{:0{}X}".format(int(bs,2),sz/4)
    def bin_repr_raw(self,bs):
        return " ".join([bs[i:i+4] for i in range(0,len(bs),4)])
    def setbit(self,regN,addr,val):
        rep = self.readRegister(regN).strip()
        sz = len(rep)
        bits = self.hex2bin(rep)
        szb = len(bits)
        if type(val)==bool:
            val = int(val)
        if type(val) == int:
            if val:
                val = 1
##            if addr<len(bits)-1:
            bits2 = bits[0:szb-addr-1]+ str(val)+ bits[szb-addr:]
##            else:
##                bits2 = bits[0:addr]+ str(val)+ bits[addr+1:]
        if type(val)==str:
            szv = len(val)
            bits2 = bits[0:szb-addr-szv]+ val+ bits[szb-addr:]
        newstr = self.bin2hex(bits2)
        #print self.bin_repr_raw(bits),">"
        print(self.bin_repr_raw(bits2),">",newstr)
        self.writeRegister(regN,newstr)
        if self.autoupdate:
            self.readRegister(regN)
    def update(self):
        return self.ask("U:")
    def setupMultiplier(self,mult = "default"):
        if mult=="default":
            self.write("I4F:")

#     def setupSingleTone(self):
#         self.setAmpFromProfile(True)
#         self.setOSK(False)
#     def setAmpFromProfile(self,enable):
#         self.setbit(1,24,enable)
#     def setOSK(self,enable):
#         self.setbit(0,9,enable)
#     def setDRG(self,enable):
#         self.setbit(1,19,enable)
#     def setClearDrgAccum(self,enable):
#         self.setbit(0,12,enable)
#     def setNoDwellHigh(self,enable):
#         self.setbit(1,18,enable)
#     def setNoDwellLow(self,enable):
#         self.setbit(1,17,enable)
#     def setupSweepRegisterQuick(self,start,stop,time):
#         step,dt = self.calcSweep(start,stop,time)
#         self.setupSweepRegister(stop,start,step,step,dt,dt)
#     def enableRAM(self,state):
#         self.setbit(0,31,state)
#     def setRamDestination(self,dest = "amplitude"):
#         bits = ""
#         dest = dest.lower()
#         if dest =="frequency":
#             bits = "00"
#             self.RAMdest = 0
#         elif dest =="phase":
#             bits = "01"
#             self.RAMdest = 1
#         elif dest =="polar":
#             bits = "11"
#             self.RAMdest = 3
#         else:
#             dest = "amplitude"
#             bits = "10"
#             self.RAMdest = 2
#         if bits:
#             print "setting RAM destination to",dest
#             self.setbit(0,29,bits)
#     def write2RAM(self,val):
#         if self.RAMdest==2:
#             self.writeRegister(0x16,"0000"+ self.amp2hex(val))
#     def setFreqCommon(self,freq):
#         return self.writeRegister(0x07,self.freq2hex(freq))
#     def getFreqCommon(self):
#         ret = self.readRegister(0x07)
#         return self.freq_from_hex(ret)
#     def Ramtest1(self):
#         start = 0
#         end = 10
#         self.setRamDestination(dest = "amplitude")
#         self.setFreqCommon(10e6)
#         self.setRAMprofile(0,start,end,250)
#         self.selectProfile(0)
#         amps = np.linspace(1,0,end)
#         for a in amps:
#             print a
#             self.write2RAM(a)
# ##        for i in xrange(start,end+1):
# ##            if i%2:
# ##                self.write2RAM(1)
# ##            else:
# ##                self.write2RAM(0)
#         self.update()
#     def Ramtest2(self):
#         start = 0
#         end = 100
#         self.setRamDestination(dest = "amplitude")
#         self.setFreqCommon(10e6)
#         self.setRAMprofile(0,start,end,250)
#         self.selectProfile(0)
#         amps = np.linspace(1,0,end)
# ##        for a in amps:
# ##            print a
# ##            self.write2RAM(a)
#         for i in xrange(start,end):
#             if i%2:
#                 self.write2RAM(1)
#             else:
#                 self.write2RAM(0)
#             time.sleep(0.2)
#         #self.update()
#     def loadRam(self,points,timeout = 0.1):
#         for i in xrange(0,points):
#             if i%2:
#                 self.write2RAM(1)
#             else:
#                 self.write2RAM(0)
#             time.sleep(timeout)
#     def RAMstart(self):
#         self.enableRAM(True)
#         self.update()
#     def RAMstop(self):
#         self.enableRAM(False)
#         self.update()



##    def ph2hex(self,phas_degrees):
##        if amp>1:
##            amp = 1
##        elif amp<0:
##            amp = 0
##        amp = int(65535*amp)
##        return "{:04X}".format(amp)
# def magic(freq = 1.3e6, amp = 1):
# ##    if freq==():
# ##        freq = 1.3e6
# ##    else:
# ##        freq = freq[0]
#     #dds.writeRegister(0,'00080202')
#     dds.writeRegister(0,'00000202') #OSk True, Manual
#     #dds.writeRegister(01,'80000000')
#     dds.writeRegister(01,'00000000')
#     #dds.writeRegister(9,'00003334')
#     dds.writeRegister(9,'0000{}'.format(dds.amp2hex(amp)))
#     dds.writeRegister(0x11,'00000000'+dds.freq2hex(freq))
#     dds.ask("P3:")

if __name__ == '__main__':
    import time
    #addr = ("129.217.155.106",1234)
    addr = ("129.217.155.3",1234)
    gpibaddr = 27
    #gpibaddr = 28

    dds = e3DDS1_Class(addr,gpib= gpibaddr,timeout = 1,gpib_single_device = True)
    dds.open()
    #dds.status()
    #dds.startSingleTone()
##    dds.setupSweep(1e6,11e6,.25,.25,1,1)
    dds.setClockinternal()
    dds.write("I4F:K:")
    #dds.readRegister(0)
    #magic()
##    dds.startSweep()
##    dds.reset()
##    dds.status()
##    dds.getProfile(2)
##    dds.readRegister(1)
##    magic()
##    dds.setAmpFromProfile(True)

