from PyQt4 import uic
import os
#uic.compileUi('py_serial.ui','py_serial_ui.py',False,4,False,False,'_rc')
#uic.compileUiDir('.',False,None)
dl = os.listdir('.')
for fname in dl:
    if fname.endswith('.ui'):
        py_name = fname[:-2]+'py'
##    elif fname.endswith('.qrc'):
##        py_name = fname[:-4]+'_rc.py'
    else:
        continue
    if os.path.exists(py_name):
        if os.path.getmtime(fname)<= os.path.getmtime(py_name):
            continue
    f = open(py_name,'w')
    uic.compileUi(fname,f,resource_suffix='_rc')
    print('%s complied to %s' % (fname,py_name))
    f.close()

