import re

class DDS_UTILITY(object):
    register_description = {}
    def loadRegistersDescription(self,filename):
            with open(filename) as f:
                lines = f.readlines()
                key = ""
                bytes_descr = []
                for l in lines:
                    words = l.split()
                    if l[0] not in ["\t"," "] and words:
                        if len(words)>0:
                            key = words[0]
                            self.register_description[key] = {"bytes":[]}
                            if len(words)>1:
                                self.register_description[key]["name"] = words[1]
                            if len(words)>2:
                                self.register_description[key]["long_name"] = words[2]

                    else:
                        #print words
                        if len(words)>1 and key:
                            m = re.search("\d+",l)
                            if m:
                                address = int(m.group())
                                pos = m.end()
                                m = re.search(":(\d+)",l[pos:])
                                if m:
                                    end_address = int(m.groups()[0])
                                    pos = m.end()
                                    sz = end_address-address
                                else:
                                    sz = 1
                                m = re.search("[\"'](.*)[\"']",l[pos:])
                                mf = re.findall("[\"'](.*?)[\"']",l[pos:])
                                descr_str = descr_long = ""
                                if len(mf)>0:
                                    descr_str = mf[0]
                                else:
                                    if "frequency" in l[pos:].lower():
                                        descr_str = "frequency"
                                    elif "amplitude" in l[pos:].lower():
                                        descr_str = "amplitude"
                                    elif "phase" in l[pos:].lower():
                                        descr_str = "phase"
                                if len(mf)>1:
                                    descr_long = mf[1]

    ##                            if m:
    ##                                desc_str = m.groups()[0]
    ##                            else:
    ##                                desc_str = ""
                            self.register_description[key]["bytes"].append((address,sz,descr_str,descr_long))
                keys = list(self.register_description)
                newkeys = []
                try:
                    newkeys = [int(key) for key in keys]
                except ValueError:
                    try:
                        newkeys = [int(key,16) for key in keys]
                    except ValueError:
                        pass
                if newkeys:
                    for newkey,oldkey in zip(newkeys,keys):
                        self.register_description[newkey] = self.register_description.pop(oldkey)
            return self.register_description

dds_utility = DDS_UTILITY()