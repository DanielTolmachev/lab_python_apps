#-------------------------------------------------------------------------------
# Name:     dds1_class.py
# Purpose:  device class for DDS1 and DDS2 generators made for e3.physik.tu-dortmund.de
#
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------

__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"
GPIB_ADDR_SELECT_STR = "++addr" # change gpib address with Prologix GPIB-Ethernet daptor
GPIB_SINGLE_DEVICE = True  # if false, gpib address will be selected each time
GPIB_DEFAULT_ADDRESS = 27

import dds_common_class
import AD9854


class DDS1_dev_class(dds_common_class.DDS_Common_Class):
    register_description_file = "AD9854.txt"
    register_description = AD9854.register_description
    resolution_amplitude = 12
    # resolution_phase = 16
    resolution_frequency = 48
    fsys = 3e8
    def __init__(self,*args,**kwargs):
        super(DDS1_dev_class,self).__init__(*args,**kwargs)
        #self.loadRegistersDescription(self.register_description_file)
        self.gpib_single_device = kwargs.get("gbib_single_device", GPIB_SINGLE_DEVICE)
        self.gpib_address = kwargs.get("gpib_address",GPIB_DEFAULT_ADDRESS)

        #registers = { "C": ("frequency1",48,self.) }
        self.setFreq = self.setFrequency  #these are just shortcuts
        self.queryFreq = self.queryFrequency #these are just shortcuts
    def open(self):
        super(DDS1_dev_class,self).open()
        #IMPORTANT: this is applied after connection to device
        self.gpib_address_select()
        self.setOSK(True)
        self.setClockinternal()
        self.setupMultiplier()
    def gpib_address_select(self):
        if self.gpib_address:
            self.write(GPIB_ADDR_SELECT_STR+str(self.gpib_address)+"\r")
    def writeRegister(self,register,data,verbose = True):
        msg = "{}{}:".format(register,data)
        if self.autoupdate:
            msg +="U:"
        if not self.gpib_single_device:
            msg = GPIB_ADDR_SELECT_STR+str(self.gpib_addr)+"\r"+msg
        self.write(msg+"\n")
        if verbose:
            print( "{}<<{}".format(register,msg))
    def readRegister(self,register):
        rep = self.ask("R{}:\n".format(register))
        if self.verbose and rep:
            print( register,">>",rep.strip())
            # if register in registers:
            #     if type(registers[register])!=list:
            #         regl = [registers[register]]
            #     else:
            #         regl = registers[register]
            cnt = 0
        return rep
    def setFrequency(self,freq):
        self.writeRegister("C",self.freq2hex(freq))
        self.frequency = freq
    def setFrequency1(self,freq):
        self.writeRegister("D",self.freq2hex(freq))
        self.frequency1 = freq
    def queryFrequency(self):
        rep = self.readRegister("C")
        try:
            i = int(rep,16)
            return self.int2freq(i)
        except:
            return ""
    def queryFrequency1(self):
        rep = self.readRegister("D")
        return self.hex2freq(rep)
    def setAmplitude(self,amp):
        self.setAmpI(amp)
        self.setAmpQ(amp)
    def setAmpI(self, amp):
        self.writeRegister("L", self.amp2hex(amp))
    def setAmpQ(self, amp):
        self.writeRegister("M", self.amp2hex(amp))
    def reset(self):
        self.ask("S:")
    def setClockinternal(self):
        self.write("X5:\n")
    def setMultiplierMax(self):
        self.setbit("I",0,"1111")
    def setupMultiplier(self,mult = "default"):
        if mult=="default":
            self.write("I4F:")
    def setOSK(self,enable):
        self.setbit("K", 5, enable)
    def status(self):
        for reg in sorted(self.register_description.keys()):
            self.register_data[reg] = self.readRegister(reg)
        return self.register_data
    def status_print(self):
        [print(reg,":",self.readRegister(reg)) for reg in sorted(self.register_description.keys())]