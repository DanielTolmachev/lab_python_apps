from dds3E_dev_class import DDS3E_Class
import nucs

if __name__ == '__main__':
    import time
    addr = ("129.217.155.87",51234)
    dds = DDS3E_Class(addr,timeout = 1)
    dds.open()
    dds.autoupdate = True
    ranges = [(1e6, 2e6),(9e6, 10e6)]
    #ranges = [(1e6, 1e8)]
    arr = nucs.maketable(ranges)
    tabl = [dds.freq2hex(f) for f in arr]
    dds.readRegister(0)
    dds.print_register_info(0)
    dds.readRegister(14)
    dds.print_register_info(14)
    dds.RAMstart()
    dds.setRAMprofile(2,0,1023,65535)
    dds.RAMstop()
    dds.selectProfile(2)
    dds.setRamDestination("frequency")
    dds.loadRam(tabl)
    dds.RAMstart()
