__author__ = 'SFB'

import sys,traceback
sys.path.append( '../modules')
from qtpy import QtGui,QtCore,QtWidgets
from register_view_tablewidget import Ui_Register_VIew_Form

from dds_dev_class import e3DDS_Class
import si_units

# class mymodel(QtGui.QStandardItemModel):
#     def __init__(self,*args):
#         super(mymodel,self).__init__(*args)
#         self.lbl = QtWidgets.QLabel("boo")
#
#
#     def data(self,model,role):
#          print model,role

# class Mydelegate(QtWidgets.QItemDelegate):
#     def __init__(self):
#         super(Mydelegate,self).__init__()

        # self.setText(str(args))
FREQUENCY = 1
AMPLITUDE = 2
PHASE = 3
INTEGER = 4

AUTO_APPLY = True

UNITS = {FREQUENCY:"Hz",
         AMPLITUDE:"",
         PHASE:"deg",
         INTEGER:""}



class RegisterCellWidget (QtWidgets.QWidget):
    max_width = 0
    max_height = 0
    bit_toggled = QtCore.Signal(object)
    cellDataChanged = QtCore.Signal(object)
    def __init__(self,row, col, bytestring, title = "",mode = 0,float2int = None,int2float = None):
        super(RegisterCellWidget,self).__init__()
        self.cellSpan =  len(bytestring)
        if not self.cellSpan % 8:
            self.showhex = True
        else:
            self.showhex = False
        self.row = row
        self.col = col
        self.mode = mode
        self.int2float = int2float
        self.float2int = float2int
        self.title = title
        # self.validator = validator
        if self.cellSpan ==1:
            self.value = bool(int(bytestring))
            bytestring = str(self.value)
        elif self.mode or self.showhex:
            self.value = int(bytestring,2)
        else:
            self.value = bytestring


        self.gl = QtWidgets.QGridLayout()
        #self.gl.setMargin(0)
        #self.gl.setContentsMargins(0,0,0,0)
        self.gl.setSpacing(0)
        self.setLayout(self.gl)
        self.te = QtWidgets.QTextEdit()
        self.te.setReadOnly(True)
        self.te.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        #self.te.setText(title)
        self.te.setFrameShape(QtWidgets.QFrame.NoFrame)
        # self.te.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.te.mouseDoubleClickEvent = self.mouseDoubleClickEvent
        col = self.palette().color(self.backgroundRole())
        pal = QtGui.QPalette()
        pal.setColor(QtGui.QPalette.Base,col)
        #self.te.setPalette(pal)
        self.teb = QtWidgets.QLineEdit()
        self.teb.setReadOnly(True)
        self.setText()
        self.teb.setFrame(False)
        self.teb.mouseDoubleClickEvent = self.mouseDoubleClickEvent
        #self.teb.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.teb.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl = QtWidgets.QLabel(bytestring)
        self.lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.gl.addWidget(self.te)
        #self.gl.addWidget(self.lbl)
        self.gl.addWidget(self.teb)
        self.te.setContentsMargins(0,0,0,0)
        w,h = self.detectGeometry()
        self.max_width = max(self.max_width,w)
        self.max_height = max(self.max_height,h)
        self.le = QtWidgets.QLineEdit(bytestring)
        self.le.hide()
        self.gl.addWidget(self.le,0,0,2,1)
        self.le.returnPressed.connect(self.exitEditMode)


    def detectGeometry(self):
        #fm = self.te.fontMetrics()
        #print fm
        return 0,0
    def mouseDoubleClickEvent(self,evt):
        if self.cellSpan ==1:
            self.toggle()
        else:
            self.goEditMode()
    def on_double_click(self):
        self.goEditMode()

    def goEditMode(self):
        self.teb.hide()
        self.te.hide()
        if self.mode == INTEGER:
            self.le.setText(str(self.value))
        elif self.mode and self.int2float:
            self.le.setText(str(self.int2float(self.value)))
        else:
            self.le.setText(str(self.value))
        self.le.show()
    def exitEditMode(self):
        self.le.hide()
        # if self.validator:
        #     self.value,fmt_str,hex_string = self.validator(self.le.text)
        # else:
        self.value = self.validate(self.le.text())
        self.cellDataChanged.emit((self.row,self.col,self.value,self.cellSpan))
        self.setText()
        self.teb.show()
        self.te.show()
    def setText(self):
        if self.mode and self.mode==INTEGER:
            self.teb.setText("{:0{}X}".format(self.value,self.cellSpan/4))
            self.te.setText(self.title + str(self.value))
        elif self.mode:
            self.teb.setText("{:0{}X}".format(self.value,int(self.cellSpan/4)))
            self.valueF = self.int2float(self.value)
            self.te.setText(self.title + si_units.si_str(self.valueF,UNITS[self.mode]))
        elif self.showhex:
            self.teb.setText("{:0{}X}".format(self.value,int(self.cellSpan/4)))
            self.te.setText(self.title)
        else:
            self.teb.setText(str(self.value))
            self.te.setText(self.title)
        self.te.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
    def validate(self,text):
        try:
            if self.mode:
                self.value = self.float2int(float(text))
                return self.value
            else:
                text = str(text)
                res = int(text,2)
        except:
            try:
                res = int(text,16)
            except:
                return self.value
        return "{:0{}b}".format(res,self.cellSpan)
    def toggle(self):
        self.value = not self.value
        self.teb.setText(str(self.value))
        self.bit_toggled.emit((self.row,self.col,self.value))

class REGISTER_VIEW(QtWidgets.QWidget,Ui_Register_VIew_Form):
    dataChanged = QtCore.Signal(object)
    rowheight  = 110
    colwidth = 110
    UNUSED_REGISTER_TEXT = "Open"
    def __init__(self,parent,registerNum):
        super(REGISTER_VIEW,self).__init__()
        self.setupUi(self)
        self.dds = parent.dds
        self.regdesc = self.dds.register_description[registerNum]
        self.fsys = self.dds.fsys
        self.bytedescdict = {}
        self.mergecells = {}
        self.le_regdata.returnPressed.connect(self.checkupdate)
        self.table.doubleClicked.connect(self.toggle)
        #self.buttonBox.clicked.connect(self.on_btn_clicked())
        # self.dds_class = dds_class
        title = ""
        if self.regdesc:
            if "long_name" in self.regdesc:
                title = "{} ({})".format(self.regdesc["long_name"],self.regdesc["name"])
            elif "name" in self.regdesc:
                title = format(self.regdesc["name"])
            if "bytes" in self.regdesc:
                try:
                    for addr,sz,desc,_ in self.regdesc["bytes"]:
                        if sz == 1:
                            self.bytedescdict[addr] = desc
                        elif sz>1:
                            self.bytedescdict[addr+sz-1] = desc
                            self.mergecells[addr+sz-1] = sz
                except:
                    print('error while parsing "bytes" in registers description',self.regdesc["bytes"])
        #if title:
        self.lbl_register.setText(title)
        self.tablew = []
        self.tableSpan = []
    def toggle(self,modelindex):
        #print modelindex
        r = modelindex.row()
        c = modelindex.column()
        nd = self.bitlen-1 - c-r*8
        self.newdatai = self.newdatai ^ 1 << nd
        print(self.datai,">>",self.newdatai)
        data = str((self.newdatai >> nd) & 1)
        self.setTableItem(r,c,data)
        self.newregisterdata(self.newdatai)
    def validateFreq(self,freq):
        if freq> self.fsys:
            freq = self.fsys
            hx = e3DDS_Class.freq2hex(freq)
        return freq,hx
    def validateAmp(self,freq):
        return freq,freq,bytes
    def validatePhas(self,freq):
        return freq,freq,bytes
    def setTableItem(self,r,c,*data):
        #index = self.model.index(r,c,QtCore.QModelIndex())
        #self.model.setData(index,data)
        n = c+r*8
        nd = self.bitlen-1-n
        if data:
            b = data[0]
        else:
            if nd in self.mergecells:
                b=self.datab[n:n+self.mergecells[nd]]
            else:
                b = self.datab[n]
        if nd in self.mergecells:
            cells = self.mergecells[nd]
            rowspan = 1
            if not cells%8:
                rowspan = cells/8
            #self.table.setSpan(r,c,rowspan,cells)
        bitlen = len(b)
        s_desc = ""
        mode = 0
        if self.bytedescdict and nd in self.bytedescdict:
            if bitlen>7:
                if "frequency" == self.bytedescdict[nd]:
                    #freq = float(int(b,2))/2**bitlen*self.fsys
                    #s_desc += "{:g}".format(freq)
                    s_desc += "Frequency: "
                    #mode = self.validateFreq
                    mode = FREQUENCY
                    float2int = self.dds.freq2int
                    int2float = self.dds.int2freq
                elif "amplitude" == self.bytedescdict[nd]:
                    #amp = float(int(b,2))/2**bitlen
                    #s_desc += "{:g}".format(amp)
                    s_desc += "Amplitude: "
                    # mode = self.validateAmp
                    mode = AMPLITUDE
                    float2int = self.dds.amp2int
                    int2float = self.dds.int2amp
                elif "phase" == self.bytedescdict[nd]:
                    #phase = float(int(b,2))/2**bitlen
                    #s_desc += "{:g}".format(phase)
                    s_desc+="Phase: "
                    # mode = self.validatePhas
                    mode = PHASE
                    float2int = self.dds.phase2int
                    int2float = self.dds.int2phase
                elif "integer" == self.bytedescdict[nd]:
                    mode = INTEGER
            else:
                s_desc += "{}".format(self.bytedescdict[nd])
        #self.model.setData(index,b)
        #it = QtWidgets.QTableWidgetItem()
        #self.table.setItem(r,c,it)
        #self.table.widget
        if self.bytedescdict and nd in self.bytedescdict and self.bytedescdict[nd] is None:
            cw = QtWidgets.QLabel(self.UNUSED_REGISTER_TEXT)
            cw.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        else:
            if mode:
                cw = RegisterCellWidget(r,c,b,s_desc,mode,float2int,int2float)
            else:
                cw = RegisterCellWidget(r,c,b,s_desc,mode)
            cw.bit_toggled.connect(self.on_bit_toggled)
            cw.cellDataChanged.connect(self.on_cellDataChanged)
        #print "adding",cw,"to",r,c
        self.table.setCellWidget(r,c,cw)
        return cw,bitlen
    def on_bit_toggled(self, xxx_todo_changeme):
        (row,col,val) = xxx_todo_changeme
        val = int(val)
        n = self.bitlen-1-(col+row*8)
        #print row,col,val
        #print "{:0{}b}".format(val,self.bitlen)
        self.newdatai ^= (-val ^ self.newdatai) & (1 << n)
        #print "{:0{}b}".format(self.newdatai,self.bitlen)
        self.newregisterdata(self.newdatai)
    def on_cellDataChanged(self, xxx_todo_changeme1):
        (row,col,val,cellSpan) = xxx_todo_changeme1
        if cellSpan==self.bitlen:
            self.newdatai = val
        else:
            n2 = self.bitlen-1-(col+row*8)
            n1 = n2 -cellSpan
            up = (self.datai>>n2)<<n2
            dn = val- (self.datai>>n1)<<n1
            self.newdatai = dn+up+val
        self.newregisterdata(self.newdatai)
    def newregisterdata(self,newdatai):
        # newreg = ""
        # for r in range(0,self.rowcount):
        #     for c in range(0,8):
        #         newreg += self.model.item(r,c).data(0).toString()
        self.newreg = "{:0{}X}".format(newdatai,int(self.bitlen/4))
        self.lbl_regdata_new.setText(self.newreg)
        if self.cb_autoupdate.isChecked():
            if self.newreg!=self.data:
                self.dataChanged.emit(self.newreg)
                self.data = self.newreg
                self.le_regdata.setText(str(self.data))
    def on_buttonBox_clicked(self,*args):
        self.dataChanged.emit(self.data)
        self.data = self.newreg
        self.le_regdata.setText(self.data)
        print("apply",args)
    def checkupdate(self):
        try:
            txt = str(self.le_regdata.text())
            i = int(txt,16)
            if len(txt)%4:
                return
        except:
            traceback.print_exc()
            return
        self.le_regdata.setText(txt.upper())
        self.draw(txt)
    def setRegData(self,data):
        self.data = data
    def draw(self,data):
        self.data = data
        self.le_regdata.setText(str(data))
        self.lbl_regdata_new.setText(str(data))
        self.bitlen = len(data)*4
        print(data)
        self.datai = int(data,16)
        self.newdatai = self.datai
        self.datab = "{:0{}b}".format(self.datai,self.bitlen)
        print(self.datab)
        self.rowcount = int(self.bitlen/8)
        self.table.setColumnCount(8)
        self.table.setRowCount(self.rowcount)
        cellSpan = 1
        for r in range(0,self.rowcount):
            self.tablew.append([])
            # self.table.setRowHeight(r,self.rowheight)
            for c in range(0,8):
                if cellSpan>1:
                    cellSpan -= 1
                    continue
                cw,cellSpan = self.setTableItem(r,c)
                if cellSpan>1:
                    self.tableSpan.append((r,c,cellSpan))
                self.tablew.append(cw) # keep all cellwidgets in table
        # print self.table.width(),self.table.verticalHeader().width(),colwidth
        # self.table.repaint()
        # headerlabels = QtCore.QStringList()

        for c in range(0,8):
            self.table.setColumnWidth(c,self.colwidth)
            pass
        #     headerlabels.append(str(7-c))
        # self.table.setHorizontalHeaderLabels(headerlabels)
        # headerlabels = QtCore.QStringList()
        for r in range(0,self.rowcount):
            #self.table.rowSpan()
            self.table.setRowHeight(r,self.rowheight)
        for r,c,span in self.tableSpan:
            cs = span-c
            if span-c<9:
                self.table.setSpan(r,c,1,span)
            elif c==0 and span%8==0:
                rs = int(span/8)
                self.table.setSpan(r,c,rs,8)
                rh = self.rowheight/rs
                for rr in range(r,r+rs):
                    self.table.setRowHeight(rr,rh)


        #     headerlabels.append(str(self.rowcount-1-r))
        # self.table.setVerticalHeaderLabels(headerlabels)
        #self.table.setGeometry(0,0,colwidth*8,rowheight*self.rowcount)
        #mydelegate = Mydelegate()
        #self.table.setItemDelegate(mydelegate)
        #print "table size",self.table.geometry()

    def edit(self):
        for i,btn in enumerate(self.list_buttons):
            if btn == self.sender():
                break
        if self.list_buttons_coord[i][2]>1:
            btn.hide()
            self.editeditem = i
            le = QtWidgets.QLineEdit()
            le.setFixedHeight(btn.height())
            self.gl.addWidget(le,self.list_buttons_coord[i][0],self.list_buttons_coord[i][1],1,
                              self.list_buttons_coord[i][2])
            le.returnPressed.connect(self.leaveEdit)
    def leaveEdit(self):
        le = self.sender()
        data  = le.text()
        le.hide()
        self.list_buttons[self.editeditem].show()
        print(data)



class DDS_QT_VIEW(QtWidgets.QWidget):
    registerChanged = QtCore.Signal(object,object)
    def __init__(self,device):
        super(DDS_QT_VIEW,self).__init__()
        self.dds = device
        gl = QtWidgets.QGridLayout()
        self.setLayout(gl)
        self.tw = QtWidgets.QTabWidget()
        #self.tw.line
        self.reg_list = []
        self.view_list = []
        self.btn_apply = QtWidgets.QPushButton("Apply")
        self.cb_apply_auto = QtWidgets.QCheckBox("auto apply")
        self.cb_apply_auto.setChecked(AUTO_APPLY)
        self.cb_apply_auto.toggled.connect(self.on_cb_apply_auto_toggled)
        gl.addWidget(self.tw,0,0,1,3)
        #gl.addWidget(self.cb_apply_auto,1,0)
        #gl.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum),1,1)
        #gl.addWidget(self.btn_apply,1,2)
        self.setWindowTitle("DDS registers")
        for reg in sorted(self.dds.register_description):
            w = REGISTER_VIEW(self,reg)
            w.dataChanged.connect(self.on_dataChanged)
            if type(reg) == int:
                regS = "0x{:0X}".format(reg)
            else:
                regS = format(reg)
            self.tw.addTab(w,regS)
            self.reg_list.append(reg)
            self.view_list.append(w)
            if reg in self.dds.register_data and self.dds.register_data[reg]:
                #print repr(self.dds.register_data[reg])
                w.draw(self.dds.register_data[reg])
                #w.table.adjustSize()
                #w.table.resizeColumnsToContents()
                w.table.repaint()
                #print w.table.geometry()
                width = w.table.verticalHeader().sizeHint().width()+20
                height = w.table.horizontalHeader().sizeHint().height()+20
                geom = self.geometry()

                if geom.height()> height:
                    height = geom.height()
                if geom.width()> width:
                    width = geom.width()
                #print width,height
                #self.resize(width,height)
                #self.adjustSize()
        self.resize(800,600)


        self.tw.currentChanged.connect(self.on_tw_currentChanged)
    def on_cb_apply_auto_toggled(self,state):
        #print state
        self.btn_apply.setEnabled(not state)
    def on_dataChanged(self,data):
        sndr = self.sender()
        for w,reg in zip(self.view_list,self.reg_list):
            if w==sndr:
                self.registerChanged.emit(reg,data)
                #print "register changed",reg,data
    def on_tw_currentChanged(self,idx):
        if idx>=0:
            reg = self.reg_list[idx]
            if reg not in self.dds.register_data:
                rep = self.dds.readRegister(reg)
                if rep:
                    self.view_list[idx].draw(rep.strip())
            else:
                self.view_list[idx].draw(self.dds.register_data[reg])
    def show(self):
        self.on_tw_currentChanged(self.tw.currentIndex())
        # reg = self.reg_list[self.tw.currentIndex()]
        # self.dds.readRegister(reg)
        # self.view_list[self.tw.currentIndex()].draw(self.dds.register_data[reg])
        # print(reg,":",self.dds.register_data[reg])
        super(DDS_QT_VIEW,self).show()
    # def validateFreq(self,freq):
    #     return freq,freq,bytes
    # def validateAmp(self,freq):
    #     return freq,freq,bytes
    # def validatePhas(self,freq):
    #     return freq,freq,bytes

if __name__ == "__main__":
    import dds3E_dev_class
    dds = dds3E_dev_class.DDS3E_Class("127.0.0.1")
    dds.register_data = {0: '00000002',
 1: '00400020',
 2: '0F3FC000',
 3: '00007F7F',
 4: '00000004',
 5: 'F2E038A5AC66FFFF',
 6: '36A22151D872FFFF',
 7: '00000000',
 8: '00000000',
 9: '00000000',
 10: '00000000',
 11: '36207B235680FE9A',
 12: '79E465BD254A481B',
 13: '28A57E28',
 14: '3FFF0000170A3D70',
 15: '3FFF0000170A3D70',
 16: '3FFF0000170A3D70',
 17: '3FFF0000170A3D70',
 18: '3FFF0000170A3D70',
 19: '3FFF0000170A3D70',
 20: '00000002',
 21: '3FFF0000170A3D70'}

    app  = QtWidgets.QApplication([])
    view = DDS_QT_VIEW(dds)
    # view = REGISTER_VIEW()
    view.show()
    #view.adjustSize()
    print(view.geometry())
    # view.le_regdata.setEnabled(True)
    # view.draw("0Ffecdfa")
    app.exec_()