#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel Tolmachev
#
# Created:     14.12.2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
# reg 0x0
#"00001002" clear  DRG accum True
# "00000002" clear DRG accum False
# reg 0x1
# "00480020" defaults + DRG enable  (both no-dwells disabled)




import traceback

from dds_common_class import DDS_Common_Class
import AD9915

DDS_MIN_STEP = 100. #Hz min  is .25
FSYS = 2.500e9

class DDS4_Class(DDS_Common_Class):
    resolution_amplitude = 12
    resolution_phase = 14
    resolution_frequency = 32
    register_description_file = "AD9915.txt"
    register_description = AD9915.register_description
    amplitude = 1
    amplitudeS = '1000'
    phase = 0
    phaseS = '0000'
    frequency = 0
    frequencyS = ''
    #fsys=1e9
    fsys = FSYS
    PROFILENUMBER = 8
    dt_min = 24./fsys
    address = '0F'
    onOpenCallback = None
    profile = None
    dstatus = { "active profile":(10,13),
    "Digital Ramp Hold":(6,7),
    "Digital Ramp Control":(5,6),
    "Digital Ramp Int / Ext":(4,5) }
    laststatus = {}
    def sortregkeys(self,reg):
        ret = list(reg.keys())
        sortd = False
        while not sortd:
            sortd = True
            for i in range(0,len(ret)-1):
                x1 = reg[ret[i]]
                if type(x1)==tuple:
                    x1 = x1[0]
                x2 = reg[ret[i+1]]
                if type(x2)==tuple:
                    x2 = x2[0]
                if x2<x1:
                    ret[i],ret[i+1]=ret[i+1],ret[i]
                    sortd = False
        #print [reg[i] for i in ret]
        return ret

    def verbal_output(self,dic,bs):
##        print bs
        sz = len(bs)
        ret = {}
        for k,v in dic.items():
            if type(v)==int: #one byte
                v2 = bool(int(bs[sz-v-1]))
                ret[k] = v2
            elif len(v)==1:  #one byte address in one element tuple
                v2 = bool(int(bs[sz-v[0]-1]))
                ret[k] = v2
            elif len(v)>1: #byte range
                s = bs[sz-v[1]:sz-v[0]]
                if len(s)==1:
                    ret[k] = bool(int(s))
                if len(s)>1:
                    #ret[k] = s
                #else:
                    ret[k] = int(s,2)
        self.laststatus = ret
        return ret
    def __init__(self,*args,**kwargs):
        super(DDS4_Class,self).__init__(*args,**kwargs)
        # self.loadRegistersDescription(self.register_description_file)
        self.setFreq = self.setFrequency
        self.queryFreq = self.queryFrequency

        self.singleton_info = {"frequency":(0,32,self.freq_from_bin),
                        "phase":(32,48),
                        "amplitude":(48,62)}
        if "autoupdate" in kwargs:
            self.autoupdate = kwargs["autoupdate"]
        else:
            self.autoupdate = False
        self.dev_open = self.open
        self.open = self.open1
    def open1(self,*args,**kwargs):
        #super(e3DDS_Class,self).open(*args,**kwargs)
        print("connecting...", end=' ')
        print(self.dev_open(*args,**kwargs))
        self.status()
        if self.profile!=None:
            self.amplitude,self.phase,self.frequency = self.getProfile(self.profile)
            print("frequency = {:g} Hz\namplitude = {:2f}\n".format(self.frequency,self.amplitude))
    def set(self):
        msg = 'N{}W{}{}{}:'.format(self.address,self.amplitudeS,self.phaseS,self.frequencyS)
        return self.ask(msg)
        #print self.ask('P1:')
    def getPhaseHex(self):
        return "0000"
    def setAmplitude(self,amp,profile = -1):
        if profile<0:
            profile = self.profile
        # self.amplitudeS = self.amp2hex(amp)
        # regN = self.single_tone_reg(profile)
        # msg = 'N{}W0000{}:'.format('09',self.amplitudeS)
        #ret = self.writeRegister(regN,self.amplitudeS + self.phaseS + self.frequencyS)
        # ret = self.writeRegister(9,self.phaseS + self.amplitudeS)
        #rep = self.ask(msg)
        #rep += self.ask('P1:')
        # ret = self.setProfile(0)
        regN = self.single_tone_reg(profile)
        self.writeRegister(regN+1,self.amp2hex(amp)+self.getPhaseHex())
        self.amplitude = amp
        # return ret
    # def setAmpCommon(self,amp):
    #     self.amplitudeS = format(self.amp2int(amp)<<1,"04x")
    #     ret = self.writeRegister(9,"0000" + self.amplitudeS)
    #     self.amplitude = amp
    def freq2hex(self,freq):
        return "{:08X}".format(int(round((2**32-1)*(freq/self.fsys))))
    def freq_from_hex(self,freqS):
        return int(freqS,16)*self.fsys/2**32
    def freq_from_bin(self,freqS):
        return int(freqS,2)*self.fsys/2**32
    def single_tone_reg(self,profile):
        return 0x0B+profile*2
    def setFrequency(self,freq,profile = -1):
        if profile<0:
            profile = self.profile
        # self.frequencyS=self.freq2hex(freq)
        # regN = self.single_tone_reg(profile)
        # ret = self.writeRegister(regN,self.frequencyS)
        profilereg = 0x0B+profile*2
        freqs = self.freq2hex(freq)
        self.writeRegister(profilereg,freqs)
        # if not self.autoupdate:
        #     self.update()
##        ret = self.setProfile(profile)
        self.frequency = freq
        # return ret
##    def setAmplitude(self,amp):
##        self.ask("N00W00080202:")
##        self.ask("N01W80000000:")
##        self.amplitudeS="{:04X}".format(int(amp))
##        self.set()
##        ret = self.ask('P1:')
##        self.amplitude = amp
##        print self
##        return ret
    def setPhase(self,phas):
        self.phaseS="{:04X}".format(int(phas))
        self.set()
        ret = self.ask('P1:')
        self.phase = phas
        print(self.phaseS)
        return ret
    def reset(self):
        self.ask("M:")
        self.register_data = {}
    def status(self):
        ret = self.ask("S:")
        if not ret:
            return ret
        bret = self.hex2bin(ret)
        print(self.bin_repr_raw(bret))
        print(self.verbal_output(self.dstatus,bret))
        if "active profile" in self.laststatus:
            self.profile = self.laststatus["active profile"]
        if "Digital Ramp Hold" in self.laststatus:
            self.DRHOLD = self.laststatus["Digital Ramp Hold"]
        if "Digital Ramp Control"in self.laststatus:
            self.DRCTL = self.laststatus["Digital Ramp Control"]
        if "Digital Ramp Int / Ext"in self.laststatus:
            self.DREXT = self.laststatus["Digital Ramp Int / Ext"]
        #print self.binformat(ret)
        return ret
    def get(self,profile):
        regN = self.single_tone_reg(profile)
        rep = self.ask("N{}R:".format(self.address))
        if rep:
            amp = int(rep[0:4],16)
            phas = int(rep[4:8],16)
            freq = float.fromhex(rep[8:16])
            self.frequency = round(freq/2**32*self.fsys)
            print(amp,phas,freq)
            print("frequency = {:g} Hz".format(self.frequency))
        return rep
    def queryFrequency(self):
        if self.isConnected():
    ##        if not self.frequency:
            if not self.profile:
                self.status()
            rep = self.readRegister(self.single_tone_reg(self.profile))
            self.frequency = self.freq_from_hex(rep)


    ##        self.get()
            # if
            #
            # rep = self.readRegister(0x0E)
            # freq = float.fromhex(rep)
            # self.frequency = round(freq/2**32*self.fsys)
            return self.frequency
    def queryAmplitude(self):
        if not self.profile:
            self.status()
        rep = self.readRegister(self.single_tone_reg(self.profile)+1)
        self.amplitude = self.ampfromhex(rep[:4])
        self.phase = self.phasefromhex(rep[4:])
        return self.amplitude
    # def open(self):
    #     print "aopo"
    #     super(e3DDS_Class,self)._open()
    #     if self.interface.connected:
    #         if self.onOpenCallback:
    #             self.onOpenCallback()
    #         else:
    #             print "connection established but no callback provided"
    def prepRegisterString(self,register):
        if type(register)==str:
            if register in self.register_names:
                register = self.register_names[register]
        if type(register)==int:
            register = "{:02X}".format(register)
        return register
    def writeRegister(self,register,data):
        regstr = self.prepRegisterString(register)
        msg = "N{}W{}:".format(regstr,data)
##        if self.autoupdate:
##            msg +="U:\n"
        rep = self.ask(msg)
        if self.verbose:
            print(msg)
            print("0x{:X}<<{}".format(register,data),rep)
        self.register_data[register] = data
        return rep
    def readRegister(self,register,verbose = True):
        regname =""
        if register in self.register_description:
            regname = self.register_description[register]["name"]
        regstr = self.prepRegisterString(register)
        rep = self.ask("N{}R:".format(regstr))
        if rep:
            rep = rep.strip()
            while rep =="OK":
                rep = self.readRegister(register,verbose=verbose)
            self.register_data[register] = rep

        #print "0x{}>>0x{}".format(regstr,rep)
        if verbose and rep:
            print("0x{:X}".format(register),regname,">>",rep.strip())
##            print self.bin_repr_raw(self.hex2bin(rep.strip()))
##            if self.registers[register]:
##                print self.verbal_output(self.registers[register],self.hex2bin(rep.strip()))

        return rep
    def printoutinfo(self,register,data):
        reg_dict =self.registers[register]
        keys = self.sortregkeys(reg_dict)
        digits = 0
        for key in keys:
            start_byte = reg_dict[key]
            if type(start_byte)==tuple:
                start_byte,end_byte = start_byte[0],start_byte[1]
            else:
                end_byte = start_byte
            div = (start_byte)/8
            #print start_byte,div
            if div != digits:
                digits = div
                print("")
            if start_byte!=end_byte:
                st = "[{}:{}] ".format(start_byte,end_byte)
            else:
                st = "[{}]\t".format(start_byte)
            print(st,key,"\t",data[key])
    def parseRegister(self,register,data):
        print("register {:}:".format(register),data)
        if type(data)==str:
            rep = data
            print(self.bin_repr_raw(self.hex2bin(rep.strip())))
            if self.registers[register]:
               self.verbal_output(self.registers[register],self.hex2bin(rep.strip()))
               self.printoutinfo(register,self.laststatus)
        return rep
    def getProfile(self,profile):
        """
        :param profile:
        :return:  amplitude, phase, frequency
        """
        if profile!=None:
            regN = self.single_tone_reg(profile)
            #DDS4 only
            freq = self.freq_from_hex(self.readRegister(regN))
            rep = self.readRegister(regN+1)
            amp = self.ampfromhex(rep[0:4])
            phas = self.phasefromhex(rep[4:])
            # retB = self.hex2bin(ret)
            # self.verbal_output(self.singleton_info,retB)
            # freq = self.laststatus.get("frequency",None)
            # amp = self.laststatus.get("amplitude",None)
            # phas = self.laststatus.get("phase",None)
            return amp,phas,freq
        else:
            return None,None,None
    def getCurrentProfile(self):
        self.status()
        return (self.profile,)+self.getProfile(self.profile)
    def setProfile(self,profile,amp,phas,freq):
        if profile!=None:
            ampS = self.amp2hex(amp)
            phaS = self.phase2hex(phas)
            freqS = self.freq2hex(freq)
            regN = self.single_tone_reg(profile)
            rep  = self.writeRegister(regN,freqS)
            rep2 = self.writeRegister(regN+1,ampS+phaS)
            return rep,rep2

    def selectProfile(self,profile_number):
        ret = self.ask("P{}:".format(profile_number))
        if ret == self.OK:
            self.profile = profile_number
        else:
            self.status()
        return ret
    def binformat(self,hex_str):
        l = len(hex_str.strip())
        ret = ""
        for i in range(0,l,2):
            ret+= "{:08b}\n".format(int(hex_str[i:i+2],16))
        return ret
    def setupSweepRegister(self,freq_max,freq_min,stepup,stepdn,dtup,dtdn):
        if freq_max < freq_min:
            freq_max,freq_min = freq_min,freq_max
        self.writeRegister(0x04 ,self.freq2hex(freq_min))
        self.writeRegister(0x05 ,self.freq2hex(freq_max))
        self.writeRegister(0x06,self.freq2hex(stepup))
        self.writeRegister(0x07,self.freq2hex(stepdn))
        self.writeRegister(0x08,"{:04X}{:04X}".format(dtup,dtdn))
        nstepup = (freq_max-freq_min)/stepup
        nstepdn = (freq_max-freq_min)/stepdn
        timeup = nstepup*dtup*self.dt_min
        timedn = nstepdn*dtdn*self.dt_min
        print("DDS4 Sweep setup:")
        print("from {} to {}".format(freq_min,freq_max))
        print("-> {} steps {}s".format(nstepup,timeup))
        print("<- {} steps {}s".format(nstepdn,timedn))
    def setupSweep(self,freq_max,freq_min,stepup,stepdn,dtup,dtdn,direction = 'up'):
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepStepsUp = abs(freq_max-freq_min)/stepup
        self.sweepStepsDn = abs(freq_max-freq_min)/stepdn
        self.sweepTimeUp = self.sweepStepsUp*dtup*self.dt_min
        self.sweepTimeDn = self.sweepStepsDn*dtdn*self.dt_min
        self.setupSweepRegister(freq_max,freq_min,stepup,stepdn,dtup,dtdn)
        print("sweeping",direction)
        #self.writeRegister(0x00,"00001002") # reset DRG accum
        #self.writeRegister(0x01,"00480020") #defaults + DRG enable
                                            #(both no-dwells disabled)
        if direction == "up":
            self.ask("K1:")
    def setupSweepUpOnce(self):
        self.setDRG(False)
        self.setClearDrgAccum(True)
        self.setNoDwellHigh(False)
        self.setNoDwellLow(False)
        self.setClearDrgAccum(False)
    def setDRGINT(self, state):
        self.DREXT = state
        return self.ask("Z{}:".format(int(state)))
    def setupSetSweepControlInt(self):
        """
        Sets Digital Ramp to External,
            Digital Ramp Hold to 1
            Digital Ramp Control to 0
        """
        #should be separate commands
        if not self.DREXT:
            self.ask("Z1:")
        if not self.DRHOLD:
            self.ask("H1:")
        #if self.DRCTL:  #it's better to run it always
        self.ask("K0:")
    def startSweep(self):
        self.setDRG(True)
        #self.writeRegister(0,"00000002")
        return time.time()
    def calcSweep(self,start,stop,time):
        rng = abs(float(stop)-start)
        speed = rng/time
        step = DDS_MIN_STEP
        dt = 1
        if step/dt/self.dt_min< speed:
            step = speed*dt*self.dt_min
        else:
            dt = int(round(step/speed/self.dt_min))
            if dt>65535:
                dt = 65535
                step = speed*dt*self.dt_min
        return step,dt
    def startSingleTone(self,*profile):
        if profile == ():
            profile = 0
        else:
            profile = profile[0]
##        self.writeRegister(0,'00080202')
##        self.writeRegister(01,'80000000')
        self.writeRegister(0,'00080002')
        self.writeRegister(0o1,'01400020')
        #self.setProfile(profile,1,0,1e6)
    def amp2hex(self,amp):
        if amp>1:
            amp = 1
        elif amp<0:
            amp = 0
        amp = self.amp2int(amp)
        return "{:04X}".format(amp)
    def ampfromint(self,amp):
        if amp!=None:
            return amp/self.max_amplitude
    def ampfromhex(self,amp_hx):
        a = int(amp_hx,16)
        return float(a)/self.max_amplitude
    def phasefromhex(self,phas_hx):
        ph = int(phas_hx,16)
        return float(ph)/self.max_phase
    def hex2bin(self,hx):
        sz = len(hx)
        if sz>0:
            try:
                return "{:0{}b}".format(int(hx,16),int(sz*4))
            except:
                traceback.print_exc()
                return hx
        else:
            return hx
    def bin2hex(self,bs):
        sz = len(bs)
        return "{:0{}X}".format(int(bs,2),int(sz/4))
    def bin_repr_raw(self,bs):
        return " ".join([bs[i:i+4] for i in range(0,len(bs),4)])
    def setbit(self,regN,addr,val):
        if regN in self.register_data and self.register_data[regN]:
            rep = self.register_data[regN]
        else:
            rep = self.readRegister(regN)
            if rep:
                self.register_data[regN] = rep.strip()
        if rep:
            try:
                sz = len(rep)
                bits = self.hex2bin(rep)
                szb = len(bits)
                if type(val)==bool:
                    val = int(val)
                if type(val) == int:
                    if val:
                        val = 1
        ##            if addr<len(bits)-1:
                    bits2 = bits[0:szb-addr-1]+ str(val)+ bits[szb-addr:]
        ##            else:
        ##                bits2 = bits[0:addr]+ str(val)+ bits[addr+1:]
                if type(val)==str:
                    szv = len(val)
                    bits2 = bits[0:szb-addr-szv]+ val+ bits[szb-addr:]
                newstr = self.bin2hex(bits2)
                #print self.bin_repr_raw(bits),">"
                print(self.bin_repr_raw(bits2),">",newstr)
                self.writeRegister(regN,newstr)
                if self.autoupdate:
                    self.readRegister(regN)
            except:
                traceback.print_exc()
                print((repr(rep)))
        else:
            print("error reading register 0x{:X}:{}".format(regN,repr(rep)))
    def update(self):
        pass
##        return self.ask("U:")
    def setupSingleTone(self):
        self.setAmpFromProfile(True)
        self.setOSK(False)
    def setAmpFromProfile(self,enable):
        self.setbit(1,24,enable)
    def setOSK(self,enable):
        self.setbit(0,8,enable)
    def setDRG(self,enable):
        self.setbit(1,19,enable)
    def setClearDrgAccum(self,enable):
        self.setbit(0,12,enable)
    def setNoDwellHigh(self,enable):
        self.setbit(1,18,enable)
    def setNoDwellLow(self,enable):
        self.setbit(1,17,enable)
    def setupSweepRegisterQuick(self,start,stop,time):
        step,dt = self.calcSweep(start,stop,time)
        self.setupSweepRegister(stop,start,step,step,dt,dt)
    def setProfileExt(self,state):
        if state:
            self.ask("Y0:")
        else:
            self.ask("Y1:")
    def setExternalProfile(self,state):
        self.setProfileExt(state)
    def readAllRegisters(self):
        for reg in self.register_description:
            self.readRegister(reg)
    def sweep_up_once(self,freq_min,freq_max,step,dt):
        print("sweep forward once")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,step,step,delta,delta)
        print("setting up amplitude",self.dds.amplitude)
        self.dds.setAmplitude(self.dds.amplitude)
        #self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        #self.dds.write("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
        if not self.loop:
            self.dds.ask("K0:")
    # def calcPointNumber(self,pulse_duration):
    #     periods = pulse_duration/self.dt_min/2**16
    #     npoints = int(np.ceil(periods))
    #     if npoints<2:    npoints = 2
    #     rate = pulse_duration/npoints/self.dt_min
    #     #print periods,npoints,rate
    #     return npoints,rate
    # def pulse1(self,pulse_duration,amp,repetition_rate):
    #     #calculate
    #     pulses,rate1 = self.calcPointNumber(pulse_duration)
    #     pulse = [self.amp2hex(amp)+"0000" for i in range(0,pulses) ]
    #     print "pts:",pulses,"\trate:",rate1
    #     print pulse
    #     paused = repetition_rate-pulse_duration
    #     pause_points,rate2 = self.calcPointNumber(paused)
    #     pause = ["00000000" for i in range(0,pause_points)]
    #     print "pts:",pause_points,"\trate:",rate2
    #     print pause
    #     #load to profiles
    #     self.setRamDestination(dest = "amplitude")
    #     self.setbit(0,17,"1000")
    #     if not 0x7 in self.register_data:
    #         freq = 8.9e6
    #         print "setting default frequency {}".format(freq)
    #         self.setFreqCommon(freq)
    #     self.setRAMprofile(0,0,pulses-1,rate1,mode = 4)
    #     self.selectProfile(0)
    #     self.loadRam(pulse)
    #     self.setRAMprofile(1,10,10+pause_points-1,rate2,mode = 4)
    #     self.selectProfile(1)
    #     self.loadRam(pause)
    # def loadRam(self,lst):
    #     self.ask("N16W:")
    #     for p in lst:
    #         msg = "L" + p+ ":"
    #         print msg
    #         print self.write(msg)
    #
    # def RAMstart(self):
    #     self.enableRAM(True)
    #     self.update()
    # def RAMstop(self):
    #     self.enableRAM(False)
    #     self.update()



##    def ph2hex(self,phas_degrees):
##        if amp>1:
##            amp = 1
##        elif amp<0:
##            amp = 0
##        amp = int(65535*amp)
##        return "{:04X}".format(amp)
# def magic(freq = 1.3e6, amp = 1):
# ##    if freq==():
# ##        freq = 1.3e6
# ##    else:
# ##        freq = freq[0]
#     #dds.writeRegister(0,'00080202')
#     dds.writeRegister(0,'00000202') #OSk True, Manual
#     #dds.writeRegister(01,'80000000')
#     dds.writeRegister(01,'00000000')
#     #dds.writeRegister(9,'00003334')
#     dds.writeRegister(9,'0000{}'.format(dds.amp2hex(amp)))
#     dds.writeRegister(0x11,'00000000'+dds.freq2hex(freq))
#     dds.ask("P3:")
#
# def setup_profiles_67():
#     freq = dds.frequency
#     amp = dds.amplitude
#     if amp==0:
#         amp =1
#     phas = dds.phase
#     dds.setProfile(6,0,0,freq)
#     dds.selectProfile(6)
#     dds.setProfile(7,amp,phas,freq)
#     dds.selectProfile(7)


if __name__ == '__main__':
    import time
    addr = ("129.217.155.83",51234)
    dds = DDS4_Class(addr,timeout = 1)

##    dds.parseRegister(0,"01400820")

    dds.open()
    dds.status()
    dds.setOSK(True)
    dds.autoupdate = True
    dds.readRegister(0)
    dds.print_register_info(0)
    dds.readRegister(14)
    dds.print_register_info(14)
##    #dds.startSingleTone()
####    dds.setupSweep(1e6,11e6,.25,.25,1,1)
##    time.sleep(1)
##    dds.reset()
##    dds.setFreqCommon(8.9e6)
##    dds.Ramtest2()
##    dds.Ramtest3(30)
##    dds.Ramtest4()
##    dds.readRegister(0)

##    dds.pulse1(1e-6,1,1e-4)
##    dds.RAMstart()
##    dds.RAMstart()
    #magic()
##    dds.startSweep()
##    dds.reset()
##    dds.status()
##    dds.getProfile(2)
##    dds.readRegister(1)
##    magic()
##    dds.setAmpFromProfile(True)

