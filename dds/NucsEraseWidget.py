from qtpy import QtWidgets,QtCore
import nucs
from collections import OrderedDict
from numpy import pi
import si_units
class NucsEraseWidget(QtWidgets.QWidget):
    commanded = QtCore.Signal(object)
    def __init__(self,dds,options,controller):
        self.dds = dds
        self.options = options
        self.controller = controller
        super(NucsEraseWidget,self).__init__()
        self.nucs = OrderedDict([(k,nucs.nucs[k]) for k in sorted(nucs.nucs)])
        self.mult = options.get("NucsEraseWidget.frequency_multiplier", 1e6)
        self.mult_time = options.get("NucsEraseWidget.time_multiplier", 1e-3)
        self.setupUi()
        self.updateLabels()
    def setupUi(self):
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)
        h1 = QtWidgets.QHBoxLayout()
        self.gl.addLayout(h1,0,0)
        self.sb_field = QtWidgets.QDoubleSpinBox()
        self.sb_field.setPrefix("field ")
        self.sb_field.setMaximum(1.4)
        self.sb_field.setSingleStep(0.01)
        self.sb_field.setSuffix(" T")
        self.sb_field.setValue(self.options.get("NucsEraseWidget.field", 0.1))
        self.sb_sweep = QtWidgets.QDoubleSpinBox()
        self.sb_sweep.setValue(self.options.get("NucsEraseWidget.sweep_range", 0.1))
        self.sb_sweep.setMaximum(1e3)
        self.sb_sweep.setSingleStep(0.1)
        self.sb_sweep.setPrefix("sweep range ")
        self.sb_sweep.setSuffix(" MHz")
        self.sb_field.valueChanged.connect(self.updateLabels)
        self.sb_sweep.valueChanged.connect(self.updateLabels)
        self.sb_time = QtWidgets.QDoubleSpinBox()
        self.sb_time.setValue(self.options.get("NucsEraseWidget.time", 10))
        self.tmin = 4./self.dds.fsys
        self.tmax = self.tmin*65535
        self.sb_time.setMaximum(self.tmax*1024/self.mult_time)
        self.sb_time.setMinimum(self.tmax*1024)
        self.sb_time.setSingleStep(1)
        self.sb_time.setPrefix("sweep time ")
        self.sb_time.setSuffix(" ms")
        self.sb_time.valueChanged.connect(self.updateLabels)
        row = 0
        h1.addWidget(self.sb_field)
        h1.addWidget(self.sb_sweep)
        h1.addWidget(self.sb_time)
        row+=1
        self.cb = []
        self.lbls = []
        for n in self.nucs:
            v = QtWidgets.QHBoxLayout()
            cb = QtWidgets.QCheckBox(n)
            cb.setChecked(True)
            cb.toggled.connect(self.updateLabels)
            self.cb.append(cb)
            # self.gl.addWidget(cb,row,0)
            l1 = QtWidgets.QLabel()
            l2 = QtWidgets.QLabel()
            f = l2.font()
            f.setPointSize(f.pointSize()*2)
            l2.setFont(f)
            l3 = QtWidgets.QLabel()
            self.lbls.append((l1,l2,l3))
            # self.gl.addWidget(l1,row,1)
            # self.gl.addWidget(l2, row, 2)
            # self.gl.addWidget(l3, row, 3)
            v.addWidget(cb)
            v.addWidget(l1)
            v.addWidget(l2)
            v.addWidget(l3)
            self.gl.addLayout(v,row,0)
            row+=1
        self.gl.addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        row += 1
        self.l_stat = QtWidgets.QLabel()
        self.gl.addWidget(self.l_stat)
        row+=1
        self.gl.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding))
        row+=1
        bl = QtWidgets.QHBoxLayout()
        self.gl.addLayout(bl, row, 0)
        self.btn_loadram = QtWidgets.QPushButton("LoadRAM")
        self.btn_loadram.pressed.connect(self.out)
        bl.addWidget(self.btn_loadram)
        bl.addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
    def updateLabels(self):
        self.ranges = []
        self.field = self.sb_field.value()
        self.sweep = self.sb_sweep.value()*self.mult
        t = self.sb_time.value() * self.mult_time
        self.dt = int(round(t / 1024 / self.tmin))
        if self.dt>65535:
            self.dt = 65535
        t = self.dt*1024*self.tmin
        self.sb_time.setValue(t/self.mult_time)
        self.total_range = 0
        for i,n in enumerate(self.nucs):
            if self.cb[i].isChecked():
                res = self.respos(n)
                rang = res-self.sweep,res+self.sweep
                self.total_range += self.sweep*2
                self.ranges.append(rang)
                self.lbls[i][0].setText("{:.3g}".format(rang[0]/self.mult))
                self.lbls[i][1].setText("{:.3g}".format(res/self.mult))
                self.lbls[i][2].setText("{:.3g}".format(rang[1]/self.mult))
            else:
                self.lbls[i][0].clear()
                self.lbls[i][1].clear()
                self.lbls[i][2].clear()
        self.speed = self.total_range/t
        self.showStats()
    def showStats(self):
        stat = "Speed is "+si_units.si_str(self.speed,"Hz/s",self.l_stat)
        self.l_stat.setText(stat)
    def respos(self,nuc):
        gamma = self.nucs[nuc]
        return self.field * gamma * 1e7 / 2 / pi
    def saveoptions(self):
        self.options["NucsEraseWidget.frequency_multiplier"] = self.mult
        self.options["NucsEraseWidget.time_multiplier"] = self.mult_time
        self.options["NucsEraseWidget.sweep_range"] = self.sb_sweep.value()
        self.options["NucsEraseWidget.field"] = self.sb_field.value()
        self.options["NucsEraseWidget.time"] = self.sb_time.value()
    def out(self):
        self.saveoptions()
        # self.ranges = [(1e6, 2e6), (9e6, 10e6)]
        arr = nucs.maketable(self.ranges)
        tabl = [self.dds.freq2hex(f) for f in arr]
        # self.commanded.emit(("RAMstop"))
        self.commanded.emit(("reset"))
        self.commanded.emit(("RAMstart"))
        self.commanded.emit(("setRAMprofile",2, 0, 1023, self.dt))
        self.commanded.emit(("selectProfile",2))
        self.commanded.emit(("setRamDestination","frequency"))
        self.commanded.emit(("RAMstop"))
        self.controller.progressChanged.connect(self.showLoadingProgress)
        self.commanded.emit(("loadRam",tabl))
        self.commanded.emit(("setOSK", True))
        self.commanded.emit(("setAmpCommon",self.dds.amplitude))
        self.commanded.emit(("RAMstart"))
    def showLoadingProgress(self,prog):
        self.l_stat.setText("loading data into ram {:d}%".format(int(prog*100)))
        if prog==1:
            self.controller.progressChanged.disconnect(self.showLoadingProgress)
            self.showStats()