from instrument_base import InstrumentBase
import re,traceback
from shlex import split
from numpy import ceil

class DDS_Common_Class(InstrumentBase):
    device_name = "DDS_base_class"
    resolution_amplitude = 14
    resolution_phase = 16
    resolution_frequency = 48
    fsys = 1e9
    OK = 'OK \n\r'
    register_description = {}
    autoupdate = False
    def __init__(self,*args,**kwargs):
        super(DDS_Common_Class,self).__init__(*args,**kwargs)
        self.WORDLEN_AMPLITUDE = int(ceil(self.resolution_amplitude/4.))
        self.WORDLEN_PHASE = int(ceil(self.resolution_phase/4.)) #
        self.register_data = {}
        #self.register_description = {}
        self.max_frequency = 2**self.resolution_frequency-1
        self.max_amplitude = 2**self.resolution_amplitude-1
        self.max_phase = 2**self.resolution_phase-1
        self.verbose = kwargs.get("verbose",True)
        self.autoupdate = kwargs.get("autoupdate",False)
        self.DRHOLD = None
        self.DRCTL = None
        self.DREXT = None
        self.frequency_hex_digits = int(self.resolution_frequency/4)
        self.amplitude_hex_digits = int(self.resolution_amplitude/4)
        self.phase_hex_digits = int(self.resolution_phase/4)
    def print_register_info(self,registerID,*data):
        if data:
            data = data[0]
        else:
            if registerID in self.register_data:
                data = self.register_data[registerID]
            else:
                return
        if registerID in self.register_description:
            if type(data) == int:
                datai = data
                datab = bin(datai)[:1:-1]
            elif data=="":
                return
            else:
                datai = int(data,16)
                sz = len(data)*4
                datab = "{:0{}b}".format(datai,sz)[::-1]

            reg = self.register_description[registerID]
            if type(registerID) == int:
                regstr = "0x{:0X}".format(registerID)
            else:
                regstr  = "{}".format(registerID)
            if "name" in reg:
                name = reg["name"]
            else:
                name = ""
            if "long_name" in reg:
                name = reg["long_name"] + "("+ name + ")"
            print("register {} {}: {}".format(regstr,name,data))
            print(datab[::-1])
            for it in reg["bytes"]:
                if len(it)==4:
                    addr,sz,desc,_ = it
                else:
                    print(it)
                    continue
                msg = "\t{}".format(addr)
                if sz>1:
                    msg +=":{}".format(addr+sz)
                msg +="\t{}: ".format(desc)
                if sz == 1:
                    s = str(bool((datai & (1<<addr))>>addr))
                elif not sz%8:
                    end = addr+sz
                    d = (datai-datai/2**end*2**end)/2**addr
                    s = "{:0{}X}  {}".format(d,sz/4,d)
                else:
                    end = addr+sz
                    while len(datab)<end:
                        datab +="0"
                    s = datab[addr:end]
                msg += s
                print(msg)
    # def loadRegistersDescription(self,filename):
    #         with open(filename) as f:
    #             lines = f.readlines()
    #             key = ""
    #             bytes_descr = []
    #             for l in lines:
    #                 words = split(l)
    #                 if l[0] not in ["\t"," "] and words:
    #                     if len(words)>0:
    #                         key = words[0]
    #                         self.register_description[key] = {"bytes":[]}
    #                         if len(words)>1:
    #                             self.register_description[key]["name"] = words[1]
    #                         if len(words)>2:
    #                             self.register_description[key]["long_name"] = words[2]
    #
    #                 else:
    #                     #print words
    #                     if len(words)>1 and key:
    #                         val_type = None
    #                         m = re.search("\d+",l)
    #                         if m:
    #                             address = int(m.group())
    #                             pos = m.end()
    #                             m = re.search(":(\d+)",l[pos:])
    #                             if m:
    #                                 end_address = int(m.groups()[0])
    #                                 pos = m.end()
    #                                 sz = end_address-address
    #                             else:
    #                                 sz = 1
    #                             m = re.search("[\"'](.*)[\"']",l[pos:])
    #                             mf = re.findall("[\"'](.*?)[\"']",l[pos:])
    #                             #print mf
    #                             descr_str = descr_long = ""
    #                             if len(mf)>0:
    #                                 descr_str = mf[0]
    #                             else:
    #                                 if "frequency" in l[pos:].lower():
    #                                     descr_str = "frequency"
    #                                 elif "amplitude" in l[pos:].lower():
    #                                     descr_str = "amplitude"
    #                                 elif "phase" in l[pos:].lower():
    #                                     descr_str = "phase"
    #                             if len(mf)>1:
    #                                 descr_long = mf[1]
    #
    # ##                            if m:
    # ##                                desc_str = m.groups()[0]
    # ##                            else:
    # ##                                desc_str = ""
    #
    #                         self.register_description[key]["bytes"].append((address,sz,descr_str,descr_long,val_type))
    #             keys = list(self.register_description)
    #             newkeys = []
    #             try:
    #                 newkeys = [int(key) for key in keys]
    #             except ValueError:
    #                 try:
    #                     newkeys = [int(key,16) for key in keys]
    #                 except ValueError:
    #                     pass
    #             if newkeys:
    #                 for newkey,oldkey in zip(newkeys,keys):
    #                     self.register_description[newkey] = self.register_description.pop(oldkey)
    #         return self.register_description
    def loadRegistersDescription(self,filename):
        with open(filename) as f:
            lines = f.readlines()
            key = ""
            bytes_descr = []
            for l in lines:
                #words = l.split()
                try:
                    words = split(l)
                except:
                    print (l)
                    traceback.print_exc()
                    words = l.split()
                if l[0] not in ["\t"," "] and words:
                    if len(words)>0:
                        key = words[0]
                        self.register_description[key] = {"bytes":[]}
                        if len(words)>1:
                            self.register_description[key]["name"] = words[1]
                        if len(words)>2:
                            self.register_description[key]["long_name"] = words[2]

                else:
                    #print words
                    if len(words)>1 and key:
                        m = re.search("\d+",l)
                        if m:
                            address = int(m.group())
                            pos = m.end()
                            m = re.search(":(\d+)",l[pos:])
                            if m:
                                end_address = int(m.groups()[0])
                                pos = m.end()
                                sz = end_address-address
                            else:
                                sz = 1
                            m = re.search("[\"'](.*)[\"']",l[pos:])
                            mf = re.findall("[\"'](.*?)[\"']",l[pos:])
                            descr_str = descr_long = ""
                            if len(mf)>0:
                                descr_str = mf[0]
                            else:
                                if "frequency" in l[pos:].lower():
                                    descr_str = "frequency"
                                elif "amplitude" in l[pos:].lower():
                                    descr_str = "amplitude"
                                elif "phase" in l[pos:].lower():
                                    descr_str = "phase"
                            if len(mf)>1:
                                descr_long = mf[1]

##                            if m:
##                                desc_str = m.groups()[0]
##                            else:
##                                desc_str = ""
                        self.register_description[key]["bytes"].append((address,sz,descr_str,descr_long))
            keys = list(self.register_description)
            newkeys = []
            try:
                newkeys = [int(key) for key in keys]
            except ValueError:
                try:
                    newkeys = [int(key,16) for key in keys]
                except ValueError:
                    pass
            if newkeys:
                for newkey,oldkey in zip(newkeys,keys):
                    self.register_description[newkey] = self.register_description.pop(oldkey)
        #print self.register_description
    def freq2int(self,freq):
        return int(round((self.max_frequency)*(freq/self.fsys)))
    # def freq2hex(self,freq):
    #     return "freq"
    def freq2hex(self,freq):
        return "{:0{}X}".format(int(round(self.max_frequency*(freq/self.fsys))),self.frequency_hex_digits)
    def int2freq(self,freqI):
        return self.fsys*freqI/self.max_frequency
    def hex2freq(self,freq_hex):
        return self.int2freq(int(freq_hex,16))
    def amp2int(self,amp):
        if amp > 1:
            amp = 1
        elif amp < 0:
            amp = 0
        return int(self.max_amplitude*amp)
    def amp2hex(self,amp):
        ampI = self.amp2int(amp)
        return "{:0{}X}".format(ampI,self.amplitude_hex_digits)
    def int2amp(self,ampI):
        return ampI/self.max_amplitude
    def phase2int(self,phas):
        return 0
    def phase2hex(self,phas):
        return "{:0{}X}".format(self.phase2int(phas), self.WORDLEN_PHASE)
    def int2phase(self,phas):
        return
    def setbit(self, regN, addr, val):
        if regN in self.register_data and self.register_data[regN]:
            rep = self.register_data[regN]
        else:
            rep = self.readRegister(regN).strip()
            self.register_data[regN] = rep
        if rep:
            sz = len(rep)
            bits = self.hex2bin(rep)
            szb = len(bits)
            if type(val) == bool:
                val = int(val)
            if type(val) == int:
                if val:
                    val = 1
                    ##            if addr<len(bits)-1:
                bits2 = bits[0:szb - addr - 1] + str(val) + bits[szb - addr:]
                ##            else:
                ##                bits2 = bits[0:addr]+ str(val)+ bits[addr+1:]
            if type(val) == str:
                szv = len(val)
                bits2 = bits[0:szb - addr - szv] + val + bits[szb - addr:]
            newstr = self.bin2hex(bits2)
            # print self.bin_repr_raw(bits),">"
            print(self.bin_repr_raw(bits2), ">", newstr)
            self.writeRegister(regN, newstr)
            if self.autoupdate:
                self.readRegister(regN)
        else:
            print("error reading register {}:{}".format(regN, repr(rep)))
    def hex2bin(self,hx):
        sz = len(hx)
        return "{:0{}b}".format(int(hx,16),sz*4)
    def bin2hex(self,bs):
        sz = len(bs)
        return "{:0{}X}".format(int(bs,2),int(sz/4))
    def bin_repr_raw(self,bs):
        return " ".join([bs[i:i+4] for i in range(0,len(bs),4)])
if __name__ == "__main__":
    d = DDS_Common_Class("no address")
    d.loadRegistersDescription("AD9915.txt")
    for key,val in list(d.register_description.items()):
        print(key,":",val)