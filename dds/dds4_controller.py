#-------------------------------------------------------------------------------
# Name:     dds_controller.py
# Purpose:  control DDS generator made for e3.physik.tu-dortmund.de
#
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Author:      Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------

import time

from qtpy import QtCore
from numpy import floor
from dds_controller_base import DDS_ControllerBase

from dds3E_dev_class import FSYS

DDS_SPEED_COEFF = 1. #9.3046e+04
DDS_STEP_TIME = 91/FSYS #from manual

class QtSweeper(QtCore.QObject):
    newValueSet = QtCore.Signal(float)
    message = QtCore.Signal(object)
    stopped = QtCore.Signal()
    def __init__(self,setFunction = None,todolist = []):
        super(QtSweeper,self).__init__()
        self.timer = None#QtCore.QTimer() #timer will be initialized later
        # self.timer.setSingleShot(True)
        self.t = 0
        self.setFunction = setFunction
        if not hasattr(todolist,"__iter__"):
            self.todolist = [todolist]
        else:
            self.todolist = todolist
    def sweep(self,val_min,val_high,step,dt,resume,loop,bidirectional):
        if not self.timer:
            self.timer = QtCore.QTimer()
            self.timer.setSingleShot(True)
        self.step = step
        self.value_low = val_min
        self.value_high = val_high
        if val_high<val_min:
            self.value_low,self.value_high = self.value_high,self.value_low
        if step>0:
            self.value = self.value_low
        elif step<0:
            self.value = self.value_high
        self.dt = dt
        self.timer.timeout.connect(self.setNextValue)
        self.t = time.time()
        self.t_start = self.t
        self.step_cnt = 0
        #self.timer.start(dt*1000)
        self.timer.start(dt*1000)
        print("sweeping from {} to {}, step is {}, dt = {}".format(self.value_low,self.value_high,self.step,self.dt))
        print("total sweep time is",abs((self.value_high-self.value_low)/self.step*dt),\
            "total cycles",(self.value_high-self.value_low)/self.step)
        self.go = True
    def setNextValue(self):
        self.step_cnt+=1
        # t = time.time()
        # delta = t-self.t
        # if
        # print "time elapsed by step:",delta
        #self.t = t

        self.value = self.value+self.step
        if self.value>=self.value_high:
            self.value = self.value_high
        elif self.value<=self.value_low:
            self.value = self.value_low
        if self.setFunction:
            self.setFunction(self.value)
            t = time.time()
            self.newValueSet.emit(self.value)
            self.message.emit((t,self.value))
            for func in self.todolist:
                func()
        else:
            print("QtSweeper: no function was set")
        if self.value>=self.value_high or self.value<=self.value_low:
            self.stop()
        #delta = t-self.t-self.dt
        if self.go:
            interval = 1000.*(2*self.dt-t+self.t)
            if interval<0:
                interval = 0
            print(interval)
            self.timer.start(interval)
        self.t = t
    def stop(self):
        self.go = False
        self.timer.stop()
        self.stopped.emit()
        self.message.emit("stop")
        tel = time.time()-self.t_start
        if self.step_cnt:
            print("sweep took {} s, {}cycles, {:.3f} s per cycle".format(tel,self.step_cnt,tel/self.step_cnt))
        else:
            print("sweep took {} s, {}cycles".format(tel, self.step_cnt))


class DDS_Controller(DDS_ControllerBase):
    sig_float = QtCore.Signal(object)
    sig_tuple = QtCore.Signal(tuple)
    sig_string = QtCore.Signal(str)
    sig = QtCore.Signal(object)
    sig_new_range = QtCore.Signal(float,float)
    timer = QtCore.QTimer()
    IntSweepTimer = QtCore.QTimer()

    queryFreq = None
    sig_stopped = QtCore.Signal()
    stopped = True
    connection_status_changed = QtCore.Signal(object)
    def __init__(self):
        super(DDS_Controller,self).__init__()
        self.sweeper_freq = QtSweeper()
        self.sweeper_freq.moveToThread(self.thread)
        #self.timer.timeout.connect(queryFreq)
        self.IntSweepTimer.timeout.connect(self.sig_stopped.emit)
    def status(self):
        self.dds.status()
    def rcv(self,t):
        #print type(t),': ',t
        if type(t)==tuple:
            self.sig.emit(t)
            if len(t)>1:
                if type(t[1])==float:
                    self.sig_float.emit(t[1])
        elif type(t)==str:
            self.sig.emit(t)
        elif type(t)==float:
            self.sig.emit((time.time(),t))
        else:
            print("sig received:",t)
    def sendrange(self,begin,end):
        self.sig.emit(('range',(begin,end)))
    def processCommand(self,arg):
        if type(arg)==tuple:
            if len(arg)>1:
                if arg[0]=="range" and type(arg[1])==tuple:
                    self.sig_new_range.emit(arg[1][0],arg[1][1])
                    #if len(arg)>3:
                    #print arg
    def setqueryFreq(self,v):
        f = self.dds.setqueryFreq(v)
        self.sig_float.emit(f)
    def sweep(self,freq_min,freq_max,step,dt,resume,loop,bidirectional):
        #print resume,loop,bidirectional
        self.loop = loop
        self.bidi = bidirectional
        #self.dds.setAmpCommon(self.dds.amplitude)
        # if bidirectional:
        #    self.sweep_up_loop_bidi(freq_min,freq_max,step,dt)
        # else:
        #     if self.loop:
        #         self.sweep_up_loop(freq_min,freq_max,step,dt)
        #     else:
        #         self.sweep_up_once(freq_min,freq_max,step,dt)
        if not self.sweeper_freq:
            self.sweeper_freq = QtSweeper()
        self.sweeper_freq.sweep(freq_min,freq_max,step,dt,resume,loop,bidirectional)
    def sweep_up_loop_bidi(self,freq_min,freq_max,step,dt):
        print("sweep bidirectional loop")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,step,step,delta,delta)
        self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setNoDwellLow(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        self.dds.ask("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
        #self.dds.ask("K0:")
    def sweep_int_setup(self, xxx_todo_changeme):
        (fstart,fstop,step,dt,mode) = xxx_todo_changeme
        self.dds.setupSweep(fstart,fstop,step,step,dt,dt)
        #self.dds.setClearDrgAccum(False)
        self.int_sweep_mode = mode
        print("internal sweep mode is",mode)
        if mode==0:
            self.dds.setupSweepUpOnce()
            self.dds.setupSetSweepControlInt()
        elif mode==4:
            #self.dds.setupSweepUpOnce()
            self.dds.setDRGINT(False)
            self.dds.setDRG(False)
            self.dds.setClearDrgAccum(True)
            self.dds.setNoDwellHigh(False)
            self.dds.setNoDwellLow(True)
            self.dds.setClearDrgAccum(False)
            self.status()
            # self.dds.setupSetSweepControlInt()
        elif mode == 1:
            self.dds.setupSetSweepControlInt()
            self.dds.setNoDwellHigh(True)
            self.dds.setNoDwellLow(True)
        elif mode == 5:
            self.dds.setDRGINT(False)
            self.dds.setNoDwellHigh(True)
            self.dds.setNoDwellLow(True)
    def intSweepStart(self,start):
        self.dds.setDRG(False)
        print("internal sweep",start,"{:g} s".format(self.dds.sweepTimeUp))
        if start:
            self.dds.setDRG(start)
        if self.int_sweep_mode == 0: #launch timer to stop gui
             self.IntSweepTimer.start(self.dds.sweepTimeUp*1000)
    def sweep_up_loop(self,freq_min,freq_max,step,dt):
        print("sweep forward loop")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,freq_max-freq_min,step,1,delta)
        #self.dds.setAmpCommon(self.dds.amplitude)
        self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setNoDwellLow(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        self.dds.ask("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
    def sweep_up_once(self,freq_min,freq_max,step,dt):
        print("sweep forward once")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,step,step,delta,delta)
        print("setting up amplitude",self.dds.amplitude)
        #self.dds.setAmpCommon(self.dds.amplitude)
       # self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        self.dds.ask("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
        if not self.loop:
            self.dds.ask("K0:")
    def announceFreq(self):
        ct = time.time()
        t_elapsed = ct-self.sweepStartTime
        tsweep = (self.sweepStop-self.sweepStart)/self.sweepSpeed
        periods = floor(t_elapsed/tsweep)
        if self.bidi and periods%2:
            t = (periods+1)*tsweep-t_elapsed
        else:
            t = t_elapsed-periods*tsweep
        #tel2 = t_elapsed - t_elapsed/self.sweepDt*DDS_STEP_TIME
        #print "time correction ",t_elapsed,tel2,"steps",t_elapsed/self.sweepDt
        freq = self.sweepStart+self.sweepSpeed*t*DDS_SPEED_COEFF
        #print freq
        self.sig.emit((ct,freq))
        self.sig_float.emit(freq)
        if self.stopped or (not self.loop and periods>0):
            self.stop()
    def stop(self):
        self.timer.stop()
        self.sig_stopped.emit()
        self.sig.emit("stop")
        if not self.stopped:
            self.stopped = True
            self.dds.setDRG(False)
            self.dds.setAmpFromProfile(True)
        freq = self.dds.queryFreq()
        self.sig_float.emit(freq)
    def toggleDREXT(self):
        # if self.dds.DREXT==None:
        #     self.status()
        # self.dds.DREXT = not self.dds.DREXT
        rep = self.dds.setDRGINT(not self.dds.DREXT)
        print(self.dds.DREXT,rep)
        self.status()
        return rep






# class e3DDS_Class_Qt(QtCore.QObject,e3DDS_Class):
#     def __init__(self,*args):
#         super(e3DDS_Class_Qt,self).__init__()




