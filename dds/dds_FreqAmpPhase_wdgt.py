
import DDS_FreqAmpPhas_widget
from qtpy import QtWidgets,QtCore
import sys
sys.path.append( '../modules')
import si_units,pyqtSpinboxAdvanced

class FreqAmpPhase(DDS_FreqAmpPhas_widget.Ui_DDS_FreqAmpPhas_widget,QtWidgets.QWidget):
    updated = QtCore.Signal()
    def __init__(self,dds):
        super(FreqAmpPhase,self).__init__()
        self.setupUi(self)
        self.sb_freq.setMaximum(dds.fsys)
        pyqtSpinboxAdvanced.upgradeAllDoubleSpinBoxes(self)
        self.mult_freq = 1e6
        self.dds = dds
        if hasattr(dds,"PROFILENUMBER"):
            self.profilesnum = self.dds.PROFILENUMBER
            if self.profilesnum>0:
                for i in range(0,self.profilesnum):
                    self.comboBox.addItem("Profile {}".format(i))
        else:
            self.profilesnum = 0
            self.comboBox.hide()
        self.queryfunc = self.dds.queryFrequency
        self.pb_apply.pressed.connect(self.apply)
        self.comboBox.currentIndexChanged.connect(self.switchProfile)
        self.updated.connect(self.show) # can be removed later
    def show(self):
        super(FreqAmpPhase,self).show()
        self.showCurrentStatus()
    def showCurrentStatus(self):
        #if self.queryfunc:
            if self.profilesnum:
                profile,freq,amp,phase = self.dds.getCurrentProfile()
                self.profile = profile
                self.label_freq.setText(si_units.si_str(freq,"Hz"))
                self.label_phase.setText("{:.1f}".format(phase))
                self.label_amp.setText("{:.1f} %".format(amp*100))
                self.label_profile.setText(str(profile))
                self.comboBox.setCurrentIndex(profile)
                self.updateProfileControls(freq,amp,phase)
    def apply(self):
        profile = self.comboBox.currentIndex()
        freq = self.sb_freq.value()*self.mult_freq
        amp  = self.sb_amp.value()/100
        phase = self.sb_phas.value()
        print("setting profile {}, {:g} Hz, amp {},phase {}".format(profile,freq,amp,phase))
        res = self.dds.setProfile(profile,freq,amp,phase)
        print(res)
        if self.profile == profile:
            self.updated.emit()
    def switchProfile(self,profile):
        freq,amp,phase = self.dds.getProfile(profile)
        self.updateProfileControls(freq,amp,phase)
    def updateProfileControls(self,freq,amp,phase):
        if freq:
            # freq1,un = si_units.si_fmt(freq,"Hz")
            # print(freq,un)
            # self.mult_freq = freq/freq1
            self.sb_freq.setValue(freq/self.mult_freq)
            # self.sb_freq.setValue(freq1)
            # self.sb_freq.setSuffix(un)
        if amp:
            self.sb_amp.setValue(amp*100)
        if phase:
            self.sb_phas.setValue(phase)

