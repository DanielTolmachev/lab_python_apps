register_description = \
{0: {'bytes': [(0, 1, 'LSB first mode', ''),
               (1, 1, 'SDIO input only', ''),
               (3, 1, 'External power-down control', ''),
               (5, 1, 'REFCLK input power-down', ''),
               (6, 1, 'DAC power-down', ''),
               (7, 1, 'Digital power-down', ''),
               (8, 1, 'OSK enable', ''),
               (9, 1, 'External OSK enable', ''),
               (11, 1, 'Clear phase accumu-lator', ''),
               (12, 1, 'Clear DRG accumulator', ''),
               (13, 1, 'Autoclear phase accumulator', ''),
               (14, 1, 'Autoclear digital ramp accumulator', ''),
               (15, 1, 'Load LRR at I/O update', ''),
               (16, 1, 'Select DDS sine output', ''),
               (17, 1, 'Parallel port streaming enable', ''),
               (24, 1, 'VCO cal enable', '')],
     'name': 'CFR1'},
 1: {'bytes': [(8, 1, 'SYNC out/in mux enable', ''),
               (9, 1, 'SYNC_OUT enable', ''),
               (10, 1, 'SYNC_CLK invert', ''),
               (11, 1, 'SYNC_CLK enable', ''),
               (13, 1, 'DRG over output enable', ''),
               (14, 1, 'Frequency jump enable', ''),
               (15, 1, 'Matched latency enable', ''),
               (16, 1, 'Program modulus enable', ''),
               (17, 1, 'DRG no dwell low', ''),
               (18, 1, 'DRG no dwell high', ''),
               (19, 1, 'DRG Enable', ''),
               (20, 2, 'DRG destination', ''),
               (22, 1, 'Parallel data port enable', ''),
               (23, 1, 'Profile mode enable', '')],
     'name': 'CFR2'},
 2: {'bytes': [(0, 2, 'Minimum LDW', ''),
               (2, 1, 'Lock detect enable', ''),
               (3, 3, 'Icp', ''),
               (6, 1, 'Manual Icp selection', ''),
               (8, 8, 'Feedback divider N', ''),
               (16, 1, 'Doubler clock edge', ''),
               (17, 1, 'PLL input divider enable', ''),
               (18, 1, 'PLL enable', ''),
               (19, 1, 'Doubler enable', ''),
               (20, 2, 'Input divider', ''),
               (22, 1, 'Input divider reset', '')],
     'name': 'CFR3'},
 3: {'bytes': [(0, 24, 'register default values', ''),
               (24, 1, 'DAC CAL enable', ''),
               (25, 1, 'DAC CAL clock power-down', ''),
               (26, 1, 'Auxiliary divider power-down', '')],
     'name': 'CFR4'},
 4: {'bytes': [], 'name': 'Digital Ramp Lower Limit'},
 5: {'bytes': [], 'name': 'Digital Ramp Upper Limit'},
 6: {'bytes': [], 'name': 'Rising Digital Ramp Step Size Register'},
 7: {'bytes': [], 'name': 'Falling Digital Ramp Step Size Register'},
 8: {'bytes': [], 'name': 'Digital Ramp Rate'},
 9: {'bytes': [], 'name': 'Lower Frequency Jump Register'},
 10: {'bytes': [], 'name': 'Upper Frequency Jump Register'},
 11: {'bytes': [(0, 32, 'frequency', '')],
      'name': 'Profile 0 Frequency Tuning Word 0 Register'},
 12: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 0 Phase/ Amplitude Register'},
 13: {'bytes': [(0, 32, 'frequency', '')],
      'name': 'Profile 1 Frequency Tuning Word 1 Register'},
 14: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 1 Phase/ Amplitude Register'},
 15: {'bytes': [(0, 32, 'frequency', '')],
      'name': 'Profile 2 Frequency Tuning Word 2 Register'},
 16: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 2 Phase/ Amplitude Register'},
 17: {'bytes': [], 'name': 'Profile 3 Frequency Tuning Word 3 Register'},
 18: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 3 Phase/ Amplitude Register'},
 19: {'bytes': [], 'name': 'Profile 4 Frequency Tuning Word 4 Register'},
 20: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 4 Phase/ Amplitude Register'},
 21: {'bytes': [], 'name': 'Profile 5 Frequency Tuning Word 5 Register'},
 22: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 5 (P7) Phase/ Amplitude Register'},
 23: {'bytes': [], 'name': 'Profile 6 Frequency Tuning Word 6 Register'},
 24: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 6 Phase/ Amplitude Register'},
 25: {'bytes': [], 'name': 'Profile 7 Frequency Tuning Word 7 Register'},
 26: {'bytes': [(0, 15, 'phase', ''), (16, 15, 'amplitude', '')],
      'name': 'Profile 7 Phase/ Amplitude Register'},
 27: {'bytes': [(0, 3, 'SYNC_IN delay ADJ', ''),
                (3, 3, 'SYNC_OUT delay ADJ', ''),
                (6, 1, 'CAL with SYNC', ''),
                (7, 1, 'Reserved', ''),
                (8, 16, 'register default values', ''),
                (24, 1, 'PLL lock', '')],
      'name': 'USR0'}}