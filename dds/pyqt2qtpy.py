import re,os
from qtpy import QtGui,QtWidgets

modules = set()
problems = []

def analyze(f):
    res = []
    f.seek(0)
    lines = f.readlines()
    modules = set()
    global problems
    for i,l in enumerate(lines):
            srch = re.search("QtGui\.(\w+)",l)
            if srch:
                orig = srch.groups()[0]
                orig_full = srch.group()
                if hasattr(QtWidgets,orig):
                    sub = "QtWidgets."+orig
                    modules.add("QtWidgets")
                elif hasattr(QtGui,orig):
                    sub = "QtGui." + orig
                    modules.add("QtGui")
                elif hasattr(QtGui,orig):
                    sub = "QtPrintSupport." + orig
                    modules.add("QtPrintSupport")
                else:
                    sub = None
                if sub:
                    res.append((i,orig_full,sub))
            srch = re.search("QtCore.pyqt(bound){0,1}Signal",l)
            if srch:
                orig_full = srch.group()
                sub = "QtCore.Signal"
                res.append((i,orig_full,sub))
        #     else:
        #         print("cannot find where to put", orig_full)
        #         problems.append(orig_full)
        #         sub = None
        #     if sub:
        #         print(orig_full,"->",sub)
        #         res.append((l,orig_full,sub))
        #     else:
        #         res.append(l)
        # else:
        #     res.append(l)
    return modules,res,lines

def replace_strings(filename, repl, import_line_index):
    #with open(filename,"w") as f:
        print("***",filename)
        lines = []
        cnt = 0
        for i,l in enumerate(repl):
            if i==import_line_index:
                n = re.sub("(\s*from\s+)PyQt\d(\s+import.*)", r"\1qtpy\2", l)
                n = re.sub("QtGui(.*)", ",".join(modules), n)
                print(repr(l))
                print(repr(n))
                lines.append(n)
                cnt+=1
            elif type(l)==str:
                lines.append(l)
            elif type(l)==tuple:
                l, orig, sub = l
                n = l.replace(orig, sub)
                print(repr(l))
                print(repr(n))
                lines.append(n)
                cnt += 1
        print("total replacements",cnt,"***")
        if do_replace:
            with open(filename,"w") as f:
                f.writelines(lines)
        return lines,cnt


def replace_strings2(filename,repl, mod, import_line_index,lines):
    # with open(filename,"a",encoding = "utf8") as f:
    #     lines = f.readlines()
        #replace imports
    for i,old,new in repl:
        l1 = lines[i]
        l2 = l1.replace(old,new)
        lines[i] = l2
        print("\t{} ->\n\t{}".format(repr(l1),repr(l2)))
    with open(filename,"w",encoding = "utf8") as f:
        f.writelines(lines)


def processFile(filename):
    with open(filename,encoding = "utf8") as f:
        go = True
        import_line_index = 0
        repl = []
        while go:
            l = f.readline()
            if "import" in l:
                if "qtpy" in l:
                    print(filename,"already uses qtpy")
                    break
                elif "PyQt" in l:
                    if "uic" in l:
                        print(filename, "uses uic, it is probably auxiliar file, skipping")
                        break
                    srch = re.search("\s*(from\s+PyQt\d+\s+import\s+)(.*)",l)
                    if srch:
                        modules = set([s.strip() for s in srch.groups()[1].split(",")])
                        if "QtGui" in modules:
                            modules.remove("QtGui")
                    else:
                        print("Error processing string",l)
                        break
                    m2,rep2,lines = analyze(f)
                    modules.update(m2)
                    repl.append((import_line_index, srch.group(),"from qtpy import "+",".join(modules)))
                    repl +=rep2

            elif not l:
                break
            import_line_index +=1
    if repl:
            print(filename, "imports:", modules)
            bn,ext = os.path.splitext(filename)
            if replace_original:
                fn = filename
            else:
                fn = bn + ".R" + ext
            if do_replace:
                replace_strings2(fn, repl, modules, import_line_index,lines)
            else:
                for r in repl:
                    print("\t{}: {} -> {}".format(r[0],r[1],r[2]))

do_replace = True
replace_original = True
os.chdir("..\Bruker_magnet")

py_files = [f for f in os.listdir() if f.endswith(".py") or f.endswith(".pyw") ]
for f in py_files:
    processFile(f)

if problems:
    print("there are some problems:")
    for p in problems:
        print("\t",p)