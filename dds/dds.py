#-------------------------------------------------------------------------------
# Name:     dds.py
# Purpose:  control DDS generator made for e3.physik.tu-dortmund.de
#
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Author:      Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------
DEV_ADDRESS = "129.217.155.87"
DEV_PORT = 51234

#PUBLISH_ADDRESS = 'tcp://127.0.0.1:5003'
PUBLISH_ADDRESS = 'tcp://127.0.0.1:5011'
SUB_PORT = 6011
UNITS_DEFAULT = "MHz"
UNITS_USED = {"Hz":1,"kHz":1e3,"MHz":1e6}

import zmq
from qtpy import QtWidgets,QtCore
import sys
sys.path.append( '../modules')
from sweeper_msg import SweeperWMsg
from zmq_publisher import Zmq_Publisher
import sweep_widget
from options import Options
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
from dds3E_dev_class import DDS3E_Class
from dds_controller import DDS_Controller
from zmq_sub2qt import Zmq2Qt
import QMenuFromDict
from console_qt import QConsole
import dds_qt_view2

opt = Options()
app = QtWidgets.QApplication(sys.argv)
cons = QConsole()

context = zmq.Context()
pub = Zmq_Publisher(context,PUBLISH_ADDRESS)
sub = Zmq2Qt(context,SUB_PORT,verbose = False)
dds = DDS3E_Class((DEV_ADDRESS,DEV_PORT),autoconnect = True,autoupdate = True)
dds.interface.keep_connection = True
dds_view = dds_qt_view2.DDS_QT_VIEW(dds)
dds_view.registerChanged.connect(dds.writeRegister)

control = DDS_Controller()
control.dds = dds
conthrd = QtCore.QThread()

win = QMainWindowAutoSaveGeometry()
win.setWindowTitle('DDS')
sb = win.statusBar()
#sb = QtWidgets.QStatusBar()
label_connection_status = QtWidgets.QLabel()
sb.addPermanentWidget(label_connection_status,1)

if 'Window.geometry' in opt:
    win.setGeometry(*opt['Window.geometry'])
def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk
win.GeometryChangedSig.connect(save_win_position)
#menu_fFromDict
units = opt.get('current_units',UNITS_DEFAULT)
if not units in list(UNITS_USED.keys()):
    units = UNITS_DEFAULT

sw = sweep_widget.SweepWidget(units = units,
                              sweepValueDesc = 'SweepFrequency',
                              units_multiplier = UNITS_USED[units],
                              units_used_dict = UNITS_USED)
widget_pwr = sweep_widget.SweepWidget(units = 'dBm',
                              sweepValueDesc = 'SweepPower',
                              units_multiplier = 1 )

tw = QtWidgets.QTabWidget()
tw.addTab(sw,'Frequency')

gl = QtWidgets.QGridLayout()
gl.addWidget(tw,0,0)
#win.setCentralWidget(tw)
win.setCentralWidget(QtWidgets.QWidget())
win.centralWidget().setLayout(gl)

sld_amp = QtWidgets.QSlider()
sld_amp.setOrientation(QtCore.Qt.Vertical)
sld_amp.setRange(0,100)
sld_amp.setSingleStep(10)
sld_amp.setSliderPosition(100)
sld_amp.setTickPosition(QtWidgets.QSlider.TicksLeft)
sld_amp.setMouseTracking(True)
gl.addWidget(sld_amp,0,2)
#gl.addWidget(tw)

QMenuFromDict.AddMenu(win.menuBar(),
                      {"device":{
                        "connect":control.open,
                        "disconnect":control.close,
                        "reset DDS":dds.reset},
                       "console":cons.show,
                       "registers":dds_view.show
                      })


def setamp(val):
    #val = sld_amp.value()
    print("Setting amplitude",val,"%")
    dds.setAmplitude(.01*val)
sld_amp.valueChanged.connect(setamp)
def amp_slider_disconnect():
    sld_amp.valueChanged.disconnect(setamp)
def amp_slider_connect():
    sld_amp.valueChanged.connect(setamp)
    setamp(sld_amp.value())
sld_amp.sliderPressed.connect(amp_slider_disconnect)
sld_amp.sliderReleased.connect(amp_slider_connect)

def show_conn_status(status):
    if status:
        label_connection_status.setText("connected to {}".format(dds.interface.address))
    else:
        label_connection_status.setText("disconnected")

#tw.addTab(widget_pwr,'Power')

#control.moveToThread(conthrd)


sw_freq = SweeperWMsg(  messager = control.rcv,
                        setfunc = dds.setFreq,
                        queryfunc = dds.queryFreq,
                        verbose = False)
# sw_pwr = SweeperWMsg(  messager = signalproxy.rcv,
#                         setfunc = dds.setPower,
#                         queryfunc = dds.queryPower,
#                         verbose = False)


sub.sigGotMessage.connect(control.processCommand)
control.sig_new_range.connect(sw.setWorkingRange)
sw.sig_range_changed.connect(control.sendrange)

sw.connect_options_dictionary(opt)
sw.sig_params_changed.connect(opt.savetodisk) #save options immidiately
sw.sig_start_sweeper.connect(control.sweep)
sw.sig_stop_sweeper.connect(control.stop)
sw.sig_started.connect(pub.send_pyobj)
control.sig_stopped.connect(sw.setGuiStopped)
# sw.sig_value_set.connect(control.setqueryFreq) #set current from GUI widget
sw.sig_value_set.connect(dds.setFreq) #set value from GUI widget
sw.sig_value_set.connect(sw.set_central_label) #set gui view from GUI widget
control.sig.connect(pub.send_pyobj) #send tuple away using zmq
control.sig_float.connect(sw.set_central_label) #modify widget central value
sw.sig_range_changed.connect(control.sendrange)
control.connection_status_changed.connect(show_conn_status)



#dds.open()
control.open()
#show_conn_status(dds.interface.connected)
dds.startSingleTone()
dds.setAmplitude(1)
dds.setAmpFromProfile(True)
#sld_amp.setSliderPosition()
sw.set_central_label(dds.queryFreq())
if __name__ == '__main__':
    if 'demo' in sys.argv:
        import demo_device
        dds = demo_device.demo_device()
    conthrd.start()
    win.show()
    app.exec_()
    sw_freq.stop()
    sw_freq.join()
    opt.savetodisk()
    dds.close()



