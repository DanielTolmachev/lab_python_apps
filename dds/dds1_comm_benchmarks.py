#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel Tolmachev
#
# Created:     14.12.2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
# reg 0x0
#"00001002" clear  DRG accum True
# "00000002" clear DRG accum False
# reg 0x1
# "00480020" defaults + DRG enable  (both no-dwells disabled)



from dds1_dev_class import e3DDS1_Class
import numpy as np
import time

def test_speed(deltat = 0.1,time_total = 10):
    t0 = time.time()
    t  = t0
    f = 1.e6
    df = 1.e4
    cnt = 0
    regdata = dds.readRegister(0,verbose = False)
    while t < t0+time_total:
        t = time.time()
        dds.setFrequency(f)
        cnt +=1
        f = f+df
        #rep = dds.readRegister(0,verbose = False)
        #print rep
##        if rep != regdata:
##            msg = "dds is overflown after {:.0f} s\t{} iterations, dt = {}".format(
##                t-t0,cnt,deltat)
##            #print msg
##            return msg

        time.sleep(deltat)
    msg = "program reached time limit {:.0f} s\t{} iterations, dt = {}".format(
                t-t0,cnt,deltat)
    return msg
if __name__ == '__main__':
    import time
    addr = ("129.217.155.106",1234)
    gpibaddr = 27
    dds = e3DDS1_Class(addr,gpib= gpibaddr,timeout = 1,gpib_single_device = True)
    dds.open()
    dds.reset()
    dds.setClockinternal()
    dds.setAmplitude(1)
    dds.write("I4F:U:")
    dds.status()
    report = []
    comment = ""
    for dt in [.001, 0.001, 0.005 , 0.1, 0.2, 1]:
##        dds.reset()
##        dds.setbit(1,23,True)
##        dds.setbit(1,14,"00")
##        dds.autoupdate = False
        result = test_speed(dt,200)
        report.append(result)
    for r in report:
        print(r)
    with open("dds1_benchmark.log","a+") as f:
        f.write( time.strftime("\r\n%Y-%m-%d %H:%M:%S\r\n"))
        if comment:
            f.write(comment)
            f.write("\r\n")
        for l in report:
            f.write(l+"\r\n")
