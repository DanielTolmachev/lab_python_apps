#-------------------------------------------------------------------------------
# Name:     dds.py
# Purpose:  control DDS1 generator made for e3.physik.tu-dortmund.de
#           through GPIB to Ethernet adaptor
#
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

DEV_ADDRESS = "129.217.155.3"
DEV_PORT = 1234
DEV_GPIB_ADDRESS = 27

#PUBLISH_ADDRESS = 'tcp://127.0.0.1:5003'
PUBLISH_ADDRESS = 'tcp://*:5015'
SUB_PORT = 6011
UNITS_DEFAULT = "MHz"
UNITS_USED = {"Hz":1,"kHz":1e3,"MHz":1e6}

import zmq
from qtpy import QtWidgets,QtCore,QtGui
import sys
sys.path.append( '../modules')
import install_exception_hook
from sweeper_msg import SweeperWMsg
from zmq_publisher import Zmq_Publisher
import sweep_widget
from options import Options
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
from dds1_dev_class import DDS1_dev_class
from dds4_controller import DDS_Controller
from zmq_sub2qt import Zmq2Qt
import QMenuFromDict,quickQtApp
from console_qt import QConsole
import dds_qt_view3
import dds_FreqAmpPhase_wdgt
import eraser_widget
from numpy import exp,e

app,opt,win,cons = quickQtApp.mkAppWin()

context = zmq.Context()
pub = Zmq_Publisher(context,opt.get("pub.address",PUBLISH_ADDRESS))
sub = Zmq2Qt(context,SUB_PORT,verbose = False)
dev_address = opt.get("dev.address",DEV_ADDRESS)
gpib_address = opt.get("dev.gpib_address",DEV_GPIB_ADDRESS)
dds = DDS1_dev_class((dev_address,DEV_PORT),
                     gpib_address = gpib_address,
                     autoconnect = True,
                     autoupdate = True,
                     verbose = True,
                     keep_connection = False,
                     gpib_single_device = False) #will send f.ex. ++addr27 each time
# dds.interface.keep_connection = True
dds_view = dds_qt_view3.DDS_QT_VIEW(dds)
dds_view.registerChanged.connect(dds.writeRegister)

control = DDS_Controller()
control.dds = dds
control.sweeper_freq.setFunction = dds.setFrequency
conthrd = QtCore.QThread()

win = QMainWindowAutoSaveGeometry()
win.setWindowTitle('DDS1')
sb = win.statusBar()
#sb = QtWidgets.QStatusBar()
label_connection_status = QtWidgets.QLabel()
sb.addPermanentWidget(label_connection_status,1)

#set application icon
icon = QtGui.QIcon("dds1.png")
win.setWindowIcon(icon)

if 'Window.geometry' in opt:
    win.setGeometry(*opt['Window.geometry'])
def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk
win.GeometryChangedSig.connect(save_win_position)
#menu_fFromDict
units = opt.get('current_units',UNITS_DEFAULT)
if not units in list(UNITS_USED.keys()):
    units = UNITS_DEFAULT

sw = sweep_widget.SweepWidget(units = units,
                              sweepValueDesc = 'SweepFrequency',
                              units_multiplier = UNITS_USED[units],
                              units_used_dict = UNITS_USED)
widget_pwr = sweep_widget.SweepWidget(units = 'dBm',
                              sweepValueDesc = 'SweepPower',
                              units_multiplier = 1 )


tw = QtWidgets.QTabWidget()
tw.addTab(sw,'sweep Frequency')

apf = dds_FreqAmpPhase_wdgt.FreqAmpPhase(dds)
tw.addTab(apf,"Main")
gl = QtWidgets.QGridLayout()
gl.addWidget(tw,0,0)
#win.setCentralWidget(tw)
win.setCentralWidget(QtWidgets.QWidget())
win.centralWidget().setLayout(gl)

if opt.get("show_eraser",True):
    w_er = eraser_widget.Eraser_Widget(dds,opt)
    tw.addTab(w_er,"Eraser")

def tw_tabChanged(idx):
    tw.currentWidget().show()
tw.currentChanged.connect(tw_tabChanged)

sld_amp = QtWidgets.QSlider()
sld_amp.setOrientation(QtCore.Qt.Vertical)
sld_amp.setRange(0,100)
sld_amp.setSingleStep(10)
sld_amp.setSliderPosition(100)
sld_amp.setTickPosition(QtWidgets.QSlider.TicksLeft)
sld_amp.setMouseTracking(True)
gl.addWidget(sld_amp,0,2)
#gl.addWidget(tw)

QMenuFromDict.AddMenu(win.menuBar(),
                      {"device":{
                        "connect":control.open,
                        "disconnect":control.close,
                        "reset DDS":dds.reset},
                       "console":cons.show,
                       "registers":dds_view.show
                      })

LOG_SCALE_COEFFICIENT = 3
amplitude_scale_db_per_tick = .25 # total ticks is 100
def setamp(val):
    #val = sld_amp.value()
    if val==0:
        amp = 0
    else:
        amp = 1./10**((100-val)*amplitude_scale_db_per_tick/10)
    # amp = ( exp(val/100) ** LOG_SCALE_COEFFICIENT - 1) / (e ** LOG_SCALE_COEFFICIENT - 1)
    print("Setting amplitude {:g} ({})".format(amp,val))
    dds.setAmplitude(amp)
sld_amp.valueChanged.connect(setamp)
def amp_slider_disconnect():
    sld_amp.valueChanged.disconnect(setamp)
def amp_slider_connect():
    sld_amp.valueChanged.connect(setamp)
    setamp(sld_amp.value())
sld_amp.sliderPressed.connect(amp_slider_disconnect)
sld_amp.sliderReleased.connect(amp_slider_connect)

def show_conn_status(status):
    if status:
        label_connection_status.setText("connected to {}".format(dds.interface.address))
    else:
        label_connection_status.setText("disconnected")

#tw.addTab(widget_pwr,'Power')

#control.moveToThread(conthrd)


sw_freq = SweeperWMsg(  messager = control.rcv,
                        setfunc = dds.setFreq,
                        queryfunc = dds.queryFreq,
                        verbose = False)
# sw_pwr = SweeperWMsg(  messager = signalproxy.rcv,
#                         setfunc = dds.setPower,
#                         queryfunc = dds.queryPower,
#                         verbose = False)


sub.sigGotMessage.connect(control.processCommand)
control.sig_new_range.connect(sw.setWorkingRange)
sw.sig_range_changed.connect(control.sendrange)

sw.connect_options_dictionary(opt)
sw.sig_params_changed.connect(opt.savetodisk) #save options immidiately
sw.sig_start_sweeper.connect(control.sweep)
sw.sig_stop_sweeper.connect(control.stop)
sw.sig_stop_sweeper.connect(control.sweeper_freq.stop)
sw.sig_started.connect(pub.send_pyobj)
control.sweeper_freq.stopped.connect(sw.setGuiStopped)
control.sig_stopped.connect(sw.setGuiStopped)
# sw.sig_value_set.connect(control.setqueryFreq) #set current from GUI widget
sw.sig_value_set.connect(dds.setFreq) #set value from GUI widget
sw.sig_value_set.connect(sw.set_central_label) #set gui view from GUI widget
#control.sweeper_freq.newValueSet.connect(sw.set_central_label)
control.sweeper_freq.message.connect(control.rcv)
control.sig.connect(pub.send_pyobj) #send tuple away using zmq
control.sig_float.connect(sw.set_central_label) #modify widget central value
sw.sig_range_changed.connect(control.sendrange)
control.connection_status_changed.connect(show_conn_status)



# dds.open()
# dds.status_print()
control.open()
#dds.setOSK(True)
#show_conn_status(dds.interface.connected)

#sld_amp.setSliderPosition()

sw.set_central_label(dds.queryFreq())
if __name__ == '__main__':
    if 'demo' in sys.argv:
        import demo_device
        dds = demo_device.demo_device()
    conthrd.start()
    win.show()
    app.exec_()
    sw_freq.stop()
    sw_freq.join()
    opt.savetodisk()
    dds.close()



