from dds3E_dev_class import DDS3E_Class
import time

class SparseSweeper(object):
    def __init__(self,dds):
        self.dds = dds
    def setSweepRegions(self,regions):
        self.regions = []
        if not isinstance(regions,(list, tuple)):
            print("sparse sweeper accepts list of tuplles containing ranges of frequencies",
                  "\ngot",regions)
            return
        for reg in regions:
            if not isinstance(reg,tuple) or len(reg)!=2:
                print("range shuld be tuple (fstart,fstop),\ngot",reg)
                return
            self.regions.append(reg)
    def setSweepSpeed(self, speed):
        self.speed = speed
    def setTimes(self,t1,t2):
        self.t1 = t1
        self.t2 = t2
    def prepare(self):
        self.dds.setNoDwellHigh(True)
        self.dds.setNoDwellLow(True)
        self.step, self.dt = self.dds.calcSweep(self.regions[0][0],self.regions[0][1],self.t1)
        self.dds._setupSweepRegister(self.regions[0][1],self.regions[0][0],
                                     self.step,self.step,
                                     self.dt, self.dt)
    def start(self):
        if not hasattr(self,"step"):
            print("must call prepare() first")
            return
        self.go = True
        self.dds.setDRG(True)
        r = 0
        rm = len(self.regions)
        while self.go:
            self.dds._setupSweepRegister(self.regions[r][1], self.regions[r][0],
                                         self.step, self.step,
                                         self.dt, self.dt)
            time.sleep(self.t2)
            r+=1
            if r>=rm:
                r = 0
    def stop(self):
        self.go = False



if __name__ =="__main__":
    addr = ("129.217.155.87", 51234)
    dds = DDS3E_Class(addr, timeout=1)
    dds.open()
    sps = SparseSweeper(dds)
    sps.setSweepRegions([(1e6,2e6),
                         (100e6,200e6)])
    sps.setTimes(1e-3,2)
    sps.prepare()