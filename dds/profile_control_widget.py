from qtpy import QtWidgets,QtCore
import si_units

class FrameLocalProfile(QtWidgets.QFrame):
    def __init__(self):
        super().__init__()
        self.setFrameStyle(1)

class ProfileControlWidget(QtWidgets.QWidget):
    sig_profile_was_changed = QtCore.Signal(int,float,float,float)
    sig_profile_was_chosen = QtCore.Signal(int)
    def __init__(self, dds, number_of_profiles = 8, freq_max = 1e9, freq_min = 0):
        super(ProfileControlWidget,self).__init__()
        self.setWindowTitle("Profile Control")
        self.dds = dds
        self.number_of_profiles = number_of_profiles
        self.autoswitch_profile = (self.dds.device_name == "DDS3E")
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)
        self.gl.addWidget(QtWidgets.QLabel("Profile"),0,0)
        self.gl.addWidget(QtWidgets.QLabel("Bits\nConnectors"), 0, 1)
        self.gl.addWidget(QtWidgets.QLabel("Phase"), 0, 2)
        self.gl.addWidget(QtWidgets.QLabel("Amplitude"), 0, 3)
        self.gl.addWidget(QtWidgets.QLabel("Frequency"), 0, 4, 1, 2)
        row = self.gl.rowCount()
        self.sb_phase = []
        self.sb_ampl = []
        self.sb_freq = []
        self.phase = []
        self.ampl = []
        self.freq = []
        self.lbl_freq_units = []
        self.freq_mult = []
        self.lbl_profile = []
        self.profile_modified = []
        self.btns_profile = []
        for i in range(0,self.number_of_profiles):
            lbl = QtWidgets.QLabel(str(i))
            self.gl.addWidget(lbl,row,0)
            self.lbl_profile.append(lbl)
            lbl = QtWidgets.QLabel("{:03b}".format(i))
            self.gl.addWidget(lbl, row, 1)
            sb = QtWidgets.QDoubleSpinBox()
            sb.valueChanged.connect(self.on_phase_changed)
            self.gl.addWidget(sb,row,2)
            self.sb_phase.append(sb)
            sb = QtWidgets.QDoubleSpinBox()
            sb.valueChanged.connect(self.on_ampl_changed)
            self.gl.addWidget(sb, row, 3)
            self.sb_ampl.append(sb)
            sb = QtWidgets.QDoubleSpinBox()
            sb.valueChanged.connect(self.on_freq_changed)
            self.gl.addWidget(sb, row, 4)
            self.sb_freq.append(sb)
            lbl = QtWidgets.QLabel("MHz")
            # print(lbl)
            self.gl.addWidget(lbl, row, 5)
            self.lbl_freq_units.append(lbl)
            btn = QtWidgets.QPushButton("set")
            btn.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Fixed)
            btn.pressed.connect(self.on_profile_choose)
            self.btns_profile.append(btn)
            # btn.setMaximumWidth(25)
            self.gl.addWidget(btn, row, 7)
            self.profile_modified.append(False)
            self.ampl.append(0.)
            self.phase.append(0.)
            self.freq.append(0.)
            self.freq_mult.append(1)
            row += 1
        self.btn_reload = QtWidgets.QPushButton("reload")
        if self.autoswitch_profile:
            self.btn_reload.setToolTip("""Do not press during experiment!
on this device ({}) this function will consequently activate each of profiles""".format(self.dds.device_name))
        self.btn_reload.pressed.connect(self.reloadInfo)
        self.gl.addWidget(self.btn_reload,0,7)
        # bg = FrameLocalProfile()
        # self.gl.addWidget(bg,row,7)
        self.cb_externalprofile = QtWidgets.QCheckBox("External\nprofile")
        self.cb_externalprofile.toggled.connect(self.on_cb_externalprofile_toggled)
        self.gl.addWidget(self.cb_externalprofile,row,7)
    #buttons in vertical layout
        self.hl = QtWidgets.QHBoxLayout()
        self.gl.addLayout(self.hl, row, 0, 1, 7)
        # self.hl.setAlignment(QtWidgets.)
        self.btn_apply = QtWidgets.QPushButton("Apply")
        self.btn_apply.setEnabled(False)
        self.btn_apply.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Maximum)
        self.btn_apply.pressed.connect(self.apply)
        self.btn_revert = QtWidgets.QPushButton("Revert")
        self.btn_revert.setEnabled(False)
        self.btn_revert.setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        # self.hl.addItem(QtWidgets.QSpacerItem(0,0))
        self.hl.addWidget(self.btn_apply)
        self.hl.addWidget(self.btn_revert)
        # self.gl.addWidget(self.btn_apply,row,2,1,4)
        self.setFrequencyLimits(freq_min,freq_max)
    def show(self):
        super(ProfileControlWidget,self).show()
        self.reloadInfo()
    def setProfile(self,profile,amplitude,phase,frequency):
        if amplitude == None:
            amplitude  = 0
        if phase == None:
            phase = 0
        self.ampl[profile] = amplitude
        self.freq[profile] = frequency
        self.phase[profile] = phase
        self.sb_ampl[profile].setValue(amplitude)
        self.sb_phase[profile].setValue(phase)
        self.displayFreq(profile,frequency)
        self.clearMidified(profile)
    def setFrequencyLimits(self,freq_min,freq_max):
        for sb in self.sb_freq:
            sb.setRange(freq_min,freq_max)
    def setModified(self,profile):
        self.lbl_profile[profile].setText(str(profile) + "*")
        self.profile_modified[profile] = True
        self.btn_apply.setEnabled(True)
        self.btn_revert.setEnabled(True)
    def clearMidified(self,profile):
        self.lbl_profile[profile].setText(str(profile))
        self.profile_modified[profile] = False
        if not any(self.profile_modified):
            self.btn_apply.setEnabled(False)
            self.btn_revert.setEnabled(False)
    def checkModified(self,profile):
        if self.sb_phase[profile].value() == self.phase[profile] and \
            self.sb_ampl[profile].value() == self.ampl[profile] and \
            self.sb_freq[profile].value()*self.freq_mult[profile] == self.freq[profile]:
                self.clearMidified(profile)
    def on_freq_changed(self,freq):
        sndr = self.sender()
        for i in range(0,self.number_of_profiles):
            if self.sb_freq[i]==sndr:
                if freq*self.freq_mult[i]!= self.freq[i]:
                    self.setModified(i)
                    return
                else:
                    self.checkModified(i)
    def on_phase_changed(self,phase):
        sndr = self.sender()
        for i in range(0,self.number_of_profiles):
            if self.sb_phase[i]==sndr:
                if phase!= self.phase[i]:
                    self.setModified(i)
                    return
                else:
                    self.checkModified(i)
    def on_ampl_changed(self,ampl):
        sndr = self.sender()
        for i in range(0,self.number_of_profiles):
            if self.sb_ampl[i]==sndr:
                if ampl!= self.ampl[i]:
                    self.setModified(i)
                    return
                else:
                    self.checkModified(i)
    def apply(self):
        for i in range(0,self.number_of_profiles):
            if self.profile_modified[i]:
                self.freq[i] = self.sb_freq[i].value()*self.freq_mult[i]
                self.displayFreq(i,self.freq[i])
                self.ampl[i] = self.sb_ampl[i].value()
                self.phase[i] = self.sb_phase[i].value()
                self.sig_profile_was_changed.emit(i,self.ampl[i],self.phase[i],self.freq[i])
                self.profile_modified[i] = False
                self.lbl_profile[i].setText(str(i))
        self.btn_apply.setEnabled(False)
        self.btn_revert.setEnabled(False)
    def on_cb_externalprofile_toggled(self,state):
        self.dds.setExternalProfile(state)
        self.setProfileButtonsEnabled(not state)
    def setProfileButtonsEnabled(self,state):
        for bt in self.btns_profile:
            bt.setEnabled(state)
    def displayFreq(self,profile,frequency):
        f, u = si_units.si_fmt(frequency, "Hz", self.sb_freq[profile])
        if f!=0:
            self.freq_mult[profile] = frequency / f
        else:
            self.freq_mult[profile] = 1
        self.sb_freq[profile].setValue(f)
        self.lbl_freq_units[profile].setText(u)
        print(profile, ":", frequency, f, u,self.lbl_freq_units[profile])
        pass
    def on_profile_choose(self):
        sndr = self.sender()
        for i in range(0,self.number_of_profiles):
            if sndr == self.btns_profile[i]:
                self.sig_profile_was_chosen.emit(i)
                self.setProfile(i, *self.dds.getProfile(i))
                self.setProfileBtnActive(i)
    def setProfileBtnActive(self, profile):
        self.active_profile = profile
        for i,btn in enumerate(self.btns_profile):
            if i==profile:
                btn.setEnabled(False)
                btn.setText("active")
            else:
                btn.setEnabled(True)
                btn.setText("set")
    def reloadInfo(self):
        self.dds.status()
        self.setProfileBtnActive(self.dds.profile)
        # NOTE: on DDS3 you have to previously activate profile
        if self.autoswitch_profile:
            act_profile = self.active_profile
            extern = (self.dds.internal_profile == False)
            self.cb_externalprofile.setChecked(False)
            if extern:
                self.dds.setExternalProfile(False)
        for i in range(0, self.number_of_profiles):
            if self.autoswitch_profile:
                self.sig_profile_was_chosen.emit(i)
            pr = self.dds.getProfile(i)
            print(pr)
            self.setProfile(i,*pr)
        if self.autoswitch_profile:
            self.sig_profile_was_chosen.emit(act_profile) #go back to profile that was active
            if extern: # set external profile if needed
                self.cb_externalprofile.setChecked(True)
                # self.dds.setExternalProfile(True)





if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    wd = ProfileControlWidget()
    wd.show()
    wd.setProfile(0,0,1,1e6)
    app.exec()