from qtpy import QtWidgets, QtCore
from numpy import log10

class Slider(QtWidgets.QSlider):
    valueSet = QtCore.Signal(float)
    def __init__(self, function = None, orientation = QtCore.Qt.Vertical, logarithimic = False,valuename = "", max_value = 1):
        """
        :param function: what to set setSomeValue(float)
        :param orientation: QtCore.Qt.Vertical
        :param logarithimic: db per ticks for log scale or False for linear scale
        :param valuename: name of value for print output
        :param max_value: value corresponding to 100 percent
        """
        self.function = function
        self.log = logarithimic
        self.valuename = valuename
        self.max_value_real = max_value
        if self.log==True:
            self.log = .25 # db per tick
        super(Slider,self).__init__()
        self.setOrientation(QtCore.Qt.Vertical)
        self.mult = max_value/100
        self.value_real = 100*self.mult
        self.setRange(0, 100)
        self.setSingleStep(10)
        self.setSliderPosition(100)
        self.setTickPosition(QtWidgets.QSlider.TicksLeft)
        self.valueChanged.connect(self.setValue)
        self.sliderPressed.connect(self.slider_disconnect)
        self.sliderReleased.connect(self.slider_connect)
    def setValue(self,val):
        if self.log:
            if val == 0:
                self.value_real = 0
            else:
                self.value_real = 1. / 10 ** ((100 - val) * self.log / 10)
        else:
            self.value_real = self.mult * val
        print("Setting {} {}%\t{:.3g}".format(self.valuename,val,self.value_real))
        if self.function:
            self.function(self.value_real)
        self.valueSet.emit(self.value_real)
    def posFromValue(self,val):
        if self.log:
            if val == 0:
                pos = 0
            else:
                pos = 100+log10(val/self.max_value_real)*10/self.log
        else:
            pos = self.value_real/self.mult
        return pos
    def slider_disconnect(self):
        self.valueChanged.disconnect(self.setValue)
    def slider_connect(self):
        self.valueChanged.connect(self.setValue)
        self.setValue(self.value())

class SliderWidget(QtWidgets.QFrame):
    SHOW_REAL_VALUE = 0
    SHOW_DB = 1
    SHOW_PERCENT = 2
    def __init__(self,function = None, orientation = QtCore.Qt.Vertical, logarithimic = False,valuename = "", max_value = 1, display_units = 0):
        """
        :param function: what to set setSomeValue(float)
        :param orientation: QtCore.Qt.Vertical
        :param logarithimic: db per ticks for log scale or False for linear scale
        :param valuename: name of value for print output
        :param max_value: value corresponding to 100 percent
        :param display_units: SliderWidget.SHOW_DB (display attenuation), self.SHOW_PERCENT , self.SHOW_REAL_VALUE
        """
        self.slider = Slider(function=function, orientation=orientation, logarithimic=logarithimic, valuename=valuename,
                             max_value=max_value)
        self.sb = QtWidgets.QDoubleSpinBox()
        self.display_units = display_units
        if display_units == self.SHOW_DB:
            self.sb.setMaximum(0)
            self.sb.setMinimum(-30)
            self.sb.setSpecialValueText("off")
            self.sb.setSuffix("dB")
        elif display_units == self.SHOW_PERCENT:
            self.sb.setMaximum(100)
            self.sb.setSuffix("%")
        else:
            self.display_units = self.SHOW_REAL_VALUE
            self.sb.setMaximum(self.slider.max_value_real)
        super(SliderWidget,self).__init__()
        self.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setFrameShape(QtWidgets.QFrame.Panel)
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)
        self.slider.valueSet.connect(self.setSbValue)
        self.sb.valueChanged.connect(self.adjustSlider)
        if valuename:
            self.gl.addWidget(QtWidgets.QLabel(valuename))
        self.gl.addWidget(self.sb)
        self.gl.addWidget(self.slider)
    def setSbValue(self,val):
        self.sb.blockSignals(True)
        if self.display_units == self.SHOW_REAL_VALUE:
            self.sb.setValue(self.slider.value_real)
        elif self.display_units == self.SHOW_PERCENT:
            self.sb.setValue(self.slider.value())
        elif self.display_units == self.SHOW_DB:
            if val:
                v = log10(self.slider.value_real/self.slider.max_value_real)*10
                self.sb.setValue(v)
            else:
                self.sb.setValue(self.sb.minimum())
        self.sb.blockSignals(False)
    def adjustSlider(self,val):
        print(val)
        if self.display_units == self.SHOW_REAL_VALUE:
            p = self.slider.posFromValue(val)
            self.slider.setSliderPosition(p)
        elif self.display_units == self.SHOW_PERCENT:
            self.slider.setSliderPosition(val)
        elif self.display_units == self.SHOW_DB:
            p = self.slider.posFromValue(self.slider.max_value_real*10**(val/10))
            print(p)
            self.slider.setSliderPosition(p)