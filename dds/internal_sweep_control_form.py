# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'internal_sweep_control_form.ui'
#
# Created: Tue Aug 09 17:05:07 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtWidgets,QtCore,QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_internal_sweeper(object):
    def setupUi(self, internal_sweeper):
        internal_sweeper.setObjectName(_fromUtf8("internal_sweeper"))
        internal_sweeper.resize(359, 325)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(internal_sweeper)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_2 = QtWidgets.QLabel(internal_sweeper)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(internal_sweeper)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 0, 3, 1, 1)
        self.label_5 = QtWidgets.QLabel(internal_sweeper)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 0, 2, 1, 1)
        self.label_6 = QtWidgets.QLabel(internal_sweeper)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.gridLayout.addWidget(self.label_6, 0, 5, 1, 1)
        self.doubleSpinBox = QtWidgets.QDoubleSpinBox(internal_sweeper)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.doubleSpinBox.setFont(font)
        self.doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.doubleSpinBox.setAccelerated(True)
        self.doubleSpinBox.setDecimals(3)
        self.doubleSpinBox.setSingleStep(0.005)
        self.doubleSpinBox.setProperty("value", 1.0)
        self.doubleSpinBox.setObjectName(_fromUtf8("doubleSpinBox"))
        self.gridLayout.addWidget(self.doubleSpinBox, 0, 1, 1, 1)
        self.doubleSpinBox_2 = QtWidgets.QDoubleSpinBox(internal_sweeper)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.doubleSpinBox_2.setFont(font)
        self.doubleSpinBox_2.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.doubleSpinBox_2.setAccelerated(True)
        self.doubleSpinBox_2.setDecimals(3)
        self.doubleSpinBox_2.setSingleStep(0.005)
        self.doubleSpinBox_2.setProperty("value", 10.0)
        self.doubleSpinBox_2.setObjectName(_fromUtf8("doubleSpinBox_2"))
        self.gridLayout.addWidget(self.doubleSpinBox_2, 0, 4, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 6, 1, 1)
        self.label_4 = QtWidgets.QLabel(internal_sweeper)
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 1, 3, 1, 1)
        self.doubleSpinBox_3 = QtWidgets.QDoubleSpinBox(internal_sweeper)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.doubleSpinBox_3.setFont(font)
        self.doubleSpinBox_3.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.doubleSpinBox_3.setAccelerated(True)
        self.doubleSpinBox_3.setDecimals(3)
        self.doubleSpinBox_3.setMinimum(0.0)
        self.doubleSpinBox_3.setMaximum(999999999.0)
        self.doubleSpinBox_3.setSingleStep(100.0)
        self.doubleSpinBox_3.setProperty("value", 100.0)
        self.doubleSpinBox_3.setObjectName(_fromUtf8("doubleSpinBox_3"))
        self.gridLayout.addWidget(self.doubleSpinBox_3, 1, 4, 1, 1)
        self.label_7 = QtWidgets.QLabel(internal_sweeper)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.gridLayout.addWidget(self.label_7, 1, 5, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(13)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.btn_start = QtWidgets.QPushButton(internal_sweeper)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_start.sizePolicy().hasHeightForWidth())
        self.btn_start.setSizePolicy(sizePolicy)
        self.btn_start.setMinimumSize(QtCore.QSize(100, 70))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.btn_start.setFont(font)
        self.btn_start.setObjectName(_fromUtf8("btn_start"))
        self.verticalLayout.addWidget(self.btn_start)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setVerticalSpacing(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem2, 4, 0, 1, 1)
        self.radioButton = QtWidgets.QRadioButton(internal_sweeper)
        self.radioButton.setChecked(True)
        self.radioButton.setObjectName(_fromUtf8("radioButton"))
        self.gridLayout_2.addWidget(self.radioButton, 1, 0, 1, 1)
        self.radioButton_2 = QtWidgets.QRadioButton(internal_sweeper)
        self.radioButton_2.setObjectName(_fromUtf8("radioButton_2"))
        self.gridLayout_2.addWidget(self.radioButton_2, 2, 0, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(39, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem3, 2, 1, 1, 1)
        self.cb_external = QtWidgets.QCheckBox(internal_sweeper)
        self.cb_external.setObjectName(_fromUtf8("cb_external"))
        self.gridLayout_2.addWidget(self.cb_external, 3, 0, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout_2)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(internal_sweeper)
        QtCore.QMetaObject.connectSlotsByName(internal_sweeper)

    def retranslateUi(self, internal_sweeper):
        internal_sweeper.setWindowTitle(_translate("internal_sweeper", "Form", None))
        self.label_2.setText(_translate("internal_sweeper", " from", None))
        self.label_3.setText(_translate("internal_sweeper", "to", None))
        self.label_5.setText(_translate("internal_sweeper", "MHz", None))
        self.label_6.setText(_translate("internal_sweeper", "MHz", None))
        self.label_4.setText(_translate("internal_sweeper", "in", None))
        self.label_7.setText(_translate("internal_sweeper", "ms", None))
        self.btn_start.setText(_translate("internal_sweeper", "Start", None))
        self.radioButton.setText(_translate("internal_sweeper", "sweep up once", None))
        self.radioButton_2.setText(_translate("internal_sweeper", "sweep up and down\n"
"in a loop", None))
        self.cb_external.setText(_translate("internal_sweeper", "external control", None))

