# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DDS_FreqAmpPhas_widget2.ui'
#
# Created: Tue Jul 19 17:30:58 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtWidgets,QtCore

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_DDS_FreqAmpPhas_widget(object):
    def setupUi(self, DDS_FreqAmpPhas_widget):
        DDS_FreqAmpPhas_widget.setObjectName(_fromUtf8("DDS_FreqAmpPhas_widget"))
        DDS_FreqAmpPhas_widget.resize(364, 293)
        self.verticalLayout = QtWidgets.QVBoxLayout(DDS_FreqAmpPhas_widget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.sb_phas = QtWidgets.QDoubleSpinBox(DDS_FreqAmpPhas_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sb_phas.sizePolicy().hasHeightForWidth())
        self.sb_phas.setSizePolicy(sizePolicy)
        self.sb_phas.setObjectName(_fromUtf8("sb_phas"))
        self.gridLayout.addWidget(self.sb_phas, 4, 2, 1, 1)
        self.sp_amp = QtWidgets.QDoubleSpinBox(DDS_FreqAmpPhas_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sp_amp.sizePolicy().hasHeightForWidth())
        self.sp_amp.setSizePolicy(sizePolicy)
        self.sp_amp.setMaximum(1.0)
        self.sp_amp.setSingleStep(0.01)
        self.sp_amp.setObjectName(_fromUtf8("sp_amp"))
        self.gridLayout.addWidget(self.sp_amp, 3, 2, 1, 1)
        self.label = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.sb_freq = QtWidgets.QDoubleSpinBox(DDS_FreqAmpPhas_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sb_freq.sizePolicy().hasHeightForWidth())
        self.sb_freq.setSizePolicy(sizePolicy)
        self.sb_freq.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.sb_freq.setAccelerated(True)
        self.sb_freq.setCorrectionMode(QtWidgets.QAbstractSpinBox.CorrectToNearestValue)
        self.sb_freq.setMaximum(1000.0)
        self.sb_freq.setObjectName(_fromUtf8("sb_freq"))
        self.gridLayout.addWidget(self.sb_freq, 2, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 3, 1, 1)
        self.label_2 = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 3, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 4, 0, 1, 1)
        self.comboBox = QtWidgets.QComboBox(DDS_FreqAmpPhas_widget)
        self.comboBox.setEditable(False)
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.gridLayout.addWidget(self.comboBox, 1, 2, 1, 1)
        self.label_phase = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        self.label_phase.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_phase.setObjectName(_fromUtf8("label_phase"))
        self.gridLayout.addWidget(self.label_phase, 4, 1, 1, 1)
        self.label_amp = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        self.label_amp.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_amp.setObjectName(_fromUtf8("label_amp"))
        self.gridLayout.addWidget(self.label_amp, 3, 1, 1, 1)
        self.label_freq = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        self.label_freq.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_freq.setObjectName(_fromUtf8("label_freq"))
        self.gridLayout.addWidget(self.label_freq, 2, 1, 1, 1)
        self.pb_apply = QtWidgets.QPushButton(DDS_FreqAmpPhas_widget)
        self.pb_apply.setObjectName(_fromUtf8("pb_apply"))
        self.gridLayout.addWidget(self.pb_apply, 5, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 1, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 0, 0, 1, 2)
        self.label_6 = QtWidgets.QLabel(DDS_FreqAmpPhas_widget)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.gridLayout.addWidget(self.label_6, 1, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)

        self.retranslateUi(DDS_FreqAmpPhas_widget)
        QtCore.QMetaObject.connectSlotsByName(DDS_FreqAmpPhas_widget)

    def retranslateUi(self, DDS_FreqAmpPhas_widget):
        DDS_FreqAmpPhas_widget.setWindowTitle(_translate("DDS_FreqAmpPhas_widget", "Form", None))
        self.sp_amp.setToolTip(_translate("DDS_FreqAmpPhas_widget", "Enter amplitude from 0 to 100%", None))
        self.sp_amp.setSpecialValueText(_translate("DDS_FreqAmpPhas_widget", "off", None))
        self.label.setText(_translate("DDS_FreqAmpPhas_widget", "Frequency", None))
        self.sb_freq.setSuffix(_translate("DDS_FreqAmpPhas_widget", " MHz", None))
        self.label_2.setText(_translate("DDS_FreqAmpPhas_widget", "Amplitude", None))
        self.label_3.setText(_translate("DDS_FreqAmpPhas_widget", "Phase", None))
        self.label_phase.setText(_translate("DDS_FreqAmpPhas_widget", "0", None))
        self.label_amp.setText(_translate("DDS_FreqAmpPhas_widget", "1", None))
        self.label_freq.setText(_translate("DDS_FreqAmpPhas_widget", "1.00 MHz", None))
        self.pb_apply.setText(_translate("DDS_FreqAmpPhas_widget", "Apply", None))
        self.label_4.setText(_translate("DDS_FreqAmpPhas_widget", "Profile 0", None))
        self.label_5.setText(_translate("DDS_FreqAmpPhas_widget", "Status", None))
        self.label_6.setText(_translate("DDS_FreqAmpPhas_widget", "Profile", None))

