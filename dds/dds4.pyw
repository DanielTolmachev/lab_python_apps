#-------------------------------------------------------------------------------
# Name:     dds.py
# Purpose:  control DDS4 generator made for e3.physik.tu-dortmund.de
#
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

DEV_ADDRESS = "129.217.155.83"
DEV_PORT = 51234

#PUBLISH_ADDRESS = 'tcp://127.0.0.1:5003'
PUBLISH_ADDRESS = 'tcp://*:5011'
SUB_PORT = 6011
UNITS_DEFAULT = "MHz"
UNITS_USED = {"Hz":1,"kHz":1e3,"MHz":1e6}

import zmq
from qtpy import QtWidgets,QtCore
import sys
sys.path.append( '../modules')
import install_exception_hook
from sweeper_msg import SweeperWMsg
from zmq_publisher import Zmq_Publisher
import sweep_widget
from dds4_dev_class import DDS4_Class
from dds4_controller import DDS_Controller
from zmq_sub2qt import Zmq2Qt
import QMenuFromDict,quickQtApp,device_address_widget
import dds_qt_view3
import dds_FreqAmpPhase_wdgt,eraser_widget,internal_sweep_control,profile_control_widget,slider_amp

app,opt,win,cons = quickQtApp.mkAppWin(title="DDS4", icon="dds4.png")
# opt = Options()
# app = QtWidgets.QApplication(sys.argv)
# cons = QConsole()

context = zmq.Context()
pub = Zmq_Publisher(context,opt.get("pub.address",PUBLISH_ADDRESS))
sub = Zmq2Qt(context,SUB_PORT,verbose = False)
dev_address = opt.get("dev.address",(DEV_ADDRESS,DEV_PORT))
dds = DDS4_Class(dev_address,autoconnect = True,autoupdate = True,verbose = True,keep_connection = True)
# dds.interface.keep_connection = True
dds_view = dds_qt_view3.DDS_QT_VIEW(dds)
dds_view.registerChanged.connect(dds.writeRegister)

control = DDS_Controller()
control.dds = dds
control.sweeper_freq.setFunction = dds.setFrequency
# conthrd = QtCore.QThread()

# win = QMainWindowAutoSaveGeometry()
# win.setWindowTitle('DDS')
sb = win.statusBar()
#sb = QtWidgets.QStatusBar()
label_connection_status = QtWidgets.QLabel()
sb.addPermanentWidget(label_connection_status,1)

#set application icon
# icon = QtGui.QIcon("dds4.png")
# win.setWindowIcon(icon)

if 'Window.geometry' in opt:
    win.setGeometry(*opt['Window.geometry'])
def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk
win.GeometryChangedSig.connect(save_win_position)
#menu_fFromDict
units = opt.get('current_units',UNITS_DEFAULT)
if not units in list(UNITS_USED.keys()):
    units = UNITS_DEFAULT

sw = sweep_widget.SweepWidget(units = units,
                              sweepValueDesc = 'SweepFrequency',
                              units_multiplier = UNITS_USED[units],
                              units_used_dict = UNITS_USED,
                              use_internal_sweeper=False)
widget_pwr = sweep_widget.SweepWidget(units = 'dBm',
                              sweepValueDesc = 'SweepPower',
                              units_multiplier = 1 )


tw = QtWidgets.QTabWidget()
tw.addTab(sw,'sweep Frequency')

swint = internal_sweep_control.InternalSweepControl(dds,opt)
tw.addTab(swint,'Internal Sweep')

apf = dds_FreqAmpPhase_wdgt.FreqAmpPhase(dds)
tw.addTab(apf,"Main")
gl = QtWidgets.QGridLayout()
gl.addWidget(tw,0,0)
#win.setCentralWidget(tw)
win.setCentralWidget(QtWidgets.QWidget())
win.centralWidget().setLayout(gl)

if opt.get("show_eraser",True):
    w_er = eraser_widget.Eraser_Widget(dds,opt)
    tw.addTab(w_er,"Eraser")

def tw_tabChanged(idx):
    tw.currentWidget().show()
tw.currentChanged.connect(tw_tabChanged)

pc = profile_control_widget.ProfileControlWidget(dds,8, dds.fsys, 0)
tw.addTab(pc,"Profile control")
pc.sig_profile_was_changed.connect(dds.setProfile)
pc.sig_profile_was_chosen.connect(dds.selectProfile)

#amplitude control
sld_amp = slider_amp.SliderWidget(dds.setAmplitude,logarithimic=.25,valuename="amplitude",
                                  display_units=opt.get("gui.amplitude_units",slider_amp.SliderWidget.SHOW_DB))
# sld_amp = QtWidgets.QSlider()
# sld_amp.setOrientation(QtCore.Qt.Vertical)
# sld_amp.setRange(0,100)
# sld_amp.setSingleStep(10)
# sld_amp.setSliderPosition(100)
# sld_amp.setTickPosition(QtWidgets.QSlider.TicksLeft)
# sld_amp.setMouseTracking(True)
gl.addWidget(sld_amp,0,2)

#gl.addWidget(tw)
w_addr = device_address_widget.DeviceAddressWidget(dds.getDeviceAddress,interfaces=["lan","serial"],default_ip_port=DEV_PORT)
# w_addr = device_address_widget.DeviceAddressWidget(dds.address,interfaces=["lan","serial"])
w_addr.sigConnectionRequested.connect(control.setConnected)
w_addr.sigNewAddressSet.connect(control.setAddress)

QMenuFromDict.AddMenu(win.menuBar(),
                      [("device",[
                                ("change address",w_addr.show),
                                ("connect",control.open),
                                ("disconnect",control.close),
                                ("reset DDS",dds.reset),
                                ("status",control.status),
                                ("setDRG int/ext",control.toggleDREXT)
                                ]
                        ),
                        ("view registers",dds_view.show)
                      ])


def setamp(val):
    #val = sld_amp.value()
    print("Setting amplitude",val,"%")
    dds.setAmplitude(.01*val)
# #slider control
# sld_amp.valueChanged.connect(setamp)
# def amp_slider_disconnect():
#     sld_amp.valueChanged.disconnect(setamp)
# def amp_slider_connect():
#     sld_amp.valueChanged.connect(setamp)
#     setamp(sld_amp.value())
# sld_amp.sliderPressed.connect(amp_slider_disconnect)
# sld_amp.sliderReleased.connect(amp_slider_connect)

def show_conn_status(status):
    if status:
        label_connection_status.setText("connected to {}".format(dds.address))
    else:
        label_connection_status.setText("disconnected")

#tw.addTab(widget_pwr,'Power')




sw_freq = SweeperWMsg(  messager = control.rcv,
                        setfunc = dds.setFreq,
                        queryfunc = dds.queryFreq,
                        verbose = False)
# sw_pwr = SweeperWMsg(  messager = signalproxy.rcv,
#                         setfunc = dds.setPower,
#                         queryfunc = dds.queryPower,
#                         verbose = False)


sub.sigGotMessage.connect(control.processCommand)
control.sig_new_range.connect(sw.setWorkingRange)
sw.sig_range_changed.connect(control.sendrange)

sw.connect_options_dictionary(opt)
sw.sig_params_changed.connect(opt.savetodisk) #save options immidiately
sw.sig_start_sweeper.connect(control.sweep)
sw.sig_stop_sweeper.connect(control.stop)
sw.sig_stop_sweeper.connect(control.sweeper_freq.stop)
sw.sig_started.connect(pub.send_pyobj)
control.sweeper_freq.stopped.connect(sw.setGuiStopped)
control.sig_stopped.connect(sw.setGuiStopped)
control.sig_stopped.connect(swint.setGuiStopped)
# sw.sig_value_set.connect(control.setqueryFreq) #set current from GUI widget
sw.sig_value_set.connect(dds.setFreq) #set value from GUI widget
sw.sig_value_set.connect(sw.set_central_label) #set gui view from GUI widget
#control.sweeper_freq.newValueSet.connect(sw.set_central_label)
sw.sig_value_set.connect(control.rcv)
control.sweeper_freq.message.connect(control.rcv)
control.sig.connect(pub.send_pyobj) #send tuple away using zmq
control.sig_float.connect(sw.set_central_label) #modify widget central value
sw.sig_range_changed.connect(control.sendrange)
control.connection_status_changed.connect(show_conn_status)
swint.sig_drg_parameters_set.connect(control.sweep_int_setup)
swint.sig_drg_enabled.connect(control.intSweepStart)



#dds.open()
control.open()
dds.setOSK(True)
#show_conn_status(dds.interface.connected)

#sld_amp.setSliderPosition()

# sw.set_central_label(dds.queryFreq())
control.call(dds.queryFreq)
if __name__ == '__main__':
    if 'demo' in sys.argv:
        import demo_device
        dds = demo_device.demo_device()
    # control.moveToThread(conthrd)
    # conthrd.start()
    win.show()
    app.exec_()
    sw_freq.stop()
    sw_freq.join()
    opt["dev.address"] = dds.getDeviceAddress()
    opt.savetodisk()
    dds.close()



