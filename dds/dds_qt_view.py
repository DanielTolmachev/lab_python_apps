__author__ = 'SFB'

from qtpy import QtWidgets,QtCore

class REGISTER_VIEW(QtWidgets.QWidget):
    def __init__(self,device_register_description_dict):
        super(REGISTER_VIEW,self).__init__()
        gl = QtWidgets.QGridLayout()
        self.setLayout(gl)
        self.reg = device_register_description_dict
        title = ""
        if "long_name" in self.reg:
            title = "{} ({})".format(self.reg["long_name"],self.reg["name"])
        elif "name" in self.reg:
            title = format(self.reg["name"])
        if title:
            ltitle = QtWidgets.QLabel(title)
            gl.addWidget(ltitle)
        self.lbl_data  = QtWidgets.QLabel()
        gl.addWidget(self.lbl_data,1,0)
        self.lbl_data_new  = QtWidgets.QLabel()
        gl.addWidget(self.lbl_data_new,1,2)
        gl.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Ignored),1,1)
        self.gl = QtWidgets.QGridLayout()
        self.gl.setVerticalSpacing(0)
        self.gl.setHorizontalSpacing(0)
        gl.addLayout(self.gl,2,0,1,3)
    def setRegData(self,data):
        self.data = data
    def draw(self,data):
        print(data)
        self.lbl_data.setText(data)
        if "bytes" in self.reg:
            szz = len(data)*4
            cols = 8
            rows = szz/cols
            datab = "{:0{}b}".format(int(data,16),szz)
            d = {}; dsc = {}
            maxwidth = 0
            fm = self.fontMetrics()
            height = 3*fm.height()
            for addr,sz,desc in self.reg["bytes"]:
                d[addr] = sz
                if desc:
                    dsc[addr] = desc
                    w = fm.width(desc)
                    if w>maxwidth:
                        maxwidth = w

            print(d)
            skip = 0
            startrow = 0
            startcol = 0
            r = 0
            colrem = 0
            i = 0
            self.list_buttons = []
            self.list_buttons_coord = []
            while i< szz:
                if i in d:
                    sz = d[i]
                    if colrem>=8:
                        colspan = 8
                        colrem = colrem-colspan
                    elif 0<colrem<8:
                        colspan = colrem
                        colrem = 0
                    else:
                        colspan = sz
                    if 8-(i-r*8)-colspan<0:
                        colrem = colspan-8-(i-r*8)
                        colspan = colspan - colrem
                else:
                    sz = 1
                    colspan = 1

                if i in dsc:
                    if sz==1:
                        title = str(bool(int(datab[i],2)))
                    else:
                        title = datab[i:i+sz]
                    title +="\n{}".format(dsc[i])
                else:
                    title = datab[i:i+sz]
                #print r+startrow,i+startcol
                pb = QtWidgets.QPushButton(title)
                pb.setFixedWidth(maxwidth*colspan)
                pb.setFixedHeight(height)
                c = 7-(i-r*8)-colspan +startcol
                row = rows-1-r+startrow
                self.gl.addWidget(pb,row,c,1,colspan)
                self.list_buttons.append(pb)
                self.list_buttons_coord.append((row,c,colspan))
                pb.pressed.connect(self.edit)
                i+=colspan
                r = i/8
    def edit(self):
        for i,btn in enumerate(self.list_buttons):
            if btn == self.sender():
                break
        if self.list_buttons_coord[i][2]>1:
            btn.hide()
            self.edit = i
            le = QtWidgets.QLineEdit()
            le.setFixedHeight(btn.height())
            self.gl.addWidget(le,self.list_buttons_coord[i][0],self.list_buttons_coord[i][1],1,
                              self.list_buttons_coord[i][2])
            le.returnPressed.connect(self.leaveEdit)
    def leaveEdit(self):
        le = self.sender()
        data  = le.text()
        le.hide()
        self.list_buttons[self.edit].show()
        print(data)



class DDS_QT_VIEW(QtWidgets.QWidget):
    def __init__(self,device):
        super(DDS_QT_VIEW,self).__init__()
        self.dds = device
        gl = QtWidgets.QGridLayout()
        self.setLayout(gl)
        self.tw = QtWidgets.QTabWidget()
        self.reg_list = []
        self.view_list = []
        self.btn_apply = QtWidgets.QPushButton("Apply")
        self.cb_apply_auto = QtWidgets.QCheckBox("auto apply")
        self.cb_apply_auto.toggled.connect(self.on_cb_apply_auto_toggled)
        gl.addWidget(self.tw,0,0,1,3)
        gl.addWidget(self.cb_apply_auto,1,0)
        gl.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum),1,1)
        gl.addWidget(self.btn_apply,1,2)
        self.setWindowTitle("DDS registers")
        for reg in self.dds.register_description:
            w = REGISTER_VIEW(self.dds.register_description[reg])
            if type(reg) == int:
                regS = "0x{:0X}".format(reg)
            else:
                regS = format(reg)
            self.tw.addTab(w,regS)
            self.reg_list.append(reg)
            self.view_list.append(w)
            if reg in self.dds.register_data and self.dds.register_data[reg]:
                print(repr(self.dds.register_data[reg]))
                w.draw(self.dds.register_data[reg])
        self.tw.currentChanged.connect(self.on_tw_currentChanged)
    def on_cb_apply_auto_toggled(self,state):
        #print state
        self.btn_apply.setEnabled(not state)
    def on_tw_currentChanged(self,idx):
        reg = self.reg_list[idx]
        if reg not in self.dds.register_data:
            rep = self.dds.readRegister(reg)
            if rep:
                self.view_list[idx].draw(rep.strip())
        else:
            self.view_list[idx].draw(self.dds.register_data[reg])

if __name__ == "__main__":
    import dds3E_dev_class
    dds = dds3E_dev_class.DDS3E_Class("127.0.0.1")
    dds.register_data = {0: '00000002',
 1: '00400020',
 2: '0F3FC000',
 3: '00007F7F',
 4: '00000004',
 5: 'F2E038A5AC66FFFF',
 6: '36A22151D872FFFF',
 7: '00000000',
 8: '00000000',
 9: '00000000',
 10: '00000000',
 11: '36207B235680FE9A',
 12: '79E465BD254A481B',
 13: '28A57E28',
 14: '3FFF0000170A3D70',
 15: '3FFF0000170A3D70',
 16: '3FFF0000170A3D70',
 17: '3FFF0000170A3D70',
 18: '3FFF0000170A3D70',
 19: '3FFF0000170A3D70',
 20: '00000002',
 21: '3FFF0000170A3D70'}

    app  = QtWidgets.QApplication([])
    view = DDS_QT_VIEW(dds)
    view.show()
    app.exec_()