import internal_sweep_control_form
from qtpy import QtWidgets,QtCore
import pyqtSpinboxAdvanced

class InternalSweepControl(internal_sweep_control_form.Ui_internal_sweeper,QtWidgets.QWidget):
    sig_drg_parameters_set = QtCore.Signal(object)
    sig_drg_enabled = QtCore.Signal(bool)
    def __init__(self,dds,opt):
        super(InternalSweepControl,self).__init__()
        self.setupUi(self)
        pyqtSpinboxAdvanced.upgradeAllDoubleSpinBoxes(self)
        self.dds = dds
        self.opt = opt
        self.started = False
        self.loaded = False
        self.params = ()
        self.doubleSpinBox.valueChanged.connect(self.value_changed)
        self.doubleSpinBox_3.valueChanged.connect(self.value_changed)
        self.doubleSpinBox_2.valueChanged.connect(self.value_changed)
        self.radioButton.toggled.connect(self.value_changed)
        self.radioButton.toggled.connect(self.value_changed)

        #print(self.doubleSpinBox.validate)
        #self.doubleSpinBox.v
        #print(pyqtSpinboxAdvanced.doubleSpinBoxHelperList)
    def value_changed(self, args):
        print(args)
        self.loaded  = False
    def getParams(self):
        if self.radioButton_2.isChecked():
                    mode = 1
        else:
                    mode = 0
        fstart = self.doubleSpinBox.value()*1e6
        fstop = self.doubleSpinBox_2.value()*1e6
        time = self.doubleSpinBox_3.value()/1000
        return fstart,fstop,time,mode
    def on_btn_start_pressed(self):
        self.started = not self.started
        if self.started:
            if not self.loaded:
                fstart,fstop,time,mode = self.getParams()
                if self.params != (fstart,fstop,time,mode):
                    step, dt = self.dds.calcSweep(fstart,fstop,time)
                    if self.cb_external.isChecked():
                        mode += 4
                    self.sig_drg_parameters_set.emit((fstart,fstop,step,dt,mode))
            self.btn_start.setText("Stop")
            self.sig_drg_enabled.emit(True)
        else:
            self.btn_start.setText("Start")
            self.sig_drg_enabled.emit(False)
    def setGuiStopped(self):
        self.started = False
        self.btn_start.setText("Start")

