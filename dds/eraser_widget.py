from eraser import Ui_eraser_wdgt
from qtpy import QtWidgets
import pyqtSpinboxAdvanced

class Eraser_Widget(Ui_eraser_wdgt,QtWidgets.QWidget):
    def __init__(self,dds = None,opt = None):
        super(Eraser_Widget,self).__init__()
        self.setupUi(self)
        pyqtSpinboxAdvanced.upgradeAllDoubleSpinBoxes(self)
        if dds:
            self.connectDDS(dds)
        if opt:
            self.connectOptionsDict(opt)
        self.go = False
    def connectDDS(self,dds):
        self.dds = dds
        self.btn_start.pressed.connect(self.toggleSweep)
    def toggleSweep(self):
        if not self.go:
            start = self.doubleSpinBox.value()*1e6
            stop = self.doubleSpinBox_2.value()*1e6
            time = self.doubleSpinBox_3.value()/1000
            self.amp = self.sb_power.value()/100
            step,dt = self.dds.calcSweep(start,stop,time)
            self.dds.setupSweep(start,stop,step,step,dt,dt)
            self.dds.setupSetSweepControlInt()
            self.saveParams(start,stop,time,self.amp)
            self.amp_old = self.dds.queryAmplitude()
            if self.amp!=self.amp_old:
                self.dds.setAmplitude(self.amp)
            self.dds.setNoDwellHigh(True)
            self.dds.setNoDwellLow(True)
            self.dds.setDRG(True)
            self.go = True
            self.btn_start.setText("Stop")
        else:
            self.dds.setDRG(False)
            self.go = False
            self.btn_start.setText("Send\nRF")
            if self.amp_old!=self.amp:
                self.dds.setAmplitude(self.amp_old)
    def connectOptionsDict(self,opt):
        self.opt = opt
        self.loadParams()
    def loadParams(self):
        if self.opt:
            self.doubleSpinBox.setValue(self.opt.get("eraser.frequency_start",1e6)/1e6)
            self.doubleSpinBox_2.setValue(self.opt.get("eraser.frequency_stop",15e6)/1e6)
            self.doubleSpinBox_3.setValue(self.opt.get("eraser.sweep_time",.01)*1000)
            self.sb_power.setValue(self.opt.get("eraser.power",1.)*100)
    def saveParams(self,start,stop,time,pwr):
        if self.opt:
            self.opt["eraser.frequency_start"] = start
            self.opt["eraser.frequency_stop"] = stop
            self.opt["eraser.sweep_time"] = time
            self.opt["eraser.power"] = pwr