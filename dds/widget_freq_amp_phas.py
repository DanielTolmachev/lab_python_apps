__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

from qtpy import QtWidgets,QtCore,QtGui
import sys,traceback
sys.path.append( '../modules')
from atof_comma_safe import atof

digits = [43,45,48, 49, 50, 51, 52, 53, 54, 55, 56, 57,69]

class SpinBoxT(QtWidgets.QDoubleSpinBox):
    def __init__(self,*args):
        super(SpinBoxT,self).__init__(*args)
    # def keyPressEvent(self, evt):
    #     key = evt.key()
    #     print key
        # if key in digits: # comma
        #     #key = QtGui.QKeyEvent()
        #     print atof(self.text())
        # else:
        # super(SpinBoxT,self).keyPressEvent(evt)
    def validate(self, QString, p_int):
        #print QString,p_int
        ret = super(SpinBoxT,self).validate(QString,p_int)
        print(ret)
        #return ret
        try:
            a = float(QString)
        except ValueError:
            if len(QString) == 0:
                return (1,0)
            if "," in QString:
                QString.replace(",",".")
            if QString.count(".")>1:
                return (0,len(QString))
            if QString.toLower().count("e")>1:
                return (0,len(QString))
            if QString[-1] in ["e",".","+","-"]:
                return (1,len(QString))
            try:
                a = float(QString)
            except ValueError:
                #traceback.print_exc()
                return (1,len(QString))
            print(QString)
            return (0,len(QString))
        return (2,len(QString))
    def valueFromText(self, QString):
        #ret = super(SpinBoxT,self).valueFromText(QString)
        #print QString,ret
        val = float(QString)
        st = self.singleStep()
        while st<val/100:
            st*=10
        while st>val/10:
            st/=10
        self.setSingleStep(st)
        return val
    def textFromValue(self, p_float):
        return "{:g}".format(p_float)

class DDS_FAP_Widget(QtWidgets.QWidget):
    def __init__(self,frequency_max = 1e9,profiles_count = 0):
        super(DDS_FAP_Widget,self).__init__()
        wl = QtWidgets.QVBoxLayout()
        self.setLayout(wl)
        sb_freq = SpinBoxT()
        sb_freq.setRange(0,frequency_max)
        #sb_freq.editingFinished
        sb_amp = SpinBoxT()
        sb_phas = SpinBoxT()
        wl.addWidget(QtWidgets.QLabel("Frequency"))
        wl.addWidget(sb_freq)
        wl.addWidget(QtWidgets.QLabel("Amplitude"))
        wl.addWidget(sb_amp)
        sb_amp.setRange(0,1)
        wl.addWidget(QtWidgets.QLabel("Phase"))
        wl.addWidget(sb_phas)


if __name__=="__main__":
    app = QtWidgets.QApplication([])
    w = DDS_FAP_Widget()
    w.show()
    app.exec_()
