# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'eraser.ui'
#
# Created: Mon Aug 01 15:43:31 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtWidgets,QtCore,QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_eraser_wdgt(object):
    def setupUi(self, eraser_wdgt):
        eraser_wdgt.setObjectName(_fromUtf8("eraser_wdgt"))
        eraser_wdgt.resize(343, 291)
        self.gridLayout = QtWidgets.QGridLayout(eraser_wdgt)
        self.gridLayout.setHorizontalSpacing(4)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtWidgets.QLabel(eraser_wdgt)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(eraser_wdgt)
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.doubleSpinBox = QtWidgets.QDoubleSpinBox(eraser_wdgt)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.doubleSpinBox.sizePolicy().hasHeightForWidth())
        self.doubleSpinBox.setSizePolicy(sizePolicy)
        self.doubleSpinBox.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.doubleSpinBox.setFont(font)
        self.doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.doubleSpinBox.setAccelerated(True)
        self.doubleSpinBox.setMaximum(2500.0)
        self.doubleSpinBox.setObjectName(_fromUtf8("doubleSpinBox"))
        self.gridLayout.addWidget(self.doubleSpinBox, 1, 1, 1, 1)
        self.doubleSpinBox_2 = QtWidgets.QDoubleSpinBox(eraser_wdgt)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.doubleSpinBox_2.sizePolicy().hasHeightForWidth())
        self.doubleSpinBox_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.doubleSpinBox_2.setFont(font)
        self.doubleSpinBox_2.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.doubleSpinBox_2.setAccelerated(True)
        self.doubleSpinBox_2.setMaximum(2500.0)
        self.doubleSpinBox_2.setObjectName(_fromUtf8("doubleSpinBox_2"))
        self.gridLayout.addWidget(self.doubleSpinBox_2, 2, 1, 1, 1)
        self.doubleSpinBox_3 = QtWidgets.QDoubleSpinBox(eraser_wdgt)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.doubleSpinBox_3.sizePolicy().hasHeightForWidth())
        self.doubleSpinBox_3.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.doubleSpinBox_3.setFont(font)
        self.doubleSpinBox_3.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.doubleSpinBox_3.setAccelerated(True)
        self.doubleSpinBox_3.setMaximum(999999999.0)
        self.doubleSpinBox_3.setObjectName(_fromUtf8("doubleSpinBox_3"))
        self.gridLayout.addWidget(self.doubleSpinBox_3, 3, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(eraser_wdgt)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(eraser_wdgt)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 5, 0, 1, 1)
        self.btn_start = QtWidgets.QPushButton(eraser_wdgt)
        self.btn_start.setMinimumSize(QtCore.QSize(0, 50))
        self.btn_start.setObjectName(_fromUtf8("btn_start"))
        self.gridLayout.addWidget(self.btn_start, 4, 0, 1, 2)
        self.label_6 = QtWidgets.QLabel(eraser_wdgt)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.gridLayout.addWidget(self.label_6, 2, 2, 1, 1)
        self.label_5 = QtWidgets.QLabel(eraser_wdgt)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 1, 2, 1, 1)
        self.label_7 = QtWidgets.QLabel(eraser_wdgt)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.gridLayout.addWidget(self.label_7, 3, 2, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(10, 0, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 3, 1, 1)
        self.label_8 = QtWidgets.QLabel(eraser_wdgt)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.gridLayout.addWidget(self.label_8, 1, 4, 1, 1)
        self.sb_power = QtWidgets.QDoubleSpinBox(eraser_wdgt)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.sb_power.setFont(font)
        self.sb_power.setAccelerated(True)
        self.sb_power.setDecimals(0)
        self.sb_power.setMaximum(100.0)
        self.sb_power.setSingleStep(10.0)
        self.sb_power.setProperty("value", 100.0)
        self.sb_power.setObjectName(_fromUtf8("sb_power"))
        self.gridLayout.addWidget(self.sb_power, 1, 5, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 1, 6, 1, 1)

        self.retranslateUi(eraser_wdgt)
        QtCore.QMetaObject.connectSlotsByName(eraser_wdgt)

    def retranslateUi(self, eraser_wdgt):
        eraser_wdgt.setWindowTitle(_translate("eraser_wdgt", "Form", None))
        self.label.setText(_translate("eraser_wdgt", "Sweep rf", None))
        self.label_4.setText(_translate("eraser_wdgt", "in", None))
        self.doubleSpinBox.setToolTip(_translate("eraser_wdgt", "minimal frequency, MHz", None))
        self.doubleSpinBox_2.setToolTip(_translate("eraser_wdgt", "maximal frequency, MHz", None))
        self.doubleSpinBox_3.setToolTip(_translate("eraser_wdgt", "time of each fast sweep, ms", None))
        self.label_3.setText(_translate("eraser_wdgt", "to", None))
        self.label_2.setText(_translate("eraser_wdgt", " from", None))
        self.btn_start.setText(_translate("eraser_wdgt", "Send\n"
"RF", None))
        self.label_6.setText(_translate("eraser_wdgt", "MHz", None))
        self.label_5.setText(_translate("eraser_wdgt", "MHz", None))
        self.label_7.setText(_translate("eraser_wdgt", "ms", None))
        self.label_8.setText(_translate("eraser_wdgt", "power\n"
"to use", None))
        self.sb_power.setToolTip(_translate("eraser_wdgt", "amplitude (%) to use while erasing\n"
"after pressing \"Stop\" amplitude will be restored to previous value\n"
"\n"
"if amplitude was set to 0 (completely off), after pressing stop it will be set again to 0, and there is no need to switch off RF source\n"
"", None))
        self.sb_power.setSuffix(_translate("eraser_wdgt", "%", None))

