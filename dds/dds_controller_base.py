from qtpy import QtCore
import time

class QtSweeper(QtCore.QObject):
    newValueSet = QtCore.Signal(float)
    message = QtCore.Signal(object)
    stopped = QtCore.Signal()
    def __init__(self,setFunction = None,todolist = []):
        super(QtSweeper,self).__init__()
        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.t = 0
        self.setFunction = setFunction
        if not hasattr(todolist,"__iter__"):
            self.todolist = [todolist]
        else:
            self.todolist = todolist
    def sweep(self,val_min,val_high,step,dt,resume,loop,bidirectional):
        self.step = step
        self.value_low = val_min
        self.value_high = val_high
        if val_high<val_min:
            self.value_low,self.value_high = self.value_high,self.value_low
        if step>0:
            self.value = self.value_low
        elif step<0:
            self.value = self.value_high
        self.dt = dt
        self.timer.timeout.connect(self.setNextValue)
        self.t = time.time()
        self.t_start = self.t
        self.step_cnt = 0
        #self.timer.start(dt*1000)
        self.timer.start(dt*1000)
        print("sweeping from {} to {}, step is {}, dt = {}".format(self.value_low,self.value_high,self.step,self.dt))
        print("total sweep time is",abs((self.value_high-self.value_low)/self.step*dt),\
            "total cycles",(self.value_high-self.value_low)/self.step)
        self.go = True
    def setNextValue(self):
        self.step_cnt+=1
        # t = time.time()
        # delta = t-self.t
        # if
        # print "time elapsed by step:",delta
        #self.t = t

        self.value = self.value+self.step
        if self.value>=self.value_high:
            self.value = self.value_high
        elif self.value<=self.value_low:
            self.value = self.value_low
        if self.setFunction:
            self.setFunction(self.value)
            t = time.time()
            self.newValueSet.emit(self.value)
            self.message.emit((t,self.value))
            for func in self.todolist:
                func()
        else:
            print("QtSweeper: no function was set")
        if self.value>=self.value_high or self.value<=self.value_low:
            self.stop()
        #delta = t-self.t-self.dt
        if self.go:
            interval = 1000.*(2*self.dt-t+self.t)
            if interval<0:
                interval = 0
            print(interval)
            self.timer.start(interval)
        self.t = t
    def stop(self):
        self.go = False
        self.timer.stop()
        self.stopped.emit()
        self.message.emit("stop")
        tel = time.time()-self.t_start
        print("sweep took {} s, {}cycles, {:.3f} s per cycle".format(tel,self.step_cnt,tel/self.step_cnt))

class DDS_ControllerBase(QtCore.QObject):
    sig_float = QtCore.Signal(object)
    sig_tuple = QtCore.Signal(tuple)
    sig_string = QtCore.Signal(str)
    sig = QtCore.Signal(object)
    sig_new_range = QtCore.Signal(float,float)
    timer = QtCore.QTimer()
    IntSweepTimer = QtCore.QTimer()
    _sigCallRequest = QtCore.Signal(object)
    _sigCallComplete = QtCore.Signal(object)

    queryFreq = None
    sig_stopped = QtCore.Signal()
    stopped = True
    connection_status_changed = QtCore.Signal(object)
    def __init__(self):
        super(DDS_ControllerBase,self).__init__()
        self.sweeper_freq = QtSweeper()
        #self.timer.timeout.connect(queryFreq)
        self.IntSweepTimer.timeout.connect(self.sig_stopped.emit)
        self._sigCallRequest.connect(self._callFunc)
        self._sigCallComplete.connect(self._dispatchCallResult)
        self.thread = QtCore.QThread()
        self.thread.start()
        self.moveToThread(self.thread)
    def status(self):
        self.dds.status()
    def rcv(self,t):
        #print type(t),': ',t
        if type(t)==tuple:
            self.sig.emit(t)
            if len(t)>1:
                if type(t[1])==float:
                    self.sig_float.emit(t[1])
        elif type(t)==str:
            self.sig.emit(t)
        else:
            print("sig received:",t)
    def sendrange(self,begin,end):
        self.sig.emit(('range',(begin,end)))
    def processCommand(self,arg):
        if type(arg)==tuple:
            if len(arg)>1:
                if arg[0]=="range" and type(arg[1])==tuple:
                    self.sig_new_range.emit(arg[1][0],arg[1][1])
                    #if len(arg)>3:
                    #print arg
    def setqueryFreq(self,v):
        f = self.dds.setqueryFreq(v)
        self.sig_float.emit(f)
    def sweep(self,freq_min,freq_max,step,dt,resume,loop,bidirectional):
        self.loop = loop
        self.bidi = bidirectional
        self.sweeper_freq.sweep(freq_min,freq_max,step,dt,resume,loop,bidirectional)
    def open(self):
        self.dds.open()
        if self.dds.isOpen():
            self.connection_status_changed.emit(True)
            self.dds.status()
            self.sig_float.emit(self.dds.queryFrequency())
    def close(self):
        self.dds.close()
        if not self.dds.interface.connected:
            self.connection_status_changed.emit(False)
    def setConnected(self,status):
        if status:
            self.open()
        else:
            self.close()
    def setAddress(self,address):
        if address!=self.dds.address:
            if self.dds.interface.connected:
                self.close()
                open_after = True
            else:
                open_after = False
            self.dds.setDeviceAddress(address)
            if open_after:
                self.open()
    def call(self,function,*args,**kwargs):
        self._sigCallRequest.emit((function,args,kwargs))
    def _callFunc(self,tup):
            func,args,kwargs = tup
            if isinstance(func,(str)):
                func = getattr(self.dds,func)
            res = func(*args,**kwargs)
            if res:
                self._sigCallComplete.emit((func,res))
    def _dispatchCallResult(self, tup):
        """
        override this function to emit desired signals
        :param tup: function that was called, and result it has returned
        :return: None
        """
        func,res = tup
        if func==self.dds.queryFrequency:
            self.sig_float.emit(res)
    def __del__(self):
        QtCore.QMetaObject.invokeMethod(self.thread,"quit")
         # self.thread.quit()
        self.thread.wait(1000)