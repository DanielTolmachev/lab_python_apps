# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'register_view_ui.ui'
#
# Created: Sun Mar 06 23:26:08 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtWidgets,QtCore

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_Register_VIew_Form(object):
    def setupUi(self, Register_VIew_Form):
        Register_VIew_Form.setObjectName(_fromUtf8("Register_VIew_Form"))
        Register_VIew_Form.resize(800, 600)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Register_VIew_Form.sizePolicy().hasHeightForWidth())
        Register_VIew_Form.setSizePolicy(sizePolicy)
        self.gridLayout_2 = QtWidgets.QGridLayout(Register_VIew_Form)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.le_regdata = QtWidgets.QLineEdit(Register_VIew_Form)
        self.le_regdata.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.le_regdata.sizePolicy().hasHeightForWidth())
        self.le_regdata.setSizePolicy(sizePolicy)
        self.le_regdata.setObjectName(_fromUtf8("le_regdata"))
        self.gridLayout_2.addWidget(self.le_regdata, 1, 1, 1, 1)
        self.lbl_regdata_new = QtWidgets.QLabel(Register_VIew_Form)
        self.lbl_regdata_new.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl_regdata_new.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl_regdata_new.setObjectName(_fromUtf8("lbl_regdata_new"))
        self.gridLayout_2.addWidget(self.lbl_regdata_new, 1, 2, 1, 1)
        self.lbl_register = QtWidgets.QLabel(Register_VIew_Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_register.sizePolicy().hasHeightForWidth())
        self.lbl_register.setSizePolicy(sizePolicy)
        self.lbl_register.setObjectName(_fromUtf8("lbl_register"))
        self.gridLayout_2.addWidget(self.lbl_register, 0, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Register_VIew_Form)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Reset)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout_2.addWidget(self.buttonBox, 3, 2, 1, 1)
        self.cb_autoupdate = QtWidgets.QCheckBox(Register_VIew_Form)
        self.cb_autoupdate.setObjectName(_fromUtf8("cb_autoupdate"))
        self.gridLayout_2.addWidget(self.cb_autoupdate, 3, 0, 1, 2)
        self.tableView = QtWidgets.QTableView(Register_VIew_Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableView.sizePolicy().hasHeightForWidth())
        self.tableView.setSizePolicy(sizePolicy)
        self.tableView.setObjectName(_fromUtf8("tableView"))
        self.gridLayout_2.addWidget(self.tableView, 2, 0, 1, 3)

        self.retranslateUi(Register_VIew_Form)
        QtCore.QMetaObject.connectSlotsByName(Register_VIew_Form)

    def retranslateUi(self, Register_VIew_Form):
        Register_VIew_Form.setWindowTitle(_translate("Register_VIew_Form", "Form", None))
        self.le_regdata.setText(_translate("Register_VIew_Form", "0000FFFF", None))
        self.lbl_regdata_new.setText(_translate("Register_VIew_Form", "TextLabel", None))
        self.lbl_register.setText(_translate("Register_VIew_Form", "TextLabel", None))
        self.cb_autoupdate.setText(_translate("Register_VIew_Form", "Auto Update", None))

