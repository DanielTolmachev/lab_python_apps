#-------------------------------------------------------------------------------
# Name:     dds_controller.py
# Purpose:  control DDS generator made for e3.physik.tu-dortmund.de
#
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Author:      Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------

from qtpy import QtCore
from dds3E_dev_class import DDS3E_Class,FSYS
from dds_controller_base import DDS_ControllerBase
import time
import numpy as np
DDS_SPEED_COEFF = 1. #9.3046e+04
DDS_STEP_TIME = 91/FSYS #from manual
class DDS_Controller(DDS_ControllerBase):
    sig_float = QtCore.Signal(object)
    sig_tuple = QtCore.Signal(tuple)
    sig_string = QtCore.Signal(str)
    sig = QtCore.Signal(object)
    sig_new_range = QtCore.Signal(float,float)
    #timer = QtCore.QTimer()
    queryFreq = None
    sig_stopped = QtCore.Signal()
    stopped = True
    connection_status_changed = QtCore.Signal(object)
    progressChanged = QtCore.Signal(float)
    def __init__(self):
        super(DDS_Controller,self).__init__()
        #self.timer.timeout.connect(queryFreq)
    def rcv(self,t):
        #print type(t),': ',t
        if type(t)==tuple:
            self.sig.emit(t)
            if len(t)>1:
                if type(t[1])==float:
                    self.sig_float.emit(t[1])
        elif type(t)==str:
            self.sig.emit(t)
        else:
            print("sig received:",t)
    def sendrange(self,begin,end):
        self.sig.emit(('range',(begin,end)))
    def processCommand(self,arg):
        if type(arg)==tuple:
            if len(arg)>1:
                if arg[0]=="range" and type(arg[1])==tuple:
                    self.sig_new_range.emit(arg[1][0],arg[1][1])
                    #if len(arg)>3:
                    #print arg
    def setqueryFreq(self,v):
        f = self.dds.setqueryFreq(v)
        self.sig_float.emit(f)
    def sweep(self,freq_min,freq_max,step,dt,resume,loop,bidirectional):
        #print resume,loop,bidirectional
        self.loop = loop
        self.bidi = bidirectional
        #self.dds.setAmpCommon(self.dds.amplitude)
        if bidirectional:
           self.sweep_up_loop_bidi(freq_min,freq_max,step,dt)
        else:
            if self.loop:
                self.sweep_up_loop(freq_min,freq_max,step,dt)
            else:
                self.sweep_up_once(freq_min,freq_max,step,dt)
    def sweep_up_loop_bidi(self,freq_min,freq_max,step,dt):
        print("sweep bidirectional loop")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,step,step,delta,delta)
        self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setNoDwellLow(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        self.dds.write("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        self.timer = QtCore.QTimer(self)
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
        #self.dds.ask("K0:")
    def sweep_up_loop(self,freq_min,freq_max,step,dt):
        print("sweep forward loop")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,freq_max-freq_min,step,1,delta)
        self.dds.setAmpCommon(self.dds.amplitude)
        self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setNoDwellLow(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        self.dds.write("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
    def sweep_up_once(self,freq_min,freq_max,step,dt):
        print("sweep forward once")
        ttot = (freq_max-freq_min)/step*dt
        step,delta = self.dds.calcSweep(freq_min,freq_max,ttot)
        print(step,delta,"dt =",dt)
        #self.dds.reset()
        self.dds.setupSweep(freq_max,freq_min,step,step,delta,delta)
        print("setting up amplitude",self.dds.amplitude)
        self.dds.setAmpCommon(self.dds.amplitude)
        self.dds.ask("K0:")
        self.dds.setNoDwellHigh(True)
        self.dds.setClearDrgAccum(False)
        self.dds.setDRG(True)
        self.sweepStart = freq_min
        self.sweepStop = freq_max
        self.sweepSpeed = step/delta/self.dds.dt_min
        self.sweepDt = delta*self.dds.dt_min
        #self.sweepStartTime = self.dds.startSweep()
        t = time.time()
        self.dds.write("K1:")
        self.dds.read()
        self.sweepStartTime = time.time()
        self.timer = QtCore.QTimer(self)
        print("start time precision",self.sweepStartTime-t)
        self.timer.timeout.connect(self.announceFreq)
        self.timer.start(dt*1000)
        print("sweep dds from {:g}Hz to {:g}Hz with {:g} Hz/s for {}s".format(freq_min,freq_max,self.sweepSpeed,ttot))
        self.stopped = False
        if not self.loop:
            self.dds.ask("K0:")
    def announceFreq(self):
        ct = time.time()
        t_elapsed = ct-self.sweepStartTime
        tsweep = (self.sweepStop-self.sweepStart)/self.sweepSpeed
        periods = np.floor(t_elapsed/tsweep)
        if self.bidi and periods%2:
            t = (periods+1)*tsweep-t_elapsed
        else:
            t = t_elapsed-periods*tsweep
        #tel2 = t_elapsed - t_elapsed/self.sweepDt*DDS_STEP_TIME
        #print "time correction ",t_elapsed,tel2,"steps",t_elapsed/self.sweepDt
        freq = self.sweepStart+self.sweepSpeed*t*DDS_SPEED_COEFF
        #print freq
        self.sig.emit((ct,freq))
        self.sig_float.emit(freq)
        if self.stopped or (not self.loop and periods>0):
            self.stop()
    def stop(self):
        self.timer.stop()
        self.sig_stopped.emit()
        self.sig.emit("stop")
        if not self.stopped:
            self.stopped = True
            self.dds.setDRG(False)
            self.dds.setAmpFromProfile(True)
        freq = self.dds.queryFreq()
        self.sig_float.emit(freq)
    def open(self):
        self.dds.open()
        if self.dds.interface.connected:
            self.connection_status_changed.emit(True)
    def close(self):
        self.dds.close()
        if not self.dds.interface.connected:
            self.connection_status_changed.emit(False)
    def executeCommand(self,args):
        if type(args)==tuple:
            if args[0] == "loadRam":
                self.loadRam(*args[1:])
                return
            func = getattr(self.dds,args[0])
            func(*args[1:])
        elif type(args)==str:
            func = getattr(self.dds, args)
            func()
        else:
            print("cannot execute command",args)
    def invokeCommand(self,*args):
        #TODO
        QtCore.QMetaObject.invokeMethod(self, args[0],*args[1:])

    def loadRam(self, lst):
            self.dds.ask("N16W:")
            sz = len(lst)
            for i,p in enumerate(lst):
                msg = "L" + p + ":"
                # print(msg)
                rep = self.dds.ask(msg)
                self.progressChanged.emit((i+1)/sz)
                #print(msg,">",rep)




# class e3DDS_Class_Qt(QtCore.QObject,e3DDS_Class):
#     def __init__(self,*args):
#         super(e3DDS_Class_Qt,self).__init__()




