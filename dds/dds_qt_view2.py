__author__ = 'SFB'

from qtpy import QtGui,QtCore,QtWidgets
from register_view_ui import Ui_Register_VIew_Form
import traceback

class mymodel(QtGui.QStandardItemModel):
    def __init__(self,*args):
        super(mymodel,self).__init__(*args)
        # self.lbl = QtWidgets.QLabel()

    # def data(self,model,role):
    #     print model,role

class Mydelegate(QtWidgets.QItemDelegate):
    def __init__(self):
        super(Mydelegate,self).__init__()

        # self.setText(str(args))

class REGISTER_VIEW(QtWidgets.QWidget,Ui_Register_VIew_Form):
    dataChanged = QtCore.Signal(str)
    def __init__(self,device_register_description_dict = None,fsys = 0):
        super(REGISTER_VIEW,self).__init__()
        self.setupUi(self)
        self.regdesc = device_register_description_dict
        self.bytedescdict = {}
        self.mergecells = {}
        self.le_regdata.returnPressed.connect(self.checkupdate)
        self.tableView.doubleClicked.connect(self.toggle)
        #self.buttonBox.clicked.connect(self.on_btn_clicked())
        self.fsys = fsys
        title = ""
        if self.regdesc:
            if "long_name" in self.regdesc:
                title = "{} ({})".format(self.regdesc["long_name"],self.regdesc["name"])
            elif "name" in self.regdesc:
                title = format(self.regdesc["name"])
            if "bytes" in self.regdesc:
                try:
                    for addr,sz,desc,_ in self.regdesc["bytes"]:
                        if sz == 1:
                            self.bytedescdict[addr] = desc
                        elif sz>1:
                            self.bytedescdict[addr+sz-1] = desc
                            self.mergecells[addr+sz-1] = sz
                except:
                    print('error while parsing "bytes" in registers description',self.regdesc["bytes"])
        #if title:
        self.lbl_register.setText(title)
    def toggle(self,modelindex):
        #print modelindex
        r = modelindex.row()
        c = modelindex.column()
        nd = self.bitlen-1 - c-r*8
        self.newdatai = self.newdatai ^ 1 << nd
        print(self.datai,">>",self.newdatai)
        data = str((self.newdatai >> nd) & 1)
        self.setTableItem(r,c,data)
        self.newregisterdata(self.newdatai)
    def setTableItem(self,r,c,*data):
        index = self.model.index(r,c,QtCore.QModelIndex())
        #self.model.setData(index,data)
        n = c+r*8
        nd = self.bitlen-1-n
        if data:
            s = data[0]
        else:
            if nd in self.mergecells:
                s=self.datab[n:n+self.mergecells[nd]]
            else:
                s = self.datab[n]
        if nd in self.mergecells:
            cells = self.mergecells[nd]
            rowspan = 1
            if not cells%8:
                rowspan = cells/8
            self.tableView.setSpan(r,c,rowspan,cells)
        bitlen = len(s)
        if self.bytedescdict and nd in self.bytedescdict:
            if bitlen>7:
                if "frequency" == self.bytedescdict[nd] and self.fsys:
                    freq = float(int(s,2))/2**bitlen*self.fsys
                    s += "\n{:g}".format(freq)
                elif "amplitude" == self.bytedescdict[nd] and self.fsys:
                    amp = float(int(s,2))/2**bitlen
                    s += "\n{:g}".format(amp)
                elif "phase" == self.bytedescdict[nd] and self.fsys:
                    phase = float(int(s,2))/2**bitlen
                    s += "\n{:g}".format(phase)
            else:
                s += "\n{}".format(self.bytedescdict[nd])
        self.model.setData(index,s)
    def newregisterdata(self,newdatai):
        # newreg = ""
        # for r in range(0,self.rowcount):
        #     for c in range(0,8):
        #         newreg += self.model.item(r,c).data(0).toString()
        self.newreg = "{:0{}X}".format(newdatai,self.bitlen/4)
        self.lbl_regdata_new.setText(self.newreg)
        if self.cb_autoupdate.isChecked():
            if self.newreg!=self.data:
                self.dataChanged.emit(self.newreg)
                self.data = self.newreg
                self.le_regdata.setText(self.data)
    def on_buttonBox_clicked(self,*args):
        self.dataChanged.emit(self.data)
        self.data = self.newreg
        self.le_regdata.setText(self.data)
        print("apply",args)
    def checkupdate(self):
        try:
            txt = str(self.le_regdata.text())
            i = int(txt,16)
            if len(txt)%4:
                return
        except:
            traceback.print_exc()
            return
        self.le_regdata.setText(txt.upper())
        self.draw(txt)
    def setRegData(self,data):
        self.data = data
    def draw(self,data):
        self.data = data
        self.le_regdata.setText(data)
        self.lbl_regdata_new.setText(data)
        self.bitlen = len(data)*4
        print(data)
        self.datai = int(data,16)
        self.newdatai = self.datai
        self.datab = "{:0{}b}".format(self.datai,self.bitlen)
        print(self.datab)
        self.rowcount = self.bitlen/8
        # self.tableView.setColumnCount(8)
        # self.tableView.setRowCount(self.rowcount)
        #
        #

        self.model = mymodel(self.rowcount,8)
        self.tableView.setModel(self.model)
        for r in range(0,self.rowcount):
            for c in range(0,8):
                n = c+r*8
                self.setTableItem(r,c)
        colwidth = 120
        # print self.tableView.width(),self.tableView.verticalHeader().width(),colwidth
        rowheight = 100
        # self.tableView.repaint()
        # headerlabels = QtCore.QStringList()

        for c in range(0,8):
            #self.tableView.setColumnWidth(c,colwidth)
            pass
        #     headerlabels.append(str(7-c))
        # self.tableView.setHorizontalHeaderLabels(headerlabels)
        # headerlabels = QtCore.QStringList()
        for r in range(0,self.rowcount):
            #self.tableView.setRowHeight(r,rowheight)
            pass
        #     headerlabels.append(str(self.rowcount-1-r))
        # self.tableView.setVerticalHeaderLabels(headerlabels)
        #self.tableView.setGeometry(0,0,colwidth*8,rowheight*self.rowcount)
        mydelegate = Mydelegate()
        self.tableView.setItemDelegate(mydelegate)
        #print "table size",self.tableView.geometry()
    def edit(self):
        for i,btn in enumerate(self.list_buttons):
            if btn == self.sender():
                break
        if self.list_buttons_coord[i][2]>1:
            btn.hide()
            self.editeditem = i
            le = QtWidgets.QLineEdit()
            le.setFixedHeight(btn.height())
            self.gl.addWidget(le,self.list_buttons_coord[i][0],self.list_buttons_coord[i][1],1,
                              self.list_buttons_coord[i][2])
            le.returnPressed.connect(self.leaveEdit)
    def leaveEdit(self):
        le = self.sender()
        data  = le.text()
        le.hide()
        self.list_buttons[self.editeditem].show()
        print(data)



class DDS_QT_VIEW(QtWidgets.QWidget):
    registerChanged = QtCore.Signal(object,object)
    def __init__(self,device):
        super(DDS_QT_VIEW,self).__init__()
        self.dds = device
        gl = QtWidgets.QGridLayout()
        self.setLayout(gl)
        self.tw = QtWidgets.QTabWidget()
        self.reg_list = []
        self.view_list = []
        self.btn_apply = QtWidgets.QPushButton("Apply")
        self.cb_apply_auto = QtWidgets.QCheckBox("auto apply")
        self.cb_apply_auto.toggled.connect(self.on_cb_apply_auto_toggled)
        gl.addWidget(self.tw,0,0,1,3)
        #gl.addWidget(self.cb_apply_auto,1,0)
        #gl.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum),1,1)
        #gl.addWidget(self.btn_apply,1,2)
        self.setWindowTitle("DDS registers")
        for reg in self.dds.register_description:
            w = REGISTER_VIEW(self.dds.register_description[reg],fsys = self.dds.fsys)
            w.dataChanged.connect(self.on_dataChanged)
            if type(reg) == int:
                regS = "0x{:0X}".format(reg)
            else:
                regS = format(reg)
            self.tw.addTab(w,regS)
            self.reg_list.append(reg)
            self.view_list.append(w)
            if reg in self.dds.register_data and self.dds.register_data[reg]:
                print(repr(self.dds.register_data[reg]))
                w.draw(self.dds.register_data[reg])
                #w.tableView.adjustSize()
                #w.tableView.resizeColumnsToContents()
                w.tableView.repaint()
                print(w.tableView.geometry())
                width = w.tableView.verticalHeader().sizeHint().width()+20
                height = w.tableView.horizontalHeader().sizeHint().height()+20
                geom = self.geometry()

                if geom.height()> height:
                    height = geom.height()
                if geom.width()> width:
                    width = geom.width()
                print(width,height)
                #self.resize(width,height)
                #self.adjustSize()



        self.tw.currentChanged.connect(self.on_tw_currentChanged)
    def on_cb_apply_auto_toggled(self,state):
        #print state
        self.btn_apply.setEnabled(not state)
    def on_dataChanged(self,data):
        sndr = self.sender()
        for w,reg in zip(self.view_list,self.reg_list):
            if w==sndr:
                self.registerChanged.emit(reg,data)
                #print "register changed",reg,data
    def on_tw_currentChanged(self,idx):
        reg = self.reg_list[idx]
        if reg not in self.dds.register_data:
            rep = self.dds.readRegister(reg)
            if rep:
                self.view_list[idx].draw(rep.strip())
        else:
            self.view_list[idx].draw(self.dds.register_data[reg])

if __name__ == "__main__":
    import dds3E_dev_class
    dds = dds3E_dev_class.DDS3E_Class("127.0.0.1")
    dds.register_data = {0: '00000002',
 1: '00400020',
 2: '0F3FC000',
 3: '00007F7F',
 4: '00000004',
 5: 'F2E038A5AC66FFFF',
 6: '36A22151D872FFFF',
 7: '00000000',
 8: '00000000',
 9: '00000000',
 10: '00000000',
 11: '36207B235680FE9A',
 12: '79E465BD254A481B',
 13: '28A57E28',
 14: '3FFF0000170A3D70',
 15: '3FFF0000170A3D70',
 16: '3FFF0000170A3D70',
 17: '3FFF0000170A3D70',
 18: '3FFF0000170A3D70',
 19: '3FFF0000170A3D70',
 20: '00000002',
 21: '3FFF0000170A3D70'}

    app  = QtWidgets.QApplication([])
    view = DDS_QT_VIEW(dds)
    # view = REGISTER_VIEW()
    view.show()
    #view.adjustSize()
    print(view.geometry())
    # view.le_regdata.setEnabled(True)
    # view.draw("0Ffecdfa")
    app.exec_()