nucs = {"As75":4.59616,
        "Ga69":6.43886,
        "Ga71":8.18117}

field = 0.1 #Tesla
sweep = 0.1 # MHz to sweep around resonance
amplitude = 1
tablesize = 1024
from numpy import pi,zeros,arange,linspace

def respos(nuc):
    if isinstance(nuc,str):
        if nuc in nucs:
            gamma = nucs[nuc]
        else:
            return
    else:
        gamma = float(nuc)
    return field*gamma*1e6/2/pi

def findranges(*nucs):
    ranges = []
    for n in nucs:
        res = respos(n)
        if res:
            ranges.append((res-sweep,res+sweep))
    return ranges

def maketable(ranges):
    """
    :param ranges: list of tuples
    :return: np.array
    """
    freqspan = 0
    rangepts = []
    tabl = zeros((tablesize))
    for r in ranges:
        if r[1]<r[0]:
            r = reversed(r)
        freqspan += r[1]-r[0]
    step = freqspan/tablesize
    print("step size is {} Hz".format(step))
    cr = 0
    for r in ranges:
        rangepts = int((r[1]-r[0])/step)
        tabl[cr:rangepts+cr] = linspace(r[0],r[1],rangepts)
        cr+=rangepts
    return tabl


if __name__ == "__main__":
    for n in nucs:
        print("{:g}".format(respos(n)))
    ranges = findranges(*tuple(nucs.keys()))
    print(ranges)
    tab = maketable(ranges)
    print(tab)

