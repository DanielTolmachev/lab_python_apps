from __future__ import print_function
import zmq
from random import choice
import numpy as np
import time

context = zmq.Context()
ss = context.socket(zmq.PUB)  #signal socket
ss.bind("tcp://127.0.0.1:5000")
ss2 = context.socket(zmq.PUB)  #signal socket
ss2.bind("tcp://127.0.0.1:5002")
sf = context.socket(zmq.PUB)  #frequency socket
sf.bind("tcp://127.0.0.1:5003")

i = 0
period = 30  #sec
totaltime = 100000
dt = 0.1
f0 = 2840.
f1 = 2940.
dtf = 0.2  #s per frequency step
dts = 0.1
dts2 = 0.3
df = (f1 - f0) / period * dtf
speed = (f1 - f0) / period
start = time.time()
cycles = totaltime / dt
f2 = f0
sig_cycles = 0
sig2_cycles = 0
while i < cycles:
    ch1 = (np.random.random() - 0.5) * .01
    ch2 = (np.random.random() - 0.5) * .01
    t = time.time()
    elapsed = t - start
    elapsed_in_this_period = elapsed - int(elapsed) / period * period
    steps = int(elapsed_in_this_period / dtf)
    f = f0 + steps * df

    if f != f2:
        sf.send_pyobj((t, f*1e6))
    f2 = f
    if int(elapsed / dts) > sig_cycles:
        x = f0 + elapsed_in_this_period * speed
        sig = np.exp(-(x - 2875) ** 2 / 20) + np.exp(-(x - 2885) ** 2 / 20)+ch1
        sig_cycles += 1
        ss.send_pyobj((t, sig, ch2))
    if int(elapsed / dts2) > sig2_cycles:
        x = f0 + elapsed_in_this_period * speed
        sig = np.exp(-(x - 2875) ** 2 / 20e3) + np.exp(-(x - 2885) ** 2 / 20)+ch1
        sig2_cycles += 1
        ss2.send_pyobj((t, sig, ch2))
    ##    msg = (i,elapsed, f, sig)
    ##    print msg
    time.sleep(0.1)
    i = i + 1

sf.close()
ss.close()
context.destroy()