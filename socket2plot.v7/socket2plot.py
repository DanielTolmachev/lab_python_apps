#-------------------------------------------------------------------------------
# Purpose:	Asynchronous plotter/recorder
#
# Operation:	this app receives data using ZMQ messagin library
#		(SUB pattern), plots on screen versus time, or X vs Y
#		and records it in text files
#
# Created:     22.01.2014
#-------------------------------------------------------------------------------
from __future__ import print_function

from round2err import round_to_err

__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"
__version__ = "7"


import sys, os, time, re, traceback
sys.path.append( '../modules')
import enable_exception_logging
from shlex import split
from qtpy import QtWidgets, QtCore
from numpy import arange,sqrt,array,interp,isnan,vstack

import pyqtgraph as pg
from socket2plot_gui import Ui_sock2plot_main_wnd
from paramsDlg import ParamsDlg
from src_selection_dlg import Src_Selection_Dlg
from pattern2name import pattern2name
from sources import list_of_sources as srcl
from record import Record, RecordLive
from TabManager import TabManager
from currentPointCursor import CurrentPointCursor
from console_qt import QConsole
from options import Options
from zmq_qt_recv import zmq_qt_recv
from module_path import PROGRAM_PATH
#from curve_manager import CurveManager
#from plot_manager import PlotManager
#from __main__ import __file__ as __filemain__ #not work with py2exe
from create_uniform_array import create_uniform_array
import console_qt,options_editor,QMenuFromDict
import install_exception_hook


colors = ['w', 'y', 'g', 'r']
options = Options()
##UDP_IP = "127.0.0.1"
##UDP_PORT = 6000
##sock = socket.socket(socket.AF_INET, # Internet
##                     socket.SOCK_DGRAM) # UDP
##sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
###sock.settimeout(1)
##sock.bind((UDP_IP, UDP_PORT))

DEFAULT_TEXT_FILE_EXTENSION = 'dat'
DEFAULT_ENCODING = 'cp1251'
SEND_CMD = False
SEND_CMD_PORT = 4999
SOURCE_TALK_BACK = True
SOURCE_TALK_BACK_SEND_RANGE = True
SAMPLE_LIST_FILE_NAME = 'sample_list.txt'
SHOW_CURSOR = False
ARRAYDEFSIZE = 1024
TIMECONSTANT = 0.1

start_modes = {
    'start': 'Start'}

import zmq

MODE_REGULAR = 0
MODE_LIVE = 1
MODE_XY = 2
MODE_XY_ACCUM = 3

context = zmq.Context()

ADDRESS = '127.0.0.1:5000'
#socket.connect("tcp://%s" % ADDRESS)
#socket.setsockopt(zmq.SUBSCRIBE, "")
# DATA_INPUT = [
#     '127.0.0.1:5000',
#     '127.0.0.1:5001'
# ]

# source_profile = [
#     [1, 2]
#     [1]
# ]
# time_data = [
#     1,
#     -1
# ]
# display_profile = [
#     [3, 4],
#     []
# ]
# gw = pg.GraphicsWidget()
numpat = re.compile('[+-]*\d+\.*\d*[eE][+-]\d*|[+-]*\d*\.\d+|[+-]*\d+\.*')
last_message_dict = {}

# def getnum(text, *num):
#     l = re.findall(numpat, unicode(text))
#     if len(num) > 0:
#         if num[0] < len(l):
#             return float(l[num[0]])
#     elif l != []:
#         return l
#     else:
#         return np.NAN


def stats(dataset):
    res = ''
    for k in [2]:
        mean = dataset[k].mean()
        err = dataset[k].std() / sqrt(dataset.shape[1])
        mean = round_to_err(mean, err)
        res = res + '%0g %.2g' % (mean, err)
    print(('%.2g %s' % (dataset[0][-1], str(res))))
    return res




class Src_Talk_Back(object):
    def __init__(self,context,list_of_sources):
        self.list_addr = []
        self.list_socket = []
        for src in list_of_sources:
            # if "subscriber" in src.dic:
            if hasattr(src,"subscriber"):
                addr = "tcp://{}".format(src.subscriber)
                self.list_addr.append(addr)
                socket = context.socket(zmq.PUB)
                self.list_socket.append(socket)
                try:
                    socket.connect(addr)
                    #print socket,addr
                except:
                    traceback.print_exc()
            else:
                self.list_addr.append(None)
                self.list_socket.append(None)
    def sendRange(self,src_number,msg):
        addr = self.list_addr[src_number]
        if addr:
            socket = self.list_socket[src_number]
            msg = ('range'.format(src_number),msg,src_number)
            socket.send_pyobj(msg)
            print("sending",msg,"to source #",src_number,"at",addr,socket)

src_talk_back = Src_Talk_Back(context,srcl)


if SEND_CMD:
    soc_pub_cmd = context.socket(zmq.PUB)
    try:
        soc_pub_cmd.bind("tcp://127.0.0.1:%d" % SEND_CMD_PORT)
    except:
        print(sys.exc_info())


def send_cmd(cmd):
    if SEND_CMD:
        try:
            soc_pub_cmd.sendRange(str(cmd))
            #     soc_pub_cmd.unbind("tcp://127.0.0.1:%d"%SEND_CMD_PORT)
            #soc.close()
            print(cmd)
        except:
            print(sys.exc_info())





class mainwnd(QtWidgets.QMainWindow, Ui_sock2plot_main_wnd):
    lbl_input_list = []
    datalist = []
    recordlist = []
    curvelist = []
    record_live = RecordLive(srcl,
                             100,
                             encoding=options.get('Save.encoding', DEFAULT_ENCODING))
    # curve_manager = CurveManager(srcl)
    srcl = srcl
    dim = 3
    dim2show = [1, 2]
    dimtime = 0
    dimx = 0
    def_size = options.get('memory.default_array_size', 1024)
    arr_size = 0
    time_start = 0.
    time_elapsed = 0.
    time_period = 0
    time_ov = False
    chan2show = 0  # total curves on screen
    recording = False
    screen_upd_timer = QtCore.QTimer()
    timeconstant = 0.1
    xyplot_enabled = False
    xyplot = None
    xlabel = None
    live = False
    xy_accum_enabled = False
    plot_xy_points = array([])
    plot_xy_lastpoint = []
    statYError = None
    started = QtCore.Signal(str)
    rec_is_started = False
    nt = []  #list of threads, that receive data from ports
    params = dict()  #dict of parameters to be saved into file
    nameadd = ''  #string to add to filename
    mode = None
    source_modified = False
    screen_upd_timeout = 100  #ms
    autoCreatePlots = options.get("gui.autoCreatePlots", True)

    def _setwingeometry(self, options):
        if options != {}:
            screen_width = QtWidgets.QDesktopWidget().availableGeometry().width()
            screen_height = QtWidgets.QDesktopWidget().availableGeometry().height()
            #self.setMaximumHeight(screen_height)
            scr_cnt = QtWidgets.QDesktopWidget().screenCount()
            scr_left = []
            scr_top = []
            for i in range(0,scr_cnt):
                scr_left.append(QtWidgets.QDesktopWidget().availableGeometry(i).left())
                scr_top.append(QtWidgets.QDesktopWidget().availableGeometry(i).top())
            scr_left = min(scr_left)
            scr_top = min(scr_top)

            width = options.get('MainWnd.width', -1)
            if width < self.minimumWidth() or width > screen_width:
                width = self.width()
            height = options.get('MainWnd.height', -1)
            if height < self.minimumHeight() or height > screen_height:
                height = self.height()
            xpos = options.get('MainWnd.x', -1)
            if xpos < scr_left:
                xpos = scr_left
            elif xpos > screen_width - width:
                xpos = screen_width - width
            ypos = options.get('MainWnd.y', -1)
            if ypos < scr_top:
                ypos = scr_top
            elif ypos > screen_height - height:
                ypos = screen_height - height
            self.setGeometry(0, 0, width, height)
            self.move(xpos, ypos)

    def __init__(self):
        super(mainwnd, self).__init__()
        self.setupUi(self)
        self._setwingeometry(options)
        self.tabManager = TabManager(self, autoCreatePlots=self.autoCreatePlots)
        self.tabManager.sigMouseMoved.connect(self.mouseMoved)
        self.curPointCursor = CurrentPointCursor(srcl,self.tabManager)
        # self.pw = PlotManager()
        self.directory = os.getcwd()
        # self.curve_manager.plotManager = self.pw
        # self.gridLayout.addWidget(self.pw,2,0,1,-1)
        self.btn_start.pressed.connect(self.startstop)
        self.btn_clr.pressed.connect(self.clear)
        self.btn_persistent.toggled.connect(self.live_mode_toggle)
        self.btn_xy_accum.toggled.connect(self.toggleXYaccum)
        self.btn_xy_accum.setChecked(options.get('Plot.XYaccum', True))
        self.btn_xy.toggled.connect(self.toggleXY)
        self.btn_xy.setChecked(options.get('Plot.XY', True))
        try:
            self.le_comment.setText(options.get('comment', ''))
        except:
            traceback.print_exc()
        self.le_comment.textChanged.connect(self.set_comment)
        self.paramsDlg = ParamsDlg()
        self.paramsDlg.set_sample_list(self.directory + os.sep + SAMPLE_LIST_FILE_NAME)
        self.paramsDlg.sample_list.set(options)
        self.paramsDlg.accept.connect(self.set_params)
        self.inputDlg = Src_Selection_Dlg(srcl)
        self.inputDlg.accept.connect(self.set_inputs)
        self.btn_input.pressed.connect(self.query_inputs)
        self.btn_setparams.pressed.connect(self.query_params)
        self.cb_save.stateChanged.connect(self.enablesave)
        # if not os.path.exists(options.get('SaveDirectory', '.')): #this no longer needed, self.checkSaveDir
        #     options['SaveDirectory'] = '.'
        save_dir = options.get('SaveDirectory',".")
        if "SaveDirectoryPattern" in options:
            try:
                pat = options["SaveDirectoryPattern"]
                save_dir_new = pattern2name(pat,options)
                if save_dir_new!=save_dir:
                    save_dir = save_dir_new
                    print("creating dirname from pattern",pat,"->",save_dir)
            except:
                traceback.print_exc()
        if os.path.isabs(save_dir):
            self.lbl_save.setText(save_dir)
            self.save_dir = save_dir
        else:
            self.lbl_save.setText('.\\' + save_dir)
            self.save_dir = self.directory + os.path.sep + save_dir

        #os.chdir(options['SaveDirectory'])
        self.lbl_pattern.setText(self.prep_name())
        self.cb_save.setChecked(options.get('SaveEnabled', False))
        options['SaveNumber'] = 1
        self.console = QConsole()
        self.btn_console.toggled.connect(self.console.setVisible)
        self.btn_console.setChecked(options.get("gui.showConsole",False))
        self.console.sigStatusChanged.connect(self.btn_console.setChecked)
        # self.pi = self.pw.getPlotItem()
        # self.vb = self.pi.vb
        # self.proxy = pg.SignalProxy(self.pi.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        # self.pw.sigMouseMoved.connect(self.mouseMoved)
        self._create_labels()
        self.set_inputs()

        if options.get('StatYError', True):
            self.statYError = QtWidgets.QLabel()
            self.lbl_Layout.addWidget(self.statYError)

        self.load_options(options)

        self.screen_upd_timer.timeout.connect(self.update_screen)

        if hasattr(srcl, 'timeconstant'):
            self.timeconstant = srcl.timeconstant
        self.setupTabs()

        if SOURCE_TALK_BACK and SOURCE_TALK_BACK_SEND_RANGE:
            for srcN,src in enumerate(srcl):
                if src.enabled:
                    if src_talk_back.list_addr[srcN]:
                        self.tabManager.xyPlotManagersDict[srcN].setRangeMovable(True, srcN)
                        self.tabManager.xyPlotManagersDict[srcN].rangeChanged.connect(src_talk_back.sendRange)

    #        self.live_mode_toggle()
    def load_options(self, options):
        self.start_mode = options.get('Opt.StartMode', 'start')
        self.live = options.get('Opt.LiveMode', True)
        if self.live and self.tabWidget.currentIndex()==0: #start updating screen immidiately if liveview is enabled
            self.screen_upd_timer.start(self.screen_upd_timeout)
        self.btn_persistent.setChecked(options.get('Opt.PersistentMode', False))
        self.autoswitchX = options.get("gui.autoSwitchX", True)
        self.autoswitchTab = options.get("gui.autoSwitchTab", True)
        self.showEmptyPlot = options.get("gui.showEmptyPlot", False)
        si = options.get("gui.showInputData", False)
        self.fr_rcv.setVisible(si)
        self.showCursor = options.get("gui.ShowCursor",SHOW_CURSOR)

    def on_btn_showRcvData_pressed(self):
        state = not self.fr_rcv.isVisible()
        self.fr_rcv.setVisible(state)
        options["gui.showInputData"] = state

    def on_btn_console_toggled(self,state):
        options["gui.showConsole"] = state

    def on_btn_save_pressed(self):
        self.saveSpectrum()
    def resetDocks(self):
        idx = self.tabWidget.currentIndex()
        if idx ==1: # time dependence
            self.tabManager.curveManagerTimemode.setup_mode(self.tabManager.curveManagerTimemode.mode)
    def setupTabs(self):
        self.xlabel = ''
        self.xunits = ""
        for src_num, src in enumerate(srcl):
            if src.enabled:
                #if src.xaxis:
                if src.xaxis:
                    print("source ", src_num, src, "xaxis", src.xaxis, src.chan_as_x)
                    self.xyplot = (src_num, src.xaxis)
                    self.tabManager.createXYtab(self.xyplot[0])
                    if hasattr(src, 'xlabel'):
                        self.xlabel = src.xlabel
                        #self.pi.setLabel('bottom',self.xlabel)
                        if hasattr(src, 'xunits'):
                            self.xunits = src.xunits
                            #self.pi.setLabel('bottom',self.xlabel,self.xunits)
                            #break
                elif src.chan_as_x:
                    print("source ", src_num, src, "xaxis", src.xaxis, src.chan_as_x)
                    self.tabManager.createXYtab(src_num)
                else:
                    print("source ", src_num, src)
                    # for chan in src.channels2show:
                    #     self.chan2show+=1
                    # self.pw.setXtitle(self.xlabel,self.xunits)

    def _savewingeometry(self, options):
        if options != None:
            #Save screen size
            options['MainWnd.width'] = self.width()
            options['MainWnd.height'] = self.height()
            options['MainWnd.x'] = self.pos().x()
            options['MainWnd.y'] = self.pos().y()

    def closeEvent(self, event):
        for nt1 in self.nt:  #stop background receiver threads
            nt1.stop()
        if options:
            #Save screen size to options object
            self._savewingeometry(options)
            #Save options to disk  - not needed, will be invoked anyway
            options.savetodisk()
        event.accept()
        QtWidgets.QApplication.closeAllWindows()

    def clear(self):
        #if self.live:
        self.record_live.clear()
        # self.curve_manager.clear()
        # self.pw.clear()
        #clear current tab
        i = self.tabWidget.currentIndex()
        if i==0:
            self.tabManager.curveManagerLivemode.clear()
        elif i==1:
            self.tabManager.curveManagerTimemode.removeAllCurves()
        else:
            src = self.tabManager.tab_index2src_number(i)
            #print("current tab is",i,self.srcl[src].name)
            self.tabManager.xyCurveManagersDict[src].removeAllCurves()
        # for i,curves in enumerate(self.datalist):
        #     if self.recording==False or i<len(self.datalist)-1:
        #         for curve in curves:
        #             self.pw.removeItem(curve)
        #             #self.datalist
        # #self.pi.clear()
    def clearAllPlots(self):
        self.tabManager.curveManagerTimemode.removeAllCurves()
        for curve_manager in self.tabManager.xyCurveManagersDict.values():
            curve_manager.removeAllCurves()
    def setlabel(self):
        if self.mode == MODE_LIVE or self.mode == MODE_REGULAR:
            self.pw.setXtitle('Time', 's')
        elif self.mode == MODE_XY or self.mode == MODE_XY_ACCUM:
            self.setupTabs()

    def live_mode_toggle(self):
        # print("this function is deprecated")
        # self.live = self.btn_live.isChecked()
        # #print 'btn_live isChecked: ', self.btn_live.isChecked()
        options['Opt.PersistentMode'] = self.btn_persistent.isChecked()
        # if self.live:
        #     print('live view enabled')
        # else:
        #     print('live view disabled')
        #     self.record_live.clear()
        #     self.curve_manager.clear_live_mode()
        #     #self.pw.update()
        # if not self.rec_is_started:
        #     if self.live:
        #         self.screen_upd_timer.start(self.screen_upd_timeout)
        #         self.set_mode()
        #     else:
        #         self.screen_upd_timer.stop()

    def on_tabWidget_currentChanged(self, c):
        """
        Launch/stop screen update timer when needed
        """
        print(c,self.screen_upd_timer.isActive())
        if c == 0 or self.recording:  #live mode is selected
            if not self.screen_upd_timer.isActive():
                self.screen_upd_timer.start(self.screen_upd_timeout)
                self.timerActivate()
        else:
            if self.screen_upd_timer.isActive():
                self.screen_upd_timer.stop()
        # print(c,self.screen_upd_timer.isActive(),self.screen_upd_timer.timerId(),self.screen_upd_timer.interval(),self.screen_upd_timer.isSingleShot())
        #self.screen_upd_timer.start(self.screen_upd_timeout)
    def timerActivate(self):
        print ("starting timer")
        self.screen_upd_timer.start(self.screen_upd_timeout)
    def mouseMoved(self, x, y):
        xs = '%g' % x
        ys = '%g' % y
        self.lbl_x.setText(xs)
        self.lbl_y.setText(ys)

    def setXsource(self, srcN):
        #TODO add posibilities to select channel
        if self.xyplot:
            curr = self.xyplot[0]
            srcl[curr]._xaxis = srcl[curr].xaxis
            srcl[curr].xaxis = None  #swithc off previous source of X
            if hasattr(srcl[srcN], "_xaxis"):
                srcl[srcN].xaxis = srcl[srcN]._xaxis
            else:
                #TODO
                srcl[srcN].xaxis = 1
        if srcl[srcN].xaxis:
            self.xyplot = (srcN, srcl[srcN].xaxis)
        else:
            self.xyplot = (srcN, srcl[srcN].chan_as_x)
        print("source of X was changed to #{} {} {}".format(srcN, srcl[srcN].name, self.xyplot[1]))
        self.tabManager.inputModified()

    def checkXsource(self, srcN):
        if self.autoswitchX:
            if self.xyplot:
                if self.xyplot[0] != srcN:
                    self.setXsource(srcN)
            else:
                self.setXsource(srcN)

    def update(self, data):
        #print QtCore.QObject.sender(self)
        #self.label_2.setText(str(data))
        if type(data) == tuple:
            src = data[0]
            if not srcl[src].enabled:
                print('received smth from ', srcl[src].name)
                return
            msg = '{0}({1}):{2}'.format(srcl[src].name, srcl[src].address, str(data[1:]))
            self.lbl_input_list[src].setText(msg)
            last_message_dict[src] = data[1:]
            # if src>5:
            #print data
            if type(data[1]) == str:
                print('{} says: {}'.format(srcl[src].name, data[1]))
                #wrds = split(data[1])
                if 'start' in data[1]:
                    self.checkXsource(data[0])
                    if not self.rec_is_started:  #start if not started
                        self.startstop()
                        return
                elif 'stop' in data[1]:
                    if self.rec_is_started:  #stop if started
                        self.startstop()
                        return
                if 'nameadd' in data[1]:
                    i = data[1].find('nameadd')
                    k = data[1][i:].find('=')
                    if k > -1 and len(data[1]) > i + k + 1:
                        wrds = split(data[1][i + k + 1:])
                        if wrds:
                            self.nameadd = wrds[0]
                if "range" in data[1] and len(data) > 2 \
                        and type(data[2]) == tuple:
                    l, r = data[2]
                    self.setRangeDisp(src, l, r)
                return

            elif type(data[1]) == dict:
                print('Parameters changed: ', data[1])
                self.params.update(data[1])
                #print self.params
                self.params = {k: v for k, v in self.params.iteritems() if v}  #clear empty values
            elif type(data[1]) == tuple and len(data[1]) == 2:
                if type(data[1][0]) == str and "range" in data[1][0] \
                        and type(data[1][1]) == tuple:
                    l, r = data[1][1]
                    self.setRangeDisp(src, l, r)
                ##            else:
                ##                print data[1]
            if self.showCursor:
                self.curPointCursor.setPoint(data)
            if self.recording:
                if len(data) > 2:
                    if type(data[2]) == str:
                        if 'skip on' in data[2]:
                            self.recordlist[-1].exclude_start = data[1]
                            print('skipping from {}'.format(self.recordlist[-1].exclude_start))
                            return
                        if 'skip off' in data[2]:
                            self.recordlist[-1].exclude_end = data[1]
                            print('skipping to {}'.format(self.recordlist[-1].exclude_end))
                            return
                            ##            if srcl[src].timechannel>=0:
                            ##                timestamp = data[srcl[src].timechannel]
                            ##            else:
                            ##                timestamp = time.clock()
                self.recordlist[-1].add(src, data)
                #exclude points while jumping from end to start of X axis

            ##                xsrc = self.xyplot[0]
            ##                xchan = srcl[src].channels2rec.index(self.xyplot[1])+1
            ##                x_raw = self.recordlist[-1].get(src,chan)
            ##        if self.recording>-1:
            ##            if self.recording>=self.arr_size:
            ##                self.arr_size +=1024
            ##                n = np.zeros((self.dim,self.arr_size)) #new array extended by 1024
            ##                n[:,0:self.recording] = self.datalist[-1][:,0:self.recording]
            ##                self.datalist[-1] = n
            ##
            ##            for i in range(0,self.dim):
            ##                self.datalist[-1][i][self.recording]=data[i]
            ##                #treat one column as time in sec
            ##            if self.dimtime>=0:
            ####                t = self.datalist[-1][self.dimtime][self.recording] /1000 #in sec max 999
            ####                if t>=self.time_start and self.time_ov: self.time_ov = False
            ####                elif t<self.time_start and not self.time_ov:
            ####                    self.time_ov = True
            ####                    self.time_period+=1
            ####                self.time_elapsed = t-self.time_start+self.time_period*1000
            ####                self.datalist[-1][self.dimtime][self.recording] =self.time_elapsed
            ####                print self.time_elapsed
            ##                t = self.datalist[-1][self.dimtime][self.recording] #time received from sender
            ##                self.time_elapsed = t-self.time_start
            ##                self.datalist[-1][self.dimtime][self.recording] =self.time_elapsed
            ##                #print self.time_elapsed
            ##            #print out average info
            ##            stats(self.datalist[-1][:,0:self.recording+1])
            ####            avrg = ()
            ####            for k in self.dim2show:
            ####                avrg = avrg  + (self.datalist[-1][k][0:self.recording+1].mean(),)
            ####            print('%.2g %s'% (self.datalist[-1][0][self.recording],str(avrg)))
            ##
            ##            self.recording +=1
            ##            if self.dim > self.dimx >=0 :
            ##                for i,k in enumerate(self.dim2show):
            ##                    self.curvelist[-1][i].setData(y = self.datalist[-1][k][0:self.recording],
            ##                    x = self.datalist[-1][self.dimx][0:self.recording])
            ##            else:
            ##                for i,k in enumerate(self.dim2show):
            ##                    self.curvelist[-1][i].setData(self.datalist[-1][k][0:self.recording])
            #    live mode
            #elif self.live: #
            if self.tabWidget.currentIndex() == 0:
                if len(data) > 2:
                    if type(data[2]) == str:
                        return
                self.record_live.add(src, data)
                #print 'live mode:updating data'
                # elif type(data) == str:
                #     if 'start' in data[1]:
                #             if not self.rec_is_started: #start if not started
                #                 self.startstop()
                #     elif 'stop' in data[1]:
                #         if self.rec_is_started: #stop if started
                #             self.startstop()

    def update_screen(self):
        #xy plot without accumulation
        # if self.xyplot_enabled and not self.xy_accum_enabled and self.rec_is_started:
        #     self.update_screen_xy()
        if self.xyplot and self.rec_is_started:
            #print self.tabManager.tabs
            if self.tabWidget.currentWidget() == self.tabManager.xyPlotManagersDict[self.xyplot[0]]:
                if not self.xy_accum_enabled:
                    self.update_screen_xy()
                else:
                    self.plot_XY_accum()
                    # elif self.live and not self.rec_is_started:
                    # self.update_screen_live()
        # else:
        #     self.update_screen_fromtime()
        if self.tabWidget.currentIndex() == 0:
            #update live screen when live tab is active
            self.update_screen_live()
        elif self.tabWidget.currentIndex() == 1:
            self.update_screen_fromtime()

    def update_screen_live(self):
        #print 'live mode: update screen'
        ##        crv = 0
        for i, src in enumerate(srcl):
            if src.enabled:
                try:
                    x = self.record_live.get(i, 0)
                    if x is not None and len(x):
                        x = x-x.min()
                        for k, chan in enumerate(src.channels2show):
                        #self.curve_manager.livemode(i,k).setData(
                        # self.tabManager.curveManagerLivemode.livemode(i,k).setData(
                        # y = self.record_live.get(i,k+1),
                        # x = self.record_live.get(i,0))
                        # if i == 7 or i==10:
                        #     print i,len(x)

                            y = self.record_live.get(i, k + 1)
                            self.tabManager.curveManagerLivemode.livemode(i, k).setData(
                                y=y, x=x)

                            ##                        crv+=1
                except:
                    print('Error, updating screen, live mode')
                    print('Source #{} {}'.format(i, src.name))
                    print(traceback.print_exc())

    def update_screen_fromtime(self):
        crv = 0
        for i, src in enumerate(srcl):
            if src.enabled:
                try:
                    for k, chan in enumerate(src.channels2show):
                        #self.datalist[-1][crv].setData(
                        #self.curve_manager.curve_regularmode(i, k).setData(
                        self.tabManager.curveManagerTimemode.curve_regularmode(i, k).setData(
                            y=self.recordlist[-1].get(i, k + 1),
                            x=self.recordlist[-1].get(i, 0))
                        crv += 1
                except:
                    print('Error, updating screen, raw data')
                    print('Source #{} {}'.format(i, src.name))
                    print(traceback.print_exc())
                    # print sys.exc_info()

    def update_screen_xy(self):
        XsrcN = self.xyplot[0]
        chan = srcl[XsrcN].channels2rec.index(self.xyplot[1]) + 1
        st = self.recordlist[-1].timestart
        if st == None:  #no data was received yet
            return
        ct = time.time()
        if self.recordlist[-1].timerelative:
            time_range = arange(0, ct - st, self.timeconstant)
        else:
            time_range = arange(st, ct, self.timeconstant)
        ##            #global x,y
        xdata = self.recordlist[-1].get(XsrcN, chan)  #xdata
        if not xdata.any():
            #self.statusbar
            return
        x = interp(time_range,  #new time points
                      self.recordlist[-1].get(XsrcN, 0),  #old time points
                      self.recordlist[-1].get(XsrcN, chan))  #xdata
        crv = 0
        for i, src in enumerate(srcl):
            if src.enabled:
                if i != XsrcN:
                    #print src.name,src.channels2show
                    for k, chan in enumerate(src.channels2show):
                        try:
                            y = interp(time_range,  #new time points
                                          self.recordlist[-1].get(i, 0),  #old time points
                                          self.recordlist[-1].get(i, k + 1))  #ydata
                            # self.curve_manager.curve_xy(i,k).setData(x,y)
                            self.tabManager.xyCurveManagersDict[XsrcN].curve_xy(i, k).setData(x, y)
                            #print 'points added to curve {}channel {}'.format(src.name,k)
                            crv += 1
                        except ValueError as exc:
                            if exc.args[0] != 'array of sample points is empty':
                                traceback.print_exc()
                        except:
                            print('Error, updating screen, XY plot, no accum\n', end=' ')
                            print(traceback.print_exc())

                            #smooth

                            ##                    if srcl.xvalues !=None:
                            ##                        ys = np.interp(srcl.xvalues,
                            ##                                        x,y)
                            ##                        self.datalist[-1][i+k].setData(srcl.xvalues,ys)

                            # print crv,"curves plotted"

    def plot_XY_accum(self):
        XsrcN = self.xyplot[0]
        chan = srcl[XsrcN].channels2rec.index(self.xyplot[1]) + 1
        x_raw = self.recordlist[-1].get(XsrcN, chan)
        #create or expand uniform array of x values
        if not self.chan2show:
            crv = 0
            for i, src in enumerate(srcl):
                if src.enabled and i != XsrcN:
                    crv += len(src.channels2show)
            if crv == 0:
                print("there is no curve to accumulate")
            self.chan2show = crv

        if not len(self.plot_xy_points):
            self.plot_xy_points = create_uniform_array(x_raw, self.chan2show * 5)
            self.plot_xy_lastpoint = []
        elif x_raw.min() < self.plot_xy_points[:, 0].min() or \
                        x_raw.max() > self.plot_xy_points[:, 0].max():
            self.plot_xy_points = create_uniform_array(x_raw, self.chan2show * 5)
            self.plot_xy_lastpoint = []
        #list of last points added to display
        if self.plot_xy_lastpoint == []:
            for i, src in enumerate(srcl):
                self.plot_xy_lastpoint.append(0)
            ##              print self.plot_xy_lastpoint
        #append new data to array which will be displayed
        crvs = 0  #number of curves
        for i, src in enumerate(srcl):
            if src.enabled and i != XsrcN:
                for j in xrange(self.plot_xy_lastpoint[i],
                                len(self.recordlist[-1].get(i, 0))):
                    #time of received data
                    t = self.recordlist[-1].get(i, 0)[j]
                    #find corresponding x
                    xind = self.recordlist[-1].get(self.xyplot[0], 0). \
                        searchsorted(t)
                    if xind < len(self.recordlist[-1].get(self.xyplot[0], 0)) and xind != 0:
                        #check if there is now x for that data yet

                        self.plot_xy_lastpoint[i] = j
                        x = self.recordlist[-1].get(self.xyplot[0], self.xyplot[1])[xind]
                        #find x index in uniform x array
                        xind = self.plot_xy_points[:, 0].searchsorted(x)
                        ##                    if x==61e6:
                        ##                        print x,self.plot_xy_points[xind,0],self.plot_xy_points[xind,1],self.plot_xy_points[xind,3]
                        ##                        pass
                        if xind < len(self.plot_xy_points):
                            #properly add new signal data to corresponding x in array
                            for k, chan in enumerate(src.channels2show):
                                y = self.recordlist[-1].get(i, k + 1)[j]
                                av = self.plot_xy_points[xind, 1 + k * 5]
                                sumrms = self.plot_xy_points[xind, 1 + k * 5 + 1]
                                n = self.plot_xy_points[xind, 1 + k * 5 + 2]
                                ##                        if not np.isnan(n) and n>0:
                                ##                            pass
                                prev = self.plot_xy_points[xind, 1 + k * 5 + 3]
                                ##                        if i==0 and k==0:
                                ##                            print av,prev
                                ##                        if y<0.2 and i==0 and k==0:
                                ##                            pass
                                ##                        if av<0.2 and i==0 and k==0:
                                ##                            pass
                                if isnan(av):
                                    av = y
                                else:
                                    av = (av * n + y) / (n + 1)
                                    ##                        if i==0 and k==0:
                                    ##                            print av,prev
                                    ##                        if av<0.2 and i==0 and k==0:
                                    ##                            pass
                                sumrms = sumrms + (y - prev) ** 2
                                prev = y
                                n += 1
                                err = sqrt(sumrms) / n
                                #src ==0
                                self.plot_xy_points[xind, 1 + crvs * 5 + k * 5] = av
                                self.plot_xy_points[xind, 1 + crvs * 5 + k * 5 + 1] = sumrms
                                self.plot_xy_points[xind, 1 + crvs * 5 + k * 5 + 2] = n
                                self.plot_xy_points[xind, 1 + crvs * 5 + k * 5 + 3] = prev
                                self.plot_xy_points[xind, 1 + crvs * 5 + k * 5 + 4] = err
                                #print i,k,1+crvs*5+k*5,av
                                # print src.name,src.channels2show
                                crv = self.tabManager.xyCurveManagersDict[XsrcN].curve_xy(i, k)
                                #crv = self.curve_manager.curve_xy(i,k)
                                #print crv
                                crv.setData(self.plot_xy_points[:, 0],
                                            self.plot_xy_points[:, 1 + k * 5])

                crvs += len(src.channels2show)

                # for k,chan in enumerate(src.channels2show):
                #     print src.name,src.channels2show
                #     crv = self.tabManager.xyCurveManagersDict[XsrcN].curve_xy(i,k)
                #     #crv = self.curve_manager.curve_xy(i,k)
                #     #print crv
                #     crv.setData(self.plot_xy_points[:,0],
                #                 self.plot_xy_points[:,1+k*5])
        # print crvs,"curves plotted"

        self.Yerr = []
        # global xy_points
        # xy_points = self.plot_xy_points
        # for i in xrange(0,self.chan2show):
        #     try:
        #         last = self.datalist[-1]
        #         crv = self.curve_manager.curve_xy(i,k)
        #         crv.setData(self.plot_xy_points[:,0],
        #                 self.plot_xy_points[:,1+i*5])
        #         if self.statYError:
        #             self.Yerr.append(self.plot_xy_points[:,1+i*5+4].mean())
        #     except OverflowError:
        #         print sys.exc_info()
        #     except IndexError:
        #         print i, self.chan2show, self.datalist
        #         traceback.print_exc()


        if self.statYError:
            msg = 'Error'
            for e in self.Yerr:
                msg += '  %g' % e
            self.statYError.setText(msg)
    def checkSaveDir(self):
        if not os.path.exists(self.save_dir):
            try:
                os.mkdir(self.save_dir)
                if os.path.exists(self.save_dir):
                    print("save directory created:",self.save_dir)
                    return
            except:
                traceback.print_exc()
            print("cannot create directory",self.save_dir)
            options['SaveDirectory'] = os.path.expanduser("~")
            self._set_save_directory()

    def saveSpectrum(self):
        ##                savendarray(self.datalist[-1][...,0:self.recording],
        ##                self.lbl_pattern.text())
        if self.xyplot:
            savexyplot = True
        else:
            savexyplot = False
        if savexyplot:
            self.recordlist[-1].save(self.save_dir + os.path.sep + self.prep_name(),
                                 savexyplot=savexyplot)
        else:
            self.recordlist[-1].saveTimeDep(self.save_dir + os.path.sep + self.prep_name())
        self.lbl_pattern.setText(self.prep_name())
        #save xy_points
        if len(self.plot_xy_points):
            xy_columns = self.plot_xy_points[:, 0]
            chans = (self.plot_xy_points.shape[1] - 1) / 5
            for k in range(0, chans):
                xy_columns = vstack((xy_columns,
                                        self.plot_xy_points[:, k * 5 + 1],
                                        self.plot_xy_points[:, k * 5 + 5]))
            savendarray(xy_columns,
                        self.save_dir + os.path.sep + self.lbl_pattern.text(),
                        headerrecord=self.recordlist[-1])
        self.lbl_pattern.setText(self.prep_name())

    def saveSpectrumAuto(self):
        if self.xyplot:
            self.recordlist[-1].xyplot = self.xyplot
            self.recordlist[-1].save(self.prep_autosave_name(),
                                     savexyplot=True)
        #autosave raw data
        self.recordlist[-1].save(self.prep_autosave_name(), savexyplot=False)
        self.recordlist[-1].saveTimeDep(self.prep_autosave_name())
        ##            if averaging_error_reduce.data.average !=[]:
        ##                arr = np.array([averaging_error_reduce.data.xlist,
        ##                averaging_error_reduce.data.average,
        ##                averaging_error_reduce.data.error])
        ##                autosave(arr)
        ##            if len(self.plot_xy_points):
        ##                autosave(self.plot_xy_points)

    def startstop(self):
        #check if scan was started by user
        prefer_time_than_raw = 0
        if self.sender() == self.btn_start:
            started_by_user = True
            prefer_time_than_raw += 1
        else:
            started_by_user = False
        if started_by_user and self.tabWidget.currentIndex()==1: #tame tab is active
            prefer_time_than_raw += 1
        #start
        if not self.rec_is_started:
            self.started.emit(self.start_mode)
            self.recording = True
            self.btn_start.setText('Stop')
            self.recordlist.append(Record(
                srcl,
                ARRAYDEFSIZE,
                encoding=options.get('Save.encoding', DEFAULT_ENCODING),
                prefer_time_than_raw = prefer_time_than_raw))
            if not self.btn_persistent.isChecked():
                self.clearAllPlots()
            self.tabManager.new()
            self.plot_xy_points = array([])

            #self.screen_upd_timer.start(self.screen_upd_timeout)
            self.timerActivate()
            self.rec_is_started = True

            #self.live = False
            #self.btn_live.setChecked(False)
            if self.autoswitchTab and self.xyplot:
                if self.xyplot[0] in self.tabManager.xyPlotManagersDict:
                    self.tabWidget.setCurrentWidget(self.tabManager.xyPlotManagersDict[self.xyplot[0]])

        #stop
        else:
            self.rec_is_started = False
            if SEND_CMD:
                self.started.emit('stop')
            self.btn_start.setText(start_modes[self.start_mode])
            #if not self.live:
            if self.tabWidget.currentIndex() != 0:
                self.screen_upd_timer.stop()
            self.recording = False
            self.recordlist[-1].sample = options.get('sample', "")
            self.recordlist[-1].comment = options.get('comment', "")
            #self.recordlist[-1].comment += " " + self.le_comment_once.text().trimmed()
            self.recordlist[-1].comment += " " + self.le_comment_once.text().strip()
            self.le_comment_once.clear()
            #add data from other sources as params
            self.params = {}
            for src,msg in  last_message_dict.items():
                try:
                    if type(msg) == tuple and len(msg)>1 and self.srcl[src].enabled and self.recordlist[-1].rec_position[src]<2:
                        name = self.srcl[src].name
                        for i,el in enumerate(msg):
                            chan_name = self.srcl[src].channels_names[i]
                            if chan_name.lower()!="time":
                                key = name+"."+chan_name
                                val = msg[i]
                                # if hasattr(self.srcl[src],"channels_units"):
                                #     units_name = self.srcl[src].channels_units[i]
                                #     val = val+" "+units_name
                                self.params[key] = val
                except:
                    traceback.print_exc()
            self.recordlist[-1].params = self.params

            #autosave xy_plot if any
            self.saveSpectrumAuto()

            if self.cb_save.isChecked():
                self.checkSaveDir()
                #save and prep new name
                self.saveSpectrum()
            #update list of samples, if any
            self.paramsDlg.sample_list.sample_saved(
                acquisition_time=self.recordlist[-1].time_elapsed,
                savenumber=options['SaveNumber'])

            #test() #debug

            self.params.clear()  #clear autoupdated params ONLY after save is done
            self.nameadd = ''

            if self.source_modified:
                self.source_modified = False
                self.set_inputs()

    def set_savefolder(self):
        self.lbl_save.setText(options['SaveDirectory'])

    def set_directory(self):
        if self.directory != os.getcwdu():
            if not os.path.exists(self.directory):
                os.makedirs(self.directory)
            os.chdir(self.directory)
            print('Current directory set to %s' % self.directory)
        ##        options['SaveDirectory']

    def prep_name(self):
        #autoparams = self.params
        #self.set_directory()
        d = self.save_dir + os.path.sep
        #options['SaveNumber']=1
        n = options.get('SavePattern', '%date%_%autovalues%_##')
        #options.get('SaveNumber',1)
        n = pattern2name(n, options)

        ext = options.get('SaveExtension', '')
        if ext == '':
            ext = DEFAULT_TEXT_FILE_EXTENSION
        if self.nameadd: n += '_{}'.format(self.nameadd)
        n += '.' + ext
        while os.path.exists(d + n):
            options['SaveNumber'] += 1
            n2 = pattern2name(options['SavePattern'], options)
            if self.nameadd: n2 += '_{}'.format(self.nameadd)
            n2 += '.' + ext
            if n2 == n:  #name doesnt include number, and doesn't change
                options['SavePattern'] += '_##'
                n = pattern2name(options['SavePattern'], options)
                if self.nameadd: n += '_{}'.format(self.nameadd)
                n += '.' + ext
            n = n2
        return n

    def prep_autosave_name(self):
        t = time.localtime()
        number = 1
        prefix = '%d%02d%02d_' % (t.tm_year, t.tm_mon, t.tm_mday)
        while os.path.exists(autosave_path + prefix + ('%03d' % number) + '.dat'):
            number += 1
        return autosave_path + prefix + ('%03d' % number) + '.dat'

    def enablesave(self):
        if self.cb_save.isChecked():
            options['SaveEnabled'] = True
        else:
            options['SaveEnabled'] = False

    def set_params(self, params):
        for key in params:
            options[key] = params[key]

        self._set_save_directory()

        self.lbl_pattern.setText(self.prep_name())
        options.savetodisk()

    def _set_save_directory(self):
        self.lbl_save.setText(options['SaveDirectory'])
        if not os.path.isabs(options['SaveDirectory']):
            progpath = os.path.dirname(sys.argv[0])
            drc = progpath + '\\' + options['SaveDirectory']
        else:
            drc = options['SaveDirectory']
        self.save_dir = drc  # self.directory contains abs path cur dir
        print("save directory set to ", self.save_dir)

    def query_params(self):
        for key in options:
            self.paramsDlg.params[key] = options[key]
        self.paramsDlg.show()

    def toggleXY(self):
        if self.btn_xy.isChecked():
            self.setupTabs()
            if self.xyplot:
                self.xyplot_enabled = True
                options['Plot.XY'] = True
            else:
                print('No x axis is provided by data sources')
                self.btn_xy.setChecked(False)
        else:
            self.xyplot_enabled = False
            options['Plot.XY'] = False
        self.set_mode()

    def toggleXYaccum(self):
        if self.btn_xy_accum.isChecked():
            self.xy_accum_enabled = True
            options['Plot.XYaccum'] = True
        else:
            self.xy_accum_enabled = False
            options['Plot.XYaccum'] = False
        self.set_mode()

    def set_mode(self):
        if self.rec_is_started:
            if self.xyplot_enabled:
                if self.xy_accum_enabled:
                    self.mode = MODE_XY_ACCUM
                else:
                    self.mode = MODE_XY
            else:
                self.mode = MODE_REGULAR
        else:
            if self.live:
                self.mode = MODE_LIVE
                #self.setlabel()

    def set_comment(self):
        options['comment'] = str(self.le_comment.text())
        options.savetodisk()

    def set_inputs(self):
        #print [src.enabled for src in srcl]
        if self.rec_is_started:
            self.source_modified = True
        else:
            self.setup_zmq_receivers()
            self.set_labels_4_raw_input()
            self.tabManager.inputModified()

    def query_inputs(self):
        self.inputDlg.show()
        self.inputDlg.raise_()
        QtWidgets.QApplication.instance().setActiveWindow(self.inputDlg)

    def _remove_labels(self):
        for lbl in self.lbl_input_list:
            self.lbl_input_list[-1]
            self.fr_rcv.layout().removeWidget(lbl)
            self.fr_rcv.setMinimumHeight(0)
            self.fr_rcv.setMaximumHeight(0)
            # self.lbl_Layout.removeWidget(lbl)

    def set_labels_4_raw_input(self):
        self._remove_labels()
        cnt = 0
        for src, lbl in zip(srcl, self.lbl_input_list):
            if src.enabled:
                cnt += 1
                self.fr_rcv.layout().addWidget(lbl)
                # print "label height",self.fr_rcv.minimumHeight(),lbl.geometry()
                # self.lbl_Layout.addWidget(lbl)

            lbl.setVisible(src.enabled)
        h = cnt * 40
        self.fr_rcv.setMinimumHeight(h)
        self.fr_rcv.setMaximumHeight(h)

    def _create_labels(self):
        for idx, SNDR in enumerate(srcl):
            if hasattr(SNDR, 'name'):
                lbl = '%s %s' % (SNDR.name, SNDR.address)
            else:
                lbl = '%s' % SNDR.address
            ql = QtWidgets.QLabel(lbl)
            self.lbl_input_list.append(ql)
            self.fr_rcv.layout().addWidget(self.lbl_input_list[-1])
            # self.lbl_Layout.addWidget(self.lbl_input_list[-1])
            ql.setVisible(SNDR.enabled)
        self.fr_rcv.setMinimumHeight(300)

    def setup_zmq_receivers(self):
        if self.nt:
            for thread in self.nt:
                try:
                    thread.updated.disconnect(self.update)
                    thread.stop()
                except:
                    print(traceback.print_exc())
            for thread in self.nt:
                thread.wait(1000)
                thread.terminate()
            self.nt = []
        for idx, sndr in enumerate(srcl):
            if sndr.enabled:
                try:
                    if sndr.type == "zmq_subscriber":
                        socket = context.socket(zmq.SUB)
                        socket.connect("tcp://%s" % sndr.address)
                        socket.setsockopt(zmq.SUBSCRIBE, b"")
                        self.nt.append(zmq_qt_recv(idx, socket))
                        self.nt[-1].updated.connect(self.update)
                    elif sndr.type == "plugin":
                        srcobj = sndr.pluginclasstype(idx,settings = options)
                        print("plugin enabled",srcobj)
                        self.nt.append(srcobj)
                        self.nt[-1].updated.connect(self.update)
                except:
                    traceback.print_exc()
        for nt1 in self.nt:
            nt1.start()

    def setRangeDisp(self, srcN, left, right):
        #print "setting display range for src",srcN,left,right
        print([it.getRange() for it in self.tabManager.xyPlotManagersDict.values() if hasattr(it, "rangeLeft")])
        self.tabManager.xyPlotManagersDict[srcN].setRange(left, right)
        print([it.getRange() for it in self.tabManager.xyPlotManagersDict.values() if hasattr(it, "rangeLeft")])
    # def onRangeChange(self,arg):
    #     for i in enumerate(srcl)




#prepare autosave directory
autosave_path = os.path.dirname(os.path.abspath(PROGRAM_PATH)) + os.sep + 'autosave' + os.sep
if not os.path.exists(autosave_path):
    os.mkdir(autosave_path)
    print("Autosave directory was created: {}".format(autosave_path))
else:
    print("Autosaves will be made in {}".format(autosave_path))


def clear_autosave_folder(autosave_path):
    if os.path.exists(autosave_path):
        lst = []
        for f in os.listdir(autosave_path):
            if f.endswith('.dat'):
                lst.append(f)
        max = options.get('MaxAutosaveCount', 1000)
        if len(lst) > max:
            lst.sort()
            for f in lst[0:len(lst) - max]:
                os.remove(autosave_path + os.sep + f)


def savendarray(data, filename, dimension=1, headerrecord=None):  #save ndarray to text file
    if len(data.shape) > 2:
        print('cannot save array with more then 2 dimensions')
    f = open(filename, 'w')
    if headerrecord:
        headerrecord.writeheader(f)
    if dimension == 1:
        for i in range(0, data.shape[dimension]):
            f.write('%g' % data[0][i])
            for k in range(1, data.shape[0]):
                f.write('\t%g' % data[k][i])
            f.write('\n')
    elif dimension == 0:
        for i in range(0, data.shape[dimension]):
            f.write('%g' % data[i, 0])
            for k in range(1, data.shape[1]):
                f.write('\t%g' % data[i][k])
            f.write('\n')
    f.close()


def autosave(data):  #save data to autosave folder with autonaming
    try:
        t = time.localtime()
        number = 1
        prefix = '%d%02d%02d_' % (t.tm_year, t.tm_mon, t.tm_mday)
        while os.path.exists(autosave_path + prefix + ('%03d' % number) + '.dat'):
            number += 1
        savendarray(data, autosave_path + prefix + ('%03d' % number) + '.dat')
    except:
        print(sys.exc_info())


app = QtWidgets.QApplication(sys.argv)
# title  = 'Receiving from %s' % ADDRESS
win = mainwnd()
win.setWindowTitle('Plot Utility v. ' + __version__)
win.show()
win.started.connect(send_cmd)
con = console_qt.QConsole()
opted = options_editor.Options_Editor(options)

QMenuFromDict.AddMenu(win.menubar,{"tools":{"console":con.show,
                                            "settings":opted.show},
                                   "view":{"reset docks":win.resetDocks}
                                    })


# l = pg.GraphicsLayout()
# #win.setCentralItem(l)
# p6 = l.addPlot()
# l.nextRow()
# lbl = l.addLabel()
# text4lbl = ''
#
# curve = p6.plot(pen='y')
# data = np.random.normal(size=(10, 1000))
# data = np.array([])
# ptr = 0


# def test():
#     f = open(SAMPLE_LIST_FILE_NAME, 'r')
#     l = f.readlines()
#     print len(l)
#     f.close()
#     f = open(SAMPLE_LIST_FILE_NAME, 'a')
#     print f
#     f.write('aaaaaaaaaaaaaaaa!')
#     f.flush()
#     f.close()


def mark(): pass


# if __name__ == '__main__':
print('current working directory is {}'.format(os.getcwd()))
#nt.run()
##    nt.start()
##    for nt1 in nt:
##        nt1.start()
##    from zmq_client_double2 import t1,t2
##    t1.updated.connect(win.update)
##    t2.updated.connect(win.update)

QtWidgets.QApplication.instance().exec_()
##    nt.stop()
##    for nt1 in nt:
##        nt1.stop()
clear_autosave_folder(autosave_path)
# options.savetodisk()
sys.exit(0)
