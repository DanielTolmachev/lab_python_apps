from qtpy import QtCore

__author__ = 'C7LabPC'


class Instr_Plugin_Base(QtCore.QThread):
    updated = QtCore.Signal(tuple)
    _id = -1
    _running = True
    __instruments_info__ = {}
    def __init__(self,ID,settings = {}):
        super(Instr_Plugin_Base,self).__init__()
        self._id = ID
        self.name = self.__instrument_info__.get("name","")
        self.settings = settings
    def loadattr(self,attrname,defaults):
        setsname = self.name+"."+ attrname
        if setsname in self.settings:
            setattr(self,attrname,self.settings[setsname])
        else:
            setattr(self,attrname,defaults)
            self.settings[setsname] = defaults
    def stop(self):
        self._running = False