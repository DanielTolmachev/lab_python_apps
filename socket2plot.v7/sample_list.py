#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     31.03.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
import sys,time,traceback
class SampleInfo(dict):
    pass

keylist = ['SampleFull',
'SampleShort',
'SaveDirectory',
'SavePattern',
'SaveNum',
'SeenFirst',
'SeenLast',
'TotalFiles',
'TotalTime']
class SampleList(list):
    def __init__(self,filename):
        super(SampleList,self).__init__()
        self._load(filename)
        self._update_sample_list()
        self.filename = filename
    def _load(self,filename):
        try:
            print("Loading sample info from {}".format(filename))
            f = open(filename)
            lns = f.readlines()
            f.close()
            for l in lns:
                if not l.strip().startswith('#'):
                    vals = l.split('\t')
                    vals[-1]=vals[-1].strip() #strip \n from last value
                    self.append({ key:value for key,value in zip(keylist,vals)})
                    # print { key:value for key,value in zip(keylist,vals)}
        except:
            print(sys.exc_info())
            return


    def save(self):
        try:
            print("saving sample info to {}".format(self.filename))
            f = open(self.filename,'w')
            f.write('#'+'\t'.join(keylist)+'\n')
            for sample in self:
                s = u'\t'.join([str(sample.get(key,'')) for key in keylist])
                #s = s.encode('utf-8')+b'\n'
                s += "\n"
                #print s #print each sample info
                f.write(s)
            f.close()
        except:
            print('Error while saving sample list')
            print(traceback.print_exc())
##    def insert(self,samp):
##        i=0
##        sname = samp['SampleFull']
##        while i < len(self):
##            if self['SampleFull']>sample_name:
##                break
##            i+=1
##        super(list).insert(i,samp)
    def issorted(self):
        for i in range(0,len(self)-1):
            if self[i]>=self[i+1]:
                return False
        return True
    def set(self,params):
        #print params
        if 'sample' in params or 'SampleFull' in params:
            samp = dict.fromkeys(keylist,'')
            print(samp)
            for key in keylist:
                if key in params:
                    samp[key] = params[key]
            #samp.update(params)
            # if not samp['SampleFull'] and params.has_key('sample'):
            #     samp['SampleFull'] = params['sample']
            if params.get('sample',"")!=params.get("SampleFull"):
                samp['SampleFull'] = params['sample'] #"sample" is more important then "SampleFull"
            #samp['SampleShort'] = params.get['s']
##            samp['SavePattern'] = params.get('SavePattern','')
##            samp['SaveExtension'] =
            #print samp
            idx,idin = self.find(samp['SampleFull']) #We are looking by "SampleFull"
            if idx>-1:
                print('sample info was updated')
                self[idx] = samp
                print(samp)
            else:
                t = time.localtime()
                samp['SeenFirst'] = time.strftime('%d.%m.%Y %H.%M.%S',t)
                print('new sample added {} {}'.format(
                    samp['SampleFull'],samp['SeenFirst']))
                self.insert(idin,samp)
                self.save()
            self.current = idin
            self._update_sample_list()
    def find(self,sample_name):
        idins = 0
        for idx,el in enumerate(self):
            if el['SampleFull']==sample_name:
                return idx,idx
            elif el['SampleFull']<sample_name:
                idins +=1
        return -1,idins
    def sample_saved(self,
                    acquisition_time = None,
                    savenumber = None    ):
        try:
            if acquisition_time:
                if self[self.current]['TotalTime']:
                    acquisition_time = acquisition_time+\
                         int(self[self.current]['TotalTime'])
                    self[self.current]['TotalTime'] = str(acquisition_time)
                else:
                    self[self.current]['TotalTime'] = acquisition_time
            if savenumber:
                self[self.current]['SaveNum'] = savenumber
            if self[self.current]['TotalFiles']:
                tf = int(self[self.current]['TotalFiles'])+1
                self[self.current]['TotalFiles'] = tf
            else:
                self[self.current]['TotalFiles'] = 1
            t = time.localtime()
            self[self.current]['SeenLast'] = time.strftime('%d.%m.%Y %H.%M.%S',t)
            self._update_sample_list()
            self.save()
        except:
            print('error in SampleList.sample_saved')
            print(sys.exc_info())
    def get_sample_list(self):
        return [s['SampleFull'] for s in self]
    def _update_sample_list(self):
        self.sample_list = [s['SampleFull'] for s in self]


if __name__ == '__main__':
    sl = SampleList('test_sample_list.txt')
    if len(sl)>0:
        sl.set(sl[0])
        sl.sample_saved(acquisition_time = 10)
        sl.sample_saved(acquisition_time = 11)
        sl.sample_saved(acquisition_time = 10)
