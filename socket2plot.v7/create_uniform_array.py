from __future__ import print_function
import sys
from numpy import diff,sort,floor,log10,arange,zeros,nan

__author__ = 'C7LabPC'


def create_uniform_array(x1, size):
     if len(x1) > 1:
         xmin = x1.min()
         xmax = x1.max()
         #global x,y,res
         #x = x1
         m = abs(diff(sort(x1))).mean()
         order = 10 ** floor(log10(m))
         dx = m / order
         if dx >= 7.5:
             dx = 10
         elif dx >= 3:
             dx = 5
         elif dx >= 1.5:
             dx = 2
         else:
             dx = 1
         dx = dx * order
         print('dx = ', dx, ' mean = ', m)
         try:
             x2 = arange(xmin, xmax + dx, dx)
         except ValueError:
             print(sys.exc_info())
             print(xmin, xmax, dx)
     else:
         x2 = x1
     y = zeros((len(x2), size + 1))
     y[:, 0] = x2
     for i in range(1, size, 5):
         y[:, i] = nan
         #print 'array increased %d' % len(x2)
     ##    if len(x2)==0 or not y:
     ##        pass
     ##    if y == None:
     ##        pass
     print("created array", y.shape, xmin, " - ", xmax, "(", len(x2), "points)")
     return y