# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'test_source_gui.ui'
#
# Created: Sun Jan 03 23:20:42 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(190, 293)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.gridLayout.addWidget(self.checkBox, 1, 0, 1, 1)
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.rb_stop = QtWidgets.QRadioButton(self.groupBox)
        self.rb_stop.setObjectName(_fromUtf8("rb_stop"))
        self.verticalLayout.addWidget(self.rb_stop)
        self.rb_wl = QtWidgets.QRadioButton(self.groupBox)
        self.rb_wl.setObjectName(_fromUtf8("rb_wl"))
        self.verticalLayout.addWidget(self.rb_wl)
        self.rb_freq = QtWidgets.QRadioButton(self.groupBox)
        self.rb_freq.setObjectName(_fromUtf8("rb_freq"))
        self.verticalLayout.addWidget(self.rb_freq)
        self.rb_field = QtWidgets.QRadioButton(self.groupBox)
        self.rb_field.setObjectName(_fromUtf8("rb_field"))
        self.verticalLayout.addWidget(self.rb_field)
        self.gridLayout.addWidget(self.groupBox, 0, 0, 1, 1)
        self.btn_sendrange = QtWidgets.QToolButton(self.centralwidget)
        self.btn_sendrange.setObjectName(_fromUtf8("btn_sendrange"))
        self.gridLayout.addWidget(self.btn_sendrange, 2, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 190, 31))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "TestSignal", None))
        self.checkBox.setText(_translate("MainWindow", "send \"start\"", None))
        self.groupBox.setTitle(_translate("MainWindow", "GroupBox", None))
        self.rb_stop.setText(_translate("MainWindow", "Stop", None))
        self.rb_wl.setText(_translate("MainWindow", "Wavelength", None))
        self.rb_freq.setText(_translate("MainWindow", "Frequency", None))
        self.rb_field.setText(_translate("MainWindow", "Magnetic field", None))
        self.btn_sendrange.setText(_translate("MainWindow", "send range", None))

