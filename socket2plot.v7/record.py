#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Copyright:   (c) Daniel 2015
#-------------------------------------------------------------------------------
from __future__ import print_function
from numpy import arange,zeros,interp,ceil,hstack,sqrt
import sys, time, traceback,codecs
import si_units

THRESHOLD_TOO_LESS_X_POINTS = 100 #number of x points is this time less then number of y point
THRESHOLD_TOO_LESS_TIMERANGE_SEC = 10 #s, one channel can be started this amount of sec after another
THRESHOLD_TOO_LESS_TIMERANGE = 0.8 #times, channels can be started asyncronously

ERR_CANNOT_SAVE_XY_PLOT = None

class Record(object):

    def __init__(self,sources_list,arraydefsize,
                encoding = 'utf-8',
                encoding_errors = 'backslashreplace',
                 prefer_time_than_raw = 0):
        self.arraydefsize = arraydefsize
        self.list_of_arrays= []
        self.list_of_params_arrays = []
        self.rec_position = []
        self.times = None
        self.timestart = None
        self.timerelative = False
        self.time_elapsed = None
        self.comment = None
        self.sample = None
        self.params = None

        self.exclude_start = 0
        self.exclude_end = 0
        self.xyplot = None
        self.sources_list = sources_list
        self.encoding = encoding
        self.encoding_errors = encoding_errors
        self.prefer_time_than_raw = prefer_time_than_raw
        self._fileName = None
        if hasattr(sources_list,'timerelative'):
            self.timerelative = sources_list.timerelative
        for i,src in enumerate(sources_list):
            #find first source that has x asxis and is not disabled
            print(src.name)
            if hasattr(src,'xaxis') and src.enabled:
                print(src.xaxis)
                if src.xaxis:
                    self.xyplot = (i,src.xaxis)
                    print('X axis is chosen {} chan #{}'.format(src.name,src.xaxis))
                    break
            if not self.xyplot:
                print('X axis was not set')
        if hasattr(self.sources_list,'timeconstant'):
            self.timeconstant = sources_list.timeconstant
        for src in sources_list:
            self.list_of_arrays.append(zeros((len(src.channels2rec)+1,self.arraydefsize)))
            self.list_of_params_arrays.append([[0,0,0,None] for chan in src.channels2params])
            self.rec_position.append(0)
        #times = np.array([i*TIMECONSTANT for i in xrange(0,self.arraydefsize)])
##    def expand(self,arr):
##        if arr.ndim==2:
##            arr_len = arr.shape[1]
##            old = arr
##            arr = np.zeros((arr.shape[0],arr_len+self.arraydefsize))
##            arr[:,0:arr_len] = old[:,:]
##        return arr
    def add(self,src,data):
        #src = data[0]
        pos = self.rec_position[src]
        chan2rec = self.sources_list[src].channels2rec
        timechan = self.sources_list[src].timechannel
        chan2param = self.sources_list[src].channels2params
        #if pos==0 and not self.timestart:
        t = time.time()
        #set timestart by first point of chanel, that provided data last
        if pos==0:
            if not self.timestart:
                self.timestart = t
        #     if t>self.timestart:
        #         print 'Rec started after{} s after start'.format(t-self.timestart)
        #         self.timestart = t


        #expand array if needed
        arr_len = self.list_of_arrays[src].shape[1]
        if self.rec_position[src]>= arr_len:
            old = self.list_of_arrays[src]
            self.list_of_arrays[src] = (zeros(((len(self.sources_list[src].channels2rec)+1,arr_len+self.arraydefsize))))
            self.list_of_arrays[src][:,0:arr_len] = old[:,:]
            print('got %d samples from %s' % (self.rec_position[src],self.sources_list[src].address))
        #add time data
        if timechan>=0:
                timestamp = data[timechan+1]
        else:
                timestamp = time.time()
        #skip points if needed
        if timestamp>self.exclude_start and\
        (timestamp<self.exclude_end or self.exclude_end<self.exclude_start):
            return
        #set relative time if needed
        if self.timerelative:
            self.list_of_arrays[src][0,pos] = timestamp-self.timestart
        else:
            self.list_of_arrays[src][0,pos] = timestamp
        #add other channels
        for idx,chan in enumerate(chan2rec):
            self.list_of_arrays[src][idx+1,pos] = data[chan+1]
            #averaging_error_reduce.data.add(src,chan+1,data[chan+1]) #!
        #idx+1 because in time is recorded to 0th column
        #chan+1 because data[0] is source number
        cur_time = self.list_of_arrays[src][0,-1]
        #expand combined arra if needed
##        if cur_time-self.timestart>=self.times[-1]:

        self.rec_position[src] +=1
        for idx,chan in enumerate(chan2param):
            #print(self.sources_list[src].name,data[chan+1])
            self._addAverage(self.list_of_params_arrays[src][idx],data[chan+1])
    def _addAverage(self,lst, data):
        lst[0] += data
        if lst[3]:
            lst[1] += (data-lst[3])**2
        lst[2] += 1
        lst[3] = data
##    def calc_time_const(self):
##        tc = 0
##        for i,arr in enumerate(list_of_arrays):
##            mn = arr.diff.mean()
##            if tc<mn:
##                tc = mn
##        self.tc = tc
##    def combine(self,timeconstant = 0.1):
##        cur_time = list_of_arrays[0][0,self.rec_position[0]]
##        if self.times==None:
##            times = np.array([i*timeconstant for i in xrange(0,1024)])
##        elif cur_time>times[-1]:
    def get(self,src,chan):
        return self.list_of_arrays[src][chan,0:self.rec_position[src]]
    def saverawdata(self, filename):
        print('saving %s (raw data)' % filename, end=' ')
        f = codecs.open(filename, "w", "utf-8")
        self.writeheader(f)
        f.write('raw data\n')
        for i, src in enumerate(self.sources_list):
            if src.enabled:
                if hasattr(src, 'name'):
                    f.write('#' + src.name + '\n')
                if hasattr(src, 'channels_names'):
                    if src.timechannel >= 0:
                        f.write('Time')
                    for k, chan in enumerate(src.channels2rec):
                        if k < len(src.channels_names):
                            f.write('\t%s' % src.channels_names[chan])
                f.write('\n')
                if hasattr(src, 'channels_units'):
                    if src.timechannel >= 0:
                        f.write('s')
                    for k, chan in enumerate(src.channels2rec):
                        if k < len(src.channels_units):
                            f.write('\t%s' % src.channels_units[chan])
                f.write('\n')
                for j in range(0, self.rec_position[i]):
                    if src.timechannel >= 0:
                        f.write('%g' %
                                self.list_of_arrays[i][src.timechannel][j])
                    for k, chan in enumerate(src.channels2rec):
                        f.write('\t{}'.format(self.list_of_arrays[i][chan][j]))
                    f.write('\n')
                f.write('\n')
                ##
                ####            for i in enumerate(self.sources_list):
                ####                f.write('%g'% data[0][i])
                ####                for k in range(1,data.shape[0]):
                ####                    f.write('\t%g'% data[k][i])
                ####                f.write('\n')
        f.close()
        print('done')
        if not self._fileName:
            self._fileName = filename
    def saveXYplot(self, filename):
        print('saving %s (xy plot)' % filename, end=' ')
        may_be_problem = 0
        src = self.xyplot[0]
        #print self.xyplot
        source = self.sources_list[src]
        chan = source.channels2rec.index(self.xyplot[1]) + 1
        st = self.timestart
        ct = max([arr[0, self.rec_position[i] - 1] for i, arr in enumerate(self.list_of_arrays)])
        if self.timerelative:
            time_range = arange(0, ct, self.timeconstant)
            time_range_len = ct
        else:
            time_range = arange(st, ct, self.timeconstant)
            time_range_len = ct-st
        xunits = ""
        if hasattr(self.sources_list[src], 'channels_names'):
            xtitle = self.sources_list[src].channels_names[chan]
            if hasattr(self.sources_list[src],"channels_units"):
                xunits =  self.sources_list[src].channels_units[chan]
        else:
            xtitle = ''
        # print "src ",src,"xtitle ",xtitle
        #global x,ylist1
        xdata = self.get(src, chan)
        if not any(xdata):
            return ERR_CANNOT_SAVE_XY_PLOT
        nxpoints = len(xdata)
        t0 = self.get(src, 0)
        xtspan = t0.max() - t0.min()
        if xtspan<time_range_len*THRESHOLD_TOO_LESS_TIMERANGE or \
            (xtspan-time_range_len)>THRESHOLD_TOO_LESS_TIMERANGE_SEC:
            may_be_problem+=1
        x = interp(time_range,  #new time points
                      t0,  #old time points
                      xdata)  #xdata
        ylist = []
        ynameslist = []
        yunitslist = []
        cnt = cnt_used = 1
        for i, src in enumerate(self.sources_list):
            if src.enabled:
                # if i == self.xyplot[0]:
                #     print "skipping X channel ", src, self.xyplot[1], src.channels2show
                for k, chan in enumerate(src.channels2show):
                    if i != self.xyplot[0] or chan != self.xyplot[1]:
                        cnt += 1
                        colunits = ""
                        if hasattr(src, 'channels_names'):
                            colname = src.channels_names[chan]
                            if hasattr(src,"channels_units"):
                                colunits =  src.channels_units[chan]
                        else:
                            colname = ''
                        try:
                            t0 = self.get(i, 0)
                            y0 = self.get(i, k + 1)
                            # tspan = t0.max()-t0.min()
                            if len(y0)>nxpoints*THRESHOLD_TOO_LESS_X_POINTS:
                                may_be_problem +=1
                            y = interp(time_range,  #new time points
                                          t0,  #old time points
                                          y0)  #ydata
                            ynameslist.append(colname)
                            yunitslist.append(colunits)
                            ylist.append(y)
                            cnt_used += 1
                        except ValueError as exc:
                            # if exc.message == 'array of sample points is empty':
                            #     pass
                            # else:
                                print(sys.exc_info())
                        except:
                            traceback.print_exc()
        print(cnt_used, "from", cnt, "columns saved")
        f = codecs.open(filename, "w", "utf-8")
        self.writeheader(f)
        if any(ynameslist):
            f.write(xtitle + '\t' + '\t'.join(ynameslist) + '\n')
            if any(yunitslist) and xunits:
                f.write(xunits + '\t' + '\t'.join(yunitslist) + '\n')
        for i in range(0, len(x)):
            f.write('%g' % x[i])
            for y in ylist:
                f.write('\t%g' % y[i])
            f.write('\n')
        f.close()
        print('done')
        self._fileName = filename
        return may_be_problem
    def saveTimeDep(self,filename):
        print('saving %s (timeseries)...' % (filename), end=' ')
        #src = self.xyplot[0]
        #print self.xyplot
        #chan = self.sources_list[src].channels2rec.index(self.xyplot[1]) + 1
        st = self.timestart
        ct = max([arr[0, self.rec_position[i] - 1] for i, arr in enumerate(self.list_of_arrays)])
        if self.timerelative:
            time_range = arange(0, ct, self.timeconstant)
        else:
            time_range = arange(st, ct, self.timeconstant)
        # if hasattr(self.sources_list[src], 'channels_names'):
        #     xtitle = self.sources_list[src].channels_names[chan]
        # else:
        #     xtitle = ''
        # print "src ",src,"xtitle ",xtitle
        #global x,ylist1
        # xdata = self.get(src, chan)
        # if not any(xdata):
        #     return 1
        # x = np.interp(time_range,  #new time points
        #               self.get(src, 0),  #old time points
        #               xdata)  #xdata
        ylist = []
        ynameslist = []
        yunitslist = []
        precisions = []
        cnt = cnt_used = 1
        for i, src in enumerate(self.sources_list):
            if src.enabled:
                # if i == self.xyplot[0]:
                #     print "skipping X channel ", src, self.xyplot[1], src.channels2show
                if hasattr(src,"precision"):
                    prec = src.precision
                else:
                    prec = 5
                for k, chan in enumerate(src.channels2rec):
                    # if i != self.xyplot[0] or chan != self.xyplot[1]:
                        cnt += 1
                        colunits = ""
                        if hasattr(src, 'channels_names'):
                            colname = src.channels_names[chan]
                            if hasattr(src,"channels_units"):
                                colunits =  src.channels_units[chan]
                        else:
                            colname = ''
                        try:
                            y = interp(time_range,  #new time points
                                          self.get(i, 0),  #old time points
                                          self.get(i, k + 1))  #ydata
                            ynameslist.append(colname)
                            yunitslist.append(colunits)
                            ylist.append(y)
                            precisions.append(prec)
                            cnt_used += 1
                        except ValueError as exc:
                            if exc.args and exc.args[0] == 'array of sample points is empty':
                                pass
                            else:
                                traceback.print_exc()
        print(cnt_used, "from", cnt, "columns saved")
        #f = open(filename, 'w')
        f = codecs.open(filename,"w","utf-8")
        self.writeheader(f)
        if any(ynameslist):
            f.write('time' + '\t' + '\t'.join(ynameslist) + '\n')
            if any(yunitslist):
                f.write("s" + '\t' + '\t'.join(yunitslist) + '\n')
        for i in range(0, len(time_range)):
            f.write('%g' % time_range[i])
            for y,prec in zip(ylist,precisions):
                # f.write('\t%g' % y[i])
                f.write('\t{:.{}g}'.format(y[i],prec))
            f.write('\n')
        f.close()
        print("done")
        self._fileName = filename
    def save(self,filename,savexyplot = True):
        if savexyplot and not self.xyplot:
            print('Error: record.xyplot is not set')
            print('''xyplot should be tuple  (source_number,channel_number)
             ''')
        # print 'saving %s' % filename,
        try:
            if self.xyplot and savexyplot:
                # print '  (xyplot)',
                r = self.saveXYplot(filename)
                if r!=0:
                    if self.prefer_time_than_raw==0:
                        if r == None:
                            print('  cannot save\n saving raw data instead...', end=' ')
                            self.saverawdata(filename)
                        else:
                            print('possible problem while saving (', r,')\n saving also raw data...', end=' ')
                            self.saverawdata(self.modifyFileName(filename,"raw"))
                    elif self.prefer_time_than_raw==2:
                        if r == None:
                            print('  cannot save\n saving time series instead...', end=' ')
                            self.saveTimeDep(filename)
                        else:
                            print('possible problem while saving (', r, ')\n saving also time series...', end=' ')
                            self.saveTimeDep(self.modifyFileName(filename,"t"))
            else:
                # print '  (raw data)...',
                self.saverawdata(filename)
                #self.saverawdata(filename)
    ##
            # print 'done'
        except:
            print('\n',traceback.print_exc())
    def encode(self,s):
        if type(s)==str:
            return s.encode(self.encoding,errors = self.encoding_errors)
        else:
            return s
    def modifyFileName(self,filename, modstring = ""):
        i = filename.rfind(".")
        if i:
            return filename[:i]+modstring+filename[i:]
    def writeheader(self,f):
        t = time.localtime(self.timestart)
        f.write(time.strftime('# %d.%m.%Y %H:%M:%S\n',t))
        if self.sample:
            #f.write('# Sample = \"{0}\"\n'.format(self.encode(self.sample)))
            f.write('# Sample = \"{0}\"\n'.format(self.sample))
        if not self.time_elapsed:
            stoptime = max(\
            [self.list_of_arrays[src][0,self.rec_position[src]-1]\
             for src,_ in enumerate(self.sources_list)])
            if not self.timerelative:
                stoptime = stoptime-self.timestart
            self.time_elapsed = int(ceil(stoptime))
        f.write('# Aquisition time = {0}\n'.format(self.time_elapsed))
        if self.comment:
            #f.write('# Comment = \"{0}\"\n'.format(self.encode(self.comment)))
            f.write('# Comment = \"{0}\"\n'.format(self.comment))
        for i,src in enumerate(self.sources_list):
            # print(src.name,self.list_of_params_arrays[i])
            for j,c in enumerate(src.channels2params):
                data = self.list_of_params_arrays[i][j]
                if data[2]>0: #we have received at least one value
                    if data[2]>1: #find average and error if we'd received more than one value
                        av = data[0]/data[2]
                        err = sqrt(data[1]/data[2])
                    else:
                        av = data[0]
                        err = 0
                    if hasattr(src,"channels_names") and len(src.channels_names)>c:
                        key = src.channels_names[c]
                    elif len(self.list_of_params_arrays[i])==1:
                        key = src.name
                    else:
                        key = src.name+str(i)
                    if hasattr(src,"channels_units") and len(src.channels_units)>c:
                        units = src.channels_units[c]
                    else:
                        units = ""
                    if units:
                        if err!=0:
                            val,un = si_units.si_fmt(av,units)
                            valerr = val/av*err
                            self.params[key] = "{:f} ± {:f} {}".format(val,valerr,un)
                        else:
                            self.params[key] = si_units.si_str(av,units).strip()
                    else:
                        self.params[key] = "{:f}".format(av)
                        if err != 0:
                            self.params[key] += " ± {:f}".format(err)


        if self.params:
            for k,v in self.params.items():
                if isinstance(v,float):
                    v = "{:g}".format(v)
                #f.write('# {} = {}\n'.format(k,self.encode(v)))
                f.write('# {} = {}\n'.format(k,v))


        f.write('\n')
    def fileName(self):
            return self._fileName

class RecordLive(Record):
    def __init__(self,*args,**kwargs):
        super(RecordLive,self).__init__(*args,**kwargs)
        self.args = args
        self.kwargs = kwargs
        self.rec_is_empty = []
        self.empty = True
        self.looped = False
    def rebuild(self):
        super(RecordLive,self).__init__(*self.args,**self.kwargs)
        self.empty = False
        for _ in self.rec_position:
            self.rec_is_empty.append(True)
    def add(self,src,data):
        if self.empty:
            'RecordLive is empty, rebuilding...'
            self.rebuild()
            return
        try:
            pos = self.rec_position[src]
            chan2rec = self.sources_list[src].channels2rec
            timechan = self.sources_list[src].timechannel
            if pos==0:
                t = time.time()
                self.timestart = t
                self.rec_is_empty[src] = False

            #append data to end of array
            arr_len = self.list_of_arrays[src].shape[1]
            if pos>=arr_len: #shift data to begin of array so it doesnt grow
                    pos=0
                    self.looped = True
            #add time data
            if timechan>=0:
                    timestamp = data[timechan+1]-self.timestart
            else:
                    timestamp = time.time()-self.timestart
            #skip points if needed #not needed in live mode
    ##        if timestamp>self.exclude_start and\
    ##        (timestamp<self.exclude_end or self.exclude_end<self.exclude_start):
    ##            return
            #set relative time if needed
    ##        if self.timerelative:
    ##            self.list_of_arrays[src][0,pos] = timestamp-self.timestart
    ##        else:
            # if self.looped:
            #     self.list_of_arrays[src][0, 0:-2] = self.list_of_arrays[src][0, 1:-1]
            #     self.list_of_arrays[src][0, pos] = timestamp
            # else:
            self.list_of_arrays[src][0,pos] = timestamp


            #add other channels
            for idx,chan in enumerate(chan2rec):
                if len(data)>chan+1:
                    self.list_of_arrays[src][idx+1,pos] = data[chan+1]
    ##        cur_time = self.list_of_arrays[src][0,-1]
            self.rec_position[src] = pos + 1
        except TypeError:
            print("record.add: incorrect data")
        except:
            traceback.print_exc()
    def get(self,src,chan):
        if self.rec_position:
            a = self.list_of_arrays[src][chan,:self.rec_position[src]]
            if self.looped and not self.rec_is_empty[src]:
                b = self.list_of_arrays[src][chan,self.rec_position[src]:]
                return hstack((b,a))
            return a
        else:
            #print 'dbg:no data received yet'
            return None
    def clear(self):
        if not self.empty:
            self.looped = False
            for i,src in enumerate(self.sources_list):
            #self.list_of_arrays[i][:,:] = 0
                self.rec_position[i] = 0
