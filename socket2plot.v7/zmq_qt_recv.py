import zmq,pickle
import traceback
from qtpy import QtCore



class zmq_qt_recv(QtCore.QThread):
    updated = QtCore.Signal(tuple)
    _id = -1
    _running = True
    def __init__(self,ID,socket):
        self._id = ID
        super(zmq_qt_recv,self).__init__()
        self.socket = socket
    def run(self):
        while self._running:
            try:
                po = self.socket.recv_pyobj()
                #print (self._id,)+po
                #signal sends tuple so we send tuple
                if type(po) == tuple:
                    self.updated.emit((self._id,)+po)
                else: #but we'll let main thread to handle all types of data
                    self.updated.emit((self._id,po))
            except pickle.UnpicklingError:
                self.updated.emit((self._id, "Unpickling error"))
            except:
                traceback.print_exc()
    def stop(self):
        self._running = False

if __name__ == '__main__':
    context = zmq.Context()
    socket1 = context.socket(zmq.SUB)
    socket1.connect("tcp://127.0.0.1:5000")
    socket1.setsockopt(zmq.SUBSCRIBE, b"")

    socket2 = context.socket(zmq.SUB)
    socket2.connect("tcp://127.0.0.1:5001")
    socket2.setsockopt(zmq.SUBSCRIBE, b"")
    t1= zmq_qt_recv(0,socket1)
    t2= zmq_qt_recv(1,socket2)
    t1.start()
    t2.start()