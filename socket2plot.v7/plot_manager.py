from __future__ import print_function
__author__ = 'user'
import pyqtgraph as pg
from pyqtgraph.dockarea import *
import traceback,time
from RangeDisplay import RangeDisplay
class myDock(Dock):
    def __init__(self,*args,**kwargs):
        super(myDock,self).__init__(*args,**kwargs)
    # def resizeEvent(self, ev):
    #     print ev
        #super(myDock,self).resizeEvent(ev)
class PlotManager(DockArea):
    sigMouseMoved = pg.QtCore.pyqtSignal(float,float)
    rangeChanged = pg.QtCore.pyqtSignal(int,tuple)

    def __init__(self,*args,**kwargs):
        super(PlotManager,self).__init__(*args,**kwargs)
        self.mode = None
        self.mouseMovedPrev=0
        self.rangedisplay = None
        self.rangeVisible = True
        self.lmdl = []  # livemodedocklist
        self.lmpl = []
        self.state = [None, None, None]
        self.dock_dict_list = [[],[],[]]
        self.plot_dict_list = [[],[],[]]
        #self.setStyleSheet('background-color:red')
        self.rangeMovable = False
        #self.rangeChanged = pg.QtCore.pyqtSignal(int,tuple)
    def createDockLive(self,title):
        d = None
        no_dock = True
        for d,p in zip(self.lmdl,self.lmpl):
            if d.name()== title:
                #print 'Dock {} is already created'.format(title)
                #for i in p.items(): #clear plot if needed
                        #p.removeItem(i)
                no_dock = False
                break
        if no_dock:
            d = myDock(title,closable=True, size=(10,10))
            p = pg.PlotWidget()
            self.lmdl.append(d)
            self.lmpl.append(p)
            print('Dock {} created'.format(title))
            d.addWidget(p)
            pi = p.getPlotItem()
            pi.scene().sigMouseMoved.connect(self.mouseMoved)
            #self.proxy = pg.SignalProxy(self.pi.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
            self.addDock(d)
        return d,p
    def createDock(self,title):
        if self.mode==None:
            print("createDock: need to set mode first")
            return
        d = None
        no_dock = True
        for d,p in zip(self.dock_dict_list[self.mode],self.plot_dict_list[self.mode]):
            if d.name()==title:
                print('Dock {} is already created'.format(title))
                # d.show()
                # d = Dock
                d.showTitleBar()
                #for i in p.items(): #clear plot if needed
                        #p.removeItem(i)
                no_dock = False
                break
        if no_dock:
            d = myDock(title,closable=True, size=(10,10))
            p = pg.PlotWidget()
            self.dock_dict_list[self.mode].append(d)
            self.plot_dict_list[self.mode].append(p)
            if self.rangedisplay:
                self.rangedisplay.addPlot(p)
            print('Dock {} created'.format(title))
            d.addWidget(p)
            pi = p.getPlotItem()
            pi.scene().sigMouseMoved.connect(self.mouseMoved)
            #self.proxy = pg.SignalProxy(self.pi.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
            self.addDock(d)
        return d,p
    def saveState(self,mode):
        #print "self.mode",self.mode,mode
        if self.mode!=mode:
            if len(self.docks): #prevent saving state of empty area
                self.state[self.mode] = DockArea.saveState(self)
                pass
        self.mode = mode
    def loadState(self,mode):
        if self.mode!=mode:
            # try:
                print("PlotManager: restoring dock area state")
                self.restoreState(self.state[self.mode])
            # except:
            #     traceback.print_exc()
        self.mode = mode
    def setMode(self,mode):
        if self.mode!=mode:
            self.saveState(self.mode)
            #self.removeAllDocks()

        self.mode = mode
        #self._hideDocks()
    def _hideDocks(self):
        s = 0; h = 0
        print(self.dock_dict_list)
        for i,dl in enumerate(self.dock_dict_list):
            for d in dl:
                if i==self.mode:
                    d.show()
                    s+=1
                else:
                    d.hide()
                    h+=1
        print("{} docks shown, {} docks hidden".format(s,h))
    def removeAllDocks(self):
        #print "removealldocks", self.docks,self._container
        for d in self.docks:
            print("removing dock",d,self.docks[d])
            self.docks[d].hide()
        #self.lmdl = []
    def mouseMoved(self,mousePoint):
        t = time.time()
        if t-self.mouseMovedPrev< 0.017:
            #print "mouse move skipped"
            return
        self.mouseMovedPrev = t
        #print self.sender(),mousePoint
        gs = self.sender()
        if gs.sceneRect().contains(mousePoint):
            vw = gs.getViewWidget()
            vb = vw.getViewBox()
            scalePoint = vb.mapSceneToView(mousePoint)
            xpos = mousePoint.x()
            ypos = mousePoint.y()
            x = scalePoint.x()
            y = scalePoint.y()
            self.sigMouseMoved.emit(x,y)
    def clear(self):
        for p in self.lmpl:
            #print "plot manager: clear",p.items()
            for item in p.items():
                if type(item)==pg.PlotCurveItem():
                    item.setData([])
        for p in self.plot_dict_list[self.mode]:
            # print "plot manager: clear",p.items()
            for item in p.items():
                if type(item)==pg.graphicsItems.PlotCurveItem.PlotCurveItem:
                    # print "clear ",item
                    item.setData([])
                # else:
                #     print type(item)
    def setXtitle(self,xtitle,xunits):
        print("Setting X title to {}({})".format(xtitle,xunits))
        for p in self.lmpl:
            pi = p.getPlotItem()
            pi.setLabel('bottom',xtitle,xunits)
    def setRange(self,left,right):
        self.rangeLeft = left
        self.rangeRight = right
        # if self.rangeVisible:
        print("PlotManagr",self,"setRange: range set to",left,right)
        self._setRange(left,right)
    def getRange(self):
        return self.rangeLeft,self.rangeRight
    def setRangeMovable(self,state,source_number):
        self.rangeMovable = state
        self.src_number = source_number #for talking back to device
        if self.rangedisplay:
            self.rangedisplay.setMovable(state)
    def _setRange(self,left,right):
        if not self.rangedisplay:
            self.rangedisplay = RangeDisplay()
            self.rangedisplay.setMovable(self.rangeMovable)
            self.rangedisplay.rangeChanged.connect(self.onRangeChange)
            self.rangedisplay.addPlot(*tuple(self.plot_dict_list[2]))
            self.rangedisplay.setVisible(True)
        self.rangedisplay.setRange(left,right)
    def onRangeChange(self,arg):
        print("plotmanager.onRangeChange:",self, "range changed",self.src_number,arg)
        self.rangeChanged.emit(self.src_number,arg)

if __name__ == '__main__':
    app = pg.QtGui.QApplication([])
    win = pg.QtGui.QMainWindow()
    win2 = pg.QtGui.QMainWindow()
    win3 = pg.QtGui.QMainWindow()
    p1 = PlotManager()
    p2 = PlotManager()
    da = DockArea()
    # gl = pg.QtGui.QGridLayout()
    win.setCentralWidget(p1)
    win2.setCentralWidget(p2)
    win3.setCentralWidget(da)
    # gl.addWidget(p1)

    # win.resize(1000,500)
    win.setGeometry(100,100,500,500)
    # win2.resize(1000,500)
    win2.setGeometry(600,100,500,500)
    win3.setGeometry(1100,100,500,500)
    win.show()
    win2.show()
    win3.show()
    p1.setMode(1)
    p2.setMode(1)
    p1.createDock("1")
    p1.createDock("2")
    p2.createDock("2")
    p2.createDock("3")
    da.addDock(Dock("4"))
    da.addDock(Dock("3"))
    s = da.saveState()
    print(s,type(s))
    app.exec_()
