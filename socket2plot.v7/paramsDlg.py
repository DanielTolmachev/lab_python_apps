#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     06.03.2014
# Copyright:   (c) Daniel 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from qtpy import QtCore, QtWidgets,QtGui
import sys
from ParamsDlg_qt_v3 import Ui_ParamsDlg
from pattern2name import pattern2name
from os.path import exists
from sample_list import SampleList
parl = ['Sample name']

class ParamsDlg(QtWidgets.QDialog,Ui_ParamsDlg):
    accept = QtCore.Signal(dict)
    params = dict()
    sample_list = SampleList
    def __init__(self,*args):
        super(ParamsDlg,self).__init__()
        self.setupUi(self)
        #set text edit look like a label
        p = QtGui.QPalette()
        color = QtWidgets.QApplication.palette().color(QtGui.QPalette.Active,
                    QtGui.QPalette.Window)
        p.setColor(QtGui.QPalette.Active,
                QtGui.QPalette.Base,color)
        self.lbl_path.setPalette(p)
        self.lbl_pattern.setPalette(p)

        #settings question sub-dialog
        self.hf_question_load_settings.hide()
        self.pb_load_settings_yes.pressed.connect(self.load_other_settings)

        #self.accepted.connect(self.dlg_accepted)
        self.buttonBox.button(self.buttonBox.Ok).clicked.connect(self.dlg_accepted)
        self.buttonBox.button(self.buttonBox.Apply).clicked.connect(self.dlg_apply)
        self.savedlg = QtWidgets.QFileDialog()
        self.savedlg.setFileMode(self.savedlg.Directory)
        self.savedlg.accepted.connect(self.set_savefolder)
        self.btn_folder.clicked.connect(self.savedlg.exec_)
        self.le_pattern.textChanged.connect(self.patternchange)
        self.le_ext.textChanged.connect(self.patternchange)
        self.cb_autonum.stateChanged.connect(self.patternchange)
        self.le_path.textEdited.connect(self.savefolderchange)
        #self.le_sample.textEdited.connect(self.set_params)
        self.le_sample.textChanged.connect(self.on_sample_changed)
        #self.le_sample.
        #self.le_sample.
        self.le_sample_short.textEdited.connect(self.set_params)
        self.fsmdl = QtWidgets.QFileSystemModel()
        self.fsmdl.setFilter(QtCore.QDir.AllDirs)
        self.fsmdl.setRootPath('.')
        self.dircmpl = QtWidgets.QCompleter()
        self.dircmpl.setModel(self.fsmdl)
        self.le_path.setCompleter(self.dircmpl)
    def on_cb_autonum_stateChanged(self):
        if self.cb_autonum.isChecked():self.params['autonumber']='##'
        else:self.params['autonumber']= False
    def set_savefolder(self):
        lst = self.savedlg.selectedFiles()
        if len(lst)>0:
            self.lbl_path.setText(lst[0])
            self.le_path.setText(lst[0])
            self.params['SaveDirectory'] = self.params["SaveDirectoryPattern"] =str(lst[0])
    def savefolderchange(self):
        s = str(self.le_path.text())
        self.params["SaveDirectoryPattern"] = s
        s = pattern2name(s,self.params)
        self.params['SaveDirectory'] = s
        if exists(s): self.lbl_path.setText(s)
        else: self.lbl_path.setText('<new> %s'%s)
    def patternchange(self):
        s = str(self.le_pattern.text())
        if self.cb_autonum.isChecked() and not s.endswith('##') :
            if s:
                while s[-1]=='_':
                    s = s[:-1] #remove underscore at the end
                    if not s:
                        break
            s += '_##'
        self.params['SavePattern'] = s
        nameexample = pattern2name(s,self.params)
        if self.le_ext.text()!='': nameexample += '.'+self.le_ext.text()
        self.lbl_pattern.setText(nameexample)
    def set_params(self):
        self.params['sample']=str(self.le_sample.text())
        self.params['SampleShort'] = str(self.le_sample_short.text())
        self.params['SaveExtension']=str(self.le_ext.text())
        self.savefolderchange()
        self.patternchange()
        #self.suggest_to_load_settings()
    def suggest_to_load_settings(self):
        kl,_ = self.compare_settings_with_saved()
        if kl: #suggest to load settings if sample is in sample list
            self._suggest_to_load_settings(*kl)
        else: #otherwise hide suggestion dialog
            self.hf_question_load_settings.hide()
    def on_sample_changed(self,evt):
        #print 'on_sample_changed ',evt
        self.params['sample']= str(self.le_sample.text())
        print(self.le_sample.text())
        self.savefolderchange()
        self.patternchange()
        self.suggest_to_load_settings()
    def compare_settings_with_saved(self):
        kl = ()
        idx = -1
        # for s in self.sample_list:
        #     print s
        if self.params['sample'] in self.sample_list.get_sample_list():
            idx,_ = self.sample_list.find(self.params['sample'])
            print("Sample info\n",self.sample_list[idx])
            for key,val in self.sample_list[idx].items():
                # print key,val
                if self.params.get(key,'')!=self.sample_list[idx][key]:
                    #print key,self.params.get(key,'')
                    kl = kl + (key,)
            # if not self.sample_list[idx].has_key("SampleShort"):
            #     kl = kl + ("SampleShort")
        # print kl,idx
        return kl,idx
    def dlg_accepted(self,event):
        self.dlg_apply()
        self.hide()
    def dlg_apply(self):
        if self.sample_list!=None:
            self.sample_list.set(self.params)
            self.qsample_list = self.sample_list.get_sample_list()
            self.samp_comp = QtWidgets.QCompleter(self.qsample_list)
            self.le_sample.setCompleter(self.samp_comp)
        self.accept.emit(self.params)
    def showEvent(self,event):
        self.setUifromParams()
    def setUifromParams(self):
        if 'SaveDirectoryPattern' in self.params: self.le_path.setText(self.params['SaveDirectoryPattern'])
        elif 'SaveDirectory' in self.params: self.le_path.setText(self.params['SaveDirectory'])
        if 'SavePattern' in self.params: self.le_pattern.setText(self.params['SavePattern'])
        if 'sample' in self.params: self.le_sample.setText(self.params['sample'])
        if 'SampleShort' in self.params: self.le_sample_short.setText(self.params['SampleShort'])
        if self.params.get('autonumber',None)==False: self.cb_autonum.setChecked(False)
        else: self.cb_autonum.setChecked(True)
    def set_sample_list(self,samplist):
        if type(samplist)==SampleList:
            self.sample_list = samplist
        elif type(samplist)==str:
            self.sample_list = SampleList(samplist)
        else:
            print('Wrong arguments: type(samplist) = {}'.format(type(samplist)))
        self._update_sample_completer()
    def _update_sample_completer(self):
        #self.qsample_list = QtCore.QStringList(self.sample_list.get_sample_list())
        self.qsample_list = self.sample_list.get_sample_list()
        self.samp_comp = QtWidgets.QCompleter(self.qsample_list)
        self.samp_comp.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.le_sample.setCompleter(self.samp_comp)
    def _suggest_to_load_settings(self,*key):
        if key:
            ks = ', '.join(key)
            self.label_question_settings.setText('''
            <html><head/><body><p>You can load other setting that was used for this sample like <span style=" font-style:italic;">{}</span></p><p>Do you want to load these settings?</p></body></html>
            '''.format(ks))
        self.hf_question_load_settings.show()
    def load_other_settings(self):
        kl,idx = self.compare_settings_with_saved()
        if kl:
            for key in kl:
                self.params[key] = self.sample_list[idx][key]
            self.setUifromParams()
            self.savefolderchange()
            self.patternchange()
        self.hf_question_load_settings.hide()




if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    paramsDlg = ParamsDlg()
    samp = SampleList('test_sample_list.txt')
    paramsDlg.set_sample_list(samp)
    sys.exit(paramsDlg.exec_())