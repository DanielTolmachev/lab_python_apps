# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'socket2plot_gui.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_sock2plot_main_wnd(object):
    def setupUi(self, sock2plot_main_wnd):
        sock2plot_main_wnd.setObjectName(_fromUtf8("sock2plot_main_wnd"))
        sock2plot_main_wnd.resize(847, 721)
        self.centralwidget = QtWidgets.QWidget(sock2plot_main_wnd)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setMargin(1)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.btn_clr = QtWidgets.QToolButton(self.centralwidget)
        self.btn_clr.setObjectName(_fromUtf8("btn_clr"))
        self.horizontalLayout.addWidget(self.btn_clr)
        self.btn_persistent = QtWidgets.QToolButton(self.centralwidget)
        self.btn_persistent.setEnabled(True)
        self.btn_persistent.setCheckable(True)
        self.btn_persistent.setAutoRaise(False)
        self.btn_persistent.setObjectName(_fromUtf8("btn_persistent"))
        self.horizontalLayout.addWidget(self.btn_persistent)
        self.btn_xy = QtWidgets.QToolButton(self.centralwidget)
        self.btn_xy.setEnabled(True)
        self.btn_xy.setVisible(False)
        self.btn_xy.setCheckable(True)
        self.btn_xy.setAutoRaise(False)
        self.btn_xy.setObjectName(_fromUtf8("btn_xy"))
        self.horizontalLayout.addWidget(self.btn_xy)
        self.btn_xy_accum = QtWidgets.QToolButton(self.centralwidget)
        self.btn_xy_accum.setCheckable(True)
        self.btn_xy_accum.setChecked(True)
        self.btn_xy_accum.setObjectName(_fromUtf8("btn_xy_accum"))
        self.horizontalLayout.addWidget(self.btn_xy_accum)
        self.btn_console = QtWidgets.QToolButton(self.centralwidget)
        self.btn_console.setCheckable(True)
        self.btn_console.setObjectName(_fromUtf8("btn_console"))
        self.horizontalLayout.addWidget(self.btn_console)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 6)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setMinimumSize(QtCore.QSize(0, 110))
        self.frame.setMaximumSize(QtCore.QSize(16777215, 100))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.btn_start = QtWidgets.QPushButton(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_start.sizePolicy().hasHeightForWidth())
        self.btn_start.setSizePolicy(sizePolicy)
        self.btn_start.setMinimumSize(QtCore.QSize(120, 80))
        self.btn_start.setBaseSize(QtCore.QSize(100, 100))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.btn_start.setFont(font)
        self.btn_start.setObjectName(_fromUtf8("btn_start"))
        self.horizontalLayout_2.addWidget(self.btn_start)
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.lbl_pattern = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_pattern.sizePolicy().hasHeightForWidth())
        self.lbl_pattern.setSizePolicy(sizePolicy)
        self.lbl_pattern.setText(_fromUtf8(""))
        self.lbl_pattern.setObjectName(_fromUtf8("lbl_pattern"))
        self.gridLayout_3.addWidget(self.lbl_pattern, 1, 0, 1, 2)
        self.le_comment = QtWidgets.QLineEdit(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.le_comment.sizePolicy().hasHeightForWidth())
        self.le_comment.setSizePolicy(sizePolicy)
        self.le_comment.setMinimumSize(QtCore.QSize(0, 20))
        self.le_comment.setObjectName(_fromUtf8("le_comment"))
        self.gridLayout_3.addWidget(self.le_comment, 6, 0, 1, 1)
        self.le_comment_once = QtWidgets.QLineEdit(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.le_comment_once.sizePolicy().hasHeightForWidth())
        self.le_comment_once.setSizePolicy(sizePolicy)
        self.le_comment_once.setMaximumSize(QtCore.QSize(200, 16777215))
        self.le_comment_once.setInputMask(_fromUtf8(""))
        self.le_comment_once.setObjectName(_fromUtf8("le_comment_once"))
        self.gridLayout_3.addWidget(self.le_comment_once, 6, 1, 1, 1)
        self.lbl_sample = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_sample.sizePolicy().hasHeightForWidth())
        self.lbl_sample.setSizePolicy(sizePolicy)
        self.lbl_sample.setMinimumSize(QtCore.QSize(10, 10))
        self.lbl_sample.setText(_fromUtf8(""))
        self.lbl_sample.setObjectName(_fromUtf8("lbl_sample"))
        self.gridLayout_3.addWidget(self.lbl_sample, 5, 0, 1, 1)
        self.lbl_save = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_save.sizePolicy().hasHeightForWidth())
        self.lbl_save.setSizePolicy(sizePolicy)
        self.lbl_save.setObjectName(_fromUtf8("lbl_save"))
        self.gridLayout_3.addWidget(self.lbl_save, 0, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.btn_save = QtWidgets.QToolButton(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_save.sizePolicy().hasHeightForWidth())
        self.btn_save.setSizePolicy(sizePolicy)
        self.btn_save.setMinimumSize(QtCore.QSize(24, 24))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("save.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_save.setIcon(icon)
        self.btn_save.setObjectName(_fromUtf8("btn_save"))
        self.gridLayout_4.addWidget(self.btn_save, 0, 4, 1, 1)
        self.btn_setparams = QtWidgets.QToolButton(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_setparams.sizePolicy().hasHeightForWidth())
        self.btn_setparams.setSizePolicy(sizePolicy)
        self.btn_setparams.setObjectName(_fromUtf8("btn_setparams"))
        self.gridLayout_4.addWidget(self.btn_setparams, 0, 1, 1, 1)
        self.cb_save = QtWidgets.QCheckBox(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cb_save.sizePolicy().hasHeightForWidth())
        self.cb_save.setSizePolicy(sizePolicy)
        self.cb_save.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.cb_save.setObjectName(_fromUtf8("cb_save"))
        self.gridLayout_4.addWidget(self.cb_save, 0, 3, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem1, 0, 0, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_4, 0, 1, 1, 1)
        self.horizontalLayout_2.addLayout(self.gridLayout_3)
        self.gridLayout.addWidget(self.frame, 9, 0, 1, 6)
        self.lbl_y = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_y.sizePolicy().hasHeightForWidth())
        self.lbl_y.setSizePolicy(sizePolicy)
        self.lbl_y.setObjectName(_fromUtf8("lbl_y"))
        self.gridLayout.addWidget(self.lbl_y, 7, 5, 1, 1)
        self.lbl_x = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbl_x.sizePolicy().hasHeightForWidth())
        self.lbl_x.setSizePolicy(sizePolicy)
        self.lbl_x.setObjectName(_fromUtf8("lbl_x"))
        self.gridLayout.addWidget(self.lbl_x, 7, 4, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 7, 3, 1, 1)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.gridLayout.addWidget(self.tabWidget, 5, 0, 1, 6)
        self.btn_input = QtWidgets.QToolButton(self.centralwidget)
        self.btn_input.setObjectName(_fromUtf8("btn_input"))
        self.gridLayout.addWidget(self.btn_input, 7, 1, 1, 1)
        self.btn_showRcvData = QtWidgets.QToolButton(self.centralwidget)
        self.btn_showRcvData.setCheckable(True)
        self.btn_showRcvData.setChecked(True)
        self.btn_showRcvData.setObjectName(_fromUtf8("btn_showRcvData"))
        self.gridLayout.addWidget(self.btn_showRcvData, 7, 2, 1, 1)
        self.fr_rcv = QtWidgets.QFrame(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fr_rcv.sizePolicy().hasHeightForWidth())
        self.fr_rcv.setSizePolicy(sizePolicy)
        self.fr_rcv.setMinimumSize(QtCore.QSize(20, 0))
        self.fr_rcv.setFrameShape(QtWidgets.QFrame.Box)
        self.fr_rcv.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.fr_rcv.setLineWidth(1)
        self.fr_rcv.setObjectName(_fromUtf8("fr_rcv"))
        self.rcv_layout = QtWidgets.QVBoxLayout(self.fr_rcv)
        self.rcv_layout.setObjectName(_fromUtf8("rcv_layout"))
        self.lbl_Layout = QtWidgets.QVBoxLayout()
        self.lbl_Layout.setObjectName(_fromUtf8("lbl_Layout"))
        self.rcv_layout.addLayout(self.lbl_Layout)
        self.gridLayout.addWidget(self.fr_rcv, 8, 0, 1, 6)
        self.gridLayout_2.addLayout(self.gridLayout, 1, 0, 1, 1)
        sock2plot_main_wnd.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(sock2plot_main_wnd)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 847, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        sock2plot_main_wnd.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(sock2plot_main_wnd)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        sock2plot_main_wnd.setStatusBar(self.statusbar)

        self.retranslateUi(sock2plot_main_wnd)
        self.tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(sock2plot_main_wnd)

    def retranslateUi(self, sock2plot_main_wnd):
        sock2plot_main_wnd.setWindowTitle(_translate("sock2plot_main_wnd", "MainWindow", None))
        self.btn_clr.setToolTip(_translate("sock2plot_main_wnd", "remove all plots from screen", None))
        self.btn_clr.setText(_translate("sock2plot_main_wnd", "clear", None))
        self.btn_persistent.setToolTip(_translate("sock2plot_main_wnd", "Enables persistent mode:\n"
"screen is not cleared after recording start\n"
"so you can see multiple records on same screen\n"
"you can clear screen with \"clear\" button", None))
        self.btn_persistent.setText(_translate("sock2plot_main_wnd", "persistent", None))
        self.btn_xy.setText(_translate("sock2plot_main_wnd", "XY", None))
        self.btn_xy_accum.setToolTip(_translate("sock2plot_main_wnd", "enable data accumulation for XY plots", None))
        self.btn_xy_accum.setText(_translate("sock2plot_main_wnd", "accum", None))
        self.btn_console.setToolTip(_translate("sock2plot_main_wnd", "show console output", None))
        self.btn_console.setText(_translate("sock2plot_main_wnd", "console", None))
        self.btn_start.setText(_translate("sock2plot_main_wnd", "Start", None))
        self.le_comment.setPlaceholderText(_translate("sock2plot_main_wnd", "Comments", None))
        self.le_comment_once.setPlaceholderText(_translate("sock2plot_main_wnd", "comment for current file", None))
        self.lbl_save.setText(_translate("sock2plot_main_wnd", "SaveFolder", None))
        self.btn_save.setText(_translate("sock2plot_main_wnd", "...", None))
        self.btn_setparams.setText(_translate("sock2plot_main_wnd", "Params", None))
        self.cb_save.setText(_translate("sock2plot_main_wnd", "autosave", None))
        self.lbl_y.setText(_translate("sock2plot_main_wnd", "***", None))
        self.lbl_x.setText(_translate("sock2plot_main_wnd", "***", None))
        self.btn_input.setToolTip(_translate("sock2plot_main_wnd", "show dialog for choosing data source(s)\n"
"\n"
"you can choose one channel to be X axis, thus enabling plotting y(x)", None))
        self.btn_input.setText(_translate("sock2plot_main_wnd", "Set Input", None))
        self.btn_showRcvData.setToolTip(_translate("sock2plot_main_wnd", "display data received from devices and other programs", None))
        self.btn_showRcvData.setText(_translate("sock2plot_main_wnd", "Show Input", None))

