__author__ = 'E3a'
import socket,time,threading,traceback
ADDRESS = ('129.217.155.106',1234)
#if not globals().has_key("soc"):
 #   soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  #  connected = False

def one_device(max_time):
    #soc.send("++addr7\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = ""
    while et < max_time:


        soc.send("snap?1,2\r")
        try:
            resp = soc.recv(30)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        av+=dt; cnt +=1
        #print "{}: {} ({:.3f}s)".format(cnt,resp,dt)
    print("One device average response time is {}".format(av/cnt))

def two_device(max_time):
    #soc.send("++addr7\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = resp2 = ""
    while et < max_time:
        soc.send("++addr7\rsnap?1,2\r")
        try:
            resp = soc.recv(64)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        dt = t2 - t1
        t1 = t2
        av+=dt
        print("{},1: {} ({:.3f}s)".format(cnt,resp,dt))
        soc.send("++addr6\rsnap?1,2\r")
        #soc.send("++addr8\r")
        #time.sleep(0.1)
        #soc.send("snap?1,2\r")
        try:
            resp2 = soc.recv(64)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        print("{}.2: {} ({:.3f}s)".format(cnt,resp2,dt))
        av+=dt; cnt +=1

    print("Two device average response time is {}".format(av/cnt))

def two_deviceS(max_time): #silent
    #soc.send("++addr7\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = resp2 = ""
    while et < max_time:
        soc.send("++addr7\rsnap?1,2\r")
        try:
            resp = soc.recv(64)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        soc.send("++addr6\rsnap?1,2\r")
        try:
            resp2 = soc.recv(64)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        av+=dt; cnt +=1

    print("Two device average response time is {}".format(av/cnt))

def two_device2(max_time): #this is not working
    #soc.send("++addr7\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = resp2 = ""
    while et < max_time:
        soc.send("++addr7\rsnap?1,2\r")
        soc.send("++addr6\rsnap?1,2\r")
        try:
            soc.send("++addr7\r")
            resp = soc.recv(64)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        dt = t2 - t1
        t1 = t2
        print("{},1: {} ({:.3f}s)".format(cnt,resp,dt))

        #soc.send("++addr8\r")
        #time.sleep(0.1)
        #soc.send("snap?1,2\r")
        try:
            soc.send("++addr6\r")
            resp2 = soc.recv(64)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        print("{}.2: {} ({:.3f}s)".format(cnt,resp2,dt))
        av+=dt; cnt +=1

    print("Two device average response time is {}".format(av/cnt))

def fast_benchmark(lim):
    i=0; et = 0
    t = time.time()
    soc.send("++mode 1\r")
    soc.send("++addr7\r")
    soc.settimeout(1)
    soc.send('fast0\r')
    soc.send('paus\r')
    soc.send('rest\r')
    soc.send('*idn?\r')
    print("*idn = ",soc.recv(128))

    #soc.send('send1\r')
    soc.send('srat13\r')
    soc.send('fast2\r')
    soc.send('fast?\r')
    print("fast mode = ",soc.recv(128))
    soc.send('strd\r')
    #soc.send("++mode0\r")
    #soc.send("++mode0\r")
    #soc.send("++lon1\r")
    #print soc.sensitivity
    #global res
    buf = bytearray(b" "*1024)
    cnt_tot = 0
    t_st = 0
    while et< lim:
        res = ''
        cnt = 0
        try:
            #soc.send("++read\r")
            cnt = soc.recv_into(buf,128)
            #d = np.frombuffer(res,dtype = np.int16)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            #d = 'nop'
            #soc.send('fast1\r')
            #soc.send('strd\r')
            pass
        t2 = time.time()
        if cnt_tot==0:
            t_st = t2
        dt = t2-t
        et+=dt
        print(t2-t, " bytes received",cnt)
        cnt_tot += cnt
        t = t2
    et = t2-t_st
    print("total data rate is {}/{} = {} bytes/sec ({} samp/s)".format(
    cnt_tot,et,cnt_tot/et,cnt_tot/et/4))

def fast_benchmark2(lim):
    i=0; et = 0
    t = time.time()
    soc.send("++mode 1\r")
    soc.send("++addr7\r")
    soc.settimeout(1)
    soc.send('fast0\r')
    soc.send('paus\r')
    soc.send('rest\r')
    soc.send('*idn?\r')
    print("*idn = ",soc.recv(128))

    #soc.send('send1\r')
    soc.send('srat9\r')
    soc.send('fast1\r')
    soc.send('fast?\r')
    print("fast mode = ",soc.recv(128))
    soc.send('strd\r')
    #soc.send("++mode0\r")
    #soc.send("++lon1\r")
    #print soc.sensitivity
    #global res
    buf = bytearray(b" "*1024)
    cnt_tot = 0
    t_st = 0
    while et< lim:
        res = ''
        cnt = 0
        try:
            #soc.send("++read\r")
            cnt = soc.recv_into(buf,128)
            #d = np.frombuffer(res,dtype = np.int16)
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            #d = 'nop'
            #soc.send('fast1\r')
            #soc.send('strd\r')
            pass
        t2 = time.time()
        if cnt_tot==0:
            t_st = t2
        dt = t2-t
        et+=dt
        print(t2-t, " bytes received",cnt)
        cnt_tot += cnt
        t = t2
    et = t2-t_st
    print("total data rate is {}/{} = {} bytes/sec ({} samp/s)".format(
    cnt_tot,et,cnt_tot/et,cnt_tot/et/4))


#soc.settimeout(1)
try:
    #if not connected:
      #  soc.connect(ADDRESS)
     #   connected = True
    soc = socket.create_connection(ADDRESS,1)
    #soc.shutdown(socket.SHUT_RDWR)
    fast_benchmark2(20)
    pass
except:
    traceback.print_exc()
#one_device(5)
#two_device(5)

#finally:
soc.send("++lon 0\r")
soc.send("++mode 1\r")
soc.send('paus\r')
soc.send('rest\r')
while True:
    try:
        print("waiting till sr830 stops")
        soc.recv(1024)
    except:
        break
soc.send('fast0\r')
soc.close()
 #   print "Connection closed"