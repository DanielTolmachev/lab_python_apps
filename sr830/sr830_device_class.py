import time,traceback
import numpy as np
# sys.path.append( '../modules')
import instrument_base
# from instr_socket import Instr_Socket,socket
TIME_TO_WAIT_FOR_ANSWER = 0.01
LAMBDA0 = 379.14e-9
MAX_WAVELENGTH = 2e-6
STEP = 2e-12


senslist = [
         2e-9,5e-9,
    1e-8,2e-8,5e-8,
    1e-7,2e-7,5e-7,
    1e-6,2e-6,5e-6, #10
    1e-5,2e-5,5e-5,
    1e-4,2e-4,5e-4,
    1e-3,2e-3,5e-3,#19
    1e-2,2e-2,5e-2,
    1e-1,2e-1,5e-1,
    1.]

class SR830(instrument_base.InstrumentBase):
    def __init__(self,address,**kwargs):
        self.id = None
        self.started = False
        self.sensitivity = None
        self.connected = False
        self.address = address
        super(SR830,self).__init__(address,**kwargs)
        self.time_to_wait = TIME_TO_WAIT_FOR_ANSWER
        # self.open()



        #self._getSens()
    # def ask(self, query):
    #     self.write('{:s}'.format(query))
    #     return self.readall()
    # def readall(self):
    #     rep = ''
    #     try:
    #         while True:
    #             rep += self.recv(64)
    #     except socket.timeout:
    #         return rep
    #     except:
    #         traceback.print_exc()
    #     return rep
    def getID(self):
        if self.id == None:
            self.id = self.ask('*IDN?\r')
        return self.id
    def getSensI(self):
        rep = ''
        try:
            rep = self.ask(b'sens?\r')
            s = int(rep)
            return s
        except:
            traceback.print_exc()
            print(s)
    def getSens(self):
        s = ''
        try:
            s = self.ask(b'sens?\r')
            s = int(s)
            self.sensitivity = senslist[s]
            print((s,senslist[s]))
            self.mult = 30000.*self.sensitivity
            return self.sensitivity
        except:
            traceback.print_exc()
            print(s)
    def setSens(self,sens_index):
        if -1<sens_index< len(senslist):
            self.write('sens{}\r'.format(sens_index).encode())
            self.sensitivity = senslist[sens_index]
    def increaseSens(self):
        if self.sensitivity:
            sens_index = senslist.index(self.sensitivity)
        else:
            sens_index = self.getSensI()
        if sens_index<len(senslist)-1:
            sens_index +=1
            self.setSens(sens_index)
            # self.getSens()
        return self.sensitivity
    def decreaseSens(self):
        if self.sensitivity:
            sens_index = senslist.index(self.sensitivity)
        else:
            sens_index = self.getSensI()
        if sens_index>0:
            sens_index -=1
            self.setSens(sens_index)
            # self.getSens()
        return self.sensitivity
    def getModFreq(self):
        s = self.ask('')
    def queryStats(self):
        self.getSens()
    # def _getSens(self):
    #     while not self.sensitivity:
    #         print 'Sensitivity set to {}'.format(self.getSens())
    def autoGain(self):
        self.write(b'AGAN\r')
    def autoPhase(self):
        self.write(b'APHS\r')
    def fastModeOn(self):
        self.write(b'fast1\r')
        self.write(b'strd\r')
    def fastModeOff(self):
        self.write(b'fast0\r')
        self.write(b'paus\r')
        try:
            while self.read():
                pass
        except:
            return



# def fast_benchmark(sr,lim):
#     i=0
#     t = time.time()
#     sr.settimeout(1)
#     sr.write('fast0\r')
#     sr.write('paus\r')
#     sr.write('rest\r')
#
#     sr.write('send1\r')
#     sr.write('srat7\r')
#     sr.write('fast1\r')
#     sr.write('strd\r')
#     print(sr.sensitivity)
#     global res
#     while i< lim:
#         res = ''
#         try:
#             res = sr.recv(100)
#             d = sr.mult/np.frombuffer(res,dtype = np.int16)
#         except:
#             #traceback.print_exc()
#             d = 'nop'
#             sr.write('fast1\r')
#             sr.write('strd\r')
#         t2 = time.time()
#         print(t2-t, d)
#         t = t2
#     sr.write('fast0\r')
#     sr.write('paus\r')

def fast_benchmark(sr,lim,rate =7, fastmode = 2):
    """
    :param sr:
    :param lim:
    :param rate:
        0 62.5 mHz 7 8 Hz
        1 125 mHz 8 16 Hz
        2 250 mHz 9 32 Hz
        3 500 mHz 10 64 Hz
        4 1 Hz 11 128 Hz
        5 2 Hz 12 256 Hz
        6 4 Hz 13 512 Hz
        14 Trigger
    :return:
    """
    i=0
    t = time.time()
    # sr.settimeout(1)
    sr.fastModeOff()
    sr.write('rest\r')
    sr.getSens()
    sr.write('send1\r')
    sr.write('srat{}\r'.format(rate))
    sr.write('fast{}\r'.format(fastmode))
    sr.write('strd\r')
    print(sr.sensitivity)
    global res
    res = b''
    while i< lim:
        try:
            res += sr.socket.recv(1000)
            if len(res)%2:
                d = sr.mult / np.frombuffer(res[:-1], dtype=np.int16)
                res = res[-1:]
            else:
                d = sr.mult/np.frombuffer(res,dtype = np.int16)
                res = b''
        except socket.timeout:
            d = "-"
        except Exception as err:
            traceback.print_exc()
            d = 'nop'
            res = b''
            # sr.write('fast{}\r'.format(fastmode))
            # sr.write('strd\r')
        t2 = time.time()
        if isinstance(d,np.ndarray):
            print(t2-t, "got",len(d),"samples")
        else:
            print(t2 - t, d)
        t = t2
    sr.write('fast0\r')
    sr.write('paus\r')

if __name__ == '__main__':
    # address = ('129.217.155.192',1234)
    address = ('COM3')
    sr = SR830(address)
    print(sr.getID())
    # fast_benchmark(sr,10,rate=9,fastmode=2)
    #snap_benchmark(sr,100)
