from qtpy import QtCore
import time,traceback,socket,errno,six
import numpy as np
#TODO This shold be redone completely with event loop
class SR830_GPIB_ETH_receiver(QtCore.QThread):

    sig_data_recieved = QtCore.Signal(object)
    sig_connected = QtCore.Signal(object)
    sig_sensitivity = QtCore.Signal(object)
    sample_rate = 7
    aux_channel = 1
    timeout = 1
    def __init__(self,instr,mode = ""):
        super(SR830_GPIB_ETH_receiver,self).__init__()
        self.instr = instr
        self.mode = mode
        self.connected = None
        self.command = []
        self._checkConnectionChange()
    def run(self):
        self.started = True
        self._checkConnectionChange()
        if self.mode=='fast':
            self.start_fast_mode()
        elif self.mode =="aux":
            self.start_aux()
        else:
            self.start_snap()
    def start_fast_mode(self):
            t = time.time()
            #stop if already in fast mode
            self.instr.fastModeOff()
            #initiate fast mode
            self.instr.write('send1\r')
            self.instr.write('srat{}\r'.format(self.sample_rate))
            self.instr.fastModeOn()
            while self.started:
                try:
                    res = self.instr.socket.recv(4)
                    t2 = time.time()
                    d = self.instr.mult/np.frombuffer(res,dtype = np.int16)
                    self.sig_data_recieved.emit((t2,d[0],d[1]))
                except:
                    #traceback.print_exc()
                    t2 = time.time()
                    d = 'nop'
                    self.instr.write('fast1\r')
                    self.instr.write('strd\r')
                    self.sig_data_recieved.emit((t2,None))
                #print t2-t, d
                t = t2
            self.instr.fastModeOff()
    def start_aux(self):
        self.command = 'oaux?{}\r'.format(self.aux_channel)
        print("requesting AUX channel with command ",self.command)
        t0 = 0.
        while self.started:
            t = time.time()
            try:
                self.instr.write(self.command)
                rep = self.instr.read()
                #print rep
                x = float(rep)
                self.sig_data_recieved.emit((t,x))
            except socket.timeout:
                #traceback.print_exc()
                if t0 == 0:
                    t0 = t;
                else:
                    if t-t0>10:
                        print("timed out too long, connection lost")
                        self.connected = False
                rep = 'timed out'
                print(rep)
                self.sig_data_recieved.emit((t,rep))
            except socket.error as err:
                if err.errno == errno.WSAECONNRESET:
                    self.connected = False
                else:
                    traceback.print_exc()
            except:
                traceback.print_exc()
            while not self.connected:
                time.sleep(1)
                print("trying to reconnect...", end=' ')
                try:
                    self.instr.create_connection()
                    self.connected = self.instr.connected
                    self.sig_connected.emit(self.instr.address)
                except socket.error as err:
                    if err.errno == 10065:
                        print("failed (10065)")
                    else:
                        traceback.print_exc()
                except:
                    traceback.print_exc()

    # ***********************************************************************************************#
    def start_snap(self):
        t0 = 0.
        while self.started:
            self._checkCommands()
            self.instr.write(b'snap?1,2\r')
            t = time.time()
            try:
                rep = self.instr.read()
                if rep:
                    if not six.PY2:
                        rep = rep.decode()
                    l = rep.split(',')
                    x = float(l[0])
                    y = float(l[1])
                    self.sig_data_recieved.emit((t,x,y))
                    if not self.connected: #update connection status upon reconnection
                        self._checkConnectionChange()
                else:
                    self._checkConnectionChange()
                    self.sig_data_recieved.emit(rep)
            # except IndexError:
            #     print("got ",rep)
            # except socket.timeout:
            #     #traceback.print_exc()
            #     rep = 'timed out'
            #     print(rep)
            #     self.sig_data_recieved.emit((t,rep))
            # except socket.error as err:
            #     if err.errno == errno.WSAECONNRESET:
            #         self.connected = False
            #     else:
            #         traceback.print_exc()
            except:
                traceback.print_exc()
            # while not self.connected:
            #     try:
            #         self.instr.open()
            #         self.connected = True
            #         self.sig_connected.emit(self.instr.address)
            #     except:
            #         traceback.print_exc()
            #     # t2 = time.time()
            #     # rep = 'nop'
            #     # print rep
            #     # self.sig_data_recieved.emit((t,rep))

    def _checkConnectionChange(self):
        if self.instr.isOpen() != self.connected:
            self.connected = self.instr.isOpen()
            if self.connected:
                self.sig_connected.emit(self.instr.address)
            else:
                self.sig_connected.emit("disconnected")
    def _checkCommands(self):
        cmds = self.command.copy()
        self.command = []
        for cmd in cmds:
            if cmd == "start":
                self.instr.open()
                self._checkConnectionChange()
            elif cmd == "stop":
                self.instr.close()
                self._checkConnectionChange()
            elif type(cmd)==tuple and len(cmd)>1 and cmd[0]== "addr":
                self.instr.setDeviceAddress(cmd[1])
    def stop(self):
        try:
            self.instr.fastModeOff()
        except:
            traceback.print_exc()
        self.started = False
    def querySens(self):
        try:
            self.stop()
            self.wait()
            sens = self.instr.getSens()
            self.sig_sensitivity.emit(sens)
            self.start()
        except:
            traceback.print_exc()
    def increaseSens(self):
        sens = self.call(self.instr.increaseSens)
        self.sig_sensitivity.emit(sens)
    def decreaseSens(self):
        sens = self.call(self.instr.decreaseSens)
        self.sig_sensitivity.emit(sens)
    def call(self,func,*args):
        try:
            self.stop()
            self.wait()
            ret = func(*args)
            self.start()
            return ret
        except:
            traceback.print_exc()
    def autoGain(self):
        self.stop()
        self.wait()
        self.instr.autoGain()
        self.instr.getSens()
        self.start()
    def autoPhase(self):
        self.stop()
        self.wait()
        self.instr.autoPhase()
        self.start()
    def setCommand(self,cmd):
        self.command = cmd
    def setMode(self,mode):
        self.mode = mode
        print("mode set to ",mode)
    def open(self):
        self.command.append("start")
    def close(self):
        self.command.append("stop")

    def setConnected(self, status):
        if status:
            self.open()
        else:
            self.close()
    def setAddress(self,address):
        self.close()
        self.command.append(("addr",address))
        self.open()