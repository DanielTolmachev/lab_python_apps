#-------------------------------------------------------------------------------
# Name:		sr830_eth_qt_aux.pyw
# Purpose:   Read out AUX channel of SRS SR830 lock-in amplifier 
#		through ethernet (i.e. using gpib to ethernet adaptor) and
#	        publish data using ZMQ messaging library	        publish data using ZMQ messaging library
#
# Created:     30.07.2015 
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

from PyQt4 import QtCore, QtGui
import numpy as np
import zmq
import socket,sys,time, traceback
sys.path.append( '../modules')
from options import Options
from SRS830_central_widget import Ui_main_widget
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
import sr830_device_class,sr830_qt_class

DEFAULT_DEVICE_ADDRESS = ('129.217.155.192',1234)
UDP_IP = "127.0.0.1"
UDP_PORT = 6000
DEFAULT_PUBLISH_PORT = 5010
print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)
DEFAULT_TIMEOUT = 1
SKIP_REPEATED_VALUES = False

config = ''
def parse_args(argv):
    if len(sys.argv)>1:
        if sys.argv[1] == 'demo':
            w.setWindowTitle('demo mode')
            global random
            random = True
        for i in range(1,len(sys.argv)-1):
                # if sys.argv[i].startswith('-a'):
                #     addr,port = sys.argv[i+1].split(':')
                #     print 'address set to {}:{}'.format(addr,port)
                #     DEV_ADDR = (addr,int(port))
                # if sys.argv[i].startswith('-p'):
                #     global publish_port
                #     publish_port = int(sys.argv[i+1])
                #     print 'port set to {}'.format(publish_port)
                if sys.argv[i].startswith('-c'):
                    global config
                    config = sys.argv[i+1]
                    print('config is set to {}'.format(config))


    usage = '-address 129.217.155.106:1234 -publishport 5008'



    usage = '-address 129.217.155.106:1234 -publishport 5008'

if __name__ == '__main__':
    parse_args(sys.argv)


opt = Options(config)
device_address = opt.get('device_address',DEFAULT_DEVICE_ADDRESS)
publish_port =  opt.get('publish_port',DEFAULT_PUBLISH_PORT)


context = zmq.Context()
sock = context.socket(zmq.PUB)
#sock.bind("tcp://127.0.0.1:5000")






bound = 0
while not bound:
    try:
        sock.bind('tcp://127.0.0.1:%d'% publish_port)
        bound = publish_port
        print('bound to %d' % publish_port)
    except socket.error as msg:
        print(msg)
        publish_port+=1
        if publish_port>65535: break;
    except:
        print(sys.exc_info())

def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk

class MainWidget(QtGui.QWidget,Ui_main_widget):
    senslist = [
    2e-9,5e-9,
    1e-8,2e-8,5e-8,
    1e-7,2e-7,5e-7,
    1e-6,2e-6,5e-6,
    1e-5,2e-5,5e-5,
    1e-4,2e-4,5e-4,
    1e-3,2e-3,5e-3,
    1e-2,5e-2,2e-2,
    1e-1,5e-1,2e-1,
    1.]
    sratlist = []
    sens = 1.
    units = {1:'V'}
    t1 = 0
    t2 = 0
    ch1p = np.Inf
    ch2p = np.Inf
    rp = np.Inf
    thetap = np.Inf
    lostcon = 0
    greyedout = False
    command_changed = QtCore.pyqtSignal(str)
    def __init__(self):
        super(MainWidget,self).__init__()
        self.setupUi(self)
        self.lbl_rate_rdt = QtGui.QLabel('Readout rate: - ')
        self.lbl_rate_upd = QtGui.QLabel('Update rate: - ')
        self.lbl_net = QtGui.QLabel('Connected to: - ')
        self.gridLayout.addWidget(self.lbl_rate_rdt,2,0)
        self.gridLayout.addWidget(self.lbl_rate_upd,3,0)
        self.gridLayout.addWidget(self.lbl_net,2,2)
    def setsens(self,s):
        try:
            # if s[0:2]!='>>':
            #     return
            # i = int(s[2:])
            self.sens = s
            print('sens is: %g V' % (s))
        except:
            traceback.print_exc()
        #repeater_thrd.start()
    def greyout(self, state):
        self.ch1label.setEnabled(not state)
        self.ch2label.setEnabled(not state)
        self.lbl_rate_rdt.setText('last response was %d sec ago' % DEFAUL_SERIAL_TIMEOUT)
        self.lbl_rate_rdt.setEnabled(not state)
        self.lbl_rate_upd.setText('')
        self.lbl_rate_upd.setEnabled(not state)
        self.greyedout = state
##        if state:
##            self.ch1label.setFont()
    def setDisp(self,ch1,ch2):
        self.ch1p = ch1
        self.ch2p = ch2
        #print ch1,ch2
        #set labels
        self.ch1label.setText('%.3e %s'%(ch1,self.units[1]))
        if ch1 >= 0:
            ch1l=0
            ch1r=min(100,ch1/self.sens*100)
        else:
            ch1r=0
            ch1l=min(100,abs(ch1)/self.sens*100)
        #print ch1l, ch1r
        self.ch1left.setValue(ch1l)
        self.ch1right.setValue(ch1r)
        self.ch2label.setText('%.3e %s'%(ch2,self.units[1]))
        if ch2 >= 0:
            ch2l=0
            ch2r=min(100,ch2/self.sens*100)
        else:
            ch2r=0
            ch2l=abs(max(-100,ch2/self.sens*100))
        self.ch2left.setValue(ch2l)
        self.ch2right.setValue(ch2r)
    def setDisp1(self,ch1):
        self.ch1p = ch1
        #set labels
        self.ch1label.setText('%.3e %s'%(ch1,'V'))
        if ch1 >= 0:
            ch1l=0
            ch1r=min(100,ch1/1000)
        else:
            ch1r=0
            ch1l=min(100,abs(ch1)/1000)
        #print ch1l, ch1r
        self.ch1left.setValue(ch1l)
        self.ch1right.setValue(ch1r)
        self.ch2label.setText('aux in')
        self.ch2left.setValue(0)
        self.ch2right.setValue(0)
    def update_data(self, args):
        t = None
        if type(args)==tuple:
            if len(args)==3:
                t = args[0]
                self.setDisp(args[1],args[2])
                sock.send_pyobj(args)
                #return
            elif len(args)==2:
                sock.send_pyobj(args)
                t = args[0]
                arg = args[1]
                if type(arg)==float:
                    self.setDisp1(arg)
                else:
                    print('update receive wrong data\n',args)
            else:
                print('update receive wrong data\n',args)
        elif args=='':
            if not self.greyedout:
                if self.lostcon:
                    if t-self.lostcon>DEFAUL_SERIAL_TIMEOUT:
                        self.greyout(True)
                        print('no response for more then %d s' % DEFAUL_SERIAL_TIMEOUT)
                else:
                    self.lostcon = time.time()
            else:
                self.lbl_rate_rdt.setText('last response was %d sec ago' % (t-self.lostcon))
            return
        if self.greyedout:
            self.greyout(False)
        #self.lostcon = 0

        # l = re.findall('-*[0-9]+\.*[0-9]*e*[+-]*[0-9]*',args)
        # try:
        #     ch1 = float(l[0])
        #     ch2 = float(l[1])
        #     r = float(l[2])
        #     theta = float(l[3])
        # except:
        #     print sys.exc_info()
        #     return
        # if random:
        #     ch1 = (np.random.random()-0.5)*2.001
        #     ch2 = (np.random.random()-0.5)*2.001
        # if SKIP_REPEATED_VALUES and\
        #         ch1==self.ch1p and ch2 ==self.ch2p \
        #         and r ==self.rp and theta == self.thetap:
        #     print 'skipped'
        # else:
        #     #remember values for next time
        #     self.ch1p = ch1
        #     self.ch2p = ch2
        #     self.rp = r
        #     self.thetap = theta
        #     #set labels
        #     self.ch1label.setText('%.3e %s'%(ch1,self.units[1]))
        #     if ch1 >= 0:
        #         ch1l=0
        #         ch1r=min(100,ch1/self.sens*100)
        #     else:
        #         ch1r=0
        #         ch1l=min(100,abs(ch1)/self.sens*100)
        #     #print ch1l, ch1r
        #     self.ch1left.setValue(ch1l)
        #     self.ch1right.setValue(ch1r)
        #     self.ch2label.setText('%.3e %s'%(ch2,self.units[1]))
        #     if ch2 >= 0:
        #         ch2l=0
        #         ch2r=min(100,ch2/self.sens*100)
        #     else:
        #         ch2r=0
        #         ch2l=abs(max(-100,ch2/self.sens*100))
        #     self.ch2left.setValue(ch2l)
        #     self.ch2right.setValue(ch2r)
        #     sock.send_pyobj((t, ch1,ch2,r,theta))
        #     if self.t2:
        #         self.updrate = t-self.t2
        #         self.lbl_rate_upd.setText('Update rate: %g s'%self.updrate)
        #     self.t2 = t
        if t:
            self.rdt = t-self.t1
            self.lbl_rate_rdt.setText('Readout rate: %.1f ms' %(self.rdt*1000))
        self.t1 = t
    def font_increase(self):
        f = self.ch1label.font()
        sz = f.pointSize()
        f.setPointSize(sz*2)
        self.ch1label.setFont(f)
        self.ch2label.setFont(f)
        h = self.ch1label.fontMetrics().height()
        w = self.ch1label.fontMetrics().width(self.ch1label.text())
        print('font size is ({},{})'.format(w,h))
        self.ch1label.setMinimumSize(w+5,h*1.2)
        self.ch2label.setMinimumSize(w+5,h*1.2)
        print('label size is ', self.ch1label.size())
        print('sizehint is ', self.ch1label.sizeHint(), end=' ')
        print('min max is ',self.ch1label.minimumSizeHint(),self.ch1label.maximumHeight())
        print('widget size is', self.size(), self. sizeHint())
##        self.ch1label.setFixedHeight(h*2)
##        self.ch2label.setFixedHeight(h*2)

        h = self.ch1left.height()
        self.ch1left.setMinimumHeight(h*2)
        self.ch1right.setMinimumHeight(h*2)
        self.ch2left.setMinimumHeight(h*2)
        self.ch2right.setMinimumHeight(h*2)
        print('bar height is ', self.ch1left.height(),self.ch1left.sizeHint(), self.ch1left.maximumSize(), self.ch1left.minimumSize())

    def font_decrease(self):
        f = self.ch1label.font()
        h = self.ch1label.height()
        sz = f.pointSize()
        f.setPointSize(sz/2)
        self.ch1label.setFont(f)
        self.ch2label.setFont(f)
        h = self.ch1label.fontMetrics().height()
        w = self.ch1label.fontMetrics().width(self.ch1label.text())
        print('font size is ({},{})'.format(w,h))
        self.ch1label.setMaximumSize(w+5,h*1.2)
        self.ch2label.setMaximumSize(w+5,h*1.2)
        print('label size is ', self.ch1label.size())
        print('sizehint is ', self.ch1label.sizeHint(), end=' ')
        print('min max is ',self.ch1label.minimumSizeHint(),self.ch1label.maximumHeight())
        h = self.ch1left.height()
        self.ch1left.setMinimumHeight(h/2)
        print('bar height is ', self.ch1left.height(),self.ch1left.sizeHint(), self.ch1left.maximumSize(), self.ch1left.minimumSize())
        self.ch1right.setMinimumHeight(h/2)
        self.ch2left.setMinimumHeight(h/2)
        self.ch2right.setMinimumHeight(h/2)
    def update_netstat(self,arg):
        if type(arg)==tuple and len(arg)>1:
            self.lbl_net.setText("Connected to {}:{}".format(arg[0],arg[1]))
        else:
            self.lbl_net.setText("Connected to {}".format(arg))
    def set_aux(self):
        chan = self.sender().text().trimmed()[-1]
        print("Aux channel ",chan)
        self.command_changed.emit("oaux?{}\r".format(chan))
app = QtGui.QApplication(sys.argv)
#w = QtGui.QMainWindow()
w = QMainWindowAutoSaveGeometry()
if 'Window.geometry' in opt:
    w.loadGeometry(opt['Window.geometry'])
w.GeometryChangedSig.connect(save_win_position)
title = 'SR830 DSP lock-in amplifier'
if config:
    title += ' {}'.format(config)
w.setWindowTitle(title)
mw = MainWidget() #create main widget
w.setCentralWidget(mw)

#set application icon
icon = QtGui.QIcon("lock-in_aux.png")
w.setWindowIcon(icon)

sr = sr830_device_class.SR830(device_address)
rcv = sr830_qt_class.SR830_GPIB_ETH_receiver(sr,mode = "aux")
rcv.sig_data_recieved.connect(mw.update_data)
rcv.sig_connected.connect(mw.update_netstat)
rcv.sig_sensitivity.connect(mw.setsens)

if sr.connected:
    mw.update_netstat(sr.address)
rcv.querySens()

##mw.ch1left.hide()
##mw.ch1right.hide()
##mw.ch2left.hide()
##mw.ch2right.hide()
#w.setmainwidget(mw) #set that widget as main
##w.frame.setFrameShape(QtGui.QFrame.NoFrame)
##w.frame.setContentsMargins(0,0,0,0)
##bw.setport(w.serport)
##bw.readout_done.connect(w.echo)

#action_view = QtGui.QAction('View',w)
action_font_increase = QtGui.QAction('Increase font',w)
action_font_increase.triggered.connect(mw.font_increase)
action_font_decrease = QtGui.QAction('Decrease font',w)
action_font_decrease.triggered.connect(mw.font_decrease)
action_sensitivity = QtGui.QAction('query Sensitivity',w)
action_sensitivity.triggered.connect(rcv.querySens)
action_auto_gain = QtGui.QAction('auto Gain',w)
action_auto_gain.triggered.connect(rcv.autoGain)
action_auto_phase = QtGui.QAction('auto Phase',w)
action_auto_phase.triggered.connect(rcv.autoPhase)

#w.menubar.addAction(action_view)
menu_view = w.menuBar().addMenu('View')
menu_view.addAction(action_font_increase)
menu_view.addAction(action_font_decrease)
menu_settings = w.menuBar().addMenu('Settings')
menu_settings.addAction(action_sensitivity)
menu_settings.addAction(action_auto_gain)
menu_settings.addAction(action_auto_phase)

menu_aux_channels = w.menuBar().addMenu('AUX Channels')
action_aux_channel1 = QtGui.QAction('Aux in 1',w)
action_aux_channel2 = QtGui.QAction('Aux in 2',w)
action_aux_channel3 = QtGui.QAction('Aux in 3',w)
action_aux_channel4 = QtGui.QAction('Aux in 4',w)
menu_aux_channels.addAction(action_aux_channel1)
menu_aux_channels.addAction(action_aux_channel2)
menu_aux_channels.addAction(action_aux_channel3)
menu_aux_channels.addAction(action_aux_channel4)
action_aux_channel1.triggered.connect(mw.set_aux)
action_aux_channel2.triggered.connect(mw.set_aux)
action_aux_channel3.triggered.connect(mw.set_aux)
action_aux_channel4.triggered.connect(mw.set_aux)
mw.command_changed.connect(rcv.setCommand)


##repeater_thrd.setport(w.serport)
##repeater_thrd.timed_data_ready.connect(mw.update)
w.show()
##repeater_thrd.cmd = 'snap?1,2,3,4\n\r'
##repeater_thrd.lineend = ''
##repeater_thrd.timeout = DEFAUL_SERIAL_TIMEOUT
##repeater_thrd.verbose = False


##if w.serport.isOpen():
##    w.serport.write('sens?\n\r')
##    bw.readout_done.connect(mw.setsens)
##    bw.start()



random =False
rcv.aux_channel = 1
#rcv.mode = "aux"

if __name__ == '__main__':
    #parse_args(sys.argv)
    #rcv.start()
    app.exec_()
rcv.stop()
sock.close()
context.destroy()
opt.savetodisk()
