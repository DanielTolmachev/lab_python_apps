__author__ = 'E3a'
import serial,time,threading,traceback
PORT = 3
soc = serial.Serial("COM3",timeout = 1,baudrate = 900000)
#if not globals().has_key("soc"):
 #   soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  #  connected = False

def one_device(max_time):
    soc.write("++addr6\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = ""
    while et < max_time:
        soc.write("snap?1,2\r")
        resp = soc.readline()
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        av+=dt; cnt +=1
        #print "{}: {} ({:.3f}s)".format(cnt,resp,dt)
    print("One device average response time is {}".format(av/cnt))

def two_device(max_time):
    #soc.write("++addr7\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = resp2 = ""
    while et < max_time:
        soc.write("++addr7\rsnap?1,2\r")
        resp = soc.readline()
        t2 = time.time()
        dt = t2 - t1
        t1 = t2
        av+=dt
        print("{},1: {} ({:.3f}s)".format(cnt,resp,dt))
        soc.write("++addr6\rsnap?1,2\r")
        resp2 = soc.readline()
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        print("{}.2: {} ({:.3f}s)".format(cnt,resp2,dt))
        av+=dt; cnt +=1

    print("Two device average response time is {}".format(av/cnt))

def two_deviceS(max_time): #silent
    soc.write("++addr6\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = resp2 = ""
    while et < max_time:
        soc.write("++addr7\rsnap?1,2\r")
        resp = soc.readline()
        soc.write("++addr6\rsnap?1,2\r")
        resp2 = soc.readline()
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        av+=dt; cnt +=1

    print("Two device average response time is {}".format(av/cnt))

def two_device2(max_time): #this is not working
    #soc.write("++addr7\r")
    t = time.time(); t1 = t; et = 0
    av = 0; cnt = 0
    resp = resp2 = ""
    while et < max_time:
        soc.write("++addr7\rsnap?1,2\r")
        soc.write("++addr6\rsnap?1,2\r")
        try:
            soc.write("++addr7\r")
            resp = soc.readline()
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        dt = t2 - t1
        t1 = t2
        print("{},1: {} ({:.3f}s)".format(cnt,resp,dt))

        #soc.write("++addr8\r")
        #time.sleep(0.1)
        #soc.write("snap?1,2\r")
        try:
            soc.write("++addr6\r")
            resp2 = soc.readline()
        except socket.timeout:
            pass
        except:
            traceback.print_exc()
            break
        t2 = time.time()
        et = t2 - t
        dt = t2 - t1
        t1 = t2
        print("{}.2: {} ({:.3f}s)".format(cnt,resp2,dt))
        av+=dt; cnt +=1

    print("Two device average response time is {}".format(av/cnt))

def fast_benchmark(lim):
    i=0; et = 0
    t = time.time()
    soc.write("++lon 0\r")
    soc.write("++addr7\r")
    soc.settimeout(1)
    soc.write('fast0\r')
    soc.write('paus\r')
    soc.write('rest\r')

    soc.write('send1\r')
    soc.write('srat9\r')
    soc.write('fast1\r')
    soc.write('strd\r')
    soc.write("++lon 1\r")
    #print soc.sensitivity
    #global res
    while et< lim:
        res = ''
        try:
            res = soc.recv(8)
            #d = np.frombuffer(res,dtype = np.int16)
        except:
            #traceback.print_exc()
            #d = 'nop'
            #soc.write('fast1\r')
            #soc.write('strd\r')
            pass
        t2 = time.time()
        dt = t2-t
        et+=dt
        print(t2-t, res)
        t = t2
    soc.write("++lon 0\r")
    soc.write('fast0\r')
    soc.write('paus\r')

#one_device(5)
#two_device(5)
two_deviceS(5)
#fast_benchmark(5)
#finally:
soc.close()
 #   print "Connection closed"