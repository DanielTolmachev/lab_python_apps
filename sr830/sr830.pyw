#-------------------------------------------------------------------------------
# Name: 	sr830_gpib_eth_qt.pyw
#
# Purpose:   Read out data from SRS SR830 lock-in amplifier 
#		through ethernet (i.e. using gpib to ethernet adaptor) and
#	        publish data using ZMQ messaging library
#
# Created:     30.07.2015 
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"


import numpy as np
import zmq
import socket,sys,time, traceback
sys.path.append( '../modules')
import install_exception_hook
from qtpy import QtCore, QtWidgets,QtGui
from options import Options
from SRS830_central_widget import Ui_main_widget
from QMainWndSavGeom import QMainWindowAutoSaveGeometry
import si_units
import sr830_device_class,sr830_qt_class
import quickQtApp,console_qt,QMenuFromDict,options_editor,device_address_widget

DEFAULT_DEVICE_ADDRESS = ('129.217.155.192',1234)
DEFAULT_DEVICE_PORT = 1234
DEVICE_ADDRESSED_LIST = ['129.217.155.106','129.217.155.188','129.217.155.192']
UDP_IP = "127.0.0.1"
UDP_PORT = 6000
DEFAULT_PUBLISH_PORT = 5002
print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)
DEFAULT_TIMEOUT = 1
SKIP_REPEATED_VALUES = False

config = ''
startup_opts = None
def parse_args(argv):
    if len(sys.argv)>1:
        if sys.argv[1] == 'demo':
            w.setWindowTitle('demo mode')
            global random
            random = True
        for i in range(1,len(sys.argv)-1):
                # if sys.argv[i].startswith('-a'):
                #     addr,port = sys.argv[i+1].split(':')
                #     print 'address set to {}:{}'.format(addr,port)
                #     DEV_ADDR = (addr,int(port))
                # if sys.argv[i].startswith('-p'):
                #     global publish_port
                #     publish_port = int(sys.argv[i+1])
                #     print 'port set to {}'.format(publish_port)
                if sys.argv[i].startswith('-c'):
                    global config
                    config = sys.argv[i+1]
                    print('config is set to {}'.format(config))
                elif sys.argv[i].startswith('-o'):
                    global startup_opts
                    startup_opts_path = sys.argv[i+1]
                    startup_opts = load_dict(sys.argv[i+1])
                    config = startup_opts.get("config",startup_opts_path+".cfg")




    usage = '-address 129.217.155.106:1234 -publishport 5008'


def load_dict(filename):
        ret = {}
        from ast import literal_eval
        try:
            file = open(filename,'r')
        except IOError:
            print('startup configuration file does not exist:\n%s' % filename)
            exists = False
            return
        except:
            print('Unhandled exception, while opening %s' % filename)
            print(sys.exc_info())
            return
        for ln in file.readlines():
            cmnts = ln.rfind('//') #strip comments but beware, double slash can be in string!
            if cmnts > -1:
                ln2 = ln[0:cmnts]
            else:
                ln2 = ln
            try:
                pos = ln2.find('=')
                if pos > 0 and pos<len(ln)-1:
                    key = ln[0:pos].strip()
                    s = ln[pos+1:len(ln)].strip()
                    try:
                        val = literal_eval(s)                        
                        if type(val) == str:
                            s = s[1:].strip('\'\"')
                            val = s.decode('utf8')
                    except:
                        print('wrong line in cfg file')
                        print(sys.exc_info())
                        val= None
##                    if val.startswith('utf8'):
##                        val = eval(val[4:])
##                        if type(val)==str:
##                            val = val.decode('utf8') #decode str to unicode
##                    else:
##                        val = eval(val)
                    if key != '' and val != None:
                        ret[key] = val
            except:
                print("Error reading startup configuration file:", sys.exc_info()[0],",", sys.exc_info()[1])
        file.close()
        print('Startup configuration was loaded from {} ({})'.format(filename,len(ret)))
        return ret


# if __name__ == '__main__':
parse_args(sys.argv)

if startup_opts:

    print('config is set to {}'.format(config))
    opt = Options(config)
    #startup options override previously saved
    device_address = startup_opts.get("device_address",opt.get('device_address',DEFAULT_DEVICE_ADDRESS))
    publish_port =  startup_opts.get('publish_port',opt.get('publish_port',DEFAULT_PUBLISH_PORT))
else:
    opt = Options(config)
    device_address = opt.get('device_address',DEFAULT_DEVICE_ADDRESS)
    publish_port =  opt.get('publish_port',DEFAULT_PUBLISH_PORT)


context = zmq.Context()
sock = context.socket(zmq.PUB)
#sock.bind("tcp://127.0.0.1:5000")






bound = 0
while not bound:
    try:
        #sock.bind('tcp://127.0.0.1:%d'% publish_port)
        zmqaddr = 'tcp://*:%d'% publish_port
        sock.bind(zmqaddr)
        bound = publish_port
        print('bound to %d' % publish_port)
    except socket.error as msg:
        print(msg)
        publish_port+=1
        if publish_port>65535: break;
    except zmq.error.ZMQError as err:
        if err.errno == 156384717: # "Address in use"
            print("zmq address in use",zmqaddr)
            break
        print(sys.exc_info())

def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk

class SensitivityWidget(QtWidgets.QWidget):
    sigSensUp = QtCore.Signal()
    sigSensDn = QtCore.Signal()
    def __init__(self):
        super(SensitivityWidget,self).__init__()
        lo = QtWidgets.QVBoxLayout()
        self.setLayout(lo)
        lo.setSpacing(0)

        #lo.addWidget(QtWidgets.QLabel("Sensitivity"))
        self.tb_sens_up = QtWidgets.QPushButton("+")
        self.tb_sens_dn = QtWidgets.QPushButton("-")
        self.tb_sens_up.setFixedHeight(25)
        self.tb_sens_dn.setFixedHeight(25)
        self.tb_sens_up.pressed.connect(self.sigSensUp.emit)
        self.tb_sens_dn.pressed.connect(self.sigSensDn.emit)
        # self.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
        self.setMaximumWidth(50)
        # lo.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding))
        lo.addWidget(self.tb_sens_up)
        lo.addWidget(self.tb_sens_dn)
        lo.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding))

class MainWidget(QtWidgets.QWidget,Ui_main_widget):
    senslist = [
    2e-9,5e-9,
    1e-8,2e-8,5e-8,
    1e-7,2e-7,5e-7,
    1e-6,2e-6,5e-6,
    1e-5,2e-5,5e-5,
    1e-4,2e-4,5e-4,
    1e-3,2e-3,5e-3,
    1e-2,5e-2,2e-2,
    1e-1,5e-1,2e-1,
    1.]
    sratlist = []
    sens = 1.
    units = {1:'V'}
    t1 = 0
    t2 = 0
    ch1p = np.Inf
    ch2p = np.Inf
    rp = np.Inf
    thetap = np.Inf
    lostcon = 0
    greyedout = False
    sigSensitivityIncorrect = QtCore.Signal()
    def __init__(self):
        super(MainWidget,self).__init__()
        self.setupUi(self)
        self.lbl_rate_rdt = QtWidgets.QLabel('Readout rate: - ')
        self.lbl_rate_upd = QtWidgets.QLabel('Update rate: - ')
        self.lbl_net = QtWidgets.QLabel('Connected to: - ')
        self.gridLayout.addWidget(self.lbl_rate_rdt,2,0)
        #self.gridLayout.addWidget(self.lbl_rate_upd,3,0)
        self.gridLayout.addWidget(self.lbl_net,2,2)
        self.showIntensBars = opt.get("gui.showIntensityBars",False)
        self.sensW = SensitivityWidget()
        self.gridLayout.addWidget(self.sensW,0,3,3,1)
        if not self.showIntensBars:
            self.ch1left.hide()
            self.ch1right.hide()
            self.ch2left.hide()
            self.ch2right.hide()
    def setsens(self,s):
        try:
            # if s[0:2]!='>>':
            #     return
            # i = int(s[2:])
            self.sens = s
            self.sens_min = s*1.e-5
            print('sens is: %g V' % (s))
        except:
            traceback.print_exc()
        #repeater_thrd.start()
    def greyout(self, state):
        self.ch1label.setEnabled(not state)
        self.ch2label.setEnabled(not state)
        self.lbl_rate_rdt.setText('last response was %d sec ago' % DEFAULT_TIMEOUT)
        self.lbl_rate_rdt.setEnabled(not state)
        self.lbl_rate_upd.setText('')
        self.lbl_rate_upd.setEnabled(not state)
        self.greyedout = state
##        if state:
##            self.ch1label.setFont()
    def setDisp(self,ch1,ch2):
        self.ch1p = ch1
        self.ch2p = ch2
        #set labels
        self.ch1label.setText(si_units.si_str(ch1,self.units[1],self.ch1label))
        if self.showIntensBars and self.sens:
            if ch1 >= 0:
                ch1l=0
                ch1r=min(100,ch1/self.sens*100)
            else:
                ch1r=0
                ch1l=min(100,abs(ch1)/self.sens*100)
            #print ch1l, ch1r
            self.ch1left.setValue(ch1l)
            self.ch1right.setValue(ch1r)
        self.ch2label.setText(si_units.si_str(ch2,self.units[1],self.ch1label))
        if self.showIntensBars and self.sens:
            if ch2 >= 0:
                ch2l=0
                ch2r=min(100,ch2/self.sens*100)
            else:
                ch2r=0
                ch2l=abs(max(-100,ch2/self.sens*100))
            self.ch2left.setValue(ch2l)
            self.ch2right.setValue(ch2r)
    def update_data(self, args):
        t = None
        if type(args)==tuple:
            if len(args)==3:
                sock.send_pyobj(args)
                t = args[0]
                self.setDisp(args[1],args[2])
                if self.showIntensBars and self.sens:
                    if abs(args[1])>self.sens or abs(args[2])>self.sens \
                            or (abs(args[1])<self.sens_min and args[1])\
                            or (abs(args[2])<self.sens_min and args[2]):
                        self.sigSensitivityIncorrect.emit()
                #return
            elif len(args)==2:
                t = args[0]
                args = args[1]
            else:
                print('update receive wrong data\n',args)
        elif not args:
            if not t:
                t = time.time()
            if not self.greyedout:
                if self.lostcon:
                    if t-self.lostcon>DEFAULT_TIMEOUT:
                        self.greyout(True)
                        msg = 'no response for more then %d s' % DEFAULT_TIMEOUT
                        self.lbl_rate_rdt.setText(msg)
                else:
                    self.lostcon = time.time()
            else:
                if self.lostcon:
                    self.lbl_rate_rdt.setText('last response was %d sec ago' % (t-self.lostcon))
                else:
                    self.lbl_rate_rdt.setText('no response')
            return
        if self.greyedout:
            self.greyout(False)
        #self.lostcon = 0

        # l = re.findall('-*[0-9]+\.*[0-9]*e*[+-]*[0-9]*',args)
        # try:
        #     ch1 = float(l[0])
        #     ch2 = float(l[1])
        #     r = float(l[2])
        #     theta = float(l[3])
        # except:
        #     print sys.exc_info()
        #     return
        # if random:
        #     ch1 = (np.random.random()-0.5)*2.001
        #     ch2 = (np.random.random()-0.5)*2.001
        # if SKIP_REPEATED_VALUES and\
        #         ch1==self.ch1p and ch2 ==self.ch2p \
        #         and r ==self.rp and theta == self.thetap:
        #     print 'skipped'
        # else:
        #     #remember values for next time
        #     self.ch1p = ch1
        #     self.ch2p = ch2
        #     self.rp = r
        #     self.thetap = theta
        #     #set labels
        #     self.ch1label.setText('%.3e %s'%(ch1,self.units[1]))
        #     if ch1 >= 0:
        #         ch1l=0
        #         ch1r=min(100,ch1/self.sens*100)
        #     else:
        #         ch1r=0
        #         ch1l=min(100,abs(ch1)/self.sens*100)
        #     #print ch1l, ch1r
        #     self.ch1left.setValue(ch1l)
        #     self.ch1right.setValue(ch1r)
        #     self.ch2label.setText('%.3e %s'%(ch2,self.units[1]))
        #     if ch2 >= 0:
        #         ch2l=0
        #         ch2r=min(100,ch2/self.sens*100)
        #     else:
        #         ch2r=0
        #         ch2l=abs(max(-100,ch2/self.sens*100))
        #     self.ch2left.setValue(ch2l)
        #     self.ch2right.setValue(ch2r)
        #     sock.send_pyobj((t, ch1,ch2,r,theta))
        #     if self.t2:
        #         self.updrate = t-self.t2
        #         self.lbl_rate_upd.setText('Update rate: %g s'%self.updrate)
        #     self.t2 = t
        if t:
            self.rdt = t-self.t1
            self.lbl_rate_rdt.setText('Readout rate: %.1f ms' %(self.rdt*1000))
        self.t1 = t
    def font_increase(self):
        f = self.ch1label.font()
        sz = f.pointSize()
        f.setPointSize(sz*2)
        self.ch1label.setFont(f)
        self.ch2label.setFont(f)
        h = self.ch1label.fontMetrics().height()
        w = self.ch1label.fontMetrics().width(self.ch1label.text())
        print('font size is ({},{})'.format(w,h))
        self.ch1label.setMinimumSize(w+5,h*1.2)
        self.ch2label.setMinimumSize(w+5,h*1.2)
        print('label size is ', self.ch1label.size())
        print('sizehint is ', self.ch1label.sizeHint(), end=' ')
        print('min max is ',self.ch1label.minimumSizeHint(),self.ch1label.maximumHeight())
        print('widget size is', self.size(), self. sizeHint())
##        self.ch1label.setFixedHeight(h*2)
##        self.ch2label.setFixedHeight(h*2)

        h = self.ch1left.height()
        self.ch1left.setMinimumHeight(h*2)
        self.ch1right.setMinimumHeight(h*2)
        self.ch2left.setMinimumHeight(h*2)
        self.ch2right.setMinimumHeight(h*2)
        print('bar height is ', self.ch1left.height(),self.ch1left.sizeHint(), self.ch1left.maximumSize(), self.ch1left.minimumSize())

    def font_decrease(self):
        f = self.ch1label.font()
        h = self.ch1label.height()
        sz = f.pointSize()
        f.setPointSize(sz/2)
        self.ch1label.setFont(f)
        self.ch2label.setFont(f)
        h = self.ch1label.fontMetrics().height()
        w = self.ch1label.fontMetrics().width(self.ch1label.text())
        print('font size is ({},{})'.format(w,h))
        self.ch1label.setMaximumSize(w+5,h*1.2)
        self.ch2label.setMaximumSize(w+5,h*1.2)
        print('label size is ', self.ch1label.size())
        print('sizehint is ', self.ch1label.sizeHint(), end=' ')
        print('min max is ',self.ch1label.minimumSizeHint(),self.ch1label.maximumHeight())
        h = self.ch1left.height()
        self.ch1left.setMinimumHeight(h/2)
        print('bar height is ', self.ch1left.height(),self.ch1left.sizeHint(), self.ch1left.maximumSize(), self.ch1left.minimumSize())
        self.ch1right.setMinimumHeight(h/2)
        self.ch2left.setMinimumHeight(h/2)
        self.ch2right.setMinimumHeight(h/2)
    def update_netstat(self,arg):
        if type(arg)==tuple and len(arg)>1:
            self.lbl_net.setText("Connected to {}:{}".format(arg[0],arg[1]))
        else:
            self.lbl_net.setText("Connected to {}".format(arg))

title = 'SR830 DSP lock-in amplifier'
if config:
    title += ' {}'.format(config)


app = QtWidgets.QApplication(sys.argv)
w = QMainWindowAutoSaveGeometry()
w.opted = options_editor.Options_Editor(opt)
w.cons = console_qt.QConsole()

sr = sr830_device_class.SR830(device_address,autoconnect = True,timeout = 1)
control = sr830_qt_class.SR830_GPIB_ETH_receiver(sr)

def saveaddr(addr):
    opt["device_address"] = addr

w_addr = device_address_widget.DeviceAddressWidget(device_address, interfaces=["serial","lan"], default_ip_port=DEFAULT_DEVICE_PORT, device_object=sr)
w_addr.sigConnectionRequested.connect(control.setConnected)
w_addr.sigNewAddressSet.connect(control.setAddress)
w_addr.sigNewAddressSet.connect(saveaddr)



QMenuFromDict.AddMenu(w.menuBar(),[("Tools",[
                                        ("console",w.cons.show),
                                        ("settings",w.opted.show)]),
                                   ("Device",[
                                       ("connect", control.open),
                                       ("disconnect", control.close),
                                       ("change address",w_addr.show)])])

if 'Window.geometry' in opt:
    w.loadGeometry(opt['Window.geometry'])
w.GeometryChangedSig.connect(save_win_position)
w.setWindowTitle(title)

mw = MainWidget() #create main widget
w.setCentralWidget(mw)

#set application icon
icon = QtGui.QIcon("icon.png")
w.setWindowIcon(icon)


sr.open()

control.sig_data_recieved.connect(mw.update_data)
control.sig_connected.connect(mw.update_netstat)
control.sig_sensitivity.connect(mw.setsens)
mw.sensW.sigSensUp.connect(control.increaseSens)
mw.sensW.sigSensDn.connect(control.decreaseSens)

if sr.isOpen():
    mw.update_netstat(sr.address)
control.querySens()

##mw.ch1left.hide()
##mw.ch1right.hide()
##mw.ch2left.hide()
##mw.ch2right.hide()
#w.setmainwidget(mw) #set that widget as main
##w.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
##w.frame.setContentsMargins(0,0,0,0)
##bw.setport(w.serport)
##bw.readout_done.connect(w.echo)

#action_view = QtWidgets.QAction('View',w)
action_font_increase = QtWidgets.QAction('Increase font',w)
action_font_increase.triggered.connect(mw.font_increase)
action_font_decrease = QtWidgets.QAction('Decrease font',w)
action_font_decrease.triggered.connect(mw.font_decrease)
action_sensitivity = QtWidgets.QAction('query Sensitivity',w)
action_sensitivity.triggered.connect(control.querySens)
action_auto_gain = QtWidgets.QAction('auto Gain',w)
action_auto_gain.triggered.connect(control.autoGain)
action_auto_phase = QtWidgets.QAction('auto Phase',w)
action_auto_phase.triggered.connect(control.autoPhase)

#w.menubar.addAction(action_view)
menu_view = w.menuBar().addMenu('View')
menu_view.addAction(action_font_increase)
menu_view.addAction(action_font_decrease)
menu_settings = w.menuBar().addMenu('Settings')
menu_settings.addAction(action_sensitivity)
menu_settings.addAction(action_auto_gain)
menu_settings.addAction(action_auto_phase)

#mw.sigSensitivityIncorrect.connect(rcv.querySens)

##repeater_thrd.setport(w.serport)
##repeater_thrd.timed_data_ready.connect(mw.update)
w.show()
##repeater_thrd.cmd = 'snap?1,2,3,4\n\r'
##repeater_thrd.lineend = ''
##repeater_thrd.timeout = DEFAUL_SERIAL_TIMEOUT
##repeater_thrd.verbose = False


##if w.serport.isOpen():
##    w.serport.write('sens?\n\r')
##    bw.readout_done.connect(mw.setsens)
##    bw.start()



random =False


if __name__ == '__main__':
    #parse_args(sys.argv)
    control.start()
    app.exec_()
control.stop()
sock.close()
context.destroy()
opt.savetodisk()
