import sys,time
sys.path.append("../modules")
import zmq,zmq_publisher
from LCR_device_class import LCR
DEVICE_ADDRESS = '129.217.155.103'
DEVICE_PORT = 51234
cont = zmq.Context()
pub = zmq_publisher.Zmq_Publisher(cont,"tcp://*:5017")

def sweep(start,stop,step,dt, tot):
    ts = time.time()
    t=ts
    tstop = ts+tot
    time.sleep(1)
    pub.send_pyobj("start")
    i = 0
    st = step
    while tot==0 or t<tstop:
    # for i in range(start,stop+start):
        lcr.ask("N0W{}:".format(i))
        t = time.time()
        pub.send_pyobj((t,i))
        # print(i)
        time.sleep(dt)
        i+=st
        if i<0:
            st = step
        elif i>stop:
            st = -step
    pub.send_pyobj("stop")


lcr = LCR((DEVICE_ADDRESS, DEVICE_PORT))
lcr.open()
sweep(0, 3995, 1, .1,0)
lcr.close()
