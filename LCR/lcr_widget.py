from qtpy import QtWidgets,QtCore,QtGui
import os,traceback,re
import numpy as np
from atof_comma_safe import atof

CALIBR_FILE_EXT = [".txt",".dat"]
CALIBR_DIR = "LCRcalibration"
MAX_PWR_DEFAULT = 50 # max power of laser in mw

class LCR_Widget(QtWidgets.QWidget):
    DAC_changed = QtCore.Signal(int)
    def __init__(self,lcr_device_class):
        self.lcr = lcr_device_class
        super(LCR_Widget,self).__init__()
        self.setupui()
        self.controller = LCR_Controller(self.lcr)
        self.conthrd = QtCore.QThread()
        self.controller.moveToThread(self.conthrd)
        self.conthrd.start()
        self.controller.gotValues.connect(self.setGuiValues)
        self.controller.connected.connect(self.connectionChange)
        self.DAC_changed.connect(self.controller.setDAC)
        # self.pw.DAC_changed.connect(self.DAC_changed.emit)
        self.pw.DAC_changed.connect(self.DACchange)
        self.pw.maxPwrChanged.connect(self.saveToDict)
        self.pw.calibrLoaded.connect(self.saveToDict)
        self.DAC_changed.connect(self.pw.setDAC)
        QtCore.QMetaObject.invokeMethod(self.controller, "open")
        QtCore.QMetaObject.invokeMethod(self.controller, "getValues")
    def setupui(self):
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)
        # self.sb_daq = QtWidgets.QLineEdit()
        self.sb_daq = QtWidgets.QSpinBox()
        self.sb_daq.setMaximum(self.lcr.MAX_DAC)
        self.sb_daq.setSingleStep(1)
        self.sb_daq.setMinimum(-10)
        self.sb_daq.valueChanged.connect(self.DACchange)
        self.le_volts = QtWidgets.QDoubleSpinBox()
        self.le_volts.setSingleStep(0.1)
        self.le_volts.setMinimum(-0.1)
        self.le_volts.setMaximum(self.lcr.MAX_VOLT)
        self.le_volts.setSpecialValueText("-")
        self.le_volts.valueChanged.connect(self.VoltChange)
        self.le_retard = QtWidgets.QDoubleSpinBox()
        self.le_retard.setSpecialValueText("-")

        for le in [self.sb_daq, self.le_retard, self.le_volts]:
            le.setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        self.lbl_connection = QtWidgets.QLabel("tcp://{}:{}\tnot connected".format(
            self.lcr.address[0],self.lcr.address[1]))
        row = 0
        self.gl.addWidget(QtWidgets.QLabel("DAQ"),row,0)
        self.gl.addWidget(self.sb_daq, row, 1)
        self.gl.addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum),row,2)
        row+=1
        self.gl.addWidget(QtWidgets.QLabel("Voltage"), row, 0)
        self.gl.addWidget(self.le_volts,row,1)
        self.gl.addWidget(QtWidgets.QLabel("V"), row, 2)
        row+=1
        # self.gl.addWidget(QtWidgets.QLabel("Retardance"), 2, 0)
        # self.gl.addWidget(self.le_retard,2,1)
        # self.gl.addWidget(QtWidgets.QLabel(""), 2, 2)
        # self.gl.addItem(QtWidgets.QSpacerItem(0,10,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding))
        row+=1

        self.pw = PowerWidget()
        self.gl.addWidget(self.pw, 0,3,row,1)
        # row+=1
        self.gl.addItem(QtWidgets.QSpacerItem(0, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding),row,3)
        row+=1
        self.gl.addWidget(self.lbl_connection, row, 0, 1, 4)
    def connectDict(self,opt):
        self.opt = opt
        cal = opt.get("LCR.Power_calibration_file")
        max_pwr = opt.get("LCR.Laser_full_power")
        self.pw.selectCal(cal)
        self.pw.setMaxPwr(max_pwr)
    def saveToDict(self,_):
        self.opt["LCR.Power_calibration_file"] = self.pw.calibrationFile
        self.opt["LCR.Laser_full_power"] = self.pw.max_pwr
    def setGuiValues(self,arg):
        i,dac,ret = arg
        if dac:
            self.sb_daq.setValue(dac)
            v = self.lcr.DACtoV(dac)
            self.le_volts.setValue(v)
            self.le_retard.setValue(ret)
        else:
            self.sb_daq.setValue(-1)
            self.le_volts.setValue(-1)
            self.le_retard.setValue(0)
    def DACchange(self,dac):
        if dac<0:
            dac = 0
            self.sb_daq.setValue(0)
        elif dac>self.lcr.MAX_DAC:
            dac = self.lcr.MAX_DAC
            self.sb_daq.setValue(dac)
        elif self.sender() != self.sb_daq:
            self.sb_daq.setValue(dac)
        self.DAC_changed.emit(dac)
        v = self.lcr.DACtoV(dac)
        self.le_volts.setValue(v)
        self.le_retard.setValue(0)
    def VoltChange(self,v):
        if v<0:
            v = 0
            self.le_volts.setValue(v)
        dac = self.lcr.VtoDAC(v)
        self.sb_daq.setValue(dac)
        self.DAC_changed.emit(dac)
    def connectionChange(self,status):
        if status:
            self.lbl_connection.setText("tcp://{}:{}\tconnected".format(
                self.lcr.address[0], self.lcr.address[1]))
        else:
            self.lbl_connection.setText("tcp://{}:{}\tnot connected".format(
                self.lcr.address[0], self.lcr.address[1]))

class DoubleSpinBoxA(QtWidgets.QDoubleSpinBox):
    def __init__(self,precision_relative = 1e-2):
        super(DoubleSpinBoxA, self).__init__()
        self.precision_relative = precision_relative
    def stepBy(self,step):
        f = self.value()
        if step>0:
            st = self.maximum() * .1
        else:
            st = self.maximum() * -.1
        step = abs(step)
        while step>0:
            if f!=0:
                r = f/self.maximum()
                while r<=0.2:
                    r = r*10
                    st = st/10
            f = f+st
            step-=1
        self.setValue(f)
    def validate(self,p_str,p_int):
        try:
            f = atof(p_str)
            if f>self.maximum() or f<self.minimum():
                return (QtGui.QValidator.Intermediate,p_str, p_int)
            mag = -np.floor(np.log10(f*self.precision_relative))
            if mag>=0:
                self.setDecimals(mag)
            return (QtGui.QValidator.Acceptable,p_str,p_int)
        except:
            return (QtGui.QValidator.Invalid,p_str,p_int)

    def valueFromText(self, p_str):
        return atof(p_str)

class PowerWidget(QtWidgets.QFrame):
    calibrLoaded = QtCore.Signal(str)
    maxPwrChanged = QtCore.Signal(float)
    DAC_changed = QtCore.Signal(int)
    def __init__(self):
        super(PowerWidget,self).__init__()
        self.calibr = None
        self.setFrameStyle(self.Sunken)
        self.setFrameShape(self.Box)
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)
        self.gl.addWidget(QtWidgets.QLabel("Laser power control"),0,0,1,2)
        self.cb_calibr = QtWidgets.QComboBox()
        self.cb_calibr.setToolTip("you can put calibration files into subdirectory:"+CALIBR_DIR+\
                                  "each calibration file should be text file with two columns:")
        self.sb_pwr = DoubleSpinBoxA()
        self.sb_pwr.setSpecialValueText("NA")
        self.sb_pwr_max = QtWidgets.QDoubleSpinBox()
        self.max_pwr = MAX_PWR_DEFAULT
        self.sb_pwr_max.valueChanged.connect(self.maxPwrChange)
        self.sb_pwr_max.setValue(self.max_pwr)
        f = self.sb_pwr.font()
        # f = QtWidgets.QFont()
        f.setPointSize(f.pointSize()*2)
        self.sb_pwr.setFont(f)
        self.gl.addWidget(self.sb_pwr_max)
        self.gl.addWidget(self.cb_calibr,1,1)
        self.gl.addWidget(self.sb_pwr,2,0,1,2)
        # self.gl.addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        self.loadCalList()
        self.cb_calibr.activated.connect(self.selectCal)
        self.sb_pwr.valueChanged.connect(self.PwrChange)
        #self.sb_pwr.editingFinished.connect(self.PwrChange)
        #self.sb_pwr.text
    def loadCalList(self):
        if not os.path.exists(CALIBR_DIR):
            os.mkdir(CALIBR_DIR)
        if os.path.exists(CALIBR_DIR) and os.path.isdir(CALIBR_DIR):
            l = os.listdir(CALIBR_DIR)
            self.calibrations = [f for f in l if os.path.splitext(f)[1] in CALIBR_FILE_EXT]
        else:
            self.calibrations = []
        self.cb_calibr.clear()
        self.cb_calibr.addItem("no calibration")
        self.cb_calibr.addItems(self.calibrations)
    def selectCal(self,calibration_file):
        if type(calibration_file)== int:
            index = calibration_file-1
            calibration_file = self.calibrations[index]
            fn = CALIBR_DIR + os.path.sep + calibration_file
        elif calibration_file is None:
            return
        else:
            fn = CALIBR_DIR+os.path.sep+calibration_file
            if not calibration_file in self.calibrations:
                if os.path.exists(fn):
                    self.calibrations.append(calibration_file)
                    self.cb_calibr.addItems(calibration_file)
                else:
                    print("file",calibration_file,"not found")
                    return
            index = self.calibrations.index(calibration_file)

        try:
            n = np.genfromtxt(fn)
            if n.ndim<2:
                print("file", calibration_file, "contains wrong shape array:", n.shape)
                return
            self.calibr = n
            self.calibr_dac_max = self.calibr[:,0].max()
            self.calibr_dac_min = self.calibr[:, 0].min()
            print("loaded calibration from", calibration_file, n.shape)
            self._setMult()
            self.cb_calibr.setCurrentIndex(index+1)
            self.calibrationFile = calibration_file
            self.calibrLoaded.emit(calibration_file)
            if n.shape[0]*2<self.calibr_dac_max-self.calibr_dac_min:
                dac = np.arange(self.calibr_dac_min, self.calibr_dac_max + 1)
                pwr = np.interp(dac, self.calibr[:, 0], self.calibr[:, 1])
                self.calibr = np.vstack((dac,pwr)).transpose()
                print("calibration table is too sparse, interpolating to", self.calibr.shape)
        except:
            print("cannot import file", calibration_file)
            traceback.print_exc()
    def _setMult(self):
        if self.calibr is not None:
            self.mult = self.max_pwr/self.calibr[:,1].max()
            print(self.calibr[:,1].max(),self.mult)
    def maxPwrChange(self,pwr):
        self.max_pwr = pwr
        self.sb_pwr.setMaximum(self.max_pwr)
        self.maxPwrChanged.emit(pwr)
        self._setMult()
    def setMaxPwr(self,pwr):
        if pwr is not None:
            self.sb_pwr_max.setValue(pwr)
            self.maxPwrChange(pwr)
    def PwrChange(self):
        pwr = self.sb_pwr.value()
        if self.calibr is not None:
            val = pwr/self.mult
            idx = (np.abs(self.calibr[:,1] - val)).argmin()
            dac = int(self.calibr[idx,0])
            print(val,idx,dac)
            self.DAC_changed.emit(dac)
        else:
            print("calibration is not set")
    def setDAC(self,dac):
        if self.calibr is not None:
            self.sb_pwr.blockSignals(True)
            if self.calibr_dac_min<=dac<=self.calibr_dac_max:
                idx = (np.abs(self.calibr[:, 0] - dac)).argmin()
                pwr = self.calibr[idx,1]*self.mult
                self.sb_pwr.setValue(pwr)
            else:
                self.sb_pwr.setValue(0)
            self.sb_pwr.blockSignals(False)





class LCR_Controller(QtCore.QObject):
    gotValues = QtCore.Signal(object)
    connected = QtCore.Signal(bool)
    def __init__(self,lcr_device_class):
        super(LCR_Controller,self).__init__()
        self.lcr = lcr_device_class
    @QtCore.Slot()
    def open(self):
        self.lcr.open()
        self.connected.emit(self.lcr.isOpen())
    @QtCore.Slot()
    def getValues(self):
        rep,i,dac,ret = self.lcr._queryStatus()
        if i==None:
            print(rep)
        self.gotValues.emit((i,dac,ret))

    @QtCore.Slot(int)
    def setDAC(self,dac):
        self.lcr.setDAC(dac)
    def setIndex(self,index):
        self.lcr.setIndex(index)


