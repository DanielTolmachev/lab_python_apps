#-------------------------------------------------------------------------------
# Name:     lcr.py
# Purpose:  control Variable Liquid Crystal Retarter through Ethernet
#           made for e3.physik.tu-dortmund.de (manual number 356)
#
#           uses Qt user interface and
#           publish data using ZMQ messaging library
#
# Created:     04.01.2017
#
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import sys
sys.path.append("../modules")
import quickQtApp,QMenuFromDict
import lcr_widget
from LCR_device_class import LCR

DEVICE_ADDRESS = '129.217.155.103'
DEVICE_PORT = 51234

app,opt,win,cons = quickQtApp.mkAppWin("Liquid Crystal Retarder",icon = "lcr.png")
lcr = LCR((DEVICE_ADDRESS,DEVICE_PORT))
lcrw = lcr_widget.LCR_Widget(lcr)
lcrw.connectDict(opt)
QMenuFromDict.AddMenu(win.menuBar(),{"device":{"refresh":lcrw.controller.getValues,
                                               "connect":lcrw.controller.open}})
win.setCentralWidget(lcrw)
win.show()

app.exec_()

opt.savetodisk()