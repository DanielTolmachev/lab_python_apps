from __future__ import print_function
import sys,traceback
import numpy as np
sys.path.append("../modules")
import instrument_base

DEVICE_ADDRESS = '129.217.155.103'
DEVICE_PORT = 51234


class LCR(instrument_base.InstrumentBase):
    MAX_DAC = 3995
    def __init__(self,*args,**kwargs):
        super(LCR,self).__init__(*args,**kwargs)
        self.table = {}
        self.MAX_VOLT = self.DACtoV(self.MAX_DAC)

    def DACtoV(self,x):
        if x==None:
            return None
        a = 0.00292880009931
        b = 0.00272739653127
        return a * x - b

    def VtoDAC(self,x):
        a = 0.00292880009931
        b = 0.00272739653127
        return round(1 / a * (x + b))

    def PtoV(self, x, params):
        a = params['a']
        b = params['b']
        c = params['c']
        d = params['d']
        return 1 / b * np.log(a / (x - d) - 1) - c

    def VtoP(self, x, params):
        a = params['a']
        b = params['b']
        c = params['c']
        d = params['d']
        return a / (np.exp(b * (x + c)) + 1) + d

    def setDAC(self, DACval):
        rep = self.loadIndex(0,DACval)
        return rep,self.setIndex(0)

    def loadIndex(self,index,DACval):
        return self.ask('N'+str(index)+'W' + str(DACval) + ':\r')

    def readIndex(self,index):
        ret = self.ask("N{:d}R:".format(index))
        i,dac,ret = self._parseResponse(ret)
        if i==index:
            self.table[index] = (dac,ret)
        else:
            print("Asked index #",index, "got",ret)
        return ret

    def setIndex(self,index):
        return self.ask("N{:d}S:".format(index))
    def _parseResponse(self,bytes_string):
        i = None; dac = None; ret = None
        tup = bytes_string.strip().split(b",")
        if len(tup) == 3:
            i = int(tup[0])
            dac = int(tup[1])
            ret = float(tup[2])
        return i,dac,ret

    def _queryStatus(self):
        i = None; dac = None; ret = None
        rep = self.ask("X:")
        try:
            tup = rep.strip().split(b",")
            if len(tup) == 3:
                tup[0] = int(tup[0])
                self.current_index = tup[0]
                i = self.current_index
                tup[1] = int(tup[1])
                self.current_DAC = tup[1]
                dac = self.current_DAC
                tup[2] = float(tup[2])
                self.current_retardance = tup[2]
                ret = self.current_retardance
        except:
            traceback.print_exc()
        finally:
            return rep,i,dac,ret
    def status(self):
        """
        :return: index, DAC, retardance (raw bytes)
        """
        return self._queryStatus()[0]
    def getValues(self):
        """
        :return: tuple: index, DAC, retardance (None in case of error)
        """
        try:
            _,i,dac,ret =  self._queryStatus()
            return i,dac,ret
        except:
            traceback.print_exc()
        finally:
            return None,None,None



if __name__ == "__main__":
    lcr = LCR((DEVICE_ADDRESS,DEVICE_PORT))
    lcr.open()
