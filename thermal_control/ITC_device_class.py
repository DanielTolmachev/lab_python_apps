__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

DEV_ADDRESS = "129.217.155.3"
DEV_PORT = 1234
DEV_GPIB_ADDRESS = 24

import sys,traceback
sys.path.append("../modules")

import instrument_base
class ITC(instrument_base.InstrumentBase):
    def __init__(self,*args,**kwargs):
        super(ITC,self).__init__(*args,**kwargs)
        self.gpib_address = kwargs.get("gpib_address",DEV_GPIB_ADDRESS)
    def getTemperature(self,sensor=2):
        rep = self.ask("++addr{}\rR{}\r".format(self.gpib_address,sensor))
        # rep = self.ask("R{}\r".format(sensor))
        try:
            if rep and rep.startswith(b"R+"):            
                return float(rep[2:].strip())/10
        except:
            traceback.print_exc()
            return rep
            


if __name__=="__main__":
    def find_gpib(dev):
        for adr in range(1,29):
            print(adr,dev.ask("++addr{}\r\n*IDN?\r\n".format(adr)))
    itc = ITC((DEV_ADDRESS,DEV_PORT),gpib_address = DEV_GPIB_ADDRESS)
    itc.open()
    # find_gpib(itc)