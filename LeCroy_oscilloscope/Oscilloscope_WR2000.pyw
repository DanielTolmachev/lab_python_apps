# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Purpose:   Read out and display data from Lecroy WaveAce oscilloscolpe.
#	     Can calculate some statistics and save waveforms as text files
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

#address = "129.217.155.164"
address = "129.217.155.196"
IP_ADDRESS_LIST = ["129.217.155.164","129.217.155.196"]
port = 1861
PUBLISH_ADDRESS = 'tcp://*:5012'
PUBLISH_ADDRESS_PWR = 'tcp://*:5013'
PUBLISH_ADDRESS_VOLTAGE = 'tcp://*:5014'
DEFAULT_EXT = "dat"
DEVICE_NAME = "Lecroy WaveAce2024"
channels = [1]
publish = [(1,"f")]
publish_voltage = [(2,"Vrms")]
publish_pwr = [1,2]
demo = False
use_remote_plot = True

autoscale = {}
#autoscale = {1:2,2:2,3:2,4:2}

import sys,time,os,datetime,socket,traceback
sys.path.append( '../modules')
import scope3
from qtpy import QtWidgets, QtCore, QtGui
import pyqtgraph as pg
import pyqtgraph.widgets.RemoteGraphicsView
# import numpy as np
from numpy import argmax,nan,log,where,diff,sign,mean,linspace,column_stack,savetxt,sin,sqrt,square
from numpy.random import random
from numpy.fft import rfft
import zmq
from zmq_publisher import Zmq_Publisher
import options, quickQtApp
# from fft import *
from si_units import *

def freq_from_crossings(sig, fs):
    """
    Estimate frequency by counting zero crossings
    """
    # Find all indices right before a rising-edge zero crossing
    #indices = np.find((sig[1:] >= 0) & (sig[:-1] < 0))
    indices = where(diff(sign(sig)))[0]

    # Naive (Measures 1000.185 Hz for 1000 Hz, for instance)
    # crossings = indices

    # More accurate, using linear interpolation to find intersample
    # zero-crossings (Measures 1000.000129 Hz for 1000 Hz, for instance)
    crossings = [i - sig[i] / (sig[i+1] - sig[i]) for i in indices]

    # Some other interpolation based on neighboring points might be better.
    # Spline, cubic, whatever

    return 1./ mean(diff(crossings))/fs/2

def spectrum(x,y):
    m = abs(rfft(y))

    f = linspace(0,.5/(x[1]-x[0]),len(m))
    return f,m

channels_count = 4
stats_count = 5
autoscale_threshold = .1


options = options.Options()
context = zmq.Context()
pub = Zmq_Publisher(context,PUBLISH_ADDRESS)
pub_pwr = Zmq_Publisher(context,PUBLISH_ADDRESS_PWR)
pub_volt = Zmq_Publisher(context,PUBLISH_ADDRESS_VOLTAGE)


#app = pg.mkQApp()
app,opt,win,cons = quickQtApp.mkAppWin(DEVICE_NAME,device_command_line = True)
scope = scope3.Scope3(host = address)

use_remote_plot  = opt.get("gui.use_pyqtgragh_remote_plot",True)

action_connect = QtWidgets.QAction('connect',win)
win.menuBar().addAction(action_connect)
def menu_connect_set():
    if scope.connected:
        action_connect.setText("disconnect")
    else:
        action_connect.setText("connect")
def connect():
    if not scope.connected:
        print("connecting")
        scope.connect()
    else:
        scope.close()
    menu_connect_set()
action_connect.triggered.connect(connect)
menu_connect_set()

view = pg.widgets.RemoteGraphicsView.RemoteGraphicsView()
pg.setConfigOptions(antialias=True)  ## this will be expensive for the local plot
view.pg.setConfigOptions(antialias=True)  ## prettier plots at no cost to the main process!



counter = 0
label = QtWidgets.QLabel()
label_freq = QtWidgets.QLabel("f = -")
rcheck = QtWidgets.QCheckBox('plot remote')
rcheck.setChecked(True)
lcheck = QtWidgets.QCheckBox('plot local')
lplt = pg.PlotWidget()
layout = pg.LayoutWidget()
##layout.addWidget(rcheck)
##layout.addWidget(lcheck)
win.setCentralWidget(layout)

stat_table = QtWidgets.QTableWidget()
p = stat_table.palette()
p.setColor(QtGui.QPalette.Base, QtGui.QColor(0, 0, 0))
stat_table.setPalette(p)
stat_table.setRowCount(channels_count*stats_count)
stat_table.setColumnCount(2)
stat_table.horizontalHeader().hide()
stat_table.verticalHeader().hide()
stat_table.verticalHeader().setDefaultSectionSize(stat_table.verticalHeader().fontMetrics().height()+2)
stat_table.horizontalHeader().resizeSection(0,stat_table.horizontalHeader().fontMetrics().width("WWWWWWWW"))
#stat_table.horizontalHeader().setStretchLastSection(False)

## Create a PlotItem in the remote process that will be displayed locally
rplt = view.pg.PlotItem()
rplt._setProxyOptions(deferGetattr=True)  ## speeds up access to rplt.plot
view.setCentralItem(rplt)
view.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)


#layout.addWidget(label,row = 0,col = 1)
#layout.addWidget(label_freq,row = 0,col = 2)
lpw  = pg.PlotWidget()
lplt = lpw.getPlotItem()

layout.addWidget(lpw, row=1, col=1, colspan=3,rowspan = channels_count+1)
layout.addWidget(view, row=1, col=1, colspan=3,rowspan = channels_count+1)
if use_remote_plot:
    lpw.hide()
else:
    view.hide()

# fftview = pg.widgets.RemoteGraphicsView.RemoteGraphicsView()
# fftplt = fftview.pg.PlotItem()
# layout.addWidget(fftview, row=1, col=4, colspan=1,rowspan = channels_count+1)
# fftview.setMaximumWidth(0)

layout.addWidget(stat_table, row=1, col=4, colspan=1,rowspan = channels_count+1)
stat_table.setMaximumWidth(400)
layout.show()
#layout.setWindowTitle(DEVICE_NAME)
win.show()

#set application icon
icon = QtGui.QIcon("icon.png")
layout.setWindowIcon(icon)




ch_b = []
curves ={}
ranges = {}
stats = {}; stats2 = {}
def set_channels():
    for i,b in enumerate(ch_b):
        if b.isChecked() and i+1 not in channels:
            channels.append(i+1)
            curves[i+1].show()
        elif not b.isChecked() and i+1 in channels:
            channels.remove(i+1)
            curves[i+1].hide()
            stats[chan] = {}
            stats2[chan] = {}
    stat_table.clear()
    options["osc.channels"] = channels

tb = QtWidgets.QToolBar()
tb.addWidget(QtWidgets.QToolButton())

channels = options.get("osc.channels",[1,2,3,4])
for ch in range(1,channels_count+1):
    b = QtWidgets.QToolButton()
    b.setText("ch{}".format(ch))
    b.setCheckable(True)
    b.toggled.connect(set_channels)
    stats[ch] = {}
    if ch in channels:
        b.setChecked(True)
    ch_b.append(b)
    layout.addWidget(b,row=ch,col = 0)

def save():
    file = str(save_dlg.selectedFiles()[0])
    ext = os.path.splitext(file)
    if not ext:
        file += "." + DEFAULT_EXT

    arr = None
    for chan in sorted(channels):
        c = curves[chan]
        x,y = c.getData()
        if y!=None:
            if arr==None:
                arr = column_stack((x,y))
            else:
                arr = column_stack((arr,y))
    print(arr.shape)
    options["save.directory"] = os.path.dirname(file)
    if arr.any() or stats:
        with open(file,"w+") as f:
            print("saving to ",file)
            f.write("# {}\n".format(DEVICE_NAME))
            dt = datetime.datetime.now().strftime("# %Y-%m-%d %H:%M:%S\n")
            f.write(dt)
            for chan in sorted(channels):
                if chan in stats and stats[chan]:
                    f.write("# statictics on channel {}:\n".format(chan))
                    for key,val in list(stats[chan].items()):
                        f.write("#\t{} = {}\n".format(key,val))
            column_labels = "\t".join(["ch{}".format(ch) for ch in sorted(channels)])
            f.write("\ntime\t{}\n".format(column_labels))
            savetxt(f,arr,fmt='%g', delimiter='\t', newline='\n')

btn_save = QtWidgets.QToolButton()
btn_save.setText("Save")
save_dlg = QtWidgets.QFileDialog()
save_dlg.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
save_dlg.setDirectory(options.get("save.directory","."))
save_dlg.setDefaultSuffix(DEFAULT_EXT)
print(save_dlg.defaultSuffix())
save_dlg.setFilters(["ASCII files (*.txt *.dat *.tsv)","All files (*)"])
btn_save.pressed.connect(save_dlg.show)
save_dlg.accepted.connect(save)

colors = ["y","c","g","r"]
for chan in range(1,channels_count+1):
    crv = rplt.plot(clear=False, pen = colors[chan-1])
    curves[chan] = crv
print(curves)
rplt.setLabel("bottom","Time","s")
rplt.setLabel("left","Voltage","V")
lplt.setLabel("bottom","Time","s")
lplt.setLabel("left","Voltage","V")



lastUpdate = pg.ptime.time()
avgFps = 0.0



plotrange = {}
def switchfft(status):
    #print rplt.getLabel("bottom")
    ab = rplt.getAxis("bottom")
    al = rplt.getAxis("left")
    if status:
        plotrange[ab.labelText] =  [ab.range,al.range]
        rplt.setLabel("bottom","Frequency","Hz")
        if "Frequency" in plotrange:
            rplt.setRange(xRange = plotrange["Frequency"][0],yRange = plotrange["Frequency"][1])
    else:
        plotrange[ab.labelText] =  [ab.range,al.range]
        rplt.setLabel("bottom","Time","s")
        if "Time" in plotrange:
            rplt.setRange(xRange = plotrange["Time"][0],yRange = plotrange["Time"][1])


btn_fft = QtWidgets.QToolButton()
btn_fft.setCheckable(True)
btn_fft.setText("FFT")
btn_fft.toggled.connect(switchfft)

def switchaccum(status):
    global accnt,yacc
    accnt = 0
    yacc = None

btn_accum = QtWidgets.QToolButton()
btn_accum.setCheckable(True)
btn_accum.setText("accum")
btn_accum.toggled.connect(switchaccum)


layout.addWidget(btn_save,row = channels_count+3,col = 0)
layout.addWidget(btn_fft,row = channels_count+3,col = 1)
layout.addWidget(btn_accum,row = channels_count+3,col = 2)

def demo_data(chan):
    xmax = scope.dx*10000
    x = linspace(0,xmax,10000)
    y = sin(x*chan*10)+sin(x*random())*0.1
    time.sleep(0.01)
    return x,y



def update_data(chan):
    return scope.getwaveform(chan)
    #return scope.getwaveform2()

def update_stats(chan):
    if stats[chan] and chan in channels:
        c = colors[chan-1]
        cc = pg.mkColor(c)
        cnt = 0
        idx = channels.index(chan)
        for key,val in list(stats[chan].items()):
            it = QtWidgets.QTableWidgetItem(key)
            it.setTextColor(cc)
            stat_table.setItem((idx)*stats_count+cnt,0,it)
            it = QtWidgets.QTableWidgetItem(val)
            it.setTextColor(cc)
            stat_table.setItem((idx)*stats_count+cnt,1,it)
            cnt = cnt+1
            #print key,val

def si_units(val,units):
    return si_fmt(val,units)

def format_units(val,units):
    if not isnan(val):
        return "{:g} {}".format(*si_units(val,units))
    else:
        return ""

def calc_stats(x,y):
    ret = {}
    ret2 = {}
    try:
        ptp = y.ptp()
        mean = y.mean()
        rms = sqrt((square(y).mean()))
        dbm = log10(rms**2/50*1000)*10
        ret2["mean"] = mean
        #if mean>ptp/10:
        ret["mean"] = format_units(mean,"V")
        ret["Vpp"] = format_units(ptp,"V")
        ret2["Vpp"] = ptp
        # freq,freqerr = freq_from_fft(y,scope.dx)
        freq = freq_from_crossings(y,scope.dx)
        ret["Vrms"] = format_units(rms,"V")
        ret2["Vrms"] = rms
        ret["PWR 50Ohm"] = "{:.1f} dBm".format(dbm)
        ret2["PWR 50Ohm"] = dbm
        ret["f"] = format_units(freq,"Hz")
        ret2["f"] = freq
    except:
        traceback.print_exc()
    return ret,ret2


xacc = yacc = None
acccnt = 0
def update(xxx_todo_changeme):
    (t,chan,x,y) = xxx_todo_changeme
    global check, label, plt, lastUpdate, avgFps, rpltfunc,label_freq,counter,xacc,yacc,acccnt
    #data = np.random.normal(size=(10000,50)).sum(axis=1)
    #data += 5 * np.sin(np.linspace(0, 10, data.shape[0]))

    # x,y = update_data()


    if btn_fft.isChecked():
        x,y = spectrum(x,y)
    #rplt.plot(x,y, clear=True, _callSync='off')  ## We do not expect a return value.
                                                  ## By turning off callSync, we tell
                                                  ## the proxy that it does not need to
                                                  ## wait for a reply from the remote
                                                  ## process.
    if btn_accum.isChecked():
        if xacc==None or len(x)!=len(xacc): #or not (xacc!=x).any():
            print("accum reset")
            xacc = x
            yacc = None
            acccnt = 0
##            if (xacc!=x).any():
##                print "hmm",(xacc!=x).any()
        if yacc == None:
            yacc = y
            acccnt = 1
        else:
            if len(y)!=len(yacc):
                pass
            yacc = yacc*acccnt + y
            acccnt +=1
            yacc = yacc/acccnt
        curves[chan].setData(xacc,yacc,_callSync='off')
    else:
        if use_remote_plot:
            curves[chan].setData(x,y,_callSync='off')
        else:
            lplt.plot(x,y, clear=True)

##
##


    fps = 1.0 / (t - lastUpdate)
    lastUpdate = t
    avgFps = avgFps * 0.8 + fps * 0.2
    counter +=1
    label.setText("Generating %0.2f fps, readout counts = %d" % (fps,counter))
    # stats[chan] = calc_stats(x,y)
    #update_stats(chan)
    #pub.send_pyobj((t,freq))

rangedict = {}

class UpdateData(QtCore.QObject):
    updated = QtCore.Signal(object)
    need_update = QtCore.Signal()
    connected = must_connect = False
    sigIOActivity = QtCore.Signal()
    sigConnectionChanged = QtCore.Signal(bool)
    sigDeviceResponded = QtCore.Signal(str)
    def __init__(self,autoconnect = False):
        super(UpdateData,self).__init__()
        #self.need_update = QtCore.pyqtBoundSignal(self)
        self.upd_signals = 0
        self.need_update.connect(self._update_data,QtCore.Qt.QueuedConnection)
        self.autoconnect = autoconnect
        self.stopped = False
    def tryit(self,func,*args,**kwargs):
        try:
            ret = func(*args,**kwargs)
            self.sigIOActivity.emit()
            return ret
        except socket.error:
            print("Exception in {}".format(func))
            traceback.print_exc()
        except Exception as ex:
            raise ex
    def write(self,cmd):
        self.tryit(scope.send,cmd)
    def ask(self,query):
        ret = self.tryit(scope.ask,query)
        if ret:
            self.sigDeviceResponded.emit(ret)
    def update_data(self):
        if not self.upd_signals:
            self.need_update.emit()
        self.upd_signals+=1
    def _update_data(self):
        #print "update data was called",self.upd_signals
        self.upd_signals = 0
        if channels and not self.stopped:
            for chan in channels:
                try:
                    if chan not in rangedict:
                        rangedict[chan] = self.tryit(scope.getRange,chan)
                        #print rangedict
                except socket.timeout:
                    print("failed to get chan",chan, "info")
                    break
                except:
                    traceback.print_exc()
                try:
                    t = time.time()
                    if not demo:
                        resp = self.tryit(update_data, chan)
                        if resp!=None:
                            x,y = resp
                    else:
                        x,y = demo_data(chan)
                    self.updated.emit((t,chan,x,y))
                except socket.timeout:
                    print("failed to get chan",chan,"data")
                    break
        #self.need_update.emit()
    def update_ranges(self):
        for chan in channels:
            rangedict[chan] = scope.getRange(chan)
    def setRange(self,arg):
        chan,chmin,chmax = arg
        print("set new range on channel {}: {} {}".format(chan,chmin,chmax))
        scope.setRange(chan,chmin,chmax)
        rng = scope.getRange(chan)
        rangedict[chan] = rng
        print("newrange is {}".format(rng))
data_thread = QtCore.QThread()
data_thread.setObjectName("HW_updater")
updateD = UpdateData()
updateD.moveToThread(data_thread)
#updateD.need_update.connect(updateD._update_data)
#data_thread.started.connect(updateD.need_update)
#data_thread.start()

class Calculator(QtCore.QObject):
    stats_done = QtCore.Signal(object)
    range_need_adjust = QtCore.Signal(object)
    clip = QtCore.Signal(object)
    def checkRange(self,chan,y):
        if chan in rangedict and rangedict[chan]:
            # print rangedict
            # print rangedict[chan]
            chmin,chmax = rangedict[chan]
            thres = (chmax-chmin)*autoscale_threshold
            # if thres<3e-3:
            #     thres = 3e-3
            ymin = y.min(); ymax = y.max()
            if ymin<chmin or ymax>chmax:
                #print "channel {} clip".format(chan)
                self.clip.emit(chan)
            if chan in autoscale:
                newmin = chmin
                newmax = chmax
                if autoscale[chan]>0: #prevent clip
                    if chmin+thres> ymin or chmax-thres<ymax:
                        #print chan,rangedict[chan],(ymin,ymax)
                        if chmin<ymin and ymax<chmax: #no clip
                            thres_new = (ymax-ymin)*(1+2*autoscale_threshold)
                            newmax = ymax+thres_new
                            newmin = ymin-thres_new
                        else:
                            if ymin<chmin:#clip
                                newmin = chmin-(ymax-ymin)
                                print("clip",ymin,chmin,"->",newmin)
                            if ymax>chmax:
                                newmax = chmax+(ymax-ymin)
                                print("clip",ymax,chmax,"->",newmax)
                if autoscale[chan]>1:
                    if newmin == chmin and ymin>chmin+2*thres:
                        newmin = ymin-1.5*thres
                        print(chmin+thres,ymin,chmin+2*thres,chmin,"->",newmin)
                    if newmax==chmax and ymax<chmax-2*thres:
                        newmax = ymax+1.5*thres
                        print(chmax-thres,ymax,chmax+2*thres,chmax,"->",newmax)
                print("newrange is",(chan,newmin,newmax))
                self.range_need_adjust.emit((chan,newmin,newmax))

    def calc_stats(self,arg):
        t,channel,x,y = arg
        stats[channel],stats2[channel] = calc_stats(x,y)
        self.stats_done.emit(channel)
        #publish frequency
        msg = (t,)
        for chan,key in publish:
            if chan in stats2 and key in stats2[chan]:
                msg = msg + (stats2[chan][key],)
        if len(msg)>1:
            # print((">",msg))
            pub.send_pyobj(msg)
        #publish voltage
        msg = (t,)
        for chan,key in publish_voltage:
            if chan in stats2 and key in stats2[chan]:
                msg = msg + (stats2[chan][key],)
        if len(msg)>1:
            pub_volt.send_pyobj(msg)
        #publish power in dbms for 50Ohm load
        msg = (t,)
        for chan in publish_pwr:
            if chan in stats2 and "PWR 50Ohm" in stats2[chan]:
                msg = msg + (stats2[chan]["PWR 50Ohm"],)
        if len(msg)>1:
            pub_pwr.send_pyobj(msg)
        self.checkRange(channel,y)

calc = Calculator()
calc_thread = QtCore.QThread()
calc_thread.setObjectName("calc_thread")
calc.moveToThread(calc_thread)
calc_thread.start()

class UpdateGui(QtCore.QObject):
    def __init__(self):
        super(UpdateGui,self).__init__()
    def update_gui(self,arg):
        t1 = time.time()
        update(arg)
        #pw.update()
        # print("update function call took", time.time()-t1)
    def update_stats(self,chan):
        #t1 = time.time()
        update_stats(chan)

        #print "update stats call took", time.time()-t1
    def on_clip(self,chan):
        pass
updatergui = UpdateGui()
gui_thrd = QtCore.QThread()
updatergui.moveToThread(gui_thrd)
gui_thrd.start()
timer = QtCore.QTimer()
timer.timeout.connect(updateD._update_data,QtCore.Qt.QueuedConnection)
updateD.updated.connect(updatergui.update_gui)
updateD.updated.connect(calc.calc_stats)
calc.stats_done.connect(updatergui.update_stats)
calc.range_need_adjust.connect(updateD.setRange)
calc.clip.connect(updatergui.on_clip)




if __name__ == '__main__':
    if not demo:
        try:
            scope.connect()
            menu_connect_set()
            cd = scope.getDispChans()
            for c in channels:
                if c not in cd:
                    channels.remove(c)
        except:
            traceback.print_exc()
    else:
        scope.dx = 2e-3

    for msg in sys.argv[1:]:
            scope.send(msg)
            if '?' in msg:
                print(repr(scope.recv()))

    #scope.close()

    data_thread.start()
    timer.start(100)
    #print "scope.dx =",scope.dx
    # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    app.exec_()
        # pass
    timer.stop()
    updateD.stopped = True
    calc_thread.quit()
    data_thread.quit()
    calc_thread.wait(1000)
    data_thread.wait(1000)
    scope.close()
