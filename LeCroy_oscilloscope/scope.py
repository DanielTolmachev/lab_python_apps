#!/usr/bin/env python
# LeCrunch
# Copyright (C) 2010 Anthony LaTorre
#
# modified by Daniel Tolmachev 2015
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Low level interface to the LeCroy Oscilloscope"""
import struct
import socket

ipaddr = ''

headerformat = '>BBBBL'

errors = { 1 : 'unrecognized command/query header',
           2 : 'illegal header path',
           3 : 'illegal number',
           4 : 'illegal number suffix',
           5 : 'unrecognized keyword',
           6 : 'string error',
           7 : 'GET embedded in another message',
           10 : 'arbitrary data block expected',
           11 : 'non-digit character in byte count field of ' \
                'arbitrary data block',
           12 : 'EOI detected during definite length data block transfer',
           13 : 'extra bytes detected during definite length data block ' \
                'transfer' }

class LeCroyScope(object):
    def __init__(self, host=ipaddr, port=1861, timeout=2.0):
        if host is None:
            raise Exception("Enter default ip address as string in 'scope.py'")
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(timeout)
        self.host = host
        self.port = port
        self.timeout = timeout

        self.connected = False
        self.connection_keep = False

    def settimeout(self, timeout):
        self.sock.settimeout(timeout)
        self.timeout = timeout

    def connect(self):
        #self.sock.connect((self.host, self.port))
        self.sock = socket.create_connection((self.host, self.port),self.timeout)
        self.connected = True
    def clear(self):
        """Clear the oscilloscope's output queue."""
        self.sock.settimeout(2.0)
        try:
            while True:
                self.sock.recv(100)
        except socket.timeout:
            pass
        self.sock.settimeout(self.timeout)

    def close(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        self.connected = False

    def send(self, stri):
        if type(stri)!=bytes:
            stri = stri.encode()
        if not stri.endswith(b'\n'):
            stri += b'\n'
        header = struct.pack(headerformat, 129, 1, 1, 0, len(stri))
        self.sock.sendall(header + stri)

    def check_last_command(self):
        self.send('cmr?')
        err = int(self.recv().split(b' ')[-1].rstrip(b'\n'))

        if err in errors:
            raise Exception(errors[err])

    def getheader(self):
        """
        Unpacks a header string from the oscilloscope into the tuple
        (operation, header version, sequence number, spare, total bytes).
        """
        return struct.unpack(headerformat, self.sock.recv(8))

    def recv(self):
        """
        Receive, concatenate, and return a 'logical series' of blocks from
        the oscilloscope. A 'logical series' consists of one or more blocks
        in which the final block is terminated by an EOI terminator
        (i.e. the EOI bit in the header block is set to '1').
        """
        reply = b''
        while True:
            operation, headerver, seqnum, spare, totalbytes  = self.getheader()

            buffer = b''

            while len(buffer) < totalbytes:
                buffer += self.sock.recv(totalbytes - len(buffer))

            reply += buffer

            if operation % 2:
                break

        return reply

if __name__ == '__main__':
    import sys
    scope = LeCroyScope(host = "129.217.155.164")
    scope.connect()

    for msg in sys.argv[1:]:
        scope.send(msg)
        if '?' in msg:
            print(repr(scope.recv()))

    scope.close()
