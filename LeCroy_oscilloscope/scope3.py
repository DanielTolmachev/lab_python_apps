#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      C7LabPC
#
# Created:     02.02.2016
# Copyright:   (c) C7LabPC 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from scope2 import *
import re,io,struct
import numpy as np

class Scope3(ScopeReader):
    def __init__(self,*args,**kwargs):
        super(Scope3,self).__init__(*args,**kwargs)
        self.vdiv = {}
        self.ofst = {}
    def getRange(self,*chan):
        if chan:
            chan = chan[0]
        else:
            chan = 1
        vdiv = self.getVDIV(chan)
        ofst = self.getOffset(chan)
        vmax = vdiv*4-ofst
        vmin = vdiv*-4-ofst
        #print chan,vdiv,ofst,vmin,vmax
        return vmin,vmax
    def setRange(self,chan,vmin,vmax):
        vdiv = (vmax-vmin)/8
        ofst = -(vmax+vmin)/2
        self.setVDIV(chan,vdiv)
        self.setOffset(chan,ofst)
    def getVDIV(self,chan):
        self.send("C{}:VDIV?".format(chan))
        ans = self.recv()
        sp = ans.split()
        if len(sp)>1:
            vdiv = sp[1][:-1]
            vdiv = float(vdiv)
            self.vdiv[chan] = vdiv
            return vdiv
        return ans
    def setVDIV(self,chan,vdiv):
        self.send("C{}:VDIV {}".format(chan,vdiv))
        self.vdiv[chan] = vdiv
    def getOffset(self,chan):
        self.send("C{}:OFST?".format(chan))
        ans = self.recv()
        sp = ans.split()
        if len(sp)>1:
            vdiv = sp[1][:-1]
            vdiv = float(vdiv)
            return vdiv
        return ans
    def setOffset(self,chan,ofst):
        self.send("C{}:OFST {}".format(chan,ofst))
        self.ofst[chan] = ofst
    def increaseVDIV(self,chan):
##        if chan not in self.vdiv:
        self.getVDIV(chan)
        vdiv = self.vdiv*2
        self.setVDIV(chan,vdiv)
    def decreaseVDIV(self,chan):
##        if chan not in self.vdiv:
        vdiv = self.getVDIV(chan)
        vdiv = self.vdiv/2
        self.setVDIV(chan,vdiv)
    def getwaveform2(self):
        """
        test
        """
        self.send("c1:wf? all")

        msg = self.recv()
##        if not int(msg[1]) == channel:
##            raise Exception("waveforms out of sync or comm_header is off.")

        data = io.StringIO()
        data.write(msg)
        data.seek(0)

        startpos = re.search('WAVEDESC', data.read()).start()

        # set endianess
        data.seek(startpos + 34)
        if struct.unpack('<'+scope2.Enum.packfmt, data.read(scope2.Enum.length)) == 0:
            endian = '>'
        else:
            endian = '<'

        data.seek(startpos)

        var = {}
        for name, pos, datatype in scope2.wavedesc:
            raw = data.read(datatype.length)
            if datatype in (scope2.String, scope2.UnitDefinition):
                var[name] = raw.rstrip('\x00')
            elif datatype in (scope2.TimeStamp,):
                var[name] = struct.unpack(endian+datatype.packfmt, raw)
            else:
                var[name] = struct.unpack(endian+datatype.packfmt, raw)[0]

        # move to binary data block position
        data.seek(startpos + var['wave_descriptor'] + var['user_text'])

        nbytes = var['wave_array_1']
        dx = var['horiz_interval']
        self.dx = dx
        xoffset = var['horiz_offset']
        dy = var['vertical_gain']
        yoffset = var['vertical_offset']
        if var['comm_type'] == 0:
            datatype = scope2.Byte
        else:
            datatype = scope2.Word

        x = np.zeros(nbytes)
        y = np.zeros(nbytes)
        #x = array.array('f')
        #y = array.array('f')

        for i, pos in enumerate(range(0, nbytes, datatype.length)):
            raw = data.read(datatype.length)
            x[i]=(dx * i + xoffset)
            yval = struct.unpack(endian+datatype.packfmt, raw)[0]
            y[i]=(yval *dy - yoffset)

        data.close()

        return x, y
    def ask(self,query):
        self.send(query)
        return self.recv()
    def isTraceOn(self,trace):
        ret = self.ask("{}:TRACE?".format(trace)).strip()
        if ret.endswith("ON"):
            return True
        else:
            return False
    def getDispChans(self):
        ret = []
        for c in range(1,5):
            if self.isTraceOn("C{}".format(c)):
                ret.append(c)
        return ret
    def getwaveformwDesc(self, channel):
        """
        Request, process, and return the x and y arrays for channel number
        <channel> from the oscilloscope.
        """
        if channel not in list(range(1, 5)):
            sys.exit("Error: channel must be in " + str(list(range(1, 5))))

        self.send("c%s:wf? all" % str(channel))

        msg = self.recv()
        if not int(msg[1]) == channel:
            raise Exception("waveforms out of sync or comm_header is off.")

        data = io.StringIO()
        data.write(msg)
        data.seek(0)

        startpos = re.search('WAVEDESC', data.read()).start()

        # set endianess
        data.seek(startpos + 34)
        if struct.unpack('<'+Enum.packfmt, data.read(Enum.length)) == 0:
            endian = '>'
        else:
            endian = '<'

        data.seek(startpos)

        var = {}
        for name, pos, datatype in wavedesc:
            raw = data.read(datatype.length)
            if datatype in (String, UnitDefinition):
                var[name] = raw.rstrip('\x00')
            elif datatype in (TimeStamp,):
                var[name] = struct.unpack(endian+datatype.packfmt, raw)
            else:
                var[name] = struct.unpack(endian+datatype.packfmt, raw)[0]

        # move to binary data block position
        data.seek(startpos + var['wave_descriptor'] + var['user_text'])

        nbytes = var['wave_array_1']
        dx = var['horiz_interval']
        self.dx = dx
        xoffset = var['horiz_offset']
        dy = var['vertical_gain']
        yoffset = var['vertical_offset']
        if var['comm_type'] == 0:
            datatype = Byte
        else:
            datatype = Word

        x = np.zeros(nbytes)
        y = np.zeros(nbytes)
        #x = array.array('f')
        #y = array.array('f')

        for i, pos in enumerate(range(0, nbytes, datatype.length)):
            raw = data.read(datatype.length)
            x[i]=(dx * i + xoffset)
            yval = struct.unpack(endian+datatype.packfmt, raw)[0]
            y[i]=(yval *dy - yoffset)

        data.close()

        return var,x,y

