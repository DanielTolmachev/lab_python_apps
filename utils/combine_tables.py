#
# Author:      SFB
#
# Created:     21.09.2015
# Copyright:   (c) SFB 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
import sys,traceback,time
import numpy as np
DEFAULT_PRECISION = 5

def load_multi_table(fn):
    print ('opening {}'.format(fn))
    f = open(fn,'rb')
    #lines = f.readlines()
    global header
    header = []
    l = f.readline()
    while not l.lower().startswith(b"raw data"):
        header.append(l)
        l = f.readline()
    global data
    data = []
    f.readline()
    #f.readline()
    l = f.readline()
    tab = 0
    column_names = []
    column_units = []
    while l: #eof condition
        data.append([]) #create list for new table
        column_names.append([])
        column_units.append([])
        while l and l.strip():  #find empty line
            try:
                data[-1].append([float(w) for w in l.split()])
            except ValueError:
                if not column_names[-1]:
                    column_names[-1] = (l.strip().split(b"\t"))
                else:
                    column_units[-1] = (l.strip().split(b"\t"))
            #print column_names,column_units
            l = f.readline()
        while l and (not l.strip() or l.startswith(b'#') ): #skip empty line but not eof
            l = f.readline()
    for i,lst in enumerate(data):
        if lst:
            data[i] = np.array(lst)
    f.close()
    return header,data,column_names,column_units


def save2txt(ft,header,data,column_names,column_units,prec = DEFAULT_PRECISION):
    try:
        f = open(ft,"wb+")
        #f.writelines([l.strip()+'\r\n' for l in header])
        [f.write(l.strip()+b'\n') for l in header]
        # for l in header:
        #     f.write(l.strip()+b'\n')
        #print column_names
        if len(column_names)== data.shape[1]:
            f.write("\t".join(column_names)+"\n")
        #print column_units
        if len(column_units)== data.shape[1]:
            f.write("\t".join(column_units)+"\n")
        [f.write('\t'.join(['{:.{}g}'.format(b,DEFAULT_PRECISION) for b in a])+'\n') for a in data]
        f.close()
        print ("saved to {}".format(ft))
    except:
        traceback.print_exc()

def combine_multi_table(fi):
    header,data,column_names,column_units = load_multi_table(fi)
    maxmin = np.array([(d.min(), d.max()) for d in data if isinstance(d,np.ndarray)])
    tmin = maxmin[:,0].max()
    tmax = maxmin[:,1].min()
    npoints = np.min([len(d) for d in data if isinstance(d,np.ndarray)])
    cols = 1
    for d in data:
        if isinstance(d,np.ndarray):
            cols += d.shape[1]-1
    global jdata
    jdata = np.zeros((npoints,cols))
    t = np.linspace(tmin,tmax,npoints)
    jdata[:,0] = t
    cnt = 1
    column_names_new = [b"Time"]
    column_units_new = [b"s"]
    for i,d in enumerate(data):
        if isinstance(d,np.ndarray):
            column_names_new += column_names[i][1:]
            print (column_names_new)
            column_units_new += column_units[i][1:]
            print (column_units_new)
            for i in range(1,d.shape[1]):
                jdata[:,cnt] = np.interp(t,d[:,0],d[:,i])
                cnt+=1
    ft = fi+".join.dat"
    save2txt(ft,header,jdata,column_names_new,column_units_new)

files = []
if __name__ == '__main__':
    #print sys.argv
    files = [r"d:\Daniel\spectra\20161128_gaas_n13_30.dat"]
    if len(sys.argv)>1:
        files = sys.argv[1:]
        print (files)
    data = []
    for fi in files:
        combine_multi_table(fi)
   #     data.append(d)
    #time.sleep(10) #to prevent python console from closing