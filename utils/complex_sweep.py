import zmq,traceback,sys,time


ports_send = {"wavelength" : 6007,
         "magnetic field":6009}
ports_rcv = {"wavelength" : 5007,
         "magnetic field":5009}

context = zmq.Context()
senders = {}
receivers = {}

go = True

def wait(whom):
    sweeping = True
    t1 = time.time()
    t2 = t1
    while sweeping:
        rep = ""
        try:
            rep = receivers[whom].recv_pyobj(zmq.NOBLOCK)
            t1 = time.time()
        except zmq.error.Again as err:
            t2 = time.time()
            #print err
        except:
            traceback.print_exc()
        if "stop" in rep:
            sweeping = False
        elif t2-t1>5:
            print( "received nothing from",ports_rcv[whom]," in 5 sec")
            sweeping = False
        if rep:
            #print rep
            pass

def sweep2(*args):
    fast = args[0]
    slow = args[1]
    while go:
        msg = "start once"
        print( "sending",msg,"to",ports_send[fast])
        senders[fast].send_pyobj(msg)
        wait(fast)
        msg = "advance"
        print( "sending",msg,"to",ports_send[fast])
        senders[slow].send_pyobj(msg)
        msg = "goto begin"
        print( "sending",msg,"to",ports_send[fast])
        senders[fast].send_pyobj(msg)
        wait(fast)


def sweep(*args):
    go = True
    for arg in args:
        if arg not in ports_send:
            print ("\"",arg,"\" is not recognized")
            print ("supported modes are:",ports_send)

    for arg in args:
        sock = context.socket(zmq.PUB)
        sock.connect("tcp://127.0.0.1:%d" % ports_send[arg])
        senders[arg]= sock
        if arg in ports_rcv:
            sockr = context.socket(zmq.SUB)
            sockr.setsockopt(zmq.RCVTIMEO,1)
            sockr.setsockopt(zmq.SUBSCRIBE, b"")
            sockr.connect("tcp://127.0.0.1:%d" % ports_rcv[arg])
            receivers[arg] = sockr

    sweep2(*args)





if __name__ == "__main__":
    sweep("wavelength","magnetic field")


context.destroy()